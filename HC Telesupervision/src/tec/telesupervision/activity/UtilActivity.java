package tec.telesupervision.activity;


import tec.telesupervision.R;
import tec.telesupervision.data.UtilData;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;

public class UtilActivity {
	
	/***
	 * Constante que contiene el nombre de la propiedad que indica si est� activado el reconocimiento de voz.
	 */
	private final String PARAM_TTS_RECOGNITION = "Activacion_entrada_voz";
	private final String PARAM_IV_TTS = "BUTTON_TTS_REC";
	
	/***
	 * Contiene las propiedades de la aplicaci�n.
	 */
	private ContentValues APPConfig;
	
	
	/**
	 * 
	 * @author mjulia
	 * @date 10/10/2012
	 * @title iniciarActivity
	 * @comment Inicia una actividad
	 * 
	 * @param context
	 *            Contexto
	 * @param c
	 *            Clase de la nueva actividad
	 * 
	 */
	public void iniciarActivity(Context context, Class<?> c) {
		Intent intent = new Intent(context, c);
		context.startActivity(intent);
	}

	/**
	 * 
	 * @author 	mjulia
	 * @date 	10/10/2012
	 * @title 	iniciarActivity
	 * @comment Inicia una actividad pasandole parametros como extra (Se
	 *          recuperan de la siguiente forma: Bundle extra =
	 *          this.getIntent().getExtras();)
	 * 
	 * @param context
	 *            Contexto
	 * @param c
	 *            Clase de la nueva actividad
	 * @param hm
	 *            HashMap con los pares de valores String para la clave y el
	 *            valor que se desean pasar a la nueva actividad
	 * 
	 */
	public void iniciarActivity(Context context, Class<?> c,
			HashMap<String, String> hm) {

		Intent intent = new Intent(context, c);

		// A�adir los valores del HashMap como Extras en la nueva actividad.
		Iterator<Entry<String, String>> it = hm.entrySet().iterator();
		while (it.hasNext()) {

			Entry<String, String> pairs = it.next();
			if (pairs.getValue() != null) {
				String key = pairs.getKey().toString();
				String value = pairs.getValue().toString();
				intent.putExtra(key, value);
			}
			it.remove();

		}

		context.startActivity(intent);

	}

	/**
	 * 
	 * @author	mjulia
	 * @date	19/06/2013
	 * @title	iniciarActivityForResult
	 * @comment	inicia una actividad dejando la actual a la espera del resultado
	 *
	 * @param context		Contexto de la aplicaci�n
	 * @param c				Activity que queremos lanzar
	 * @param requestCode	C�digo de referencia para diferenciar la actividad lanzada 
	 *
	 */
	public void iniciarActivityForResult(Context context, Class<?> c, int requestCode) {
		Intent intent = new Intent(context, c);
		Activity act = (Activity) context;
		act.startActivityForResult(intent, requestCode);
	}

	/**
	 * 
	 * @author	mjulia
	 * @date	19/06/2013
	 * @title	iniciarActivityForResult
	 * @comment	inicia una actividad dejando la actual a la espera del resultado
	 *
	 * @param context		Contexto de la aplicaci�n
	 * @param c				Activity que queremos lanzar
	 * @param requestCode	C�digo de referencia para diferenciar la actividad lanzada 
	 * @param hm			HashMap con los valores a pasar a la nueva actividad.
	 *
	 */
	public void iniciarActivityForResult(Context context, Class<?> c, int requestCode, HashMap<String, String> hm) {

		Intent intent = new Intent(context, c);

		// A�adir los valores del HashMap como Extras en la nueva actividad.
		Iterator<Entry<String, String>> it = hm.entrySet().iterator();
		while (it.hasNext()) {

			Entry<String, String> pairs = it.next();
			String key = pairs.getKey().toString();
			String value = pairs.getValue().toString();

			intent.putExtra(key, value);
			it.remove();

		}

		Activity act = (Activity) context;
		act.startActivityForResult(intent, requestCode);

	}

	public static void presentarConfirmacionCerrarActivity(final Context context, final Activity act) {

		String titulo = context.getResources().getString(R.string.title_dialog_close_Activity);
		String mensaje = context.getResources().getString(R.string.message_dialog_close_Activity);
		String si = context.getResources().getString(R.string.yes_dialog);
		String no = context.getResources().getString(R.string.no_dialog);

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setTitle(titulo);
		builder.setMessage(mensaje);
		builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		builder.setPositiveButton(si, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				
				act.finish();
				//System.exit(0);

			}
		});

		final AlertDialog alert = builder.create();
		alert.show();

	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	02/10/2013
	 * @title	presentarMensajerActivity
	 * @comment	Presentar una ventana con un mensaje		
	 *
	 * @param context	Contexto
	 * @param act		Actividad principal
	 * @param titulo	T�tulo del mensaje
	 * @param mensaje	Mensaje
	 *
	 */
	public static void presentarMensajerActivity(final Context context, final Activity act, String titulo, String mensaje) {
		
		String aceptar = context.getResources().getString(R.string.aceptar);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setTitle(titulo);
		builder.setMessage(mensaje);
		builder.setNeutralButton(aceptar, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		
		final AlertDialog alert = builder.create();
		alert.show();

	}
	

	/**
	 * 
	 * @author 	jjulia
	 * @date 	27/05/2013
	 * @title 	configuracionUI
	 * @comment Procedimiento para explorar todos los controles de todas las View de la Actividad recibida
	 * 			El primer "Layout" de la actividad debe llamarse "parent" sea del tipo que sea
	 * 			android:id="@+id/parent"
	 * 
	 * @param 	activity	Actividad a explorar
	 * 
	 */
	public void configuracionUI(Activity activity) {
		
		View view = activity.findViewById(R.id.parent);
		
		// Obtenemos los par�metros generales de configuraci�n.
		// *****************************************************
		UtilData ud = new UtilData(view.getContext());
		this.APPConfig = ud.obtenerParametrosConfiguracionGeneral();
		ud = null;
		// ***************************************************************
		
		configuracionUI_explorarView(activity, view);
		
		
	}
	
	//Funcionalidad recursiva para explorar todos los controles de una Activity
	private void configuracionUI_explorarView(Activity activity, View view) {

		
		//Controles en los que no debe profundizarse
		if ((view instanceof TimePicker)) {
			return;
		}
		if ((view instanceof DatePicker)) {
			return;
		}
			
		
		//Incliur AQU� 	todas las funcionalidades que se deseen agregar para configurar el UI
		configuracionUI_ocultarTeclado(activity, view);
		configuracionUI_ocultarBotonesReconocimientoVoz(view);
		
		
		//Gestionar la recurrencia de controles
		if (view instanceof ViewGroup) {
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				View innerView = ((ViewGroup) view).getChildAt(i);
				configuracionUI_explorarView(activity, innerView);
			}
		}

	}

	//Funcionalidad para gestionar la presentaci�n del teclado de los controles EDITTEXT
	private void configuracionUI_ocultarTeclado(final Activity activity, View view) {
	
		if(view != null) {
		
			if (!(view instanceof EditText)) {

				view.setOnTouchListener(new OnTouchListener() {

					public boolean onTouch(View v, MotionEvent event) {
						//hideSoftKeyboard(activity);
						((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
						return false;
					}

				});

				view.setOnFocusChangeListener(new OnFocusChangeListener() {

					public void onFocusChange(View v, boolean hasFocus) {
						if (hasFocus == true) {
							//hideSoftKeyboard(activity);
							((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
						}
					}

				});

			}
			
			if ((view instanceof EditText)) {

				view.setOnFocusChangeListener(new OnFocusChangeListener() {
		
					 public void onFocusChange(View v, boolean hasFocus) {
			        	 if (hasFocus)
		                    {
			        		 
			        		   //open keyboard
		                       ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(v, InputMethodManager.SHOW_FORCED);
		                       
		                       //Evitar el desplazamiento de los controles al mostrar el teclado.
	                		   //activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		                     
		                    } else { 
		                    	//close keyboard
		                        ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
		                    }
			        }

				});

			}
		}

	}
	
	//Funcionalidad para gestionar la presentaci�n de los botones utilizados para activar el reconocimiento de voz.
	private void configuracionUI_ocultarBotonesReconocimientoVoz( View view) {
		
		if(view instanceof ImageView) {
			if(view.getTag()!=null) {
				if(view.getTag().toString().compareTo(PARAM_IV_TTS)==0) {
					if(APPConfig.getAsString(PARAM_TTS_RECOGNITION).compareTo("true")!=0){
						view.setVisibility(ImageView.GONE);
					}
				}
			}
		}
		
	}

	public static void hideSoftKeyboard(Activity activity) {
		if (activity.getCurrentFocus() != null) {
			InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
		}
	}

}
