package tec.telesupervision.data;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



/**
 * 
 * @author 	mcanal
 * @date 	06/06/2012
 * @title	DataBase
 * @comment	Clase para el control de la base de datos 
 * 			Implenta la interface Usuarios para obtener variables y constantes para el manejo de la tabla de tel�fono
 * 
 */
public class DataBase extends SQLiteOpenHelper  {	
	
	private SQLiteDatabase dataBase = null;
	private ContentValues comando = null;
	
	private final Context myContext;
	private static int DB_Version = 5;
	private static String DB_PATH = "";
	private static String DB_NAME = "telesupervision.db";
	
 	   	 
	 

	/**
	 * 
	 * @author	mcanal
	 * @date	03/07/2012
	 * @title	DataBase
	 * @comment	Constructor de la clase
	 *
	 * @param context	Contexto de la aplicaci�n	
	 *
	 */
	public DataBase(Context context) {
		super(context, DB_NAME, null, DB_Version);
		
		this.myContext = context;
		DB_PATH = context.getFilesDir().getParent() + "/databases/";
	}	
	
//	
//
//	/**
//	 * 
//	 * @author	mcanal
//	 * @date	03/07/2012
//	 * @title	DataBase
//	 * @comment	Constructor de la clase
//	 *
//	 * @param context	Contexto de la aplicaci�n
//	 * @param factory	Objeto de la clase CursorFactory
//	 *
//	 */
//	public DataBase(Context context, CursorFactory factory) {
//		super(context, DB_NAME, factory, DB_Version);
//		this.myContext = context;
//	}	
	
	/* 
	 * El m�todo onCreate() ser� ejecutado autom�ticamente por nuestra clase SQLiteHelper 
	 * cuando sea necesaria la creaci�n de la base de datos, es decir, cuando a�n no exista. 
	 * Las tareas t�picas que deben hacerse en este m�todo ser�n la creaci�n de todas las tablas necesarias 
	 * y la inserci�n de los datos iniciales si son necesarios. 
	 * Para la creaci�n de la tabla utilizaremos la sentencia SQL ya definida en la interface Usuario
	 * y la ejecutaremos contra la base de datos utilizando el m�todo m�s sencillo de los disponibles en la API de SQLite 
	 * proporcionada por Android, llamado execSQL(). 
	 * Este m�todo se limita a ejecutar directamente el c�digo SQL que le pasemos como par�metro.(non-Javadoc)
	 * 
	 * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
    }

	/*
	 * El m�todo onUpgrade() se lanzar� autom�ticamente cuando sea necesaria una actualizaci�n 
	 * de la estructura de la base de datos o una conversi�n de los datos. 
	 * Un ejemplo pr�ctico: imaginemos que publicamos una aplicaci�n que utiliza una tabla 
	 * con los campos usuario e email (llam�moslo versi�n 1 de la base de datos). 
	 * M�s adelante, ampliamos la funcionalidad de nuestra aplicaci�n y necesitamos que la tabla
	 * tambi�n incluya un campo adicional por ejemplo con la edad del usuario (versi�n 2 de nuestra base de datos). 
	 * Pues bien, para que todo funcione correctamente, la primera vez que ejecutemos la versi�n ampliada de la aplicaci�n 
	 * necesitaremos modificar la estructura de la tabla Usuarios para a�adir el nuevo campo edad. 
	 * Pues este tipo de cosas son las que se encargar� de hacer autom�ticamente el m�todo onUpgrade() 
	 * cuando intentemos abrir una versi�n concreta de la base de datos que a�n no exista. 
	 * Para ello, como par�metros recibe la versi�n actual de la base de datos en el sistema, 
	 * y la nueva versi�n a la que se quiere convertir. 
	 * En funci�n de esta pareja de datos necesitaremos realizar unas acciones u otras. 
	 * En nuestro caso de ejemplo optamos por la opci�n m�s sencilla: 
	 * borrar la tabla actual y volver a crearla con la nueva estructura, 
	 * pero como se indica en los comentarios del c�digo, lo habitual ser� que necesitemos 
	 * algo m�s de l�gica para convertir la base de datos de una versi�n a otra y por supuesto para conservar 
	 * los datos registrados hasta el momento.(non-Javadoc)
	 * 
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
	
	
	/**
	 * 
	 * @author	mcanal
	 * @date	03/07/2012
	 * @title	createDataBase
	 * @comment	Crea una base de datos vac�a en el sistema y la reescribe con nuestro fichero de base de datos.
	 *
	 * @throws IOException	
	 *
	 */
	public void createDataBase() throws IOException{
		
		String myPath = DB_PATH + DB_NAME;
		File baseDatos = new File(myPath);
		if (baseDatos.exists() == false) {
			
			//Llamando a este m�todo se crea la base de datos vac�a en la ruta por defecto del sistema
			//de nuestra aplicaci�n por lo que podremos sobreescribirla con nuestra base de datos.
			dataBase = myContext.openOrCreateDatabase(myPath, Context.MODE_PRIVATE, null);
					
			try {			 
				copyDataBase();			 
			} catch (IOException e) {
				throw e;
			}
			
		} 
		
//		boolean dbExist = checkDataBase();		 
//		if(dbExist == false){
//			
//			//Llamando a este m�todo se crea la base de datos vac�a en la ruta por defecto del sistema
//			//de nuestra aplicaci�n por lo que podremos sobreescribirla con nuestra base de datos.					
//											
//			dataBase = myContext.openOrCreateDatabase(myPath, Context.MODE_PRIVATE, null);
//					
//			try {			 
//				copyDataBase();			 
//			} catch (IOException e) {
//				throw e;
//			}
//		}
	}
	
//	/**
//	 * 
//	 * @author	mcanal
//	 * @date	03/07/2012
//	 * @title	checkDataBase
//	 * @comment	Comprueba si la base de datos existe para evitar copiar siempre el fichero cada vez que se abra la aplicaci�n.
//	 *
//	 * @return	true si existe la base de datos, false si no existe
//	 *
//	 */
//	private boolean checkDataBase(){
//	 
//		boolean existe = false;
//		SQLiteDatabase checkDB = null;		 
//		try{
//		 
//			String myPath = DB_PATH + DB_NAME;
//			checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
//			if (checkDB != null) {
//				checkDB.close();	
//				checkDB = null;
//				existe = true;
//			}
//		 
//		}catch(SQLiteException e){		 
//			//si llegamos aqui es porque la base de datos no existe todav�a.		 
//		} 
//		
//		return existe;
//	}	
	
	/**
	 *
	 * @author	Jordi
	 * @date	03/11/2013
	 * @title	forzarCreateDataBase
	 * @comment	Forzar la creaci�n de la base de datos
	 *
	 * @throws IOException
	 *
	 */
	public void forzarCreateDataBase() throws IOException{
		
		String myPath = DB_PATH + DB_NAME;
					
		//Llamando a este m�todo se crea la base de datos vac�a en la ruta por defecto del sistema
		//de nuestra aplicaci�n por lo que podremos sobreescribirla con nuestra base de datos.
		dataBase = myContext.openOrCreateDatabase(myPath, Context.MODE_PRIVATE, null);
					
		try {			 
			copyDataBase();			 
		} catch (IOException e) {
			throw e;
		}
				
	} 
	
	/**
	 * 
	 * @author	mcanal
	 * @date	03/07/2012
	 * @title	copyDataBase
	 * @comment	Copia nuestra base de datos desde la carpeta assets a la reci�n creada
	 * 			base de datos en la carpeta de sistema, desde d�nde podremos acceder a ella.
	 *			Esto se hace con bytestream.
	 *
	 * @throws IOException	
	 *
	 */
	private void copyDataBase() throws IOException{
	 
		//Abrimos el fichero de base de datos como entrada
		InputStream myInput = myContext.getAssets().open(DB_NAME);
		 
		//Ruta a la base de datos vac�a reci�n creada
		String outFileName = DB_PATH + DB_NAME;
		 
		//Abrimos la base de datos vac�a como salida
		OutputStream myOutput = new FileOutputStream(outFileName);
		 
		//Transferimos los bytes desde el fichero de entrada al de salida
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer))>0){
			myOutput.write(buffer, 0, length);
		}
		 
		//Liberamos los streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	 
	}
		
	
	/**
	 * 
	 * @author	mcanal
	 * @date	03/07/2012
	 * @title	open
	 * @comment	Abre una conexi�n a la base de datos en modo de lectura y escritura
	 *
	 * @throws SQLException	
	 *
	 */
	public boolean open() {
		 
		boolean open = false;
		 
		try {
			String myPath = DB_PATH + DB_NAME;
			dataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
			open = true;
		} catch(SQLException e) {
			open = false;
		}
		
		return open;
		 
	}
	
	 
	/**
	 * Cierra la base de datos
	 */
	@Override
	public synchronized void close() {
		if(dataBase != null)
			dataBase.close();
		super.close();
	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	04/06/2012
	 * @title 	getDataBase
	 * @comment Obtiene nuestra base de datos
	 *
	 * @return 	Una instancia de la classe SQLiteDataBase
	 */
	public SQLiteDatabase getDataBase() {
		if(dataBase != null) {
			return dataBase;
		}else {
			open();
			return dataBase;
		}
	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	07/06/2012
	 * @title	getDataBaseVersion
	 * @comment	Devuleve el n�mero de versi�n de la base de datos.
	 *
	 * @return	N�mero de versi�n de la base de datos.
	 */
	public int getDataBaseVersion() {
		return dataBase.getVersion();
	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	07/06/2012
	 * @title	inDataBaseTransaciton
	 * @comment	Comprueba si hay una transacci�n pendiente
	 *
	 * @return	true si hay una transacci�n pendiente, false en caso contrario.
	 */
	public boolean inDataBaseTransaciton() {
		return dataBase.inTransaction();
	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	04/06/2012
	 * @title 	isNullDatabase
	 * @comment	Comprueba si se ha conectado correctamente a la base de datos.
	 *
	 * @return 	false si se conecto correctamente y la base de datos no devulee un valor null
	 * 			, true en caso de que la base de datos sea null.
	 */
	public boolean isNullDatabase() {		
		boolean isNull = false;
		if (dataBase == null) {
			isNull = true;
		}
		return isNull;
	}	
	
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	04/06/2012
	 * @title	executeQuery
	 * @comment	Ejecuta una consulta a la base de datos que no devuelva resultados.
	 *
	 * @param query	Consulta a ejecutar en la base de datos.
	 * 
	 */
	public void executeQuery(String query) {
		dataBase.execSQL(query);
	}		
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	04/06/2012
	 * @title	insert
	 * @comment	Inserta un registro en una tabla de la base de datos.
	 *
	 * @param tableName			Nombre de la tabla que queremos actualizar
	 * @param nullColumnHack	Puede ser null, en ese caso todas las columnas de la tabla dever�n tener 
	 * 							su correspondiente par de clave-valor en el ContentValues de la funci�n insert.
	 * 							De no ser null, se le pasar� el nombre de una columna que pueda almacenar valores NULL
	 * 							en la tabla, solo ser� necessaria el nombre de una columna que admita valores NULL.
	 * @param values			Colecci�n de pares clave-valor con los nombres de las columnas de la tabla como clave.
	 * 
	 */
	public void insert(String tableName, String nullColumnHack, ContentValues values) {		
		dataBase.insert(tableName, nullColumnHack, values);
	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	04/06/2012
	 * @title	insert
	 * @comment	Inserta un registro en una tabla de la base de datos.
	 *
	 * @param tableName			Nombre de la tabla que queremos actualizar
	 * @param nullColumnHack	Puede ser null, en ese caso todas las columnas de la tabla dever�n tener 
	 * 							su correspondiente par de clave-valor en el ContentValues de la funci�n insert.
	 * 							De no ser null, se le pasar� el nombre de una columna que pueda almacenar valores NULL
	 * 							en la tabla, solo ser� necessaria el nombre de una columna que admita valores NULL.	 
	 * 
	 */
	public void insert(String tableName, String nullColumnHack) {		
		insert(tableName, nullColumnHack, comando);
	}
	
	/**
	 * 
	 * @author	mcanal
	 * @date 	04/06/2012
	 * @title	update
	 * @comment	Actualiza un registro de una tabla de la base de datos
	 *
	 * @param tableName		Nombre de la tabla que queremos actualizar.
	 * @param values		Colecci�n de par clave-valor con las columnas y valores a actualizar.
	 * @param whereClause	Puede ser null, clausula where de la consulta.
	 * @param whereArgs		Puede ser null, Array string con los valores de la clausula where.
	 * 
	 */
	public void update(String tableName, ContentValues values, String whereClause, String[] whereArgs) {
		dataBase.update(tableName, values, whereClause, whereArgs);
	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	06/06/2012
	 * @title	update
	 * @comment	Actualiza un registro de una tabla de la base de datos
	 *
	 * @param tableName		Actualiza un registro de una tabla de la base de datos
	 * @param whereClause	Puede ser null, clausula where de la consulta.
	 * @param whereArgs		Puede ser null, Array string con los valores de la clausula where.
	 */
	public void update(String tableName, String whereClause, String[] whereArgs) {
		update(tableName, comando, whereClause, whereArgs);
	}	
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	04/06/2012
	 * @title	delete
	 * @comment	Elimina registros de una tabla de la base de datos
	 *
	 * @param tableName		Nombre de la tabla de donde se eliminar�n los datos
	 * @param whereClause	Puede ser null, clausula where de la consulta.
	 * @param whereArgs		Puede ser null, Array string con los valores de la clausula where.
	 * 
	 */
	public void delete(String tableName, String whereClause, String[] whereArgs) {
		dataBase.delete(tableName, whereClause, whereArgs);
	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	04/06/2012
	 * @title	getQuery
	 * @comment	Devuelve un cursor hacia la base de datos con los resultados de la consulta SQL
	 *
	 * @param query		Consulta sql de selecci�n de datos 
	 * @param args		Puede ser null, array con los valores de la consulta en el caso de que estos vengan como ? en la consulta
	 * 					Por ej: 
	 * 							query ="SELECT COLUMNA FROM TABLA WHERE COLUMNA = ?"
	 * 							args = new String[] {"usu1"}
	 * 
	 * @return			Cursor 
	 */
	public Cursor getQuery(String query, String[] args) {		
		Cursor c = dataBase.rawQuery(query, args);
		return c;
	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	05/06/2012
	 * @title	inicializarTransaccion
	 * @comment	Inicializa una transacci�n en la base de datos
	 *
	 */
//	public void inicializarTransaccion() {
//		dataBase.beginTransaction();
//	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	05/06/2012
	 * @title	confirmarTransaccion
	 * @comment	Confirma los datos de una transacci�n en la base de datos.
	 * 			(Si se llama al m�todo finalizarTransaccion sin haver confirmado la transacci�n
	 * 			se considerar� como un rollback y no se guardar�n los canvios en la base de datos).
	 *
	 */
//	public void confirmarTransaccion(){
//		dataBase.setTransactionSuccessful();
//	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	05/06/2012
	 * @title	finalizarTransaccion
	 * @comment Finaliza una transacci�n en la base de datos.
	 * 			(Si se llama al m�todo finalizarTransaccion sin haver confirmado la transacci�n
	 * 			se considerar� como un rollback y no se guardar�n los canvios en la base de datos).
	 *
	 */
//	public void finalizarTransaccion() {
//		dataBase.endTransaction();
//	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	06/06/2012
	 * @title	crearComando
	 * @comment	Crea un nuevo contenedor para almacenar pares de clave-valor, donde clave es el nombre de la columna de
	 * 			la base de datos y valor el valor ha insertar en esa columna. 
	 *
	 */
	public void crearComando() {
		comando = new ContentValues();
	}
	
	/**
	 * 
	 * @author 	mcanal
	 * @date 	06/06/2012
	 * @title	a�adirParametroComando
	 * @comment	A�ade un par de clave-valor en el contenedor, donde clave es el nombre de la columna de
	 * 			la base de datos y valor el valor ha insertar en esa columna. 
	 *
	 * @param colName	Nombre de la columna de la base de datos
	 * @param value		Valor que se debe almacenar en la base de datos.
	 * 					Los posibles valores son objetos de las siguientes clases:
	 * 					String, Integer, Boolean, Long, Byte, byte[], Double, Float y Short.
	 * 
	 */
	public void addParametroComando(String colName, Object value) {
		
		if (value instanceof String) {
			String s= (String) value;			
			comando.put(colName, s);
		}
		
		if (value instanceof Integer) {
			
			Integer i = (Integer) value;
			comando.put(colName, i);
		}
		
		if (value instanceof Boolean) {
			Boolean b = (Boolean) value;
			comando.put(colName, b);
		}
		
		if (value instanceof Long) {
			Long l = (Long) value;
			comando.put(colName, l);
		}
		
		if (value instanceof Byte) {
			Byte b = (Byte) value;
			comando.put(colName, b);
		}
		
		if (value instanceof byte[]) {
			byte[] b = (byte[]) value;
			comando.put(colName, b);
		}
		
		if (value instanceof Double) {
			Double d = (Double) value;
			comando.put(colName, d);
		}
		
		if (value instanceof Float) {
			Float f = (Float) value;
			comando.put(colName, f);
		}
		
		if (value instanceof Short) {
			Short s = (Short) value;
			comando.put(colName, s);
		}		
		
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	23/09/2013
	 * @title	excSQL
	 * @comment	Ejecutar sentencias de acci�n SQL
	 *
	 * @param sentencia	
	 *
	 */
	public void execSQL(String sentencia) {
		dataBase.execSQL(sentencia);
	}
}

