package tec.telesupervision.data;

import tec.telesupervision.R;
import tec.telesupervision.Objects.*;
import tec.telesupervision.adaptador.AdaptadorVertical_Acometidas;
import tec.telesupervision.adaptador.AdaptadorVertical_Concertaciones;
import tec.telesupervision.adaptador.AdaptadorVertical_Planificacion;
import tec.telesupervision.data.instruccionesSQL.ordenacion;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Locale;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

/**
 * 
 * @author	mjulia
 * @date	26/09/2012
 * @title	UtilData
 * @comment	Utilidades generales para la obteneci�n de datos de la base de datos.
 *
 */
public class UtilData {
	
	private Context c;
	
	private String claveValue = "*";
	private String claveTexto = "-- Seleccione --";

	//Declaraci�n de las URI necessarias para los m�todos que usan el proveedor de base de datos.
    //public static final Uri URI_CONFIGURACION_GENERAL = Uri.parse("content://tec.dc.providers/configuracionGeneral");
    //public static final Uri URI_MENSAJES = Uri.parse("content://tec.dc.providers/mensajes");
	//private static final Uri URI_MEDICIONES = Uri.parse("content://tec.dc.providers/mediciones");
	//private static final Uri URI_ALARMAS = Uri.parse("content://tec.dc.providers/alarmas");
	
	//Constantes para las fechas y el tiempo
	public static final String FORMAT_DATE_SHORT_BD = "yyyy-MM-dd";
	public static final String FORMAT_DATE_FULL_BD = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_DATE_FULL_CONVERT = "dd-MM-yyyy HH:mm:ss";
	public static final String FORMAT_DATE_FULL_VISUAL = "dd-MM-yyyy HH:mm";
	
	public static final String FORMAT_DATE_OUT = "dd-MM-yyyy";
	public static final String FORMAT_TIME_OUT = "HH:mm:ss";
	
	public static final String FORMAT_DATE_MONTH = "dd-MM";
	public static final String FORMAT_HOUR_MINUTE = "HH:mm";
	
	public static final String TABLA_IDIOMAS = "IDIOMAS";
	
	/**
	 * 
	 * @author	mjulia
	 * @date	26/09/2012
	 * @title	UtilData
	 * @comment	Constructor de la clase.
	 *
	 * @param c	Contexto de la aplicaci�n.
	 *
	 */
	public UtilData(Context c) {
		super();
		this.c = c;
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	26/09/2012
	 * @title	UtilData
	 * @comment	Constructor de la clase.
	 *
	 *
	 */
	public UtilData(){
		super();
	}

	/**
	 * 
	 * @author	jjulia
	 * @date	22/04/2013
	 * @title	obtenerTabla
	 * @comment	Obtiene una lista con los elementos de una determinada Tabla
	 *
	 * @param tabla			Id de la tabla de la que obtener la descripci�n.
	 * @return				Lista con los elementos de una determinada tabla.
	 *
	 */
	public ArrayList<ObjetoTabla> obtenerTabla(String tabla){
			
		ArrayList<ObjetoTabla> lista = new ArrayList<ObjetoTabla>();
		
		String sql = "SELECT * FROM tablas WHERE IDTabla = '"+ tabla.trim() +"'";
		
		DataBase db = new DataBase(this.c);
		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(sql, null);
		while (c.moveToNext()){
			ObjetoTabla tab= new ObjetoTabla();
			String sNum1 = c.getString(c.getColumnIndex("Num1"));
			String sNum2 = c.getString(c.getColumnIndex("Num2"));
			String sValorMin = c.getString(c.getColumnIndex("ValorMin"));
			String sValorMax = c.getString(c.getColumnIndex("ValorMax"));
			if (sNum1 != null){sNum1 = sNum1.replace(",", ".");} else {sNum1="0";}
			if (sNum2 != null){sNum2 = sNum2.replace(",", ".");} else {sNum2="0";}
			if (sValorMin != null){sValorMin = sValorMin.replace(",", ".");} else {sValorMin="0";}
			if (sValorMax != null){sValorMax = sValorMax.replace(",", ".");} else {sValorMax="0";}
			
			tab.setIdElemento(c.getString(c.getColumnIndex("IDElemento")));
			tab.setDescripcion(c.getString(c.getColumnIndex("Descripcion")));
			tab.setAlfa1(c.getString(c.getColumnIndex("Alfa1")));
			tab.setAlfa2(c.getString(c.getColumnIndex("Alfa2")));
			tab.setNum1(new BigDecimal(sNum1));
			tab.setNum2(new BigDecimal(sNum2));
			tab.setValorMin(new BigDecimal(sValorMin));
			tab.setValorMax(new BigDecimal(sValorMax));
			lista.add(tab);
		}
		
		c.close();
		db.close();
		
		return lista;
		
	}
	

	
	/**
	 * 
	 * @author	mjulia
	 * @date	08/05/2013
	 * @title	obtenerParametrosConfiguracionGeneral
	 * @comment	Obtiene un ContentValues con los valores de la tabla configuracionGeneral
	 * 			key del ContentValues = clave de configuracionGeneral
	 * 			value del ContentValues = valor de configuracionGeneral
	 *
	 * @return	ContentValues con los valores de la tabla configuracionGeneral
	 *
	 */
	public ContentValues obtenerParametrosConfiguracionGeneral() {
		
		ContentValues values = new ContentValues();		
		String query = "select * from configuracionGeneral";
		
		DataBase db = new DataBase(this.c);		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(query, null);
		while (c.moveToNext()){
			values.put(c.getString(0), c.getString(1));
		}
		c.close();
		c = null;
		
		if (values.size() < 1) {
			values = null;
		}
		
		db.close();
		db = null;
		
		return values;
		
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/05/2013
	 * @title	establecerIdiomaConfiguracion
	 * @comment	Establecer el idioma configurado
	 *
	 */
	public void establecerIdiomaConfiguracion(){		
		
	    String idiomaConfiguracion = obtenerParametroTablaConfiguracion("Idioma");
	   
	    Locale idioma= new Locale(idiomaConfiguracion);
	    Locale.setDefault(idioma);
	   	   
	    Configuration configuracion = new Configuration();
	    configuracion.locale = idioma;
	    this.c.getResources().updateConfiguration(configuracion, this.c.getResources().getDisplayMetrics());
	
	}
	
	
	/**
	 * 
	 * @author	mcanal
	 * @date	25/09/2012
	 * @title	obtenerClaveSustancia
	 * @comment	Obtiene la clave de un elemento de la tabla tablas
	 *
	 * @param idTabla		Clave de la tabla de tablas.
	 * @param descripcion 	Nombre de la sustancia de la que obtener su clave
	 * @return				Clave de la sustancia
	 *
	 */
	public String obtenerIDElementoPorDescripcion(String idTabla, String descripcion){
				
		String clave = "";
		
		String sql = "SELECT IDElemento FROM tablas WHERE IDTabla = '"+ idTabla.trim() +"'";
		sql += " AND Descripcion = '"+ descripcion.trim() +"'";
		
		DataBase db = new DataBase(c);
		//db.open();
		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(sql, null);
		if (c.getCount() == 1) {
			c.moveToFirst();
			clave = c.getString(0);
		}
		
		c.close();
		db.close();
		
		return clave;
		
	}	
	
	/**
	 * 
	 * @author	mjulia
	 * @date	26/09/2012
	 * @title	obtenerDescripcionPorIdElemento
	 * @comment	Obtiene la descripci�n de un elemento de una tabla del fichero tablas de la base de datos
	 *
	 * @param idTabla	Id de la tabla de la que obtener la descripci�n.
	 * @param clave		Id del elemento de la tabla.
	 * @return			Descripci�n del elemento de la tabla.
	 *
	 */
	public String obtenerDescripcionPorIdElemento(String idTabla, String clave) {
			
		String descripcion = "";
		
		String sql = "SELECT Descripcion FROM tablas WHERE IDTabla = '"+ idTabla.trim() +"'";
		sql += " AND IDElemento = '"+ clave.trim() +"'";
		
		DataBase db = new DataBase(c);
		//db.open();
		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(sql, null);
		if (c.getCount() == 1) {
			c.moveToFirst();
			descripcion = c.getString(0);
		}
		
		c.close();
		db.close();
		
		return descripcion;
		
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	15/10/2012
	 * @title	obtenerValorMaximoPorIDElemento
	 * @comment	Obtiene el valor m�ximo de un elemento de tablas seg�n su id
	 *
	 * @param tabla			Id de la tabla de la que obtener la descripci�n.
	 * @param clave			Id del elemento de la tabla.
	 * @return				Valor m�ximo del elemento
	 *
	 */
	public double obtenerValorMaximoPorIDElemento(String tabla, String clave){
			
		double valorMax = 0;
		
		String sql = "SELECT ValorMax FROM tablas WHERE IDTabla = '"+ tabla.trim() +"'";
		sql += " AND IDElemento = '"+ clave.trim() +"'";
		
		DataBase db = new DataBase(c);
		//db.open();
		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(sql, null);
		if (c.getCount() == 1) {
			c.moveToFirst();
			valorMax = c.getDouble(0);
		}
		
		c.close();
		db.close();
		
		return valorMax;
		
	}
	
	/***
	 * 	
	 * @author	jibanez
	 * @date	03/01/2013
	 * @title	obtenerElementoTabla
	 * @comment	Devuelve un elemento de la tabla TABLAS
	 *
	 * @param tabla		-> Nombre de la tabla del fichero de tablas.
	 * @param columna	-> Columna de la cual queremos obtener el valor.
	 * @param clave		-> Argumento del elemento de la tabla.
	 * @return	
	 *			Devuelve un objeto String con el valor deseado.
	 */
	public String obtenerElementoTabla(String tabla, String columna, String clave){
			
		String valor = "";
		
		String sql = "SELECT " + columna.trim() + " FROM tablas WHERE IDTabla = '"+ tabla.trim() +"'";
		sql += " AND IDElemento = '"+ clave.trim() +"'";
		
		DataBase db = new DataBase(c);
		//db.open();
		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(sql, null);
		if (c.getCount() >= 1) {
			c.moveToFirst();
			valor = c.getString(0);
		}
		
		c.close();
		db.close();
		
		return valor;
		
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	28/01/2013
	 * @title	obtenerParametroTablaConfiguracion
	 * @comment	Obtiene el valor de un par�metro definido en la tabla de configuraci�n.
	 *
	 * @param claveParametro	Clave del parametro solicitado
	 * @return					Valor del par�metro solicitado	
	 *
	 */
	public String obtenerParametroTablaConfiguracion(String claveParametro){
			
		String valor = "";		
		String query = "select valor from configuracionGeneral where clave = '" + claveParametro + "'";
		
		DataBase db = new DataBase(c);
		Cursor c = null;
		try {
			//db.open();
			boolean openBD = db.open();
			while (openBD == false) {
				 openBD = db.open();
			}
			
			c = db.getQuery(query, null);
			if (c.getCount() == 1) {
				c.moveToFirst();
				valor = c.getString(0);
			}			
		} catch(Exception e) {
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString());
		} finally {
			if (c != null) {c.close();}
			db.close();
		}
		
		return valor;
	}
	
		
	/**
	 * 
	 * @author	jjulia
	 * @date	05/10/2012
	 * @title	obtenerCampoFechaBaseDatosEnFormato
	 * @comment	Convertir una fecha recuperada de la base de datos a un formato determinado
	 *
	 * @param valorFechaBaseDatos  	Fecha recuperada de la base de datos
	 * @param formatoFechaBD		Formato de la fecha en la base de datos
	 * @param formatoSalida			Formato de salida 
	 * @return
	 * @throws ParseException
	 *
	 */
	public String obtenerCampoFechaBaseDatosEnFormato(String valorFechaBaseDatos, String formatoFechaBD, String formatoSalida) throws ParseException{
				
		String fechaOut =null;
		
		SimpleDateFormat df_yyyymmdd = new SimpleDateFormat(formatoFechaBD); 
		SimpleDateFormat df_ddmmyyyy = new SimpleDateFormat(formatoSalida); 
		
		Date cnvfecha = df_yyyymmdd.parse(valorFechaBaseDatos);		
		fechaOut = df_ddmmyyyy.format(cnvfecha);
		
		return fechaOut;
		
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	05/10/2012
	 * @title	obtenerCampoCalendar
	 * @comment	Convertir un string fecha y un string hora en un campo Calendario
	 *
	 * @param fecha			  	Fecha en formato FORMAT_DATE_OUT
	 * @param hora       		Hora en formato FORMAT_TIME_OUT
	 * @return
	 * @throws ParseException
	 * 
	 *  Ejemplos de como utilizar el Calendario
     *  int dia = cal.get(Calendar.DAY_OF_MONTH);
	 *  int mes = cal.get(Calendar.MONTH)+1;
     *  int any = cal.get(Calendar.YEAR);
	 *  int hor = cal.get(Calendar.HOUR_OF_DAY);
	 *  int min = cal.get(Calendar.MINUTE);
	 *  int sec = cal.get(Calendar.SECOND);
	 *
	 */
	public Calendar obtenerCampoCalendar(String fecha, String hora) throws ParseException{
							
		Calendar cal = GregorianCalendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE_FULL_CONVERT);
	   
	    Date d=sdf.parse(fecha+' '+hora);
	    cal.setTime(d);
	   		
		return cal;
	}

	

	
	/***
	 * 
	 * @author	jibanez
	 * @date	28/11/2012
	 * @title	llenarSpinner
	 * @comment	Funci�n para rellenar un objeto Spinner (lista desplegable).
	 *
	 * @param cmb
	 * 				Objeto Spinner que se va a rellenar.
	 * @param nombreTabla
	 * 				Nombre de la tabla de la tabla "tablas", cuyos elementos se cargaran en el Spinner.
	 * @param columnaValue
	 * 				Nombre de la columna de la tabla "tablas" que se utilizar� como value de cada elemento del Spinner.
	 * @param columnaTexto
	 * 				Nombre de la columna de la tabla "tablas" que se utilizar� como descripci�n de cada elemento del Spinner.
	 * @param seleccione	
	 *				Indica si se quiere mostrar un primer elemento en el Spinner, que indique al usuario que aun no se ha seleccionado ning�n elemento.
	 * @param ordenarPor	
	 *				Nombre de la columna por la que se realizar� la ordenadoci�n de los elementos mostrados
	 */
	public void llenarSpinner(Spinner cmb, String nombreTabla, String columnaValue, String columnaTexto, boolean seleccione, String ordenarPor){
				
		LinkedList<ClaseListItem> elementos = new LinkedList<ClaseListItem>();
		
		String valor;
		String descripcion;
		
		if(seleccione==true){
			this.claveValue = this.c.getResources().getString(R.string.combo_valor);
			this.claveTexto = this.c.getResources().getString(R.string.combo_descripcion);
			elementos.add(new ClaseListItem(this.claveValue, this.claveTexto));
		}
        
		String sql;
		sql = "SELECT " + columnaValue + ", " + columnaTexto;
		sql += " FROM tablas";
		sql += " WHERE IDTabla = '" + nombreTabla + "'";
		sql += " ORDER BY " + ordenarPor;

		DataBase dbs = new DataBase(this.c);
		//dbs.open();

		boolean openBD = dbs.open();
		while (openBD == false) {
			 openBD = dbs.open();
		}
		
		Cursor cur = dbs.getQuery(sql, null);
		while (cur.moveToNext()){
			
			valor = cur.getString(cur.getColumnIndex(columnaValue));
			descripcion = cur.getString(cur.getColumnIndex(columnaTexto));
			
			elementos.add(new ClaseListItem(valor, descripcion));
		} 
		
		cur.close();
		dbs.close();
		
		ArrayAdapter<ClaseListItem> spinner_adapter = new ArrayAdapter<ClaseListItem>(this.c, android.R.layout.simple_spinner_item, elementos);
		spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		cmb.setAdapter(spinner_adapter);
		
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	04/07/2013
	 * @title	llenarSpinner
	 * @comment	Cargar un combo a partir de una tabla de tablas
	 *
	 * @param cmb				Spinner donde cargar la tabla
	 * @param nombreTabla		Nombre de la tabla a cargar
	 * @param filtroWhere		Condici�n where de la consulta sql
	 * @param columnaValue		Nombre de la columna a mostrar como id del elemento
	 * @param columnaTexto		Nombre de la columna a mostrar como descripci�n del elemento
	 * @param seleccione		boolean para determinar si se a�ade seleccione como primer elemento del Spinner
	 * @param ordenarPor		Ordenaci�n de los datos
	 * @param setIdSeleccionado	Id del elemento a dejar seleccionado.
	 *
	 */
	public void llenarSpinner(Spinner cmb, String nombreTabla, String filtroWhere, String columnaValue, String columnaTexto, boolean seleccione, String ordenarPor, String setIdSeleccionado){
		
		LinkedList<ClaseListItem> elementos = new LinkedList<ClaseListItem>();
		
		String valor;
		String descripcion;
		int position =-1;
		
		if(seleccione==true){
			this.claveValue = this.c.getResources().getString(R.string.combo_valor);
			this.claveTexto = this.c.getResources().getString(R.string.combo_descripcion);
			elementos.add(new ClaseListItem(this.claveValue, this.claveTexto));
		}
        
		String sql;
		sql = "SELECT " + columnaValue + ", " + columnaTexto;
		sql += " FROM tablas";
		sql += " WHERE IDTabla = '" + nombreTabla + "'";
		sql += " AND (" + filtroWhere + ") ";
		sql += " ORDER BY " + ordenarPor;

		DataBase dbs = new DataBase(this.c);
		//dbs.open();
		
		boolean openBD = dbs.open();
		while (openBD == false) {
			 openBD = dbs.open();
		}

		int cont =-1;
		Cursor cur = dbs.getQuery(sql, null);
		while (cur.moveToNext()){
			
			cont +=1;
			valor = cur.getString(cur.getColumnIndex(columnaValue));
			descripcion = cur.getString(cur.getColumnIndex(columnaTexto));
			
			elementos.add(new ClaseListItem(valor, descripcion));
			if(valor.trim().equals(setIdSeleccionado.trim())==true){
				position=cont;
			}
			
		} 
		
		
		cur.close();
		dbs.close();
		
		ArrayAdapter<ClaseListItem> spinner_adapter = new ArrayAdapter<ClaseListItem>(this.c, android.R.layout.simple_spinner_item, elementos);
		spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		cmb.setAdapter(spinner_adapter);
		if (position > -1){
			if(seleccione==true){
				position += 1;
			}
			cmb.setSelection(position);
		}
		
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	08/05/2013
	 * @title	seleccionarElementoSpinner
	 * @comment	Selecciona un elemento del Spinner
	 *
	 * @param cmb		Spinner
	 * @param value		Id del elemento a seleccionar.
	 *
	 */
	public void seleccionarElementoSpinner(Spinner cmb, String value){
		
		ClaseListItem item;
		
		if(cmb.getCount()>0){
			int i = 0;
			for(i=0;i<cmb.getCount();i++){

				item = (ClaseListItem) cmb.getItemAtPosition(i);
				if(item.getId().compareTo(value)==0){
					break;
				}
				
			}
			cmb.setSelection(i);
		}
		
		item = null;
	}
	
	
	

	
	/***
	 * 
	 * @author	jibanez
	 * @date	29/07/2013
	 * @title	
	 * @comment	
	 *
	 * @param idioma
	 * @return	
	 *
	 */
	public String obtenerCodigoLenguaje(String idioma) {
		
		String codigo = "es_ES";
		
		String sql;
		sql = "SELECT * ";
		sql += "FROM tablas ";
		sql += "WHERE IDTabla = '" + TABLA_IDIOMAS + "' ";
		sql += "AND Alfa1 = '" + idioma + "' ";
		
		DataBase dbs = new DataBase(this.c);
		
		boolean openBD = dbs.open();
		while (openBD == false) {
			 openBD = dbs.open();
		}

		Cursor cur = dbs.getQuery(sql, null);
		while (cur.moveToNext()){
			codigo = cur.getString(cur.getColumnIndex("Alfa2"));
		} 

		cur.close();
		dbs.close();

		return codigo;
	}
	
	

	/**
	 * 
	 * @author	jjulia
	 * @date	30/09/2013
	 * @title	ObtenerListaAcometidasConcertaciones
	 * @comment	Obtener lista de acometidas-concertaciones segun consulta sql
	 *
	 * @param sql	Consulta sql para obtener las acometidas-concertaciones
	 * @return		Lista de objetos acometida-concertaciones
	 *
	 */
	public ArrayList<ObjetoAcometida> ObtenerListaAcometidasConcertaciones(String sql) {

		// Genera la matriz de salida
		ArrayList<ObjetoAcometida> m_obras = new ArrayList<ObjetoAcometida>();
	
	    // Obtine los datos de la base de datos local	
		DataBase db = new DataBase(this.c);
		Cursor c = null;
		try {
			
			boolean openBD = db.open();
			while (openBD == false) {
				 openBD = db.open();
			}
			c = db.getQuery(sql, null);
		
			while (c.moveToNext()) {

				// Genera elemento de la matriz
				ObjetoAcometida oAcom = new ObjetoAcometida();
				
				oAcom.setCod_obra(c.getString(c.getColumnIndex("IDObra")));
				oAcom.setDireccion(c.getString(c.getColumnIndex("Direccion")));
				oAcom.setMunicipio(c.getString(c.getColumnIndex("Municipio")));
				
				if (c.getString(c.getColumnIndex("Comentarios")) != null){
					oAcom.setComentarios(c.getString(c.getColumnIndex("Comentarios")));
				}else{
					oAcom.setComentarios("");
				}
				
				if (c.getString(c.getColumnIndex("ID")) != null){
					oAcom.setTelesupIDConcertacion(c.getLong(c.getColumnIndex("ID")));
				}
				if (c.getString(c.getColumnIndex("NomGestorObra")) != null){
					oAcom.setTelesupGestorObra(c.getString(c.getColumnIndex("NomGestorObra")));
				}
				if (c.getString(c.getColumnIndex("Resultado")) != null){
					oAcom.setTelesupResultado(c.getString(c.getColumnIndex("Resultado")));
				}
				if (c.getString(c.getColumnIndex("CausasRechazo")) != null){
					oAcom.setTelesupCausas(c.getString(c.getColumnIndex("CausasRechazo")));
				}
								
				String outFechaPlanificacion="";
				String outFechaInicio="";
				String outFechaPeS="";
				String outFechaFinal="";
				String outFechaAcepNomb="";
				String outTelesupFechaPlanificacion="";
								
				if (c.getString(c.getColumnIndex("FechaPlan")) != null){
					if (c.getString(c.getColumnIndex("FechaPlan")).trim().equals("")==false){
						outFechaPlanificacion = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaPlan")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_OUT);
					}
				}
				if (c.getString(c.getColumnIndex("FechaIni")) != null){
					if (c.getString(c.getColumnIndex("FechaIni")).trim().equals("")==false){
						outFechaInicio = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaIni")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaPeS")) != null){
					if (c.getString(c.getColumnIndex("FechaPeS")).trim().equals("")==false){
						outFechaPeS = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaPeS")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaFin")) != null){
					if (c.getString(c.getColumnIndex("FechaFin")).trim().equals("")==false){
						outFechaFinal = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaFin")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaAcepNomb")) != null){
					if (c.getString(c.getColumnIndex("FechaAcepNomb")).trim().equals("")==false){
						outFechaAcepNomb = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaAcepNomb")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("InicioPlanificacion")) != null){
					if (c.getString(c.getColumnIndex("InicioPlanificacion")).trim().equals("")==false){
						outTelesupFechaPlanificacion = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("InicioPlanificacion")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
							
				oAcom.setFechaPlanificacion(outFechaPlanificacion);
				oAcom.setFechaInicio(outFechaInicio);
				oAcom.setFechaPeS(outFechaPeS);
				oAcom.setFechaFinal(outFechaFinal);
				oAcom.setFechaAcepNomb(outFechaAcepNomb);
				oAcom.setTelesupFechaPlanificacion(outTelesupFechaPlanificacion);
				
				Boolean outCarpetasOonair=false;
				if (c.getString(c.getColumnIndex("CarpetasOonair")) != null){
					if (c.getString(c.getColumnIndex("CarpetasOonair")).trim().toLowerCase().equals("true")==true){
						outCarpetasOonair=true;
					}
				}
				oAcom.setCarpetasOonair(outCarpetasOonair);
				
				
				// A�ade el elemento a la matriz
				m_obras.add(oAcom);

			}

		} catch (SQLException e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} finally {
			// Cierra la conexi�n a la base de datos
			c.close();
			db.close();
		}

		// Devuelve la matriz de datos
		return m_obras;

	}
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	cargarListaMiPosiblePlanificacion
	 * @comment	Cargar datos en la lista de Mi posible planificaci�n
	 *
	 * @param act		Activity donde reside la Lista
	 * @param orden		Ordenaci�n de la lista
	 *
	 */
	public void cargarListaMiPosiblePlanificacion(Activity act, ordenacion orden) {
			
		String sql = instruccionesSQL.seleccionarMiPosiblePlanificacion(orden);
		
		ArrayList<ObjetoAcometida> m_obras;
		m_obras = ObtenerListaAcometidasConcertaciones(sql);
		
		ListView lstPlanificacion = (ListView) act.findViewById(R.id.lstPlanificacion);
		AdaptadorVertical_Planificacion m_adapter = new AdaptadorVertical_Planificacion(this.c, R.layout.listitem_vertical_acometidas, m_obras);
		lstPlanificacion.setAdapter(m_adapter);
				
	}
	

	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	cargarListaAcometidas
	 * @comment	Cargar datos en la lista de Mis acometidas
	 *
	 * @param act		Activity donde reside la Lista
	 * @param orden		Ordenaci�n de la lista
	 *
	 */
	public void cargarListaAcometidas(Activity act, ordenacion orden, String codObra) {
		
		String sql = instruccionesSQL.seleccionarMisAcometidas(orden);
		
		ArrayList<ObjetoAcometida> m_obras;
		m_obras = ObtenerListaAcometidasConcertaciones(sql);
		
		ListView lstAcometidas = (ListView) act.findViewById(R.id.lstAcometidas);
		AdaptadorVertical_Acometidas m_adapter = new AdaptadorVertical_Acometidas(this.c, R.layout.listitem_vertical_acometidas, m_obras);
		lstAcometidas.setAdapter(m_adapter);
							
		//Posicinar la lista de acoemtida en una determinada obra
		if (codObra.equals("")==false){
			int savedPosition=0;
			for (int i = 0; i < m_adapter.getCount(); ++i) {
				ObjetoAcometida obj = m_adapter.getItem(i);
				if (obj.getCod_obra().equals(codObra)==true){
					savedPosition=i;
					break;
				}
		    }
			lstAcometidas.setSelection(savedPosition);
		}
		
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	cargarListaConcertaciones
	 * @comment	Cargar datos en la lista de Mis concertaciones
	 *
	 * @param act		Activity donde reside la Lista
	 * @param orden		Ordenaci�n de la lista
	 *
	 */
	public void cargarListaConcertaciones(Activity act, ordenacion orden, String codObra) {
				
		String sql = instruccionesSQL.seleccionarMisConcertaciones(orden);
		
		ArrayList<ObjetoAcometida> m_obras;
		m_obras = ObtenerListaAcometidasConcertaciones(sql);
		
		ListView lstConcertaciones = (ListView) act.findViewById(R.id.lstConcertaciones);
		AdaptadorVertical_Concertaciones m_adapter = new AdaptadorVertical_Concertaciones(this.c, R.layout.listitem_vertical_concertaciones, m_obras);
		lstConcertaciones.setAdapter(m_adapter);
		
		//Posicinar la lista de concertaciones en una determinada obra
		if (codObra.equals("")==false){
			int savedPosition=0;
			for (int i = 0; i < m_adapter.getCount(); ++i) {
				ObjetoAcometida obj = m_adapter.getItem(i);
				if (obj.getCod_obra().equals(codObra)==true){
					savedPosition=i;
					break;
				}
		    }
			lstConcertaciones.setSelection(savedPosition);
		}
		
	}
	
	
	
}
