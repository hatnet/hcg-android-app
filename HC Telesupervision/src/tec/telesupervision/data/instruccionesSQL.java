package tec.telesupervision.data;

public class instruccionesSQL {
	
		  	  
	public static enum ordenacion{
		OBRA,
		MUNICIPIO,
		ESTADO	
	}
	
	public static String seleccionarMisAcometidas(ordenacion orden){
		
		String sql = "";
		sql += " select *, t0.Cod_Obra as IDObra, t0.GestorObra as NomGestorObra ";
		sql += " from asignaciones t0 ";
		sql += " left join concertaciones t1 on t0.Cod_Obra=t1.Cod_Obra ";
		sql += " where t1.Cod_Obra is null ";
		
		if (orden.equals(ordenacion.OBRA)==true){
			sql += " order by t0.Cod_Obra ";
		}
		if (orden.equals(ordenacion.MUNICIPIO)==true){
			sql += " order by t0.Municipio, t0.FechaPeS, t0.FechaIni, t0.FechaPlan ";
		}
		if (orden.equals(ordenacion.ESTADO)==true){
			sql += " order by t0.FechaPeS, t0.FechaIni, t0.FechaPlan ";
		}
		
		return sql;
		
	}
	
	
	public static String seleccionarMisConcertaciones(ordenacion orden){
			
			String sql = "";
			sql += " select *, t0.Cod_Obra as IDObra, t1.GestorObra as NomGestorObra ";
			sql += " from asignaciones t0 ";
			sql += " left join concertaciones t1 on t0.Cod_Obra=t1.Cod_Obra ";
			sql += " where not t1.Cod_Obra is null ";
					
			if (orden.equals(ordenacion.OBRA)==true){
				sql += " order by t0.Cod_Obra ";
			}
			if (orden.equals(ordenacion.MUNICIPIO)==true){
				sql += " order by t0.Municipio, t1.Resultado, t1.InicioPlanificacion ";
			}
			if (orden.equals(ordenacion.ESTADO)==true){
				sql += "  order by t1.Resultado, t1.InicioPlanificacion ";
			}
			
			return sql;
			
	}
	
	
	public static String seleccionarMiPosiblePlanificacion(ordenacion orden){
		
		String sql = "";
		sql += " select t0.*, t2.*, t0.Cod_Obra as IDObra, t1.GestorObra as NomGestorObra, t1.FechaPlan, t1.FechaIni, t1.FechaPeS, t1.FechaFin, t1.FechaAcepNomb, t1.Comentarios ";
		sql += " from acometidas t0 ";
		sql += " left join asignaciones t1 on t0.Cod_Obra=t1.Cod_Obra ";
		sql += " left join concertaciones t2 on t0.Cod_Obra=t2.Cod_Obra ";
		sql += " where t1.Cod_Obra is null ";
				
		if (orden.equals(ordenacion.OBRA)==true){
			sql += " order by t0.Cod_Obra ";
		}
		if (orden.equals(ordenacion.MUNICIPIO)==true){
			sql += " order by t0.Municipio ";
		}
				
		return sql;
		
	}
	
}
