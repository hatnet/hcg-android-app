package tec.telesupervision.mod.aplicacion;

import tec.telesupervision.R;
import tec.telesupervision.activity.UtilActivity;
import tec.telesupervision.data.DBHelper;
import tec.telesupervision.data.DataBase;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.services.ConexionesSincronizacion;
import tec.telesupervision.services.wakeful.OnBootReceiver;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Inicio extends Activity  {
	
	private String userWeb = "";
	private String passWeb = "";
	
	private Context context;
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		this.context = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inicio);
		
		procedimientoInicial_1();
		
	}	
	
	/**
	 * Evitar que se pierdan los valores cuando se canvia la orientaci�n de la
	 * pantalla en el manifiesto se debe a�adir a la actividad ...
	 * 
	 * <activity android:name=".ConsultaMediciones"
	 * android:label="@string/title_activity_consulta_mediciones"
	 * android:configChanges="keyboard|keyboardHidden|orientation" > </activity>
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	06/05/2013
	 * @title	procedimientoInicial (1 y 2)
	 * @comment	Procedimiento inicial de la aplicaci�n
	 *
	 */
	private void procedimientoInicial_1(){

		//Comprobaci�n base de datos (Crear, actualizar, ...)
		comprobacionCreacionBaseDatos();
				 
		//Registro del usuario
		obtenecionDatosPerfilUsuario();
		 
	}
	
	private void procedimientoInicial_2(){
		
		//Iniciar los servicios modo WAKE LOCK		  
	    Log.i(this.getClass().getName(), "INICIAR LOS SERVICIOS -WAKE LOCK-");
	    sendBroadcast(new Intent(this, OnBootReceiver.class));
	    
	    //Iniciar la funcionalidad Principal.
	    Log.i(this.getClass().getName(), "INICIAR ACTIVIDAD PRINCIPAL");
	    iniciarActividadPrincipal();
			       
	}
	
	

	/**
	 * 
	 * @author	jjulia
	 * @date	06/05/2013
	 * @title	comprobacionCreacionBaseDatos
	 * @comment	Gestionar la estructura de la base de datos local (Creaci�n, Actualizaci�n,...)
	 * 
	 */
	private void comprobacionCreacionBaseDatos() {
		
		DBHelper dbh = new DBHelper(this);
		dbh.open();
		dbh.close();
		
	
//		DataBase db = new DataBase(this);
//		try {
//			db.createDataBase();
//			db.close();
//		} catch (IOException e) {			
//			e.printStackTrace();
//			Log.e(this.getClass().getName(), e.getStackTrace().toString());
//		}
//		db = null;
	}
		
	
	/**
	 * 
	 * @author	jjulia
	 * @date	06/05/2013
	 * @title	obtenecionDatosPerfilUsuario
	 * @comment	Obtener los credenciales del usuario y en su defecto solicitarlos
	 *
	 */
	private void obtenecionDatosPerfilUsuario(){
		
		UtilData ud = new UtilData(this);
		
		try{
			this.userWeb = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			this.passWeb = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
		} catch(Exception e){
			this.userWeb = "";
			this.passWeb = "";			
		}
		
			
		if (userWeb.trim().equals("") == true || passWeb.trim().equals("") == true) {
				solicitarValidarUsuarioPassword();
		} else {
				procedimientoInicial_2();
		}
				
	}
			

	/**
	 * 
	 * @author	mcanal
	 * @date	06/05/2013
	 * @title	solicitarValidarUsuarioPassword
	 * @comment	Solicitar y validar los credenciales del usuario
	 *
	 */
	private void solicitarValidarUsuarioPassword(){
		
		// Presentamos un dialog donde el usuario pueda introducir sus credenciales
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// Asignamos el layout personalizado
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_inicio_user_web,(ViewGroup) this.findViewById(R.layout.activity_inicio));

		// Asignamos la vista del AlertDialog a nuestro propio layout
		builder.setView(layout);
		final AlertDialog alertDialog = builder.create();
							
		final EditText txtUsuario = (EditText) layout.findViewById(R.id.txtUsuario);
		final EditText txtPassword = (EditText) layout.findViewById(R.id.txtPassword);
				
		layout.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(txtPassword.getWindowToken() , 0);
			}
			
		});

		layout.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(txtPassword.getWindowToken() , 0);
				return true;
			}
		});
		
		txtUsuario.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		    public void onFocusChange(View v, boolean hasFocus) {
		        
		        if (!hasFocus) {
		        	InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
					inputMethodManager.hideSoftInputFromWindow(txtPassword.getWindowToken() , 0);
				
		        }
		    }
		});
		
		txtPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		    public void onFocusChange(View v, boolean hasFocus) {
		       
		        if (!hasFocus) {
		        	InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
					inputMethodManager.hideSoftInputFromWindow(txtPassword.getWindowToken() , 0);
				}
		    }
		});
		
		txtUsuario.requestFocus();
		
		
		// Asignamos el evento click a nuestro bot�n de aceptar (en este
		// caso cierra el alertdialog)
		final Button btnAceptar = (Button) layout.findViewById(R.id.btnAceptar);
//		final ImageButton btnAceptar = (ImageButton) layout.findViewById(R.id.btnBotonCabecera1);
//		final ImageButton btn2 = (ImageButton) layout.findViewById(R.id.btnBotonCabecera2);
//		btn2.setVisibility(View.GONE);
				
		//final Button btnAceptar = (Button) layout.findViewById(R.id.btnAceptar);
		btnAceptar.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				
				try {
					Log.i(this.getClass().getName(), "REGISTRO USUARIO. Iniciar validaci�n.");
					
					String user = txtUsuario.getText().toString();
					String pass = txtPassword.getText().toString();
					Boolean correcto = false;
					
					if (user.trim().equals("") == false && pass.trim().equals("") == false) {
										
						ConexionesSincronizacion us = new ConexionesSincronizacion(context);
						UtilData ud = new UtilData(context);
						boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));
						if(autorizado == true) {
							//Validar usuario y password introducidos como credenciales
							correcto = us.ValidacionCredencialesUsuario(user, pass);
						}else{
							Log.i(this.getClass().getName(), "REGISTRO USUARIO. No se permite la conexi�n a internet.");
						}
					
					}
														
					if (correcto==true) {					
						//Actualizar los credenciales del usuario validado
						actualizaCredencialesUsuario(user, pass);
						
						Toast.makeText(context, context.getResources().getString(R.string.usuario_Registrado) , Toast.LENGTH_LONG).show();
						Log.i(this.getClass().getName(), "REGISTRO USUARIO. Registrado correctamente.");
					} else {
						
						Toast.makeText(context, context.getResources().getString(R.string.usuario_No_Registrado), Toast.LENGTH_LONG).show();
						Log.i(this.getClass().getName(), "REGISTRO USUARIO. El usuario introducido NO se ha registrado.");
						
						finish();
																	
					}
					
				} catch (Exception e) {
					
					e.printStackTrace();
					Log.e(this.getClass().getName(), e.getStackTrace().toString());
					
				} finally {
				
					// Cerramos el dialogo
					alertDialog.dismiss();
					
					//Contoinuar con el procedimiento inicial (2� parte)
					procedimientoInicial_2();
				}
				
			}
		});

		// Mostramos el alertdialog
		alertDialog.show();
		Log.i(this.getClass().getName(), "REGISTRO USUARIO. Presentar formulario registro.");
		
	}
	
	
	
	/**
	 * 
	 * @author	mcanal
	 * @date	06/03/2013
	 * @title	actualizaCredencialesUsuario
	 * @comment	Actualiza en la tabla de configuradci�n general los credenciales validados del usuario
	 *
	 * @param user			nombre de usuario
	 * @param pass			pass del usuario
	 *
	 */
	private void actualizaCredencialesUsuario(String user, String pass) {
		
		String update = "update configuracionGeneral";
		update += " set valor = '" + user + "'";
		update += " where clave = 'userNombreUsuario'";
		
		String update2 = "update configuracionGeneral";
		update2 += " set valor = '" + pass + "'";
		update2 += " where clave = 'userPasswordUsuario'";
		
		DataBase db = new DataBase(this);
	
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}
		
		db.executeQuery(update);
		db.executeQuery(update2);
	
		db.close();
		db = null;
	}
	
	
	
	/**
	 * 
	 * @author	mjulia
	 * @date 	17/10/2012
	 * @title 	iniciarActividadPrincipal
	 * @comment Inicia la actividad principal de la aplicaci�n (esta aplicaci�n no tiene men�)
	 *  
	 */
	private void iniciarActividadPrincipal() {
		
		//Establece el ididoma
		UtilData ud = new UtilData(this);
		ud.establecerIdiomaConfiguracion();
		ud = null;
		
		//Inicia la actividad principal
		UtilActivity ut = new UtilActivity();
		ut.iniciarActivity(this, AcometidasActivity.class);
		ut = null;		
		
		finish();

	}

	

	

	
}
