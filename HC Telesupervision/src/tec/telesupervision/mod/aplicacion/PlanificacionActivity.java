package tec.telesupervision.mod.aplicacion;

import tec.telesupervision.R;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.data.instruccionesSQL.ordenacion;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import android.widget.TextView;

public class PlanificacionActivity extends Activity { 
	

	/**
	 * Procedimiento inicial de la Actividad
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_planificacion);
		
		inicializarBotonesCabecera();
		establecerTituloPlanificacion();
		
		UtilData ud = new UtilData(this);
		ud.cargarListaMiPosiblePlanificacion(this, ordenacion.MUNICIPIO);
				
	}
	

	
	
	/**
	 * Evitar que se pierdan los valores cuando se canvia la orientaci�n de la
	 * pantalla en el manifiesto se debe a�adir a la actividad ...
	 * 
	 * <activity android:name=".ConsultaMediciones"
	 * android:label="@string/title_activity_consulta_mediciones"
	 * android:configChanges="keyboard|keyboardHidden|orientation" > </activity>
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	/***
	 * 
	 * @author		jjulia
	 * @date		08/08/2013
	 * @title		inicializarBotonesCabecera
	 * @comment		Iniciliazar los botones gen�ricos de cabecera para el m�dulo en particular.
	 *	
	 *
	 */
	private void inicializarBotonesCabecera(){
	
		ImageButton btnCancelarPlanificacion = (ImageButton) findViewById(R.id.btnBotonCabecera1);
		btnCancelarPlanificacion.setImageResource(getResources().getIdentifier("drawable/" + "cabecera_cancelar", null, getPackageName()));
		btnCancelarPlanificacion.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
						
				finish();
			}
		});
		
		ImageButton btnSinUso2 = (ImageButton) findViewById(R.id.btnBotonCabecera2);
		btnSinUso2.setVisibility(View.GONE);
	
	}
	
	/***
	 * 
	 * @author		jjulia
	 * @date		08/08/2013
	 * @title		onCreateOptionsMenu / onOptionsItemSelected
	 * @comment		Gestionar el men� de opciones de la actividad
	 *	
	 *
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_activity_planificacion, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		UtilData ud = new UtilData(this);
		
		switch (item.getItemId()) {
		case R.id.ordenacionMunicipio:
		
			ud.cargarListaMiPosiblePlanificacion(this, ordenacion.MUNICIPIO);
			break;

		case R.id.ordenacionObra:
			
			ud.cargarListaMiPosiblePlanificacion(this, ordenacion.OBRA);
			break;
	
		}

		return true;
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	02/10/2013
	 * @title	establecerTituloPlanificacion
	 * @comment	Establecer el t�tulo de planificaci�n
	 *	
	 *
	 */
	private void establecerTituloPlanificacion(){
		
		Resources res = this.getResources();
						
		// Establecer el t�tulo de la presentaci�n de Planificaci�n
		TextView lblPlanificacion = (TextView) findViewById(R.id.lblPlanificacion);
			
    	String tituloPlanificacion=res.getString(R.string.item_label_planificacion);
		lblPlanificacion.setText(tituloPlanificacion);
		
	}
	
	
}
