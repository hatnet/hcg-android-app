package tec.telesupervision.mod.aplicacion;

import java.io.IOException;

import tec.telesupervision.R;
import tec.telesupervision.activity.UtilActivity;
import tec.telesupervision.data.DBHelper;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.data.instruccionesSQL.ordenacion;
import tec.telesupervision.services.ProcesoSincronizacion;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;


public class AcometidasActivity extends Activity {
	
	private Context context;
	private Activity act;
		
	
	
	/**
	 * Procedimiento inicial de la Actividad
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_acometidas);
		
		this.context= (Context) this;
		this.act= (Activity) this;
		
		UtilData ud = new UtilData(this);
		
		Resources res = this.getResources();
    	String titulo=res.getString(R.string.app_description)+ " - "+ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
		this.setTitle(titulo);
		
		comprobacionCredenciales();
		inicializarBotonesCabecera();
		inicializarTabControl();
		
		ud.cargarListaAcometidas(this, ordenacion.ESTADO, "");

	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	30/09/2013
	 * @title	comprobacionCredenciales
	 * @comment	Procedimiento para comprobar si se han registrado los credenciales, 
	 * 			en caso negativo salir de la aplicaci�n.
	 *	
	 *
	 */
	private void comprobacionCredenciales(){
		
		String userWeb="";
		String passWeb="";
		try{
			UtilData ud = new UtilData(this);
			userWeb = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			passWeb = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
		} catch(Exception e){
		}
			
		if (userWeb.trim().equals("") == true || passWeb.trim().equals("") == true) {
			finish();	
		}
				
	}
	
	/**
	 * Evitar que se pierdan los valores cuando se canvia la orientaci�n de la
	 * pantalla en el manifiesto se debe a�adir a la actividad ...
	 * 
	 * <activity android:name=".ConsultaMediciones"
	 * android:label="@string/title_activity_consulta_mediciones"
	 * android:configChanges="keyboard|keyboardHidden|orientation" > </activity>
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	

	/**
	 * Evento al pulsar el bot�n Atr�s del dispositivo m�vil
	 */
	@Override
	public void onBackPressed() {

		Context context = this;
		Activity act = this;
		UtilActivity.presentarConfirmacionCerrarActivity(context, act);

	}
	
	
	/**
	 * Evento al recuperar el primer plano la activity
	 */
	@Override
	protected void onResume() {
		super.onResume();

		TabHost tabs=(TabHost)findViewById(android.R.id.tabhost);
		UtilData ud = new UtilData(context);
		
		if (tabs.getCurrentTabTag().equals("Acometidas")==true){
    		ud.cargarListaAcometidas(act, ordenacion.ESTADO, "");
		}
		if (tabs.getCurrentTabTag().equals("Concertaciones")==true){
    		ud.cargarListaConcertaciones(act, ordenacion.ESTADO,"");
		}

	}


	
	/***
	 * 
	 * @author		jjulia
	 * @date		08/08/2013
	 * @title		inicializarBotonesCabecera
	 * @comment		Iniciliazar los botones gen�ricos de cabecera para el m�dulo en particular.
	 *	
	 *
	 */
	private void inicializarBotonesCabecera(){
	
		ImageButton btnPlanificacion = (ImageButton) findViewById(R.id.btnBotonCabecera1);
		btnPlanificacion.setImageResource(getResources().getIdentifier("drawable/" + "cabecera_planificacion", null, getPackageName()));
		btnPlanificacion.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
						
				inicializarPresentacionPlanificacion();
				
			}
		});
		
		ImageButton btnSinUso2 = (ImageButton) findViewById(R.id.btnBotonCabecera2);
		btnSinUso2.setVisibility(View.GONE);
	
	}
	
	
	/**
	 *
	 * @author	jjulia
	 * @date	24/09/2013
	 * @title	inicializarTabControl
	 * @comment	Procedimiento para la inicializaci�n del control de Pesta�as
	 *	
	 */
	private void inicializarTabControl(){
		
		Resources res = getResources();
		 
		TabHost tabs=(TabHost)findViewById(android.R.id.tabhost);
		tabs.setup();
		 
		TabHost.TabSpec spec=tabs.newTabSpec("Acometidas");
		spec.setContent(R.id.tbAcometidas);
		spec.setIndicator(res.getString(R.string.tab_title_acometidas), res.getDrawable(R.drawable.tab_acometidas));
		tabs.addTab(spec);
		 
		spec=tabs.newTabSpec("Concertaciones");
		spec.setContent(R.id.tbConcertaciones);
		spec.setIndicator(res.getString(R.string.tab_title_concertaciones), res.getDrawable(R.drawable.tab_concertaciones));
		tabs.addTab(spec);
		
		tabs.setCurrentTab(0);
		
		//Evento de cambio de pesta�a
		tabs.setOnTabChangedListener(new OnTabChangeListener() {
		    public void onTabChanged(String tabId) {
		    	UtilData ud = new UtilData(context);
		    	
		        if (tabId.equals("Acometidas")==true){
		    		ud.cargarListaAcometidas(act, ordenacion.ESTADO, "");
		        }
		        if (tabId.equals("Concertaciones")==true){
		    		ud.cargarListaConcertaciones(act, ordenacion.ESTADO, "");
		        }
		    }
		});
		
	}

	
	/***
	 * 
	 * @author		jjulia
	 * @date		08/08/2013
	 * @title		onCreateOptionsMenu / onOptionsItemSelected
	 * @comment		Gestionar el men� de opciones de la actividad
	 *	
	 *
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_activity_acometidas, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		TabHost tabs=(TabHost)findViewById(android.R.id.tabhost);
		UtilData ud = new UtilData(context);
		
		switch (item.getItemId()) {
		case R.id.ordenacionEstado:

			if (tabs.getCurrentTabTag().equals("Acometidas")==true){
	    		ud.cargarListaAcometidas(act, ordenacion.ESTADO,"");
			}
			if (tabs.getCurrentTabTag().equals("Concertaciones")==true){
	    		ud.cargarListaConcertaciones(act, ordenacion.ESTADO, "");
			}
			break;

		case R.id.ordenacionMunicipio:
			
			if (tabs.getCurrentTabTag().equals("Acometidas")==true){
				ud.cargarListaAcometidas(act, ordenacion.MUNICIPIO, "");
			}
			if (tabs.getCurrentTabTag().equals("Concertaciones")==true){
				ud.cargarListaConcertaciones(act, ordenacion.MUNICIPIO, "");
			}
			break;

		case R.id.ordenacionObra:
			
			if (tabs.getCurrentTabTag().equals("Acometidas")==true){
				ud.cargarListaAcometidas(act, ordenacion.OBRA, "");
			}
			if (tabs.getCurrentTabTag().equals("Concertaciones")==true){
	    		ud.cargarListaConcertaciones(act, ordenacion.OBRA, "");
			}
			break;
	
			
		case R.id.recargarDatos:

			// Men� Recarga de datos desde la web central
			recargarDatosBaseDatosLocal();
			break;
			
			
		case R.id.limpiarDatos:

			// Men� para la limpieza de la base de datos local
			limp�arDatosBaseDatosLocal();
			break;

		}

		return true;
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	02/10/2013
	 * @title	recargarDatosBaseDatosLocal
	 * @comment	Ejecutar el procedimiento de sincronizaci�n con web central para descargar los datos del
	 * 			usuario, en un proceso en primer plano con una ventana de progreso.
	 *
	 */
	private void recargarDatosBaseDatosLocal(){
		
		// Ejecutar el procedimiento de sincronizaci�n en un hilo secundario detr�s de una ventana de progreso
		String titulo = this.getResources().getString(R.string.item_menu_recargarDatos);
		String mensaje = this.getResources().getString(R.string.descargardatos);
		final ProgressDialog pd = ProgressDialog.show(
				this,
				titulo,
				mensaje,
				true, false);
		
		
		//Hilo secundario de recarga de datos
		new Thread(new Runnable(){
			public void run(){
				recargarDatosBaseDatosLocal_Ejecucion();
				pd.dismiss();

				//Notifica al hilo principal que ha finalizado el secundario
				Message msg = new Message();
				puenteProcesos.sendMessage(msg);
			}
		}).start();
			
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	17/10/2013
	 * @title	recargarDatosBaseDatosLocal_Ejecucion
	 * @comment	Procedimiento del hilo secundario de recarga de datos
	 */
	private void recargarDatosBaseDatosLocal_Ejecucion() {

		if (ProcesoSincronizacion.isRunnig()==false){
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this);
			sin.run();
			ProcesoSincronizacion.finalizar();
		}
						
	}
	
	/**
	 *
	 * @author	Jordi
	 * @date	03/11/2013
	 * @title	limp�arDatosBaseDatosLocal
	 * @comment	Procedimieto para ejecutar la opci�n de men� de limpieza de datos
	 *
	 *
	 */
	private void limp�arDatosBaseDatosLocal(){
		
	    String titulo = context.getResources().getString(R.string.title_dialog_confirmacion_accion);
		String mensaje = context.getResources().getString(R.string.mensaje_limpiarDatos);
		String si = context.getResources().getString(R.string.yes_dialog);
		String no = context.getResources().getString(R.string.no_dialog);
							
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setTitle(titulo);
		builder.setMessage(mensaje);
		builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		builder.setPositiveButton(si, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
							
				
				try {
				
					DBHelper dbh = new DBHelper(context);
					dbh.open();
					dbh.regenerar();
					dbh.close();
					
					finish();
						
				} catch (IOException e) {
					Log.e(this.getClass().getName(), e.getStackTrace().toString());
				}
					
			}
		});

		final AlertDialog alert = builder.create();
		alert.show();
	
	}
	
		
	/**
	 * 
	 * @author	jjulia
	 * @date	17/10/2013
	 * @title	puenteProcesos
	 * @comment	Procedimiento ha ejecutar al finalizar el hilo secundario
	 * 			Handler que se activa al finalizar el hilo secundario
	 */
	private Handler puenteProcesos = new Handler() {
		  @Override
		  public void handleMessage(Message msg) {
			  
			 if (ProcesoSincronizacion.erroresEnSincrnizacion().equals("")==false){
					String titulo = context.getResources().getString(R.string.item_menu_recargarDatos);
					String mensaje =ProcesoSincronizacion.erroresEnSincrnizacion();
					UtilActivity.presentarMensajerActivity(context,act, titulo, mensaje);
			 }else{
				  TabHost tabs=(TabHost)findViewById(android.R.id.tabhost);
				  UtilData ud = new UtilData(context);
					
				  if (tabs.getCurrentTabTag().equals("Acometidas")==true){
					ud.cargarListaAcometidas(act, ordenacion.ESTADO, "");
			      } 
				  if (tabs.getCurrentTabTag().equals("Concertaciones")==true){
					ud.cargarListaConcertaciones(act, ordenacion.ESTADO, "");
				  }
			 }
			 
		  }
	};
	
   /**
	 * 
	 * @author	jjulia
	 * @date	01/10/2013
	 * @title	inicializarPresentacionPlanificacion
	 * @comment	Procedimiento para iniciar la activity de Planificaci�n de acometidas
	 *	
	 *
	 */
	private void inicializarPresentacionPlanificacion() {
			
		//Inicia la actividad de Planificaci�n
		UtilActivity ut = new UtilActivity();
		ut.iniciarActivity(this, PlanificacionActivity.class);
		ut = null;		
			
	}
	
	
	
	
}
