package tec.telesupervision.services;

import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;

import tec.telesupervision.R;
import tec.telesupervision.Objects.json.DescargaDatosWeb;
import tec.telesupervision.data.UtilData;
import android.content.Context;
import android.util.Log;

public class ProcesosEjecucionAcciones {
	
	private Context context;
	private static String erroresProceso;
	
	public ProcesosEjecucionAcciones(Context context) {
		super();
		this.context = context;
		erroresProceso="";
	}
	
	public static String erroresEnAccion(){
		return erroresProceso;
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionPlanificacion
	 * @comment	Procedimiento para la acci�n de Planificaci�n de la acometida
	 *
	 * @param codObra			C�digo de la obra a planificar
	 * @param fPlanificacion	Fecha planificaci�n escogida
	 *
	 */
	public void ejecucionAccionPlanificacion(String codObra, String fPlanificacion) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PLANIFICACION - Iniciar planificaci�n, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION PLANIFICACION - Autorizado, acceder al servicio web.");
				ejecucionAccionPlanificacion_Procedimiento(userName, password, codObra, fPlanificacion, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionPlanificacion_Procedimiento(String user, String password, String codObra, String fPlanificacion, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionPLanificacion(codObra, fPlanificacion, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PLANIFICACION. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PLANIFICACION. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarAcometida(codObra, descargaJSON);
			sin.finalizarDinamico();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION PLANIFICACION. Final acci�n.");
			
	}
	
	

	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionInicio
	 * @comment	Procedimiento para la acci�n de inicio de una acometida
	 *
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionInicio(String codObra) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION INICIO - Iniciar Inicio, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION INICIO - Autorizado, acceder al servicio web.");
				ejecucionAccionInicio_Procedimiento(userName, password, codObra, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionInicio_Procedimiento(String user, String password, String codObra, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionInicio(codObra, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION INICIO. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION INICIO. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarAcometida(codObra, descargaJSON);
			sin.finalizarDinamico();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION INICIO. Final acci�n.");
			
	}
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionPeS
	 * @comment	Procedimiento para la acci�n de PeS de una acometida
	 *
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionPeS(String codObra) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PES - Iniciar PeS, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION PES - Autorizado, acceder al servicio web.");
				ejecucionAccionPeS_Procedimiento(userName, password, codObra, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionPeS_Procedimiento(String user, String password, String codObra, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionPeS(codObra, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PES. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PES. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarAcometida(codObra, descargaJSON);
			sin.finalizarDinamico();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION PES. Final acci�n.");
			
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionFinal
	 * @comment	Procedimiento para la acci�n de Final de una acometida
	 *
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionFinal(String codObra) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL - Iniciar Final, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL - Autorizado, acceder al servicio web.");
				ejecucionAccionFinal_Procedimiento(userName, password, codObra, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionFinal_Procedimiento(String user, String password, String codObra, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionFinal(codObra, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarAcometida(codObra, descargaJSON);
			sin.finalizarDinamico();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL. Final acci�n.");
			
	}
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionComentarios
	 * @comment	Procedimiento para la entrada de comentarios en la acometida
	 *
	 * @param codObra			C�digo de la obra a planificar
	 * @param comentarios   	Comentarios a la acometida
	 *
	 */
	public void ejecucionAccionComentarios(String codObra, String comentarios) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ENTRADA COMENTARIOS - Iniciar entrada comentarios, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ENTRADA COMENTARIOS - Autorizado, acceder al servicio web.");
				ejecucionAccionComentarios_Procedimiento(userName, password, codObra, comentarios, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionComentarios_Procedimiento(String user, String password, String codObra, String comentarios, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionComentarios(codObra, comentarios, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ENTRADA COMENTARIOS. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ENTRADA COMENTARIOS. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarAcometida(codObra, descargaJSON);
			sin.finalizarDinamico();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ENTRADA COMENTARIOS. Final acci�n.");
			
	}
	

}
