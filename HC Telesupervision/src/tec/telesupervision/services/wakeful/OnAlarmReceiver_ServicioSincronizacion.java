package tec.telesupervision.services.wakeful;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;


/**
 * 
 * @author	jjulia
 * @date	12/06/2013
 * @title	OnAlarmReceiver_ServicioSincronizacion
 * @comment	Clase para establecer el BroadcastReceiver del servicio de sincronización con la web
 *
 */
public class OnAlarmReceiver_ServicioSincronizacion extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(this.getClass().getName(), "ONALARMRECEIVER_SERVICIOSINCRONIZACION");
		WakefulIntentService.sendWakefulWork(context, ServicioSincronizacion.class);
	}
	
}
