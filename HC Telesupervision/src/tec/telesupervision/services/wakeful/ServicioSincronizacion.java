package tec.telesupervision.services.wakeful;

import java.util.Date;

import tec.telesupervision.data.UtilData;
import tec.telesupervision.services.LanzarProcesos;
import android.content.Intent;
import android.util.Log;

import com.commonsware.cwac.wakeful.WakefulIntentService;

/**
 * 
 * @author 		jjulia
 * @date 		12/06/2013
 * @title 		ServicioSincronizacion
 * @comment  	Servicio de sincronizaci�n con la Web
 * 
 * 				Este servicio realiza una sincronizaci�n con la web, 
 * 				sino est� autorizado a Internet o no hay registrado un usuario 
 * 				no inicia el procedimiento.
 *  
 * 
 */
public class ServicioSincronizacion extends WakefulIntentService {

	public ServicioSincronizacion() {
		super("ServicioSincronizacion");
	}

	@Override
	protected void doWakefulWork(Intent intent) {

		Log.i(this.getClass().getName(), "SERVICIO SINCRONIZACION. "	+ new Date().toString());
		try {

			UtilData ud = new UtilData(this.getApplicationContext());
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("") == true || password.trim().equals("") == true) {
				autorizado = false;
			}

			if (autorizado == true) {
				
				LanzarProcesos lp = new LanzarProcesos(this.getApplicationContext());			
				lp.inicializarSincronizacion();
				lp = null;

			} else {
				Log.i(this.getClass().getName(), "SERVICIO SINCRONIZACION. No autorizado a internet o usuario no registrado.");
			}

		} catch (Exception e) {

			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString());

		}
	}
}
