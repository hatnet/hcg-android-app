package tec.telesupervision.services.wakeful;

import tec.telesupervision.data.UtilData;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

/**
 * 
 * @author	jjulia
 * @date	12/06/2013
 * @title	OnBootReceiver
 * @comment	Clase para establecer el BroadcastReceiver para iniciar todos los servicios
 *			según la frecuencia establecida en los parámetros de configuración.
 *
 */
public class OnBootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
			
		try {
			
			//Obtener parámetros de configuración y establecer las frecuencias
			UtilData ud = new UtilData(context);
		    String minSincronizacion = ud.obtenerParametroTablaConfiguracion("minutosEjecucionServicioSincronizacion");
			int milisegundosSincronizacion = Integer.parseInt(minSincronizacion)*60*1000;
					
		    //Servicio de Sincronización con la Web
		    Log.i(this.getClass().getName(), "BOOTRECEIVER SINCRONIZACION");
		    
		    AlarmManager mgr2=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		    Intent i2=new Intent(context, OnAlarmReceiver_ServicioSincronizacion.class);
		    PendingIntent pi2=PendingIntent.getBroadcast(context, 0, i2, 0);
		    
		    int demoraInicial = 0; //30000;
		    mgr2.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
		                      SystemClock.elapsedRealtime()+demoraInicial,
		                      milisegundosSincronizacion,
		                      pi2);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getMessage());
		
		}

	}
}