package tec.telesupervision.services;


import android.content.Context;
import android.util.Log;


/**
 * 
 * @author		jjulia
 * @date		12/06/2013
 * @title		LanzarProcesos
 * @comment		Clase para gestionar el inicio de la ejecución de los procedimientos vinculados a los servicios
 *	
 *
 */
public class LanzarProcesos {

	Context context;

	private static final String threadNameSincronizacion = "sincronizacion";
			
	private Thread procesoSincronizacion;
	
	/**
	 * 
	 * @author		jjulia
	 * @date		12/06/2013
	 * @title		LanzarProcesos
	 * @comment		Contructor de la clase
	 *
	 * @param context	Contexto en el que se ejecutan los procedimiento
	 *
	 */
	public LanzarProcesos(Context context) {
		super();
		this.context = context;
	}
	
	
	/**
	 * 
	 * @author		jjulia
	 * @date		12/06/2013
	 * @title		inicializarSincronizacion
	 * @comment		Inicialización del procedimiento del Servicio de Sincronización con la Web
	 * 
	 *	
	 */
	public void inicializarSincronizacion() {
				
		if (ProcesoSincronizacion.isRunnig() == false ) {
			Log.i(this.getClass().getName(), "INICIALIZAR PROCESO SINCRONIZACION - INICIAR ");
     		procesoSincronizacion = new Thread(new ProcesoSincronizacion(context), threadNameSincronizacion);
			procesoSincronizacion.start();
		}
	}

	/**
	 * 
	 * @author		jjulia
	 * @date		12/06/2013
	 * @title		detenerSincronizacion
	 * @comment		Método para detener el procedimiento de sincronización
	 *	
	 *
	 */
	public  void detenerSincronizacion() {		
		ProcesoSincronizacion.finalizar();
		System.gc();		
	}
	
	
}
