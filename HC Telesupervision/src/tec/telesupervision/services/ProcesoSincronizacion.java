package tec.telesupervision.services;

import java.util.Date;
import java.util.Iterator;

import com.fasterxml.jackson.databind.ObjectMapper;

import tec.telesupervision.R;
import tec.telesupervision.Objects.json.DescargaDatosWeb;
import tec.telesupervision.Objects.json.JSONAsignacion;
import tec.telesupervision.Objects.json.JSONConcertacion;
import tec.telesupervision.Objects.json.JSONMensaje;
import tec.telesupervision.Objects.json.JSONObra;
import tec.telesupervision.Objects.json.SincronizacionElemento;
import tec.telesupervision.data.DataBase;
import tec.telesupervision.data.UtilData;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;



/**
 * 
 * @author	mjulia
 * @date	08/03/2013
 * @title	Proceso
 * @comment	Clase que implementa la interfaz runnable para la ejecución del servicio de Sincronización
 *
 */
public class ProcesoSincronizacion implements Runnable {

	private Context context;

	//Es importante declarar la variable como volatile para que cuando el estado de la variable cambie el while del método run se entere.
	private volatile static boolean isRunningProceso = false;
	
	private static String erroresSincrnizacion = "";
	
	
	public ProcesoSincronizacion(Context context) {
		super();
		this.context = context;
		isRunningProceso = true;
		erroresSincrnizacion="";
	}

	public static boolean isRunnig (){
		return isRunningProceso;
	}
	
	public static String erroresEnSincrnizacion(){
		return erroresSincrnizacion;
	}
	
	
	/**
	 * Finaliza el proceso
	 */
	public static void finalizar(){
		isRunningProceso = false;	
		System.gc();
	}
	public void finalizarDinamico(){
		isRunningProceso = false;	
		System.gc();
	}
	
	
	/**
	 * Inicializa el proceso mientras isRunningProceso sea true.
	 */
	public void run() {	
		
		while(isRunningProceso == true) {	
			
			try{
				UtilData ud = new UtilData(context);
				boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
				String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
				String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
				if (userName.trim().equals("")==true || password.trim().equals("")==true){
					autorizado=false;
				}
					
				ConexionesSincronizacion us = new ConexionesSincronizacion(context);
				if (us.networkAvailable() != true ) {
					autorizado=false;
				}
				
				if(autorizado == true) {
						
					//Recepción de datos de la web al terminal
					Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. Recepción de datos de la web.");
					SincronizacionRecepcionarDatosWeb(userName, password, us);
											
				}else{
					erroresSincrnizacion = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
				}
							
			} catch (Exception e) {	
				
				e.printStackTrace();
				Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
				
				erroresSincrnizacion = context.getResources().getString(R.string.error_SIncronizacion_Error);
				
			} finally{
												
				//Finalizar el proceso
				finalizar();
				
			}
		}
	}
	
	
	
	
	
	/**
	 * 
	 * @author		jjulia
	 * @date		14/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb
	 * @comment		Procedimientode la sincronización para recepcionar los datos que la Web Central envía al terminal
	 *
	 * @param user		Identificador de usuario configurado en el terminal
	 * @param password	Contraseña del usuario configurado
	 * @param us		Objeto clase con utilidades para la sincronización de datos
	 * @throws Exception 
	 *
	 */
	public void SincronizacionRecepcionarDatosWeb(String user, String password, ConexionesSincronizacion us) throws Exception {
						
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para obtener los datos a cargar en el terminal
    	String datosJSON = us.recepcionarDatosWebService(user, password);
		if (datosJSON == null){
			//(no se han descargado datos parapendientes de actualizar)
			Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. No se han descargado datos en el Terminal.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. Documento JSON con los datos descargados:" + datosJSON);
									
			//Proceso de actualización en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class );  
			SincronizacionRecepcionarDatosWeb_ActualizacionDescarga(descargaJSON); 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. Final recepción.");
			
	}
		
	
	/**
	 * 
	 * @author		jjulia
	 * @date		18/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb_ActualizacionDescarga
	 * @comment		Procedimiento para gestionar el documento recibido de la web con los datos a cargar en el Terminal
	 * 	  
	 * @param   	descargaJSON	Documento JSON recibido de la web con los datos a cargar en el Terminal
	 *
	 */
	private void SincronizacionRecepcionarDatosWeb_ActualizacionDescarga(DescargaDatosWeb descargaJSON){
		
		DataBase db = new DataBase(context);
	    boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}
				
		//Eliminar tablas de Acometidas, Asignaciones y Concertaciones
		db.execSQL("DELETE FROM acometidas");
		db.execSQL("DELETE FROM asignaciones");
		db.execSQL("DELETE FROM concertaciones");
		
		
		//Lectura y actualizazición de los datos recibidos
		Iterator<SincronizacionElemento> iter = descargaJSON.iterator();
		while (iter.hasNext()) {
			SincronizacionElemento elemento = iter.next();
									
			//Procesar las actualizaciones en las Obras
			if (elemento.getElementoObra() != null){
				JSONObra ctl = (JSONObra) elemento.getElementoObra();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Obras(ctl, db);
			}
			
			//Procesar las actualizaciones en las Asignaciones
			if (elemento.getElementoAsignacion() != null){
				JSONAsignacion ctl = (JSONAsignacion) elemento.getElementoAsignacion();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Asignaciones(ctl, db);
			}
			
			//Procesar las actualizaciones en las Concertaciones
			if (elemento.getElementoConcertacion() != null){
				JSONConcertacion ctl = (JSONConcertacion) elemento.getElementoConcertacion();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Concertacion(ctl, db);
			}
			
			//Procesar las mensajes
			if (elemento.getElementoMensaje() != null){
				JSONMensaje ctl = (JSONMensaje) elemento.getElementoMensaje();
				Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. Mensaje: " + ctl.getMensaje());
			}
			
			//Finalizaciín elemento
			//Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. Recepción elemento");
							
		}
		
		//Finalización actualización descarga 
		db.close();
		
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	SincronizacionActualizarAcometida
	 * @comment	Procedimiento para actualizar los datos relativos a una obra
	 *
	 * @param CodObra		Código de la obra a actualizar
	 * @param descargaJSON	Documento JSON con los datos de la obra actualizados	
	 *
	 */
	public void SincronizacionActualizarAcometida(String CodObra, DescargaDatosWeb descargaJSON){
		
		DataBase db = new DataBase(context);
	    boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}
		
		
		//Comprobar si se han recibido datos de la obra o un mensaje
		if (descargaJSON.size()>0){
			SincronizacionElemento elemento = descargaJSON.get(0);
			
			if (elemento.getElementoObra() != null || elemento.getElementoAsignacion() != null || elemento.getElementoConcertacion() != null){
				//Si se han recibido datos de la obra,
				//eliminar de lastablas de Acometidas, Asignaciones y Concertaciones la obra
				db.execSQL("DELETE FROM acometidas WHERE Cod_Obra='" + CodObra + "' ");
				db.execSQL("DELETE FROM asignaciones WHERE Cod_Obra='" + CodObra + "' ");
				db.execSQL("DELETE FROM concertaciones WHERE Cod_Obra='" + CodObra + "' ");
				Log.i(this.getClass().getName(), "EJECUTAR ACCIÓN - ACTUALIZAR UNA OBRA. Eliminar datos de la obra en el dispositivo. ");

			}
		}
		
	
		//Lectura y actualizazición de los datos recibidos
		Iterator<SincronizacionElemento> iter = descargaJSON.iterator();
		while (iter.hasNext()) {
			SincronizacionElemento elemento = iter.next();
					
			Log.i(this.getClass().getName(), "EJECUTAR ACCIÓN - ACTUALIZAR UNA OBRA. Recargar nuevos datos recibidos. ");
			
			//Procesar las actualizaciones en las Obras
			if (elemento.getElementoObra() != null){
				JSONObra ctl = (JSONObra) elemento.getElementoObra();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Obras(ctl, db);
			}
			
			//Procesar las actualizaciones en las Asignaciones
			if (elemento.getElementoAsignacion() != null){
				JSONAsignacion ctl = (JSONAsignacion) elemento.getElementoAsignacion();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Asignaciones(ctl, db);
			}
			
			//Procesar las actualizaciones en las Concertaciones
			if (elemento.getElementoConcertacion() != null){
				JSONConcertacion ctl = (JSONConcertacion) elemento.getElementoConcertacion();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Concertacion(ctl, db);
			}
			
			//Procesar las mensajes
			if (elemento.getElementoMensaje() != null){
				JSONMensaje ctl = (JSONMensaje) elemento.getElementoMensaje();
				Toast.makeText(context, ctl.getMensaje() , Toast.LENGTH_LONG).show();
				Log.i(this.getClass().getName(), "EJECUTAR ACCIÓN - ACTUALIZAR UNA OBRA. Mensaje: " + ctl.getMensaje());
			}
			
					
		}
		
		//Finalización actualización descarga 
		db.close();
		
	}
	
	
	/**
	 * 
	 * @author		jjulia
	 * @date		20/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Obras
	 * @comment		Actualización de las OBRAS desde documento JSON 
	 *
	 * @param ctl	Elemento JSON de Obras
	 * @param db	Instancia de la base de datos local del terminal	
	 *
	 */
	private void SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Obras(JSONObra ctl, DataBase db){
	
		ContentValues values = new ContentValues();
		values.put("Cod_Obra", ctl.getCodObra());
		values.put("Direccion", ctl.getDireccion());
		values.put("Municipio", ctl.getMunicipio());
		values.put("CarpetasOonair", ctl.getCarpetasOonair());
		
		db.insert("acometidas", null, values);
		
	}
	
	/**
	 * 
	 * @author		jjulia
	 * @date		20/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Asignaciones
	 * @comment		Actualización de las ASIGNACIONES desde documento JSON 
	 *
	 * @param ctl	Elemento JSON de Asignaciones
	 * @param db	Instancia de la base de datos local del terminal	
	 *
	 */
	private void SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Asignaciones(JSONAsignacion ctl, DataBase db){
	
		ContentValues values = new ContentValues();
		values.put("Cod_Obra", ctl.getCodObra());
		values.put("Direccion", ctl.getDireccion());
		values.put("Municipio", ctl.getMunicipio());
		values.put("CarpetasOonair", ctl.getCarpetasOonair());
		values.put("FechaPlan", ctl.getFechaPlanificacion());
		values.put("FechaIni", ctl.getFechaInicio());
		values.put("FechaPeS", ctl.getFechaPeS());
		values.put("FechaFin", ctl.getFechaFinal());
		values.put("FechaAcepNomb", ctl.getFechaAceptacionNombramiento());
		values.put("GestorObra", ctl.getGestorTelesup());
		values.put("Comentarios", ctl.getComentarios());
						
		db.insert("asignaciones", null, values);
		
	}
	
	/**
	 * 
	 * @author		jjulia
	 * @date		20/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Concertacion
	 * @comment		Actualización de CONCERTACIONES desde documento JSON 
	 *
	 * @param ctl	Elemento JSON de Concertaciones
	 * @param db	Instancia de la base de datos local del terminal	
	 *
	 */
	private void SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Concertacion(JSONConcertacion ctl, DataBase db){
					
		ContentValues values = new ContentValues();
		values.put("ID", ctl.getID());
		values.put("Cod_Obra", ctl.getCodObra());
		values.put("InicioPlanificacion", ctl.getInicioPlanificacion());
		values.put("Resultado", ctl.getTelesupervisionResultado());
		values.put("CausasRechazo", ctl.getTelesupervisionCausas());
		values.put("GestorObra", ctl.getTelesupervisionGestorObra());
		
		db.insert("concertaciones", null, values);
	}
	
}	
	
