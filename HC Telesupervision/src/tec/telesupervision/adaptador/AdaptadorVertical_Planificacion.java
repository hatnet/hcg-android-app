package tec.telesupervision.adaptador;

import java.util.ArrayList;
import java.util.Date;

import tec.controles.calendar.CalendarControl;
import tec.telesupervision.R;
import tec.telesupervision.Objects.ObjetoAcometida;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.data.instruccionesSQL.ordenacion;
import tec.telesupervision.services.ProcesosEjecucionAcciones;
import tec.telesupervision.utilidades.UtilidadesFechas;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * 
 * @author 	jjulia
 * @date 	05/10/2012
 * @title 	Adaptador para cargar una matriz de datos "ObjetoAcometida" en un Layout concreto
 * @comment
 * 
 */
public class AdaptadorVertical_Planificacion extends ArrayAdapter<ObjetoAcometida> {

	private Context context;
	private Activity act;
	private ArrayList<ObjetoAcometida> objects;
	
	
	
	/***
	 * 
	 * @author jjulia
	 * @date 05/10/2012
	 * @title Constructor del adaptador
	 * @comment
	 * 
	 * @param context		      Contexto
	 * @param textViewResourceId  Layout particular donde deben cargarse los datos
	 * @param objects             Matriz con los datos a cargar
	 * 
	 */
	public AdaptadorVertical_Planificacion(Context context, int textViewResourceId, ArrayList<ObjetoAcometida> objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.context = context;
		this.act= (Activity) context;
		
		
	}
	
	
	/**
	 * Gestionar la carga de los datos en los elementos del listitem
	 */
	public View getView(int position, View convertView, ViewGroup parent) {

		Resources res = context.getResources();
		
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.listitem_vertical_planificacion, null);
		}
		
		
		// Obtener los valores del listitem
		ObjetoAcometida i = objects.get(position);
		if (i != null) {
							
			// Obtener los controles 
			TextView lblObra = (TextView) v.findViewById(R.id.lblObra);
			ImageButton btnPlanificacion = (ImageButton) v.findViewById(R.id.btnPlanificacion);
		
			// Cargar los datos en los controles
			String obra="";
			if (i.getDireccion().trim().contains("-") == true){
				obra= i.getCod_obra()+" - "+i.getDireccion();
			}else{
				obra= i.getCod_obra()+" - "+i.getMunicipio()+" - "+i.getDireccion();
			}
			lblObra.setText(obra);
			btnPlanificacion.setTag(i);
			
			// Establecer las acciones en los controladores de eventos
			btnPlanificacion.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					ImageButton btn = (ImageButton) v ;  
					ObjetoAcometida acom = (ObjetoAcometida) btn.getTag();	
					confirmarAccionPlanificacion( acom);
				}
			});
			
			//Dar estilo a los controles
			LinearLayout linContenido = (LinearLayout) v.findViewById(R.id.linContenido);
			LinearLayout linPlanificacion = (LinearLayout) v.findViewById(R.id.linPlanificacion);
			linContenido.setBackgroundColor(res.getColor(R.color.colorWhite));
			linPlanificacion.setBackgroundColor(res.getColor(R.color.colorWhite));
			btnPlanificacion.setBackgroundColor(res.getColor(R.color.colorWhite));
		}

		// Devuelve la vista con los datos cargados en el Layout
		return v;

	}

	

	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	confirmarAccionPlanificacion
	 * @comment	Procedimiento para confirmar la acci�n de planificaci�n y enlazar con el Proceso de ejecici�n de la acci�n
	 *
	 * @param obj	Objeto acometida con los datos seleccionados
	 *
	 */
	private void confirmarAccionPlanificacion(ObjetoAcometida obj){
		
		//Generamos un dialogo para introducir la fecha de planificaci�n y confirmar la acci�n
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_planificacion,(ViewGroup) act.findViewById(R.layout.activity_planificacion));
		builder.setView(layout);
		
		final AlertDialog alertDialog = builder.create();
		final CalendarControl calPlanificacion =  (CalendarControl) layout.findViewById(R.id.calPlanificacion);
		final TextView lblPlanificacion =  (TextView) layout.findViewById(R.id.lblPlanificacion);
		
		//Establece el t�tulo del dialogo
		String obra="";
		if (obj.getDireccion().trim().contains("-") == true){
			obra= obj.getCod_obra()+" - "+obj.getDireccion();
		}else{
			obra= obj.getCod_obra()+" - "+obj.getMunicipio()+" - "+obj.getDireccion();
		}
		lblPlanificacion.setText(obra);
	
		//Establece los controladores de eventos para las acciones de los botenes del dialogo
		Button btnAceptar =  (Button) layout.findViewById(R.id.btnAceptarPlanificacion);
		Button btnCancelar =  (Button) layout.findViewById(R.id.btnCancelarPlanificacion);
		
		btnAceptar.setTag(obj);
				
		btnCancelar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		
		});
		
		btnAceptar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Button btn = (Button) v ;  
				ObjetoAcometida acom = (ObjetoAcometida) btn.getTag();	
				
				Date fecha = calPlanificacion.getSelectedDate();
				if (fecha != null) { 
					
					UtilidadesFechas utf = new UtilidadesFechas();
					String fPlanificacion =utf.convertirFechaToString(fecha, UtilData.FORMAT_DATE_SHORT_BD);
					
					//Ejecutar la acci�n de planificaci�n
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionPlanificacion(acom.getCod_obra(), fPlanificacion);
					
					//Recargar la lista de acometidas pendientes de planificaci�n
					UtilData ud = new UtilData(context);
					ud.cargarListaMiPosiblePlanificacion(act, ordenacion.MUNICIPIO);
					
				}
				
				alertDialog.dismiss();
			}
		
		});
		
		// Mostramos el alertdialog
		alertDialog.show();
						
	}
	

}
