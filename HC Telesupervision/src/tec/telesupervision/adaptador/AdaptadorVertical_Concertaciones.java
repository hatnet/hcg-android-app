package tec.telesupervision.adaptador;

import java.util.ArrayList;

import tec.telesupervision.R;
import tec.telesupervision.Objects.ObjetoAcometida;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.data.instruccionesSQL.ordenacion;
import tec.telesupervision.services.ProcesosEjecucionAcciones;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 
 * @author 	jjulia
 * @date 	05/10/2012
 * @title 	Adaptador para cargar una matriz de datos "ObjetoAcometida" en un Layout concreto
 * @comment
 * 
 */
public class AdaptadorVertical_Concertaciones extends ArrayAdapter<ObjetoAcometida> {

	private Context context;
	private Activity act;
	private ArrayList<ObjetoAcometida> objects;
	private String resultadoRechazada;
	private String resultadoRechazada24h;
	private String resultadoRechazada48h;
	private String resultadoAceptada;
	
	
	/***
	 * 
	 * @author jjulia
	 * @date 05/10/2012
	 * @title Constructor del adaptador
	 * @comment
	 * 
	 * @param context		      Contexto
	 * @param textViewResourceId  Layout particular donde deben cargarse los datos
	 * @param objects             Matriz con los datos a cargar
	 * 
	 */
	public AdaptadorVertical_Concertaciones(Context context, int textViewResourceId, ArrayList<ObjetoAcometida> objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.context = context;	
		this.act =(Activity) context;
		
		UtilData ud=new UtilData(context);
		resultadoRechazada48h=ud.obtenerParametroTablaConfiguracion("resultadoRechazada48h");
		resultadoRechazada24h=ud.obtenerParametroTablaConfiguracion("resultadoRechazada24h");
		resultadoRechazada=ud.obtenerParametroTablaConfiguracion("resultadoRechazada");
		resultadoAceptada=ud.obtenerParametroTablaConfiguracion("resultadoAceptada");
	}
	
	
	// Obtener la vista donde se genera el Layout
	public View getView(int position, View convertView, ViewGroup parent) {

		Resources res = context.getResources();
		
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.listitem_vertical_concertaciones, null);
		}

		// Obtener los valores de la matriz
		ObjetoAcometida i = objects.get(position);
		if (i != null) {
							
			// Obtener los controles a gestionar
			TextView lblObra = (TextView) v.findViewById(R.id.lblObra);
			TextView lblFPeS = (TextView) v.findViewById(R.id.lblFechaPeS);
			TextView lblFFinal = (TextView) v.findViewById(R.id.lblFechaFinal);
			TextView lblFTelesup = (TextView) v.findViewById(R.id.lblFechaTelesupervision);
			TextView datFPeS = (TextView) v.findViewById(R.id.datFechaPeS);
			TextView datFFinal = (TextView) v.findViewById(R.id.datFechaFinal);
			TextView lblTelesup = (TextView) v.findViewById(R.id.lblTelesupPlanificacion);
			TextView datFTelesup = (TextView) v.findViewById(R.id.datFechaTelesupervision);
			TextView datTelesupGestor = (TextView) v.findViewById(R.id.datTelesupGestor);
			TextView datTelesupCausas = (TextView) v.findViewById(R.id.datTelesupCausas);
			
			
			// Cargar los datos en el elemento
			String obra="";
			if (i.getDireccion().trim().contains("-") == true){
				obra= i.getCod_obra()+" - "+i.getDireccion();
			}else{
				obra= i.getCod_obra()+" - "+i.getMunicipio()+" - "+i.getDireccion();
			}
			
			lblObra.setText(obra);
			lblFFinal.setText(res.getString(R.string.item_label_fecFinal));
			lblFPeS.setText(res.getString(R.string.item_label_fecPeS));
			lblFTelesup.setText(res.getString(R.string.item_label_fecTelesup));
			datFFinal.setText(i.getFechaFinal());
			datFPeS.setText(i.getFechaPeS());
			datFTelesup.setText(i.getTelesupFechaPlanificacion());
					
			//lblTelesup.setText(res.getString(R.string.item_label_telesupervision));
     	    datTelesupGestor.setText(i.getTelesupGestorObra());
		    datTelesupCausas.setText(i.getTelesupCausas());
		    
		    	
		    //Establecer acci�n y color de fondo en base al estado de la acometida
		    LinearLayout linContenido = (LinearLayout) v.findViewById(R.id.linContenido);
		    LinearLayout linAcciones = (LinearLayout) v.findViewById(R.id.linAcciones);
	   	    ImageButton btnAccion = (ImageButton) v.findViewById(R.id.btnBotonAccion);
			btnAccion.setTag(i);
		    btnAccion.setVisibility(View.GONE);
		    int colorBtn =R.color.colorWhite;
		    
		   
		    
		    if (i.getTelesupResultado().equals(resultadoRechazada)==true ){
		    	btnAccion.setVisibility(View.GONE);
		    	colorBtn=R.color.colorRedLight;
		    	String resultado= res.getString(R.string.item_label_telesupervision) + " - " + i.getTelesupResultado().toUpperCase();
		    	lblTelesup.setText(resultado);
		    }else if (i.getTelesupResultado().equals(resultadoRechazada24h)==true ){
			   	btnAccion.setVisibility(View.GONE);
			   	colorBtn=R.color.colorRedLight;
			   	String resultado= res.getString(R.string.item_label_telesupervision) + " - " + i.getTelesupResultado().toUpperCase();
			   	lblTelesup.setText(resultado);
		    }else if (i.getTelesupResultado().equals(resultadoRechazada48h)==true){
			   	btnAccion.setVisibility(View.GONE);
			   	colorBtn=R.color.colorRedLight;
			   	String resultado= res.getString(R.string.item_label_telesupervision) + " - " + i.getTelesupResultado().toUpperCase();
			   	lblTelesup.setText(resultado);
		    }else if (i.getTelesupResultado().equals(resultadoAceptada)==true){
		    	btnAccion.setVisibility(View.GONE);
		    	colorBtn=R.color.colorGreenLight;
		    	String resultado= res.getString(R.string.item_label_telesupervision) + " - " + i.getTelesupResultado().toUpperCase();
		    	lblTelesup.setText(resultado);
		    } else {
		    	btnAccion.setVisibility(View.VISIBLE);
		    	String resultado= res.getString(R.string.item_label_telesupervision);
		    	lblTelesup.setText(resultado);
		    }
		    
		    linContenido.setBackgroundColor(res.getColor(colorBtn));
	    	linAcciones.setBackgroundColor(res.getColor(colorBtn));
		    if (i.getFechaFinal().equals("")==true){
			    btnAccion.setBackgroundColor(res.getColor(colorBtn));
			    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_final", null, context.getPackageName()));
			    btnAccion.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						ObjetoAcometida i = (ObjetoAcometida) v.getTag();
						procesarAccionSeleccionada("Finalizar", i);
					}
				});
		    } else {
		    	btnAccion.setVisibility(View.GONE);
		    }
		    
		   
		}

		// Devuelve la vista con los datos cargados en el Layout
		return v;

	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	03/10/2013
	 * @title	procesarAccionSeleccionada
	 * @comment	Procesar la acci�n seleccionada de la l�nea de acometidas
	 *
	 * @param accion	Clave de la acci�n seleccionada
	 * @param obj	    Objeto acometida seleccionada 
	 *
	 */
	private void procesarAccionSeleccionada(final String accion, final ObjetoAcometida obj){
		
	    String titulo = context.getResources().getString(R.string.title_dialog_confirmacion_accion);
		String mensaje ="";
		
		if (accion.equals("Finalizar")==true){
			mensaje += context.getResources().getString(R.string.mensaje_finalizar);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		String si = context.getResources().getString(R.string.yes_dialog);
		String no = context.getResources().getString(R.string.no_dialog);
						
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setTitle(titulo);
		builder.setMessage(mensaje);
		builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		builder.setPositiveButton(si, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
							
				//Ejecutar la acci�n seleccionada
				if (accion.equals("Finalizar")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionFinal(obj.getCod_obra());
				}
				
				//Recargar la lista de acometidas 
				UtilData ud = new UtilData(context);
				ud.cargarListaConcertaciones(act, ordenacion.ESTADO, obj.getCod_obra());	
						
			}
		});

		final AlertDialog alert = builder.create();
		alert.show();
		
	}
	
	
}
