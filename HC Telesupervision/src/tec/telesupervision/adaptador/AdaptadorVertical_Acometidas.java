package tec.telesupervision.adaptador;

import java.util.ArrayList;

import tec.telesupervision.R;
import tec.telesupervision.Objects.ObjetoAcometida;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.data.instruccionesSQL.ordenacion;
import tec.telesupervision.services.ProcesosEjecucionAcciones;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * 
 * @author 	jjulia
 * @date 	05/10/2012
 * @title 	Adaptador para cargar una matriz de datos "ObjetoAcometida" en un Layout concreto
 * @comment
 * 
 */
public class AdaptadorVertical_Acometidas extends ArrayAdapter<ObjetoAcometida> {

	private Context context;
	private Activity act;
	private ArrayList<ObjetoAcometida> objects;
	
	
	/***
	 * 
	 * @author jjulia
	 * @date 05/10/2012
	 * @title Constructor del adaptador
	 * @comment
	 * 
	 * @param context		      Contexto
	 * @param textViewResourceId  Layout particular donde deben cargarse los datos
	 * @param objects             Matriz con los datos a cargar
	 * 
	 */
	public AdaptadorVertical_Acometidas(Context context, int textViewResourceId, ArrayList<ObjetoAcometida> objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.context = context;	
		this.act =(Activity) context;
	}
	
	
	// Obtener la vista donde se genera el Layout
	public View getView(int position, View convertView, ViewGroup parent) {

		Resources res = context.getResources();
		
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.listitem_vertical_acometidas, null);
		}

		// Obtener los valores de la matriz
		ObjetoAcometida i = objects.get(position);
		if (i != null) {
							
			// Obtener los controles a gestionar
			TextView lblObra = (TextView) v.findViewById(R.id.lblObra);
			TextView lblFAceptacionNomb = (TextView) v.findViewById(R.id.lblFechaAcpetacionNombramiento);
			TextView lblFPlanificacion = (TextView) v.findViewById(R.id.lblFechaPlanificacion);
			TextView lblFInicio = (TextView) v.findViewById(R.id.lblFechaInicio);
			TextView lblFPeS = (TextView) v.findViewById(R.id.lblFechaPeS);
			TextView lblFFinal = (TextView) v.findViewById(R.id.lblFechaFinal);
			TextView lblGTelesup = (TextView) v.findViewById(R.id.lblTelesupervision);
			TextView datFAceptacionNomb = (TextView) v.findViewById(R.id.datFechaAcpetacionNombramiento);
			TextView datFPlanificacion = (TextView) v.findViewById(R.id.datFechaPlanificacion);
			TextView datFInicio = (TextView) v.findViewById(R.id.datFechaInicio);
			TextView datFPeS = (TextView) v.findViewById(R.id.datFechaPeS);
			TextView datFFinal = (TextView) v.findViewById(R.id.datFechaFinal);
			TextView datGTelesup = (TextView) v.findViewById(R.id.datTelesupGestor);
			TextView datComentarios = (TextView) v.findViewById(R.id.datComentarios);
			
			LinearLayout linComentarios = (LinearLayout) v.findViewById(R.id.linComentarios);
					
			// Cargar los datos en el elemento
			String obra="";
			if (i.getDireccion().trim().contains("-") == true){
				obra= i.getCod_obra()+" - "+i.getDireccion();
			}else{
				obra= i.getCod_obra()+" - "+i.getMunicipio()+" - "+i.getDireccion();
			}
			lblObra.setText(obra);
				
			lblFAceptacionNomb.setText(R.string.item_label_fecAcepNomb);
			lblFPlanificacion.setText(res.getString(R.string.item_label_fecPlanificacion));
			lblFInicio.setText(res.getString(R.string.item_label_fecInicio));
			lblFPeS.setText(res.getString(R.string.item_label_fecPeS));
			lblFFinal.setText(res.getString(R.string.item_label_fecFinal));
			lblGTelesup.setText(res.getString(R.string.item_label_gesTelesup));
			
			datFAceptacionNomb.setText(i.getFechaAcepNomb());
			datFPlanificacion.setText(i.getFechaPlanificacion());
			datFInicio.setText(i.getFechaInicio());
			datFPeS.setText(i.getFechaPeS());
			datFFinal.setText(i.getFechaFinal());
			datGTelesup.setText(i.getTelesupGestorObra());
			
			if (i.getComentarios().equals("")==false){
				linComentarios.setVisibility(View.VISIBLE);
				datComentarios.setText(i.getComentarios());
			}else{
				linComentarios.setVisibility(View.GONE);
			}
			
		    //Ocultar fechas aun no informadas
			if(i.getFechaAcepNomb().equals("") == true){
				lblFAceptacionNomb.setVisibility(View.GONE);
				datFAceptacionNomb.setVisibility(View.GONE);
			} else {
				lblFAceptacionNomb.setVisibility(View.VISIBLE);
				datFAceptacionNomb.setVisibility(View.VISIBLE);
			}
		    if (i.getFechaInicio().equals("")==true){
		    	lblFInicio.setVisibility(View.GONE);
		    	datFInicio.setVisibility(View.GONE);
		    }else{
		    	lblFInicio.setVisibility(View.VISIBLE);
		    	datFInicio.setVisibility(View.VISIBLE);
		    }
		    if (i.getFechaPeS().equals("")==true){
		    	lblFPeS.setVisibility(View.GONE);
		    	datFPeS.setVisibility(View.GONE);
		    }else{
		    	lblFPeS.setVisibility(View.VISIBLE);
		    	datFPeS.setVisibility(View.VISIBLE);
		    }
		    if (i.getFechaFinal().equals("")==true){
		    	lblFFinal.setVisibility(View.GONE);
		    	datFFinal.setVisibility(View.GONE);
		    }else{
		    	lblFFinal.setVisibility(View.VISIBLE);
		    	datFFinal.setVisibility(View.VISIBLE);
		    }
		    
		    
		    //Establecer la acci�n para la introducci�n de comentarios a la obra por parte del JOC
		   	lblObra.setTag(i);
		   	lblObra.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					ObjetoAcometida i = (ObjetoAcometida) v.getTag();
					procesarAccionComentariosObra(i);
				}
			});
		    
			
			//Establecer acci�n y color de fondo en base al estado de la acometida
		    LinearLayout linContenido = (LinearLayout) v.findViewById(R.id.linContenido);
		    LinearLayout linAcciones = (LinearLayout) v.findViewById(R.id.linAcciones);
		    ImageButton btnAccion = (ImageButton) v.findViewById(R.id.btnBotonAccion);
		    btnAccion.setTag(i);
		    btnAccion.setVisibility(View.GONE);
		    
		    if (i.getFechaFinal().equals("")==false){
			    linContenido.setBackgroundColor(res.getColor(R.color.colorRedLight));
			    linAcciones.setBackgroundColor(res.getColor(R.color.colorRedLight));
			    btnAccion.setVisibility(View.GONE);
			    
		    }else if (i.getFechaPeS().equals("") == false){
				    linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
				    linAcciones.setBackgroundColor(res.getColor(R.color.colorBlue2));
				    
				    btnAccion.setVisibility(View.VISIBLE);
				    btnAccion.setBackgroundColor(res.getColor(R.color.colorBlue2));
				    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_final", null, context.getPackageName()));
				    btnAccion.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							ObjetoAcometida i = (ObjetoAcometida) v.getTag();
							procesarAccionSeleccionada("Finalizar", i);
						}
					});
				    
		    }else if (i.getFechaInicio().equals("") == false ){
			    linContenido.setBackgroundColor(res.getColor(R.color.colorBlue1));
			    linAcciones.setBackgroundColor(res.getColor(R.color.colorBlue1));

			    btnAccion.setVisibility(View.VISIBLE);
			    btnAccion.setBackgroundColor(res.getColor(R.color.colorBlue1));
			    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_pes", null, context.getPackageName()));
			    btnAccion.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						ObjetoAcometida i = (ObjetoAcometida) v.getTag();
						procesarAccionSeleccionada("PeS", i);
					}
				});
			    
		    }else{
		    	// mostrar el bot�n de inicio cuando la fecha de aceptaci�n del nombramiento este informada.
		    	if (i.getFechaAcepNomb().equals("") == false) {	    		
		    	
			    	linContenido.setBackgroundColor(res.getColor(R.color.colorWhite));
			    	linAcciones.setBackgroundColor(res.getColor(R.color.colorWhite));
			    			    	
			    	btnAccion.setVisibility(View.VISIBLE);
				    btnAccion.setBackgroundColor(res.getColor(R.color.colorWhite));
				    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_inicio", null, context.getPackageName()));
				    btnAccion.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							ObjetoAcometida i = (ObjetoAcometida) v.getTag();
							procesarAccionSeleccionada("Iniciar", i);
						}
					});
		    	}
		    }
		    

				
		}

		// Devuelve la vista con los datos cargados en el Layout
		return v;

	}
	

	/**
	 * 
	 * @author	jjulia
	 * @date	03/10/2013
	 * @title	procesarAccionSeleccionada
	 * @comment	Procesar la acci�n seleccionada de la l�nea de acometidas
	 *
	 * @param accion	Clave de la acci�n seleccionada
	 * @param obj	    Objeto acometida seleccionada 
	 *
	 */
	private void procesarAccionSeleccionada(final String accion, final ObjetoAcometida obj){
		
	    String titulo = context.getResources().getString(R.string.title_dialog_confirmacion_accion);
		String mensaje ="";
		
		if (accion.equals("Iniciar")==true){
			mensaje += context.getResources().getString(R.string.mensaje_iniciar);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		if (accion.equals("PeS")==true){
			mensaje += context.getResources().getString(R.string.mensaje_pes);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		if (accion.equals("Finalizar")==true){
			mensaje += context.getResources().getString(R.string.mensaje_finalizar);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		
		String si = context.getResources().getString(R.string.yes_dialog);
		String no = context.getResources().getString(R.string.no_dialog);
						
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setTitle(titulo);
		builder.setMessage(mensaje);
		builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		builder.setPositiveButton(si, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
							
				//Ejecutar la acci�n seleccionada
				if (accion.equals("Iniciar")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionInicio(obj.getCod_obra());
				}
				if (accion.equals("PeS")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionPeS(obj.getCod_obra());
				}
				if (accion.equals("Finalizar")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionFinal(obj.getCod_obra());
				}
				
				//Recargar la lista de acometidas 
				UtilData ud = new UtilData(context);
				ud.cargarListaAcometidas(act, ordenacion.ESTADO, obj.getCod_obra());	
						
			}
		});

		final AlertDialog alert = builder.create();
		alert.show();
		
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	05/11/2013
	 * @title	procesarAccionComentariosObra
	 * @comment	Procedimiento para la introducci�n de los comentarios y su env�o a la web central
	 *
	 * @param obj	Objeto Acometida seleccionada
	 *
	 */
	private void procesarAccionComentariosObra(ObjetoAcometida obj){
		
		//Generamos un dialogo para introducir la fecha de planificaci�n y confirmar la acci�n
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_comentariosobra,(ViewGroup) act.findViewById(R.layout.activity_acometidas));
		builder.setView(layout);
		
		final AlertDialog alertDialog = builder.create();
		final EditText tbComentarios =  (EditText) layout.findViewById(R.id.tbComentarios);
		final TextView lblComentarios =  (TextView) layout.findViewById(R.id.lblComentarios);
		
		//Establece el t�tulo del dialogo
		String obra="";
		if (obj.getDireccion().trim().contains("-") == true){
			obra= obj.getCod_obra()+" - "+obj.getDireccion();
		}else{
			obra= obj.getCod_obra()+" - "+obj.getMunicipio()+" - "+obj.getDireccion();
		}
		lblComentarios.setText(obra);
	
		//Establece los controladores de eventos para las acciones de los botenes del dialogo
		Button btnAceptar =  (Button) layout.findViewById(R.id.btnAceptarComentarios);
		Button btnCancelar =  (Button) layout.findViewById(R.id.btnCancelarComentarios);
		
		btnAceptar.setTag(obj);
				
		btnCancelar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		
		});
		
		btnAceptar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Button btn = (Button) v ;  
				ObjetoAcometida acom = (ObjetoAcometida) btn.getTag();	
				String Comentarios = tbComentarios.getText().toString();
								
				if (Comentarios.equals("") == false) { 
					
										
					//Ejecutar la acci�n de planificaci�n
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionComentarios(acom.getCod_obra(), Comentarios);
					
					//Recargar la lista de acometidas
					UtilData ud = new UtilData(context);
					ud.cargarListaAcometidas(act, ordenacion.ESTADO , acom.getCod_obra());
					
				}
				
				alertDialog.dismiss();
			}
		
		});
		
		// Mostramos el alertdialog
		alertDialog.show();
						
	}
	
	
}
