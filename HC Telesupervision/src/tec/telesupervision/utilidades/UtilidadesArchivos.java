package tec.telesupervision.utilidades;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.StringTokenizer;

import android.os.Environment;

/**
 * 
 * @author mcanal
 * @date 25/07/2012
 * @title UtilidadesArchivos
 * @comment Clase con las utilidades generales para el manejo de archivos en
 *          android
 * 
 * 
 */
public class UtilidadesArchivos {

	private static final int READ_BLOCK_SIZE = 100;

	/**
	 * 
	 * @author mcanal
	 * @date 25/07/2012
	 * @title crearArchivoTexto
	 * @comment Crea un archivo de texto en una ubicaci�n determinada
	 * 
	 * @param path
	 *            Ubicaci�n donde creara el archivo
	 * @param texto
	 *            Contenido del archivo
	 * @param codificacion
	 *            Codificaci�n del archivo a crear, si recibe una cadena en
	 *            blanco codifica en UTF-8
	 * @return True si el archivo se cre� correctamente / False en caso
	 *         contrario
	 * @throws IOException
	 * 
	 */
	public boolean crearArchivoTexto(String path, String texto,
			String codificacion) throws IOException {

		// Comprobamos si la codificaci�n esta vac�a para poner una por defecto.
		if (codificacion.equals("")) {
			codificacion = "UTF-8";
		}

		// Creamos un nuevo archivo
		File archivo = new File(path);
		// Creamos un objeto OutputStream el cual representa un stream del
		// archivo que vamos a crear
		OutputStream os = new FileOutputStream(archivo);
		// Creamos un array de bytes con el texto del archivo y establecemos la
		// codificaci�n de salida del archivo
		byte[] data = texto.getBytes(codificacion);
		// Escribimos el archivo
		os.write(data);
		// Liberamos recursos
		os.flush();
		os.close();

		return archivo.exists();

	}

	/**
	 * 
	 * @author mcanal
	 * @date 25/07/2012
	 * @title abrirArchivoTexto
	 * @comment Devuelve el texto de un fichero de texto
	 * 
	 * @param path
	 *            Ubicaci�n del archivo
	 * @return Texto del archivo.
	 * @throws IOException
	 * 
	 */
	public String abrirArchivoTexto(String path) throws IOException {

		String texto = "";
		// Creamos un nuevo archivo
		File archivo = new File(path);
		// Creamos un objeto FileInputStream el cual representa un stream del
		// archivo que vamos a leer
		FileInputStream fis = new FileInputStream(archivo);
		// Creamos un objeto InputStreamReader que nos permitira leer el stream
		// del archivo abierto
		InputStreamReader isr = new InputStreamReader(fis);

		// Se lee el archivo de texto mientras no se llegue al final de �l
		char[] inputBuffer = new char[READ_BLOCK_SIZE];
		int charRead;
		while ((charRead = isr.read(inputBuffer)) > 0) {
			// Se lee por bloques de 100 caracteres
			// ya que se desconoce el tama�o del texto
			// Y se va copiando a una cadena de texto
			String strRead = String.copyValueOf(inputBuffer, 0, charRead);
			texto += strRead;
			inputBuffer = new char[READ_BLOCK_SIZE];
		}
		// Liberamos recursos
		isr.close();
		return texto;
	}

	/**
	 * 
	 * @author mcanal
	 * @date 24/08/2012
	 * @title getStorageDirectories
	 * @comment Funci�n para obtener todos los puntos de montaje disponibles en
	 *          el SmartPhone Para obtener los puntos de montaje que hay
	 *          actualmente en el SmartPhone, se lee el fichero /proc/mounts,
	 *          este es un fichero de texto plano donde se encuentran todos los
	 *          dispositivos que tiene montados actualmente el dispositivo
	 *          SmarPhone.
	 * 
	 * @return Lista con los paths de los dispositivos externos conectados al
	 *         SmartPhone
	 * 
	 */
	public static String[] getStorageDirectories() {
		String[] dirs = null;
		BufferedReader bufReader = null;
		try {

			// Cargar el archivo /proc/mounts en un buffer para su lectura.
			bufReader = new BufferedReader(new FileReader("/proc/mounts"));
			ArrayList<String> list = new ArrayList<String>();
			String line;

			// Lectura del archivo /proc/mounts
			while ((line = bufReader.readLine()) != null) {

				// Comprobar las unidades particionadas en FAT32 o las que se
				// encuentran ubicadas en /mnt
				if (line.contains("vfat") || line.contains("/mnt")) {

					StringTokenizer tokens = new StringTokenizer(line, " ");
					String s = tokens.nextToken();
					s = tokens.nextToken();

					// Excluir los elementos del sistema operativo.
					if (s.equals(Environment.getExternalStorageDirectory()
							.getPath())) {
						list.add(s);
					} else if (line.contains("/dev/block/vold")) {
						if (!line.contains("/mnt/secure")
								&& !line.contains("/mnt/asec")
								&& !line.contains("/mnt/obb")
								&& !line.contains("/dev/mapper")
								&& !line.contains("tmpfs")) {
							list.add(s);
						}
					}
				}
			}

			// Cargar los elementos seleccionados en una array String
			dirs = new String[list.size()];
			for (int i = 0; i < list.size(); i++) {
				dirs[i] = (String) list.get(i);
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {

			// Cerrar el buffer.
			if (bufReader != null) {
				try {
					bufReader.close();
				} catch (IOException e) {
				}
			}

		}
		return dirs;

	}

	/**
	 * 
	 * @author mjulia
	 * @date 25/10/2012
	 * @title eliminarArchivo
	 * @comment Elimina un archivo si existe y es un archivo
	 * 
	 * @param path
	 *            Path del fichero a eliminar
	 * @return true si ha eliminado el archivo, false en caso contrario.
	 * 
	 */
	public boolean eliminarArchivo(String path) {

		boolean eliminado = false;
		File f = new File(path);
		if (f.exists() == true && f.isFile() == true) {
			eliminado = f.delete();
		} else {
			eliminado = true;
		}
		f = null;
		return eliminado;
	}

	/**
	 * 
	 * @author jjulia
	 * @date 25/10/2012
	 * @title renombrarExtensionArchivo
	 * @comment Renombra la extensi�n del archivo para darlo por procesado
	 * 
	 * @param path
	 *            Path del fichero a eliminar
	 * @return true si ha renom,brado el archivo, false en caso contrario.
	 * 
	 */
	public boolean renombrarExtensionArchivo(String path) {

		boolean renombrado = false;
		File f = new File(path);
		File fold = new File(path + ".old");
		if (f.exists() == true && f.isFile() == true) {
			renombrado = f.renameTo(fold);
		} else {
			renombrado = true;
		}
		f = null;
		return renombrado;
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	31/01/2013
	 * @title	crearDirectorio
	 * @comment	Crea el directorio especificado
	 *
	 * @param path URL de directorio a crear
	 * @return	true si el directorio se creo correctamente
	 *
	 */
	public boolean crearDirectorio(String path) {
	
		boolean creado = false;
		
		File f = new File(path);
		if (f.exists() == false) {			
			creado = f.mkdir();			
		}
		
		return creado;
	}
	
	
	 public static String convertStreamToString(InputStream is) throws IOException {
	        Writer writer = new StringWriter();
	        char[] buffer = new char[2048];
	        try {
	            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	            int n;
	            while ((n = reader.read(buffer)) != -1) {
	                writer.write(buffer, 0, n);
	            }
	        } finally {
	            is.close();
	        }
	        String text = writer.toString();
	        return text;
	   }
}
