package tec.telesupervision.utilidades;

import com.steema.teechart.drawing.Color;

/**
 * 
 *
 * @author	jfernandez
 * @date	19/12/2012
 * @title	UtilidadesColor
 * @comment	Clase para tratar el color
 *
 *
 */

public class UtilidadesColor {
	
	/**
	 * 
	 *
	 * @author	jfernandez
	 * @date	19/12/2012
	 * @title	getTeechartColor
	 * @comment	Funcion que a�ade el valor alpha al valor de los tres integers que contiene dicho color.
	 *
	 * @param color		Color.
	 * @param alpha		Valor alpha delcolor.
	 * @return
	 *
	 */
	public Color getTeechartColor(Color color,int alpha){
		
		return Color.fromArgb(alpha,color.getRed(),color.getGreen(),color.getBlue());		
	}
}
