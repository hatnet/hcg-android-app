package tec.telesupervision.utilidades;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;


public class UtilidadesTelefono {
	
	private Context c;
	
	
	

	public UtilidadesTelefono(Context context) {
		super();
		this.c = context;
	}

	/**
	 * 
	 * @author	mjulia
	 * @date	29/01/2013
	 * @title	getPhoneNumber
	 * @comment	Devuleve el n�mero de tel�fono del dispositivo m�vil
	 *
	 * @return	Devuleve el n�mero de tel�fono del dispositivo m�vil
	 *
	 */
	public String getPhoneNumber(){
		
		TelephonyManager mTelephonyManager;
		mTelephonyManager = (TelephonyManager) this.c.getSystemService(Context.TELEPHONY_SERVICE); 
		return mTelephonyManager.getLine1Number();
		
	}
	
	/***
	 * 
	 * @author	jibanez
	 * @date	01/03/2013
	 * @title	getOnline
	 * @comment	Indica si el dispositivo tiene conexi�n a internet.
	 *
	 * @return	Devuelve un valor boolean que indica si el dispositivo esta� conectado a internet.
	 *
	 */
	public boolean getIsOnline() {
		
		boolean online = false;
		
		ConnectivityManager cm = (ConnectivityManager)this.c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		
		if(netInfo!=null && netInfo.isConnected()) {
			online = true;
		}
		
		return online;
		
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	18/06/2013
	 * @title	getIMEI
	 * @comment	Devuleve el n�mero de IMEU dispositivo m�vil
	 *
	 * @return	Devuleve el n�mero de IMEU dispositivo m�vil
	 *
	 */
	public String getIMEI(){
		
		String imei= "";
		TelephonyManager mTelephonyManager;

		mTelephonyManager = (TelephonyManager) this.c.getSystemService(Context.TELEPHONY_SERVICE); 
		if (mTelephonyManager.getDeviceId()==null) {
			imei =  Secure.getString(this.c.getContentResolver(),  Secure.ANDROID_ID); 
		}else {
			imei = mTelephonyManager.getDeviceId();
		}
		
		return imei;
		
	}
	
}
