package tec.telesupervision.Objects;


public class ObjetoAcometida {
	
	private String Cod_obra;
	private String Direccion;
	private String Municipio;
	private String FechaPlanificacion;
	private String FechaInicio;
	private String FechaPeS;
	private String FechaFinal;
	private String FechaAcepNomb;
	private String Comentarios;
	
	private Long TelesupIDConcertacion;
	private String TelesupFechaPlanificacion;
	private String TelesupGestorObra;
	private String TelesupResultado;
	private String TelesupCausas;
	
	private Boolean seleccionada;
	private Boolean carpetasOonair;

	
	
	public ObjetoAcometida() {
		super();
	}



	public ObjetoAcometida(String cod_obra, String direccion, String municipio,
			String fechaPlanificacion, String fechaInicio, String fechaPeS,
			String fechaFinal, String fechaAcepNomb, String comentarios, Long telesupIDConcertacion,
			String telesupFechaPlanificacion, String telesupGestorObra,
			String telesupResultado, String telesupCausas,
			Boolean seleccionada, Boolean carpetasOonair) {
		super();
		Cod_obra = cod_obra;
		Direccion = direccion;
		Municipio = municipio;
		FechaPlanificacion = fechaPlanificacion;
		FechaInicio = fechaInicio;
		FechaPeS = fechaPeS;
		FechaFinal = fechaFinal;
		FechaAcepNomb = fechaAcepNomb;
		Comentarios = comentarios;
		TelesupIDConcertacion = telesupIDConcertacion;
		TelesupFechaPlanificacion = telesupFechaPlanificacion;
		TelesupGestorObra = telesupGestorObra;
		TelesupResultado = telesupResultado;
		TelesupCausas = telesupCausas;
		this.seleccionada = seleccionada;
		this.carpetasOonair = carpetasOonair;
	}



	public final String getCod_obra() {
		return Cod_obra;
	}



	public final void setCod_obra(String cod_obra) {
		Cod_obra = cod_obra;
	}



	public final String getDireccion() {
		return Direccion;
	}



	public final void setDireccion(String direccion) {
		Direccion = direccion;
	}



	public final String getMunicipio() {
		return Municipio;
	}



	public final void setMunicipio(String municipio) {
		Municipio = municipio;
	}



	public final String getFechaPlanificacion() {
		return FechaPlanificacion;
	}



	public final void setFechaPlanificacion(String fechaPlanificacion) {
		FechaPlanificacion = fechaPlanificacion;
	}



	public final String getFechaInicio() {
		return FechaInicio;
	}



	public final void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}



	public final String getFechaPeS() {
		return FechaPeS;
	}



	public final void setFechaPeS(String fechaPeS) {
		FechaPeS = fechaPeS;
	}



	public final String getFechaFinal() {
		return FechaFinal;
	}



	public final void setFechaFinal(String fechaFinal) {
		FechaFinal = fechaFinal;
	}



	public String getFechaAcepNomb() {
		return FechaAcepNomb;
	}



	public void setFechaAcepNomb(String fechaAcepNomb) {
		FechaAcepNomb = fechaAcepNomb;
	}



	public final Long getTelesupIDConcertacion() {
		return TelesupIDConcertacion;
	}



	public final void setTelesupIDConcertacion(Long telesupIDConcertacion) {
		TelesupIDConcertacion = telesupIDConcertacion;
	}



	public final String getTelesupFechaPlanificacion() {
		return TelesupFechaPlanificacion;
	}



	public final void setTelesupFechaPlanificacion(String telesupFechaPlanificacion) {
		TelesupFechaPlanificacion = telesupFechaPlanificacion;
	}



	public final String getTelesupGestorObra() {
		return TelesupGestorObra;
	}



	public final void setTelesupGestorObra(String telesupGestorObra) {
		TelesupGestorObra = telesupGestorObra;
	}



	public final String getTelesupResultado() {
		return TelesupResultado;
	}



	public final void setTelesupResultado(String telesupResultado) {
		TelesupResultado = telesupResultado;
	}



	public final String getTelesupCausas() {
		return TelesupCausas;
	}



	public final void setTelesupCausas(String telesupCausas) {
		TelesupCausas = telesupCausas;
	}



	public final Boolean getSeleccionada() {
		return seleccionada;
	}



	public final void setSeleccionada(Boolean seleccionada) {
		this.seleccionada = seleccionada;
	}



	public final Boolean getCarpetasOonair() {
		return carpetasOonair;
	}



	public final void setCarpetasOonair(Boolean carpetasOonair) {
		this.carpetasOonair = carpetasOonair;
	}



	public final String getComentarios() {
		return Comentarios;
	}



	public final void setComentarios(String comentarios) {
		Comentarios = comentarios;
	}

	
	
	
	
}
