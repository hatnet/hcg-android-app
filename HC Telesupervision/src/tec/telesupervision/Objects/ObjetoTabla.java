package tec.telesupervision.Objects;

import java.math.BigDecimal;


public class ObjetoTabla {
	
	private String idElemento;
	private String descripcion;
	private String alfa1;
	private String alfa2;
	private BigDecimal Num1;
	private BigDecimal Num2;
	private BigDecimal valorMin;
	private BigDecimal valorMax;
	
	
	public ObjetoTabla() {
		super();
	}




	public ObjetoTabla(String idElemento, String descripcion, String alfa1, String alfa2,
			BigDecimal num1, BigDecimal num2, BigDecimal valorMin, BigDecimal valorMax) {
		super();
		this.idElemento = idElemento;
		this.descripcion = descripcion;
		this.alfa1 = alfa1;
		this.alfa2 = alfa2;
		Num1 = num1;
		Num2 = num2;
		this.valorMin = valorMin;
		this.valorMax = valorMax;
	}
	
	
	
	
	public String getIdElemento() {
		return idElemento;
	}
	public void setIdElemento(String idElemento) {
		this.idElemento = idElemento;
	}
	public final String getDescripcion() {
		return descripcion;
	}
	public final void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getAlfa1() {
		return alfa1;
	}
	public void setAlfa1(String alfa1) {
		this.alfa1 = alfa1;
	}
	public String getAlfa2() {
		return alfa2;
	}
	public void setAlfa2(String alfa2) {
		this.alfa2 = alfa2;
	}
	public BigDecimal getNum1() {
		return Num1;
	}
	public void setNum1(BigDecimal num1) {
		Num1 = num1;
	}
	public BigDecimal getNum2() {
		return Num2;
	}
	public void setNum2(BigDecimal num2) {
		Num2 = num2;
	}
	public BigDecimal getValorMin() {
		return valorMin;
	}
	public void setValorMin(BigDecimal valorMin) {
		this.valorMin = valorMin;
	}
	public BigDecimal getValorMax() {
		return valorMax;
	}
	public void setValorMax(BigDecimal valorMax) {
		this.valorMax = valorMax;
	}
	
	
	
}
