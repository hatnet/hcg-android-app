package tec.telesupervision.Objects;

/**
 * 
 * @author	mjulia
 * @date	27/09/2012
 * @title   Clase que representa un elemento ListItem	
 * @comment	Representación adaptada de un objeto ListItem para controles tipo ListControl
 *
 *
 */
public class ClaseListItem {
	
	private String id;
	private String descripcion;
	
	static final String sep = " .- ";

	public ClaseListItem(String idItem, String descItem){
		
		this.setId(idItem);
		this.setDescripcion(descItem);
	}

	public String getId() {
		return id;
	}

	public void setId(String value) {
		this.id = value;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getDescripcionCompleta() {
		return this.getId() + sep + this.getDescripcion();
	}
	
	@Override
	public String toString() {
		return this.getDescripcion();
	}

}