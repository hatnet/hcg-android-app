package tec.telesupervision.Objects.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONAsignacion {


	@JsonProperty("cod") private String codObra;
	@JsonProperty("dir") private String Direccion;
	@JsonProperty("mun") private String Municipio;
	@JsonProperty("car") private String CarpetasOonair;
	@JsonProperty("fplan") private String FechaPlanificacion;
	@JsonProperty("fini") private String FechaInicio;
	@JsonProperty("fpes") private String FechaPeS;
	@JsonProperty("ffin") private String FechaFinal;
	@JsonProperty("facep") private String FechaAceptacionNombramiento;
	@JsonProperty("goTelesup") private String GestorTelesup;
	@JsonProperty("comen") private String Comentarios;

	public JSONAsignacion() {
		super();
	}



	public final String getCodObra() {
		return codObra;
	}



	public final void setCodObra(String codObra) {
		this.codObra = codObra;
	}



	public final String getDireccion() {
		return Direccion;
	}



	public final void setDireccion(String direccion) {
		Direccion = direccion;
	}



	public final String getMunicipio() {
		return Municipio;
	}



	public final void setMunicipio(String municipio) {
		Municipio = municipio;
	}



	public final String getCarpetasOonair() {
		return CarpetasOonair;
	}



	public final void setCarpetasOonair(String carpetasOonair) {
		CarpetasOonair = carpetasOonair;
	}



	public final String getFechaPlanificacion() {
		return FechaPlanificacion;
	}



	public final void setFechaPlanificacion(String fechaPlanificacion) {
		FechaPlanificacion = fechaPlanificacion;
	}



	public final String getFechaInicio() {
		return FechaInicio;
	}



	public final void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}



	public final String getFechaPeS() {
		return FechaPeS;
	}



	public final void setFechaPeS(String fechaPeS) {
		FechaPeS = fechaPeS;
	}



	public final String getFechaFinal() {
		return FechaFinal;
	}



	public final void setFechaFinal(String fechaFinal) {
		FechaFinal = fechaFinal;
	}



	public String getFechaAceptacionNombramiento() {
		return FechaAceptacionNombramiento;
	}



	public void setFechaAceptacionNombramiento(String fechaAceptacionNombramiento) {
		FechaAceptacionNombramiento = fechaAceptacionNombramiento;
	}



	public final String getGestorTelesup() {
		return GestorTelesup;
	}



	public final void setGestorTelesup(String gestorTelesup) {
		GestorTelesup = gestorTelesup;
	}



	public final String getComentarios() {
		return Comentarios;
	}



	public final void setComentarios(String comentarios) {
		Comentarios = comentarios;
	}


	
}
