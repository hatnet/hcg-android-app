package tec.telesupervision.Objects.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SincronizacionElemento {

	@JsonProperty("elmO") private JSONObra elementoObra;
    @JsonProperty("elmC") private JSONConcertacion elementoConcertacion;
    @JsonProperty("elmA") private JSONAsignacion elementoAsignacion;
    @JsonProperty("elmM") private JSONMensaje elementoMensaje;
     
    public SincronizacionElemento() {
		super();
	}

	public final JSONObra getElementoObra() {
		return elementoObra;
	}

	public final void setElementoObra(JSONObra elementoObra) {
		this.elementoObra = elementoObra;
	}

	public final JSONConcertacion getElementoConcertacion() {
		return elementoConcertacion;
	}

	public final void setElementoConcertacion(JSONConcertacion elementoConcertacion) {
		this.elementoConcertacion = elementoConcertacion;
	}

	public final JSONAsignacion getElementoAsignacion() {
		return elementoAsignacion;
	}

	public final void setElementoAsignacion(JSONAsignacion elementoAsignacion) {
		this.elementoAsignacion = elementoAsignacion;
	}

	public final JSONMensaje getElementoMensaje() {
		return elementoMensaje;
	}

	public final void setElementoMensaje(JSONMensaje elementoMensaje) {
		this.elementoMensaje = elementoMensaje;
	}
    
	
	
}
