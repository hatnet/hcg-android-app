package tec.telesupervision.Objects.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONConcertacion {

	@JsonProperty("ID") private Long ID;
	@JsonProperty("cod") private String codObra;
	@JsonProperty("iPlanin") private String InicioPlanificacion;
	@JsonProperty("resTelesup") private String TelesupervisionResultado;
	@JsonProperty("cauTelesup") private String TelesupervisionCausas;
	@JsonProperty("goTelesup") private String TelesupervisionGestorObra;
	

	public JSONConcertacion() {
		super();
	}


	public final Long getID() {
		return ID;
	}


	public final void setID(Long iD) {
		ID = iD;
	}


	public final String getCodObra() {
		return codObra;
	}


	public final void setCodObra(String codObra) {
		this.codObra = codObra;
	}


	public final String getInicioPlanificacion() {
		return InicioPlanificacion;
	}


	public final void setInicioPlanificacion(String inicioPlanificacion) {
		InicioPlanificacion = inicioPlanificacion;
	}


	public final String getTelesupervisionResultado() {
		return TelesupervisionResultado;
	}


	public final void setTelesupervisionResultado(String telesupervisionResultado) {
		TelesupervisionResultado = telesupervisionResultado;
	}


	public final String getTelesupervisionCausas() {
		return TelesupervisionCausas;
	}


	public final void setTelesupervisionCausas(String telesupervisionCausas) {
		TelesupervisionCausas = telesupervisionCausas;
	}


	public final String getTelesupervisionGestorObra() {
		return TelesupervisionGestorObra;
	}


	public final void setTelesupervisionGestorObra(String telesupervisionGestorObra) {
		TelesupervisionGestorObra = telesupervisionGestorObra;
	}


	
}
