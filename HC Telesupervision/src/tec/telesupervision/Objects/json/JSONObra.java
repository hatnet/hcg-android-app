package tec.telesupervision.Objects.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONObra {


	@JsonProperty("cod") private String codObra;
	@JsonProperty("dir") private String Direccion;
	@JsonProperty("mun") private String Municipio;
	@JsonProperty("car") private String CarpetasOonair;
	

	public JSONObra() {
		super();
	}


	public final String getCodObra() {
		return codObra;
	}


	public final void setCodObra(String codObra) {
		this.codObra = codObra;
	}


	public final String getDireccion() {
		return Direccion;
	}


	public final void setDireccion(String direccion) {
		Direccion = direccion;
	}


	public final String getMunicipio() {
		return Municipio;
	}


	public final void setMunicipio(String municipio) {
		Municipio = municipio;
	}


	public final String getCarpetasOonair() {
		return CarpetasOonair;
	}


	public final void setCarpetasOonair(String carpetasOonair) {
		CarpetasOonair = carpetasOonair;
	}


	
	
}
