package tec.telesupervision.Objects.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONMensaje {
	
	@JsonProperty("msg") private String Mensaje;
	

	public JSONMensaje() {
		super();
	}


	public final String getMensaje() {
		return Mensaje;
	}


	public final void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}



}
