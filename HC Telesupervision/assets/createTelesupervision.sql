
DROP TABLE IF EXISTS configuracionGeneral;
DROP TABLE IF EXISTS concertaciones;
DROP TABLE IF EXISTS acometidas;
DROP TABLE IF EXISTS asignaciones;

CREATE TABLE configuracionGeneral (clave VARCHAR( 250 ) PRIMARY KEY NOT NULL UNIQUE, 
								   valor VARCHAR( 250 ) NULL
);

CREATE TABLE concertaciones (ID       		     INTEGER PRIMARY KEY NOT NULL UNIQUE,
    						 Cod_Obra  			 VARCHAR( 50 ),
    						 InicioPlanificacion DATETIME,
    						 Resultado           VARCHAR( 50 ),
    						 CausasRechazo       VARCHAR( 500 ),
    					     GestorObra          VARCHAR( 500 ) 
);

CREATE TABLE acometidas ( ID             INTEGER         PRIMARY KEY NOT NULL UNIQUE,
    					  Cod_Obra       VARCHAR( 50 ),
    					  Direccion      VARCHAR( 250 ),
    					  Municipio      VARCHAR( 250 ),
    					  CarpetasOonair BOOLEAN 
);

CREATE TABLE asignaciones ( ID             INTEGER         PRIMARY KEY NOT NULL UNIQUE,
    						Cod_Obra       VARCHAR( 50 ),
    						Direccion      VARCHAR( 250 ),
   			 				Municipio      VARCHAR( 250 ),
    						FechaPlan      DATETIME,
    						FechaIni       DATETIME,
    						FechaPeS       DATETIME,
    						FechaFin       DATETIME,
    						FechaAcepNomb  DATETIME,
    						CarpetasOonair BOOLEAN,
    						GestorObra     VARCHAR,
    						Comentarios    VARCHAR 
);

INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('userNombreUsuario', '');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('userPasswordUsuario', '');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('Autorizacion_Internet', 'true');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('minutosEjecucionServicioSincronizacion', 180);
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_DescargaDatosEnDispovitivoMovil', 'DescargaDatosEnDispositivoMovil');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_URL_ServicioSincronizacionOld', 'http://217.76.145.48:85/ServiciosWeb/sincronizacion.asmx');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_URL_ServicioSincronizacionProd', 'http://82.223.244.59:85//ServiciosWeb/sincronizacion.asmx');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_URL_ServicioSincronizacionServer', 'http://www.tecnicsoft.com/telesupervision3/ServiciosWeb/sincronizacion.asmx');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_URL_ServicioSincronizacion', 'http://10.0.3.2:2926/ServiciosWeb/sincronizacion.asmx');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('Idioma', 'es_ES');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_ValidacionCredencialesUsuario', 'ValidacionCredencialesUsuario');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('diasPlanificacionAcometidas', 2);
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('resultadoRechazada', 'Rechazada');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('resultadoAceptada', 'Aceptada');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionPlanificacion', 'PlanificacionAcometida');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionInicio', 'InicioAcometida');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionPeS', 'PeSAcometida');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionFinal', 'FinalAcometida');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('resultadoRechazada24h', 'Rechazada +24h PeS');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('resultadoRechazada48h', 'Rechazada +48h PeS');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionComentarios', 'ComentariosAcometida');


