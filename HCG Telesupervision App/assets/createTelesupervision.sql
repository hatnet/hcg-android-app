
DROP TABLE IF EXISTS configuracionGeneral;
DROP TABLE IF EXISTS planificacion;
DROP TABLE IF EXISTS acometidas;
DROP TABLE IF EXISTS obras50m;
DROP TABLE IF EXISTS recursosPreventivos;

CREATE TABLE configuracionGeneral (clave VARCHAR( 250 ) PRIMARY KEY NOT NULL UNIQUE, 
								   valor VARCHAR( 250 ) NULL
);

CREATE TABLE planificacion ( ID             INTEGER         PRIMARY KEY NOT NULL UNIQUE,
    					     Cod_Obra       VARCHAR( 50 ),
    					     Tipo_Obra       VARCHAR( 50 ),
    					     Direccion      VARCHAR( 250 ),
    					     Municipio      VARCHAR( 250 ),
    					     CarpetasOonair BOOLEAN 
);

CREATE TABLE acometidas    ( ID             INTEGER         PRIMARY KEY NOT NULL UNIQUE,
    						 Cod_Obra       VARCHAR( 50 ),
    						 Situacion      VARCHAR( 50 ),
    						 SituacionDes   VARCHAR( 250 ),
    						 Direccion      VARCHAR( 250 ),
   			 				 Municipio      VARCHAR( 250 ),
   			 				 CarpetasOonair BOOLEAN,
   			 				 Comentarios    VARCHAR( 250 ), 
   			 				 FechaAcepNomb  DATETIME,
    						 FechaPlan      DATETIME,
    						 FechaIni       DATETIME,
    						 FechaPeS       DATETIME,
    						 FechaFin       DATETIME,
    						 TSGestorObra   VARCHAR,
    						 TSTelefonoGO   VARCHAR,
    						 TSInicioPlan   DATETIME,
    						 TSResultadoFec DATETIME,
    						 TSResultado    INTEGER,
    						 HistorialObra  VARCHAR
);

CREATE TABLE obras50m      ( ID                  INTEGER         PRIMARY KEY NOT NULL UNIQUE,
    						 Cod_Obra            VARCHAR( 50 ),
    						 Perfil              VARCHAR( 50 ),
    						 Situacion           VARCHAR( 50 ),
    						 SituacionDes        VARCHAR( 250 ),
    						 Direccion           VARCHAR( 250 ),
   			 				 Municipio           VARCHAR( 250 ),
   			 				 CarpetasOonair      BOOLEAN,
   			 				 Comentarios         VARCHAR( 250 ),
   			 				 Trazado             VARCHAR( 500 ),  
   			 				 TrazadoMod          VARCHAR( 500 ),
   			 				 FechaAcepNomb       DATETIME,
    						 FechaPlan           DATETIME,
    						 FechaIni            DATETIME,
    						 FechaTraSol         DATETIME,
    						 FechaTraCon         DATETIME,
    						 FechaTraSolMod      DATETIME,
    						 FechaTraConMod      DATETIME,
    						 FechaSolicitudPeS   DATETIME,
    						 FechaAcepConten     DATETIME,
    						 FechaFinalPrueba    DATETIME,
    						 FechaAceptadaPeG    DATETIME,
    						 FechaPeS            DATETIME,
    						 FechaFin            DATETIME,
    						 TSGestorObra        VARCHAR,
    						 TSTelefonoGO        VARCHAR,
    						 TSInicioPlan        DATETIME,
    						 TSResultadoFec      DATETIME,
    						 TSResultado         INTEGER,
    						 HistorialObra       VARCHAR 
);

CREATE TABLE recursosPreventivos      ( Codigo        VARCHAR         PRIMARY KEY NOT NULL UNIQUE,
    						            Nombre        VARCHAR
);  						 

INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('Idioma', 'es_ES');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SENDER_ID', '241440987291');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('userNombreUsuario', '');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('userPasswordUsuario', '');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('userIDClienteGCM', '');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('userAppVersionClienteGCM', '0');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('userFechaCaducidadClienteGCM', '-1');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('Autorizacion_Internet', 'true');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('minutosEjecucionServicioSincronizacion', 600);
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('diasPlanificacionAcometidas', 2);
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('telefonoCTR', '902214263');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_URL_ServicioSincronizacion', 'http://82.223.244.59:82/ServiciosWeb/sincronizacion.asmx');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_URL_ServicioSincronizacionNEX', 'http://212.92.57.87:86/serviciosweb/sincronizacion.asmx');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_URL_ServicioSincronizacionSDKGeny', 'http://10.0.3.2:6749/ServiciosWeb/sincronizacion.asmx');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_URL_ServicioSincronizacionSDKAndroid', 'http://10.0.2.2:6749/ServiciosWeb/sincronizacion.asmx');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_DescargaDatosEnDispovitivoMovil', 'DescargaDatosEnDispositivoMovil');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_DescargaUnaObraEnDispovitivoMovil', 'DescargaDatosEnDispositivoMovilUnaObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_ValidacionCredencialesUsuario', 'ValidacionCredencialesUsuario');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionComentarios', 'ComentariosObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionImprevistosProyecto', 'ImprevistosProyecto');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionPlanificacion', 'PlanificacionObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionInicio', 'InicioObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionTrazado', 'TrazadoObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionModificacionTrazado', 'ModificacionTrazadoObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionSolicitudPeS', 'SolicitudPeSObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionFinalPrueba', 'FinalPruebaObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionCambioRecPreventivo', 'CambioRecPreventivoObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionPeS', 'PeSObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionFinal', 'FinalObra');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_DescargaRecursosPreventivos', 'DescargaRecursosPreventivos');
INSERT INTO [configuracionGeneral] ([clave], [valor]) VALUES ('SW_Metodo_AccionCambioPlanificacion', 'CambioPlanificacionObra');
