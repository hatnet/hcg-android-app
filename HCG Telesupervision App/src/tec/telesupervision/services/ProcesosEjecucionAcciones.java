package tec.telesupervision.services;

import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;

import tec.telesupervision.Objects.json.DescargaDatosWeb;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.ver2.R;
import android.content.Context;
import android.util.Log;

public class ProcesosEjecucionAcciones {
	
	private Context context;
	private static String erroresProceso;
	
	public ProcesosEjecucionAcciones(Context context) {
		super();
		this.context = context;
		erroresProceso="";
	}
	
	public static String erroresEnAccion(){
		return erroresProceso;
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionDescargarObra
	 * @comment	Procedimiento para la acci�n de descargar una obra
	 *
	 * @param codObra			C�digo de la obra a planificar
	 *
	 */
	public void ejecucionAccionDescargarObra(String codObra) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION DESCARGAR OBRA - Inicio, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION DESCARGAR OBRA - Autorizado, acceder al servicio web.");
				ejecucionAccionDescargarObra_Procedimiento(userName, password, codObra, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionDescargarObra_Procedimiento(String user, String password, String codObra,  ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.recepcionarDatosWebService_UnaObra(user, password, codObra);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION DESCARGAR OBRA. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION DESCARGAR OBRA. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
						 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION DESCARGAR OBRA. Final acci�n.");
			
	}
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionPlanificacion
	 * @comment	Procedimiento para la acci�n de Planificaci�n de la obra
	 *
	 * @param codObra			C�digo de la obra a planificar
	 * @param fPlanificacion	Fecha planificaci�n escogida
	 *
	 */
	public void ejecucionAccionPlanificacion(String codObra, String fPlanificacion) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PLANIFICACION - Iniciar planificaci�n, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION PLANIFICACION - Autorizado, acceder al servicio web.");
				ejecucionAccionPlanificacion_Procedimiento(userName, password, codObra, fPlanificacion, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionPlanificacion_Procedimiento(String user, String password, final String codObra, String fPlanificacion, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionPLanificacion(codObra, fPlanificacion, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PLANIFICACION. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PLANIFICACION. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION PLANIFICACION. Final acci�n.");
			
	}
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionCambioPlanificacion
	 * @comment	Procedimiento para la acci�n de Cambio de Planificaci�n de la obra
	 *
	 * @param codObra			C�digo de la obra a planificar
	 * @param fPlanificacion	Fecha planificaci�n escogida
	 *
	 */
	public void ejecucionAccionCambioPlanificacion(String codObra, String fPlanificacion) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION CAMBIO PLANIFICACION - Iniciar cambio planificaci�n, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION CAMBIO PLANIFICACION - Autorizado, acceder al servicio web.");
				ejecucionAccionCambioPlanificacion_Procedimiento(userName, password, codObra, fPlanificacion, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionCambioPlanificacion_Procedimiento(String user, String password, final String codObra, String fPlanificacion, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionCambioPlanificacion(codObra, fPlanificacion, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION CAMBIO PLANIFICACION. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION CAMBIO PLANIFICACION. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION CAMBIO PLANIFICACION. Final acci�n.");
			
	}
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionTrazado
	 * @comment	Procedimiento para la acci�n de trazado de una obra
	 *
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionTrazado(String codObra) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION TRAZADO - Iniciar Trazado, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION TRAZADO - Autorizado, acceder al servicio web.");
				ejecucionAccionTrazado_Procedimiento(userName, password, codObra, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionTrazado_Procedimiento(String user, String password, final String codObra, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionTrazado(codObra, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION TRAZADO. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION TRAZADO. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION TRAZADO. Final acci�n.");
			
	}
	
	
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionModificacionTrazado
	 * @comment	Procedimiento para la acci�n de modificaci�n del trazado de una obra
	 *
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionModificacionTrazado(String codObra) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION MODIFICACI�N TRAZADO - Iniciar Modificaci�n Trazado, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION MODIFICACI�N TRAZADO - Autorizado, acceder al servicio web.");
				ejecucionAccionModificacionTrazado_Procedimiento(userName, password, codObra, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionModificacionTrazado_Procedimiento(String user, String password, final String codObra, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionModificacionTrazado(codObra, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION MODIFICACI�N TRAZADO. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION MODIFICACI�N TRAZADO. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION MODIFICACI�N TRAZADO. Final acci�n.");
			
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionInicio
	 * @comment	Procedimiento para la acci�n de inicio de una obra
	 *
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionInicio(String codObra) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION INICIO - Iniciar Inicio, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION INICIO - Autorizado, acceder al servicio web.");
				ejecucionAccionInicio_Procedimiento(userName, password, codObra, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionInicio_Procedimiento(String user, String password, final String codObra, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionInicio(codObra, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION INICIO. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION INICIO. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION INICIO. Final acci�n.");
			
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionSolicitudPeS
	 * @comment	Procedimiento para la acci�n de la Solicitud de PeS de una obra
	 *
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionSolicitudPeS(final String codObra) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION SOLICITUD PES - Iniciar solicitud PeS, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION SOLICITUD PES - Autorizado, acceder al servicio web.");
				ejecucionAccionSolicitudPeS_Procedimiento(userName, password, codObra, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
			
			//Cargar en segundo plano la lista de recursos preventivos
			new Thread(new Runnable() {
			    public void run() {
			    	ProcesoSincronizacion sin = new ProcesoSincronizacion(context);
			    	sin.SincronizacionListaRecursosPreventivos(codObra);
			    }
			}).start();
					
		}
	}
	
	private void ejecucionAccionSolicitudPeS_Procedimiento(String user, String password, final String codObra, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionSolicitudPeS(codObra, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION SOLICITUD PES. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION SOLICITUD PES. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION SOLICITUD PES. Final acci�n.");
			
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionFinalPrueba
	 * @comment	Procedimiento para la acci�n de indicar el final de la prueba de una obra
	 *
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionFinalPrueba(String codObra, String RecursoPreventivo) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL PRUEBA - Iniciar final prueba, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL PRUEBA - Autorizado, acceder al servicio web.");
				ejecucionAccionFinalPrueba_Procedimiento(userName, password, codObra, RecursoPreventivo, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionFinalPrueba_Procedimiento(String user, String password, final String codObra, String RecursoPreventivo, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionFinalPrueba(codObra, RecursoPreventivo, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL PRUEBA. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL PRUEBA. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL PRUEBA. Final acci�n.");
			
	}
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/12/2014
	 * @title	ejecucionAccionCambioRecPreventivo
	 * @comment	Procedimiento para la acci�n de cambiar el recurso 
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionCambioRecPreventivo(String codObra, String RecursoPreventivo, String Mot_RecursoPreventivo) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION CAMBIO RECURSO PREVENTIVO - Iniciar cambio, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION CAMBIO RECURSO PREVENTIVO - Autorizado, acceder al servicio web.");
				ejecucionAccionCambioRecPreventivo_Procedimiento(userName, password, codObra, RecursoPreventivo, Mot_RecursoPreventivo, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionCambioRecPreventivo_Procedimiento(String user, String password, final String codObra, String RecursoPreventivo, String Mot_RecursoPreventivo, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionCambioRecPreventivo(codObra, RecursoPreventivo, Mot_RecursoPreventivo, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION CAMBIO RECURSO PREVENTIVO. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION CAMBIO RECURSO PREVENTIVO. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION CAMBIO RECURSO PREVENTIVO. Final acci�n.");
			
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionPeS
	 * @comment	Procedimiento para la acci�n de PeS de una obra
	 *
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionPeS(String codObra) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PES - Iniciar PeS, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION PES - Autorizado, acceder al servicio web.");
				ejecucionAccionPeS_Procedimiento(userName, password, codObra, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionPeS_Procedimiento(String user, String password, final String codObra, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionPeS(codObra, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PES. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION PES. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION PES. Final acci�n.");
			
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionFinal
	 * @comment	Procedimiento para la acci�n de Final de una obra
	 *
	 * @param codObra	C�digo de la obra seleccionada
	 *
	 */
	public void ejecucionAccionFinal(String codObra) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL - Iniciar Final, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL - Autorizado, acceder al servicio web.");
				ejecucionAccionFinal_Procedimiento(userName, password, codObra, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionFinal_Procedimiento(String user, String password, final String codObra, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionFinal(codObra, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			 
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ACCION FINAL. Final acci�n.");
			
	}
		
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionImprevistosProyectos
	 * @comment	Procedimiento para la entrada de comentarios de imprevistos de proyecto en la obra
	 *
	 * @param codObra			C�digo de la obra a planificar
	 * @param comentarios   	Comentarios a la acometida
	 *
	 */
	public void ejecucionAccionImprevistosProyectos(String codObra, String comentarios) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ENTRADA IMPREVISTOS PROYECTO - Iniciar entrada comentarios, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ENTRADA IMPREVISTOS PROYECTO - Autorizado, acceder al servicio web.");
				ejecucionAccionImprevistosProyectos_Procedimiento(userName, password, codObra, comentarios, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionImprevistosProyectos_Procedimiento(String user, String password, final String codObra, String comentarios, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionImprevistosProyecto(codObra, comentarios, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ENTRADA IMPREVISTOS PROYECTO. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ENTRADA IMPREVISTOS PROYECTO. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR IMPREVISTOS PROYECTO. Final acci�n.");
			
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	ejecucionAccionComentarios
	 * @comment	Procedimiento para la entrada de comentarios en la obra
	 *
	 * @param codObra			C�digo de la obra a planificar
	 * @param comentarios   	Comentarios a la acometida
	 *
	 */
	public void ejecucionAccionComentarios(String codObra, String comentarios) {
		
		try{
			
			Log.i(this.getClass().getName(), "EJECUTAR ENTRADA COMENTARIOS - Iniciar entrada comentarios, comprobar autorizaci�n.");
			
			UtilData ud = new UtilData(context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
										
			ConexionesSincronizacion us = new ConexionesSincronizacion(context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
					
			if(autorizado == true) {
							
				Log.i(this.getClass().getName(), "EJECUTAR ENTRADA COMENTARIOS - Autorizado, acceder al servicio web.");
				ejecucionAccionComentarios_Procedimiento(userName, password, codObra, comentarios, us);
												
			}else{
				erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
			}
								
		} catch (Exception e) {	
					
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
					
			erroresProceso = context.getResources().getString(R.string.error_SIncronizacion_Error);
					
		} finally{
													
			//Finalizar el proceso
			System.gc();
					
		}
	}
	
	private void ejecucionAccionComentarios_Procedimiento(String user, String password, final String codObra, String comentarios, ConexionesSincronizacion us) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para la acci�n
    	String datosJSON = us.ejecutarAccionComentarios(codObra, comentarios, user, password);
		if (datosJSON == null){
			//(no se ha obtenido respuesta del servidor)
			Log.i(this.getClass().getName(), "EJECUTAR ENTRADA COMENTARIOS. No se ha recibido respuesta del servidor.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR ENTRADA COMENTARIOS. Documento JSON con la respuesta del servidor: " + datosJSON);
									
			//Proceso de actualizaci�n en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class);
			
			ProcesoSincronizacion sin = new ProcesoSincronizacion(this.context);
			sin.SincronizacionActualizarObra(codObra, descargaJSON);
			sin.finalizarDinamico();
			
			new Thread(new Runnable() {
			    public void run() {
			    	ejecucionAccionDescargarObra(codObra);
			    }
			}).start();
			 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR ENTRADA COMENTARIOS. Final acci�n.");
			
	}
	

}
