package tec.telesupervision.services;


import java.util.Date;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import tec.telesupervision.data.UtilData;
import tec.telesupervision.utilidades.CryptographicInteroperability;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class ConexionesSincronizacion {
		
	private String puerto = "6748"; //DEBUG eclipse-visual studio (Android-SW). Este es el puerto del servidor de desarrollo ASP.NET
		
	private static final String NAMESPACE = "com.tecnicsoft.telesupervision.v2";		
	private Context context;
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	13/06/2013
	 * @title	UtilidadesSincronizacion
	 * @comment	Constructores de la clase
	 *	
	 *
	 */
	public ConexionesSincronizacion() {
		super();
	}
	
	public ConexionesSincronizacion(Context _context) {
		super();
		this.context = _context;
	}


	/**
	 * 
	 * @author	mjulia
	 * @date	25/01/2013
	 * @title	networkAvailable
	 * @comment	Comprueba si el dispositivo m�vil tiene conexi�n a internet
	 *
	 * @return	true si el dispositivo m�vil esta conectado a internet false en caso contrario
	 *
	 */
	public boolean networkAvailable() {		
    	    	 
    	ConnectivityManager connectMgr = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
    	
    	if (connectMgr != null) {
    		
    		NetworkInfo[] netInfo = connectMgr.getAllNetworkInfo();
    		if (netInfo != null) {
    			for (NetworkInfo net : netInfo) {
    				if (net.getState() == NetworkInfo.State.CONNECTED) {
    					return true;
    				}
    			}
    		}    		
    	} 
    	else {
    		Log.d("NETWORK", "No network available");
    	}
    	return false;
    }
	
	
	/**
	 * 
	 * @author		mjulia
	 * @date		27/02/2013
	 * @title		conexionMetodoServicioWeb
	 * @comment		Conexi�n a un m�todo de un servicio web y obtiene una respuesta
	 *
	 * @param URL			url del servicio web y del m�todo
	 * @param SOAP_ACTION	acci�n soap a realizar
	 * @param request		objeto soap con los par�metros necesarios para el m�todo web
	 * @param timeout		tiempo de espera para emcontrar respuesta del servicio web
	 * @return				resultado devuelto por el servidor en un string
	 *
	 */
	private String conexionMetodoServicioWeb(String URL, String SOAP_ACTION, SoapObject request, int timeout) {
				
		String res = null;
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);        	 
		envelope.dotNet = true;        	 
		envelope.setOutputSoapObject(request);    	    	  
			
		HttpTransportSE transporte = new HttpTransportSE(URL, timeout);
		try
		{
		    transporte.call(SOAP_ACTION, envelope); 
		 
		    if(envelope.getResponse() != null) {
			    //Se procesa el resultado devuelto
			    //...
			    
		    	//SoapPrimitive resultado =(SoapPrimitive)envelope.getResponse();		    	
		    	SoapObject resultado = (SoapObject) envelope.bodyIn;
		    	
			    if (resultado != null) {
			    		
			    	res = resultado.getProperty(0).toString();
			    	if (res.trim().equals("anyType{}")) {
			    		res = null;
			    	}
			    	
			    } 
		    }
		    
		}
		catch (Exception e)
		{
			res = null;
			e.printStackTrace();
		    Log.e(this.getClass().getName(), e.getMessage() 	
		    		+"\n "+ e.getStackTrace().toString()
		    		+"\n "+ request.getName()
		    		+"\n "+ URL 
		    		+"\n "+ new Date().toString());  
		    
		} 
	    	
		return res;
	}	
	

	
	/**
	 * 
	 * @author	mjulia
	 * @date	06/03/2013
	 * @title	ValidacionCredencialesUsuario
	 * @comment	Validaci�n de los credenciales del usuario mediante un servicio web
	 *
	 * @param 	user       	Identificador del usuario a validar en el registro del servidor web
	 * @param 	pass		Contrase�a del usuario a validar en el registro del servidor web
	 * @param 	appID		Identificador de la App.Mobile para el Sistema Push de GCM
	 * 	
	 * @return	Indicador boleano. True = registro correcto
	 *                             False= registro err�nea.
	 *
	 */
	public boolean ValidacionCredencialesUsuario(String user, String pass, String appID) throws Exception{
		
		boolean correcto=false;
		
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
		//appID = CryptographicInteroperability.Encrypt(appID);
   	
    	UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_ValidacionCredencialesUsuario");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
    
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME); 
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
        request.addProperty("appID", appID);
               
        int respuesta = 0;
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = Integer.valueOf(res.trim());
        	}
        	if (respuesta > 0){
        		correcto=true;
        	}
        } 

        return correcto;
    	
    }
	
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	recepcionarDatosWebService
	 * @comment	Procedimiento para ejecutar el servicio web de recuperaci�n de datos del usuario
	 *
	 * @param user	Identificador del usuario para recuperar los datos
	 * @param pass	Contrase�a del usuario
	 * @return		Documento JSON con los datos recibidos
	 * @throws Exception	
	 *
	 */
	public String recepcionarDatosWebService(String user, String pass) throws Exception{
		
		String respuesta = null;
		
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_DescargaDatosEnDispovitivoMovil");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	recepcionarDatosWebService_UnaObra
	 * @comment	Procedimiento para ejecutar el servicio web de recuperaci�n de datos del usuario de solo una obra
	 *
	 * @param user	Identificador del usuario para recuperar los datos
	 * @param pass	Contrase�a del usuario
 	 * @param codObra	C�digo de la obra
	 * @return		Documento JSON con los datos recibidos
	 * @throws Exception	
	 *
	 */
	public String recepcionarDatosWebService_UnaObra(String user, String pass, String codObra) throws Exception{
		
		String respuesta = null;
		
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
		codObra = CryptographicInteroperability.Encrypt(codObra);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_DescargaUnaObraEnDispovitivoMovil");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
        request.addProperty("codigoObra", codObra);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	ejecutarAccionPLanificacion
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n de planificaci�n de la obra
	 *
	 * @param codObra			C�digo de la obra
	 * @param fPlanificacion	Fecha planificaci�n de la obra
	 * @param user				Identificador del usuario para recuperar los datos
	 * @param pass				Contrase�a del usuario
	 * @return		Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionPLanificacion(String codObra, String fPlanificacion, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);
		fPlanificacion = CryptographicInteroperability.Encrypt(fPlanificacion);
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionPlanificacion");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("fechaPlanificacion", fPlanificacion);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	ejecutarAccionCambioPlanificacion
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n de cambio de planificaci�n de la obra
	 *
	 * @param codObra			C�digo de la obra
	 * @param fPlanificacion	Fecha planificaci�n de la obra
	 * @param user				Identificador del usuario para recuperar los datos
	 * @param pass				Contrase�a del usuario
	 * @return		Documento JSON con los datos actualizados
	 * @throws 		Exception	
	 *
	 */
	public String ejecutarAccionCambioPlanificacion(String codObra, String fPlanificacion, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);
		fPlanificacion = CryptographicInteroperability.Encrypt(fPlanificacion);
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionCambioPlanificacion");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("fechaPlanificacion", fPlanificacion);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	ejecutarAccionTrazado
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n de trazado de la obra
	 *
	 * @param codObra	C�digo de la obra
	 * @param user		Identificador del usuario para recuperar los datos
	 * @param pass		Contrase�a del usuario
	 * @return			Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionTrazado(String codObra, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);		
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionTrazado");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	ejecutarAccionModificacionTrazado
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n de modificacion de trazado de la obra
	 *
	 * @param codObra	C�digo de la obra
	 * @param user		Identificador del usuario para recuperar los datos
	 * @param pass		Contrase�a del usuario
	 * @return			Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionModificacionTrazado(String codObra, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);		
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionModificacionTrazado");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	

	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	ejecutarAccionInicio
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n de inicio de la obra
	 *
	 * @param codObra	C�digo de la obra
	 * @param user		Identificador del usuario para recuperar los datos
	 * @param pass		Contrase�a del usuario
	 * @return			Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionInicio(String codObra, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);		
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionInicio");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	ejecutarAccionSolicitudPeS
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n de Solicitud de PeS de la obra
	 *
	 * @param codObra	C�digo de la obra
	 * @param user		Identificador del usuario para recuperar los datos
	 * @param pass		Contrase�a del usuario
	 * @return			Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionSolicitudPeS(String codObra, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);		
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionSolicitudPeS");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	

	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	ejecutarAccionFinalPrueba
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n de inidcar el final de la prueba de la obra
	 *
	 * @param codObra	C�digo de la obra
	 * @param RecursoPreventivo C�digo del recurso preventivo
	 * @param user		Identificador del usuario para recuperar los datos
	 * @param pass		Contrase�a del usuario
	 * @return			Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionFinalPrueba(String codObra, String RecursoPreventivo, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);	
		RecursoPreventivo = CryptographicInteroperability.Encrypt(RecursoPreventivo);	
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionFinalPrueba");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("RecursoPreventivo", RecursoPreventivo);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/12/2014
	 * @title	ejecutarAccionCambioRecPreventivo
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n de cambiar el recurso preventivo de la obra
	 *
	 * @param codObra	C�digo de la obra
	 * @param RecursoPreventivo C�digo del recurso preventivo
	 * @param user		Identificador del usuario para recuperar los datos
	 * @param pass		Contrase�a del usuario
	 * @return			Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionCambioRecPreventivo(String codObra, String RecursoPreventivo, String Mot_RecursoPreventivo, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);	
		RecursoPreventivo = CryptographicInteroperability.Encrypt(RecursoPreventivo);	
		Mot_RecursoPreventivo= CryptographicInteroperability.Encrypt(Mot_RecursoPreventivo);	
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionCambioRecPreventivo");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("RecursoPreventivo", RecursoPreventivo);
        request.addProperty("MotivoRecursoPreventivo", Mot_RecursoPreventivo);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	ejecutarAccionPeS
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n de PeS de la obra
	 *
	 * @param codObra	C�digo de la obra
	 * @param user		Identificador del usuario para recuperar los datos
	 * @param pass		Contrase�a del usuario
	 * @return			Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionPeS(String codObra, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);		
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionPeS");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	ejecutarAccionFinal
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n de PeS de la obra
	 *
	 * @param codObra	C�digo de la obra
	 * @param user		Identificador del usuario para recuperar los datos
	 * @param pass		Contrase�a del usuario
	 * @return			Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionFinal(String codObra, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);		
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionFinal");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	5/11/2013
	 * @title	ejecutarAccionComentarios
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n entrada de comentarios a la acometida
	 *
	 * @param codObra		C�digo de la obra
	 * @param comentarios	Comentarios a la obra
	 * @param user			Identificador del usuario para recuperar los datos
	 * @param pass			Contrase�a del usuario
	 * @return				Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionComentarios(String codObra, String comentarios, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);		
		comentarios = CryptographicInteroperability.Encrypt(comentarios);
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionComentarios");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("comentarios", comentarios);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	5/11/2013
	 * @title	ejecutarAccionImprevistosProyecto
	 * @comment	Procedimiento para ejecutar el servicio web para la acci�n entrada de imprevistos de proyecto
	 *
	 * @param codObra		C�digo de la obra
	 * @param comentarios	Comentarios a la obra
	 * @param user			Identificador del usuario para recuperar los datos
	 * @param pass			Contrase�a del usuario
	 * @return				Documento JSON con los datos actualizados
	 * @throws Exception	
	 *
	 */
	public String ejecutarAccionImprevistosProyecto(String codObra, String comentarios, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		codObra = CryptographicInteroperability.Encrypt(codObra);		
		comentarios = CryptographicInteroperability.Encrypt(comentarios);
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_AccionImprevistosProyecto");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("comentarios", comentarios);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	recepcionarRecursosPreventivos
	 * @comment	Procedimiento para ejecutar el servicio web de recuperaci�n de los recursos preventivos de una obra
	 *
	 * @param codObra	C�digo de la obra
	 * @param user		Identificador del usuario para recuperar los datos
	 * @param pass		Contrase�a del usuario
	 * @return			Documento JSON con los datos recibidos
	 * @throws Exception	
	 *
	 */
	public String recepcionarRecursosPreventivos(String codObra, String user, String pass) throws Exception{
		
		String respuesta = null;
		
		user = CryptographicInteroperability.Encrypt(user);
		pass = CryptographicInteroperability.Encrypt(pass);
		codObra = CryptographicInteroperability.Encrypt(codObra);
   	
		UtilData ud = new UtilData(this.context);      	
    	String METHOD_NAME = ud.obtenerParametroTablaConfiguracion("SW_Metodo_DescargaRecursosPreventivos");
    	String URL = ud.obtenerParametroTablaConfiguracion("SW_URL_ServicioSincronizacion");
    	String SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;    	
    	ud = null;
		   
    	//URL = "http://10.0.2.2:"+puerto+"/ServiciosWeb/sincronizacion.asmx"; //DEBUG eclipse-visual studio (Android-SW). URL del servidor de desarrollo ASP.NET
    	
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        request.addProperty("codObra", codObra);
        request.addProperty("usuario", user);
        request.addProperty("password", pass);
                      
        int timeout =  60000;
        String res = conexionMetodoServicioWeb(URL, SOAP_ACTION, request, timeout);
        if (res != null) {
        	if (res.trim().equals("") == false) {
        		respuesta = res.trim();
        		respuesta = CryptographicInteroperability.Decrypt(respuesta);
        	}
        } 

    	return respuesta;
    	
    }
	
}
