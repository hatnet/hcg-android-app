package tec.telesupervision.services;

import java.io.IOException;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import tec.telesupervision.data.DataBase;
import tec.telesupervision.data.UtilData;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;


/**
 * 
 * @author 	jjulia
 * @date 	27/10/2014
 * @title 	RegistroServicioGCM
 * @comment Clase para gestionar el registro en el Sistema Push - Servicio
 *          Google Cloud Messaging
 *
 *
 */
public class RegistroServicioGCM {
	
	private Context context;
	private Activity act;
	private GoogleCloudMessaging gcm;
	  
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public static final long EXPIRATION_TIME_MS = 1000 * 3600 * 24 * 7;
	
	
	/**
	 * 
	 * @author		jjulia
	 * @date		27/10/2014
	 * @title		ServicioGCMSistemaPush
	 * @comment		Contructor de la clase
	 *
	 * @param context	Contexto en el que se ejecutan los procedimiento
	 * @param act       Activity sobre la que se lanza el procedimiento
	 *
	 */
	public RegistroServicioGCM(Context _context, Activity _act) {
		super();
		this.context = _context;
		this.act = _act;
	}
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	27/10/2014
	 * @title	registrarClienteGCM
	 * @comment	Procidimiento principal para comprobar y/o registrar App.Mobile/usuario en GCM 
	 *
	 */
	public void registrarClienteGCM() 
	{
				
		//Chequemos si est� instalado Google Play Services
		if(checkPlayServices())
		{
	        gcm = GoogleCloudMessaging.getInstance(this.context);
			
	        //Obtenemos el Registration ID de cliente
	        String regid = obtenerKeyClienteGCM();
	
	        //Si no disponemos de Registration ID comenzamos el registro
	        if (regid.equals("")) {
	        	
	        	UtilData ud = new UtilData(this.context);      	
	        	String ID_SENDER = ud.obtenerParametroTablaConfiguracion("SENDER_ID");
	        	String usuario = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
	        	String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
	        		        	
	    		TareaRegistroGCM tarea = new TareaRegistroGCM();
	    		tarea.execute(ID_SENDER, usuario, password);
	        }
		}
		else 
		{
            Log.i(this.getClass().getName(), "SERVICIO GCM - No se ha encontrado Google Play Services.");
        }
	}

	
	/**
	 * 
	 * @author	jjulia
	 * @date	27/10/2014
	 * @title	obtenerKeyClienteGCM
	 * @comment	Obtiene el valor la clave de registro del cliente GCM
	 *
	 * @return	Key del cliente (app.mobile) GCM	
	 *
	 */
	public String obtenerKeyClienteGCM(){
		
		String registrationId="";
		int registeredVersion =0;
		long expirationTime =0;
		 
	    //Obtener de la tabla los valores del registro del cliente GCM
		String query = "select * from configuracionGeneral where clave LIKE '%ClienteGCM%' ";
		DataBase db = new DataBase(this.context);
		Cursor c=null;
		try {
			boolean openBD = db.open();
			while (openBD == false) {
				 openBD = db.open();
			}
			
			c = db.getQuery(query, null);
			while (c.moveToNext()){
				
				if (c.getString(c.getColumnIndex("clave")).equals("userIDClienteGCM")==true) {
					registrationId=c.getString(c.getColumnIndex("valor"));
				}
				if (c.getString(c.getColumnIndex("clave")).equals("userAppVersionClienteGCM")==true) {
					registeredVersion=c.getInt(c.getColumnIndex("valor"));
				}
				if (c.getString(c.getColumnIndex("clave")).equals("userFechaCaducidadClienteGCM")==true) {
					expirationTime=c.getLong(c.getColumnIndex("valor"));
				}
		
			} 
						
		} catch(Exception e) {
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString());
		} finally {
			if (c != null) {c.close();}
			db.close();
		}
		
		
		//Si no existe clave de Registro GCM sale de la funci�n para obtener una nueva
		if (registrationId.length() == 0) {
	        Log.i(this.getClass().getName(), "SERVICIO GCM - Registro GCM no encontrado.");
	        return "";
		}
		
		//En caso de que exista la clave comprobar� la versi�n de la aplicaci�n y la fecha de caducidad de la clave
        int currentVersion = getAppVersion(this.context);
 	    if (registeredVersion != currentVersion) {
 	    	Log.i(this.getClass().getName(), "SERVICIO GCM - Registro GCM - Nueva versi�n aplicaci�n.");
 	        return "";
	    }
		    
 	    if (System.currentTimeMillis() > expirationTime) {
 	    	Log.i(this.getClass().getName(), "SERVICIO GCM - Registro GCM - Registro caducado.");
 	        return "";
 	    }

		return registrationId;
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	27/10/2014
	 * @title	getAppVersion
	 * @comment	Comprobar la versi�n del App.Mobile
	 *
	 * @param context	Contexto en el que se ejecutan los procedimiento
	 * 
	 * @return	Versi�n del App.Mobile
	 *
	 */
	private static int getAppVersion(Context context){
	    try
	    {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	 
	        return packageInfo.versionCode;
	    }
	    catch (NameNotFoundException e)
	    {
	        throw new RuntimeException("SERVICIO GCM - Registro GCM - Error al obtener versi�n: " + e);
	    }
	}
	
	

	/**
	 * 
	 * @author	jjulia
	 * @date	27/10/2014
	 * @title	checkPlayServices
	 * @comment	Comprobar si estan instalados en el dispositivo los Play Services de Google
	 *
	 * 
	 * @return	true=instalados / false=no instalados
	 *
	 */
	private boolean checkPlayServices() {
		
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.context);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
	        {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, this.act,
	                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        }
	        else
	        {
	           Log.i(this.getClass().getName(), "SERVICIO GCM - Check Play Services - Dispositivo no soportado.");
	        }
	        return false;
	    }
	    return true;
	    
	}
	
	
	
	

	/**
	 * 
	 * @author	jjulia
	 * @date	27/10/2014
	 * @title	TareaRegistroGCM
	 * @comment	Tarea asincrona para realizar el registro del App.Mobile en GCM
	 *
	 */
	private class TareaRegistroGCM extends AsyncTask<String,Integer,String>
	{
		@Override
        protected String doInBackground(String... params) 
		{
           String msg = "";
           String regid="";
           String SERVER_ID =  params[0];
           String usuario =  params[1];
           String password =  params[2];
            
            try 
            {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }
                
                //Nos registramos en los servidores de GCM
                regid = gcm.register(SERVER_ID);
                
                Log.i(this.getClass().getName(), "SERVICIO GCM - Registro cliente App.Mobile - App.Mobile registrada.");

                //Nos registramos en nuestro servidor
                ConexionesSincronizacion us = new ConexionesSincronizacion(context);
                boolean registrado  = us.ValidacionCredencialesUsuario(usuario, password, regid);
                
                //Guardamos los datos del registro
                if(registrado)      {
                	actualizarRegistroClienteGCM( regid);
                }
            
            } catch (IOException ex) {
            	Log.e(this.getClass().getName(), "SERVICIO GCM - Registro cliente App.Mobile - Error registro en GCM:" + ex.getMessage());
            } catch (Exception e) {
            	Log.e(this.getClass().getName(), "SERVICIO GCM - Registro cliente App.Mobile - Error registro en GCM:" + e.getMessage());
			}
            
            return msg;
        }
	}
	
	
	/**
	 * 
	 * @author	mcanal
	 * @date	27/10/2043
	 * @title	actualizarRegistroClienteGCM
	 * @comment	Actualiza en la tabla de configuradci�n general los datos de registro del App.Mobile en GCM
	 *
	 * @param appID			Identificador de registro de App.Mobile en GCM
	 * 
	 */
	private void actualizarRegistroClienteGCM(String regid) {
		
		String update = "update configuracionGeneral";
		update += " set valor = '" + regid + "'";
		update += " where clave = 'userIDClienteGCM'";
		
		String update2 = "update configuracionGeneral";
		update2 += " set valor = '" + getAppVersion(context) + "'";
		update2 += " where clave = 'userAppVersionClienteGCM'";
				
		String update3 = "update configuracionGeneral";
		update3 += " set valor = '" + System.currentTimeMillis() + EXPIRATION_TIME_MS + "'";
		update3 += " where clave = 'userFechaCaducidadClienteGCM'";
		
		DataBase db = new DataBase(context);
	
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}
		
		db.executeQuery(update);
		db.executeQuery(update2);
		db.executeQuery(update3);
	
		db.close();
		db = null;
	}
	
	
}




