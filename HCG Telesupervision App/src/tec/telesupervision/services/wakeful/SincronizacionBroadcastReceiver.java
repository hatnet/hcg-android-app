package tec.telesupervision.services.wakeful;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

/**
 * 
 * @author	jjulia
 * @date 	27/10/2014
 * @title 	SincronizacionBroadcastReceiver
 * @comment Broadcat para gestionar la sincronización de datos entre App.Mobile y la Web Central
 *
 *
 */
public class SincronizacionBroadcastReceiver extends WakefulBroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		Log.i(this.getClass().getName(), "SincronizacionBroadcastReceiver");
		
		ComponentName comp = new ComponentName(context.getPackageName(), SincronizacionIntentService.class.getName());

		startWakefulService(context, (intent.setComponent(comp)));

		setResultCode(Activity.RESULT_OK);
	}
	
}