package tec.telesupervision.services.wakeful;

import java.util.Date;
























import tec.telesupervision.mod.aplicacion.DetalleObraActivity;
import tec.telesupervision.services.ProcesosEjecucionAcciones;
import tec.telesupervision.utilidades.CryptographicInteroperability;
import tec.telesupervision.ver2.R;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


/**
 * 
 * @author 	jjulia
 * @date 	27/10/2014
 * @title 	GCMIntentService
 * @comment Procedimiento para la recepción de mensajes del Servicio Google Cloud Messaging
 *
 *
 */
public class GCMIntentService extends IntentService {

	private static final int NOTIF_ALERTA_ID = 1;


	
	public GCMIntentService() {
		super("GCMIntentService");
	}

	 
	
	 
	@Override
	protected void onHandleIntent(Intent intent) {
		
		Log.i(this.getClass().getName(), "GCM Intent Service. Se ha recibido un mensaje GCM - "	+ new Date().toString());
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);
		Bundle extras = intent.getExtras();

		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
				
				String codObra="";
				try {

					String respuesta = extras.getString("msg");
					respuesta = CryptographicInteroperability.Decrypt(respuesta);
					
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(this);
					prc.ejecucionAccionDescargarObra(respuesta);

					mostrarNotification(respuesta);
					
//					String[] elementos = respuesta.split("###");
//					codObra = elementos[0];
//					datosJSON = elementos[1];
//					
//					mostrarNotification(codObra);
//									  
//					ObjectMapper mapper = new ObjectMapper();
//					DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON,	DescargaDatosWeb.class);
//
//					ProcesoSincronizacion sin = new ProcesoSincronizacion(this);
//					sin.SincronizacionActualizarObra(codObra, descargaJSON);
//					sin.finalizarDinamico();
					
				
				} catch (Exception e) {
					Log.e(this.getClass().getName(),
							"GCMIntentService - " + e.getMessage());
					mostrarNotification("GCMIntentService error: " + codObra);
				}

			}
		}

		GCMBroadcastReceiver.completeWakefulIntent(intent);
	}

	
	private void mostrarNotification(String msg) 
	{
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE); 
		Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		
		NotificationCompat.Builder mBuilder = 
			new NotificationCompat.Builder(this)  
				.setSmallIcon(R.drawable.ic_notification) 
				.setSound(alarmSound)
				.setAutoCancel(true)
				.setContentTitle(getResources().getString(R.string.notificacion_receiver_title))  
				.setContentText(getResources().getString(R.string.notificacion_receiver_text)+ ' '+ msg);
		

		Intent notIntent =  new Intent(this, DetalleObraActivity.class); 
		String key = "codigoObra";
		String value =  msg;
		notIntent.putExtra(key, value);
		
		
		PendingIntent contIntent = PendingIntent.getActivity(this, 0, notIntent, 0);   
		
		mBuilder.setContentIntent(contIntent);
		
		mNotificationManager.notify(NOTIF_ALERTA_ID, mBuilder.build());
		
	
		Log.i(this.getClass().getName(), "GCM Intent Service. Envio notificación: "	+ msg);
    }
	
		
}