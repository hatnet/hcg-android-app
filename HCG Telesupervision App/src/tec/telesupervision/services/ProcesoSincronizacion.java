package tec.telesupervision.services;

import java.util.Date;
import java.util.Iterator;

import tec.telesupervision.Objects.json.DescargaDatosWeb;
import tec.telesupervision.Objects.json.DescargaRecursosPreventivos;
import tec.telesupervision.Objects.json.JSONAcometida;
import tec.telesupervision.Objects.json.JSONMensaje;
import tec.telesupervision.Objects.json.JSONObra50m;
import tec.telesupervision.Objects.json.JSONPlanificacion;
import tec.telesupervision.Objects.json.JSONRecursoPreventivo;
import tec.telesupervision.Objects.json.SincronizacionElemento;
import tec.telesupervision.data.DataBase;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.ver2.R;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;



/**
 * 
 * @author	mjulia
 * @date	08/03/2013
 * @title	Proceso
 * @comment	Clase que implementa la interfaz runnable para la ejecución del servicio de Sincronización
 *
 */
public class ProcesoSincronizacion implements Runnable {

	private Context context;

	//Es importante declarar la variable como volatile para que cuando el estado de la variable cambie el while del método run se entere.
	private volatile static boolean isRunningProceso = false;
	
	private static String erroresSincrnizacion = "";
	
	
	public ProcesoSincronizacion(Context context) {
		super();
		this.context = context;
		isRunningProceso = true;
		erroresSincrnizacion="";
	}

	public static boolean isRunnig (){
		return isRunningProceso;
	}
	
	public static String erroresEnSincrnizacion(){
		return erroresSincrnizacion;
	}
	
	
	/**
	 * Finaliza el proceso
	 */
	public static void finalizar(){
		isRunningProceso = false;	
		System.gc();
	}
	public void finalizarDinamico(){
		isRunningProceso = false;	
		System.gc();
	}
	
	
	/**
	 * Inicializa el proceso mientras isRunningProceso sea true.
	 */
	public void run() {	
		
		while(isRunningProceso == true) {	
			
			try{
				UtilData ud = new UtilData(context);
				boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
				String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
				String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
				if (userName.trim().equals("")==true || password.trim().equals("")==true){
					autorizado=false;
				}
					
				ConexionesSincronizacion us = new ConexionesSincronizacion(context);
				if (us.networkAvailable() != true ) {
					autorizado=false;
				}
				
				if(autorizado == true) {
						
					//Recepción de datos de la web al terminal
					Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. Recepción de datos de la web.");
					SincronizacionRecepcionarDatosWeb(userName, password, us);
											
				}else{
					erroresSincrnizacion = context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado);
				}
							
			} catch (Exception e) {	
				
				e.printStackTrace();
				Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
				
				erroresSincrnizacion = context.getResources().getString(R.string.error_SIncronizacion_Error);
				
			} finally{
												
				//Finalizar el proceso
				finalizar();
				
			}
		}
	}
	
	
	
	
	
	/**
	 * 
	 * @author		jjulia
	 * @date		14/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb
	 * @comment		Procedimientode la sincronización para recepcionar los datos que la Web Central envía al terminal
	 *
	 * @param user		Identificador de usuario configurado en el terminal
	 * @param password	Contraseña del usuario configurado
	 * @param us		Objeto clase con utilidades para la sincronización de datos
	 * @throws Exception 
	 *
	 */
	public void SincronizacionRecepcionarDatosWeb(String user, String password, ConexionesSincronizacion us) throws Exception {
						
		ObjectMapper mapper = new ObjectMapper();
						
		//Invocar al servico web para obtener los datos a cargar en el terminal
    	String datosJSON = us.recepcionarDatosWebService(user, password);
		if (datosJSON == null){
			//(no se han descargado datos parapendientes de actualizar)
			Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. No se han descargado datos en el Terminal.");
		
    	}else {
				
			Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. Documento JSON con los datos descargados:" + datosJSON);
									
			//Proceso de actualización en el Terminal
			DescargaDatosWeb descargaJSON = mapper.readValue(datosJSON, DescargaDatosWeb.class );  
			SincronizacionRecepcionarDatosWeb_ActualizacionDescarga(descargaJSON); 
		}
											
		Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. Final recepción.");
			
	}
		
	
	/**
	 * 
	 * @author		jjulia
	 * @date		18/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb_ActualizacionDescarga
	 * @comment		Procedimiento para gestionar el documento recibido de la web con los datos a cargar en el Terminal
	 * 	  
	 * @param   	descargaJSON	Documento JSON recibido de la web con los datos a cargar en el Terminal
	 *
	 */
	private void SincronizacionRecepcionarDatosWeb_ActualizacionDescarga(DescargaDatosWeb descargaJSON){
		
		DataBase db = new DataBase(context);
	    boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}
		
		//Eliminar tablas de Planificacion, Acometidas y Obras50m
		db.execSQL("DELETE FROM planificacion");
		db.execSQL("DELETE FROM acometidas");
		db.execSQL("DELETE FROM obras50m");
		db.execSQL("DELETE FROM recursosPreventivos");
		
		//Lectura y actualizazición de los datos recibidos
		Iterator<SincronizacionElemento> iter = descargaJSON.iterator();
		while (iter.hasNext()) {
			SincronizacionElemento elemento = iter.next();
									
			//Procesar las obras pendientes de Planificar
			if (elemento.getElementoPlanificacion() != null){
				JSONPlanificacion ctl = (JSONPlanificacion) elemento.getElementoPlanificacion();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Planificacion(ctl, db);
			}
			
			//Procesar las acometidas en curso
			if (elemento.getElementoAcometida() != null){
				JSONAcometida ctl = (JSONAcometida) elemento.getElementoAcometida();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Acometida(ctl, db);
			}
			
			//Procesar las obras <= 50m. en curso
			if (elemento.getElementoObra50m() != null){
				JSONObra50m ctl = (JSONObra50m) elemento.getElementoObra50m();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Obra50m(ctl, db);
			}
			
			//Procesar los recursos preventivos
			if (elemento.getElementoRecursoPreventivo() != null){
				JSONRecursoPreventivo ctl = (JSONRecursoPreventivo) elemento.getElementoRecursoPreventivo();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_RecursosPreventivos(ctl, db);
			}
						
			//Procesar las mensajes
			if (elemento.getElementoMensaje() != null){
				JSONMensaje ctl = (JSONMensaje) elemento.getElementoMensaje();
				Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. Mensaje: " + ctl.getMensaje());
			}
			
			//Finalización elemento
			//Log.i(this.getClass().getName(), "EJECUTAR PROCESO SINCRONIZACIÓN - RECEPCIONAR. Recepción elemento");
							
		}
		
		//Finalización actualización descarga 
		db.close();
		
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	SincronizacionActualizarObra
	 * @comment	Procedimiento para actualizar los datos relativos a una obra en particular
	 *
	 * @param CodObra		Código de la obra a actualizar
	 * @param descargaJSON	Documento JSON con los datos de la obra actualizados	
	 *
	 * En el procesimiento de sincronizar una Obra no se actualiza la lista de recursos preventivos
	 */
	public void SincronizacionActualizarObra(String CodObra, DescargaDatosWeb descargaJSON){
		
		DataBase db = new DataBase(context);
	    boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}
		
		
		//Comprobar si se han recibido datos de la obra o un mensaje
		if (descargaJSON.size()>0){
			SincronizacionElemento elemento = descargaJSON.get(0);
			
			if (elemento.getElementoPlanificacion() != null || elemento.getElementoAcometida() != null || elemento.getElementoObra50m() != null){
				//Si se han recibido datos de la obra,
				//eliminar de las tablas de Planificacion, Acometidas, Obras50m
				db.execSQL("DELETE FROM planificacion WHERE Cod_Obra='" + CodObra + "' ");
				db.execSQL("DELETE FROM acometidas WHERE Cod_Obra='" + CodObra + "' ");
				db.execSQL("DELETE FROM obras50m WHERE Cod_Obra='" + CodObra + "' ");
			    Log.i(this.getClass().getName(), "EJECUTAR ACCIÓN - ACTUALIZAR UNA OBRA. Eliminar datos de la obra en el dispositivo. ");
			}
		}
		
	
		//Lectura y actualizazición de los datos recibidos
		Iterator<SincronizacionElemento> iter = descargaJSON.iterator();
		while (iter.hasNext()) {
			SincronizacionElemento elemento = iter.next();
					
			Log.i(this.getClass().getName(), "EJECUTAR ACCIÓN - ACTUALIZAR UNA OBRA. Recargar nuevos datos recibidos. ");
			
			//Procesar las actualizaciones en las Obras
			if (elemento.getElementoPlanificacion() != null){
				JSONPlanificacion ctl = (JSONPlanificacion) elemento.getElementoPlanificacion();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Planificacion(ctl, db);
			}
			
			//Procesar las acometidas en curso
			if (elemento.getElementoAcometida() != null){
				JSONAcometida ctl = (JSONAcometida) elemento.getElementoAcometida();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Acometida(ctl, db);
			}
			
			//Procesar las obras <= 50m. en curso
			if (elemento.getElementoObra50m() != null){
				JSONObra50m ctl = (JSONObra50m) elemento.getElementoObra50m();
				SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Obra50m(ctl, db);
			}
								
			//Procesar las mensajes
			if (elemento.getElementoMensaje() != null){
				JSONMensaje ctl = (JSONMensaje) elemento.getElementoMensaje();
				Toast.makeText(context, ctl.getMensaje() , Toast.LENGTH_LONG).show();
				Log.i(this.getClass().getName(), "EJECUTAR ACCIÓN - ACTUALIZAR UNA OBRA. Mensaje: " + ctl.getMensaje());
			}
					
		}
		
		//Finalización actualización descarga 
		db.close();
		
	}
	
	
	/**
	 * 
	 * @author		jjulia
	 * @date		20/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Planificacion
	 * @comment		Actualización de las Planificaciones (obras pendientes de planificar) desde documento JSON 
	 *
	 * @param ctl	Elemento JSON de Planificación
	 * @param db	Instancia de la base de datos local del terminal	
	 *
	 */
	private void SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Planificacion(JSONPlanificacion ctl, DataBase db){
	
		ContentValues values = new ContentValues();
		values.put("Cod_Obra", ctl.getCodObra());
		values.put("Tipo_Obra", ctl.getTipoObra());
		values.put("Direccion", ctl.getDireccion());
		values.put("Municipio", ctl.getMunicipio());
		values.put("CarpetasOonair", ctl.getCarpetasOonair());
		
		db.insert("planificacion", null, values);
		
	}
	
	/**
	 * 
	 * @author		jjulia
	 * @date		20/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Acometida
	 * @comment		Actualización de las Planificaciones (obras pendientes de planificar) desde documento JSON 
	 *
	 * @param ctl	Elemento JSON de Acometidas
	 * @param db	Instancia de la base de datos local del terminal	
	 *
	 */
	private void SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Acometida(JSONAcometida ctl, DataBase db){
	
		ContentValues values = new ContentValues();
		values.put("Cod_Obra", ctl.getCodObra());
		values.put("Situacion", ctl.getSituacion());
		values.put("SituacionDes", ctl.getSituacionDescripcion());
		values.put("Direccion", ctl.getDireccion());
		values.put("Municipio", ctl.getMunicipio());
		values.put("CarpetasOonair", ctl.getCarpetasOonair());
		values.put("Comentarios", ctl.getComentarios());
		values.put("FechaAcepNomb", ctl.getFechaAceptacionNombramiento());
		values.put("FechaPlan", ctl.getFechaPlanificacion());
		values.put("FechaIni", ctl.getFechaInicio());
		values.put("FechaPeS", ctl.getFechaPeS());
		values.put("FechaFin", ctl.getFechaFinal());
		values.put("TSGestorObra", ctl.getTelesupervisionGODF());
		values.put("TSTelefonoGO", ctl.getTelesupervisionTelefonoGODF());
		values.put("TSInicioPlan", ctl.getTelesupervisionInicioPlanificacion());
		values.put("TSResultadoFec", ctl.getTelesupervisionFechaResultado());
		values.put("TSResultado", ctl.getTelesupervisionResultado());
		values.put("HistorialObra", ctl.getHistorialObra());
		
		db.insert("acometidas", null, values);
		
	}
	
	
	/**
	 * 
	 * @author		jjulia
	 * @date		20/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Obra50m
	 * @comment		Actualización de las Planificaciones (obras pendientes de planificar) desde documento JSON 
	 *
	 * @param ctl	Elemento JSON de Obras <= 50m.
	 * @param db	Instancia de la base de datos local del terminal	
	 *
	 */
	private void SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_Obra50m(JSONObra50m ctl, DataBase db){
	
		ContentValues values = new ContentValues();
		values.put("Cod_Obra", ctl.getCodObra());
		values.put("Situacion", ctl.getSituacion());
		values.put("SituacionDes", ctl.getSituacionDescripcion());
		values.put("Direccion", ctl.getDireccion());
		values.put("Municipio", ctl.getMunicipio());
		values.put("CarpetasOonair", ctl.getCarpetasOonair());
		values.put("Comentarios", ctl.getComentarios());
		values.put("Trazado", ctl.getTrazado());
		values.put("TrazadoMod", ctl.getTrazadoMod());
		values.put("FechaAcepNomb", ctl.getFechaAceptacionNombramiento());
		values.put("FechaPlan", ctl.getFechaPlanificacion());
		values.put("FechaTraSol", ctl.getFechaTrazadoSolicitud());
		values.put("FechaTraCon", ctl.getFechaTrazadoConfirmacion());
		values.put("FechaTraSolMod", ctl.getFechaTrazadoSolicitudMod());
		values.put("FechaTraConMod", ctl.getFechaTrazadoConfirmacionMod());
		values.put("FechaIni", ctl.getFechaInicio());
		values.put("FechaSolicitudPeS", ctl.getFechaSolicitudPeS());
		values.put("FechaAcepConten", ctl.getFechaAcepContenidos());
		values.put("FechaFinalPrueba", ctl.getFechaFinalPrueba());
		values.put("FechaAceptadaPeG", ctl.getFechaAceptadaPeG());
		values.put("FechaPeS", ctl.getFechaPeS());
		values.put("FechaFin", ctl.getFechaFinal());
		values.put("TSGestorObra", ctl.getTelesupervisionGODF());
		values.put("TSTelefonoGO", ctl.getTelesupervisionTelefonoGODF());
		values.put("TSInicioPlan", ctl.getTelesupervisionInicioPlanificacion());
		values.put("TSResultadoFec", ctl.getTelesupervisionFechaResultado());
		values.put("TSResultado", ctl.getTelesupervisionResultado());
		values.put("HistorialObra", ctl.getHistorialObra());
				
		UtilData ud = new UtilData(context);
		String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
		
		boolean perfilAsignado = false;
		if (userName.toLowerCase().trim().equals(ctl.getCod_JefeObra().toLowerCase().trim())==true ){
			if(userName.toLowerCase().trim().equals(ctl.getCod_RecPreventivo().toLowerCase().trim())==true){
				values.put("Perfil", "");
				perfilAsignado=true;
			}
		}
		if (perfilAsignado==false){
				
			if (userName.toLowerCase().equals(ctl.getCod_RecPreventivo().toLowerCase())==true){
				values.put("Perfil", "RPREV");
			} else {
				values.put("Perfil", "JOC");
			}
		}
		
		db.insert("obras50m", null, values);
		
	}
	
	
	/**
	 * 
	 * @author		jjulia
	 * @date		20/06/2013
	 * @title		SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_RecursosPreventivos
	 * @comment		Actualización de la lista de Recursos preventivos desde documento JSON 
	 *
	 * @param ctl	Elemento JSON de recurso preventivo
	 * @param db	Instancia de la base de datos local del terminal	
	 *
	 */
	private void SincronizacionRecepcionarDatosWeb_ActualizacionDescarga_RecursosPreventivos(JSONRecursoPreventivo ctl, DataBase db){
				
		ContentValues values = new ContentValues();
	    values.put("Codigo", ctl.getCod_JefeObra());
		values.put("Nombre", ctl.getNom_JefeObra());
									
		db.insert("recursosPreventivos", null, values);
		
	}
		
		
	
	
	
	/**
	 * Procedimiento para cargar en la tabla la lista de recusrsos preventivos
	 * 
	 * @param codObra Código de la obra
	 */
	public void SincronizacionListaRecursosPreventivos(String codObra) {

		String respuesta="";
		
		try{
			
			UtilData ud = new UtilData(this.context);
			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
			if (userName.trim().equals("")==true || password.trim().equals("")==true){
				autorizado=false;
			}
				
			ConexionesSincronizacion us = new ConexionesSincronizacion(this.context);
			if (us.networkAvailable() != true ) {
				autorizado=false;
			}
			
			if(autorizado == true) {
					
				Log.i(this.getClass().getName(), "EJECUTAR RECURSOS PREVENTIVOS. Conectar con servicio web.");
				respuesta=us.recepcionarRecursosPreventivos(codObra, userName, password);
				
				if (respuesta.equals("")==false){
					
					ObjectMapper mapper = new ObjectMapper();
					DescargaRecursosPreventivos descargaJSON = mapper.readValue(respuesta, DescargaRecursosPreventivos.class );  
					
					Log.i(this.getClass().getName(), "EJECUTAR RECURSOS PREVENTIVOS.Iniciar carga tabla. "+respuesta);
					
					DataBase db = new DataBase(context);
				    boolean openBD = db.open();
					while (openBD == false) {
						 openBD = db.open();
					}
					
					//db.execSQL("DELETE FROM recursosPreventivos WHERE Cod_Obra='" + codObra + "' ");
					db.execSQL("DELETE FROM recursosPreventivos ");
										
					Iterator<JSONRecursoPreventivo> iter = descargaJSON.iterator();
					while (iter.hasNext()) {
						JSONRecursoPreventivo elemento = iter.next();
						
						ContentValues values = new ContentValues();
						values.put("Codigo", elemento.getCod_JefeObra());
						values.put("Nombre", elemento.getNom_JefeObra());
												
						db.insert("recursosPreventivos", null, values);
						
					}
					
					db.close();
				}
				
				Log.i(this.getClass().getName(), "EJECUTAR RECURSOS PREVENTIVOS. Finalizar carga tabla.");
													
			}else{
				
				Log.e(this.getClass().getName(),  new Date().toString()+" - "+this.context.getResources().getString(R.string.error_SIncronizacion_NoAutorizado));

			}
						
		} catch (Exception e) {	
			
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());

		}
		
	}
	
	
	
}	
	
