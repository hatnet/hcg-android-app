package tec.telesupervision.Objects.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONPlanificacion {


	@JsonProperty("cod") private String codObra;
	@JsonProperty("tip") private String tipoObra;
	@JsonProperty("dir") private String Direccion;
	@JsonProperty("mun") private String Municipio;
	@JsonProperty("facep") private String FechaAceptacionNombramiento;
	@JsonProperty("car") private String CarpetasOonair;
	

	public JSONPlanificacion() {
		super();
	}


	public final String getCodObra() {
		return codObra;
	}


	public final void setCodObra(String codObra) {
		this.codObra = codObra;
	}
	
	public final String getTipoObra() {
		return tipoObra;
	}


	public final void setTipoObra(String tipoObra) {
		this.tipoObra = tipoObra;
	}

	public final String getDireccion() {
		return Direccion;
	}


	public final void setDireccion(String direccion) {
		Direccion = direccion;
	}


	public final String getMunicipio() {
		return Municipio;
	}


	public final void setMunicipio(String municipio) {
		Municipio = municipio;
	}


	public final String getCarpetasOonair() {
		return CarpetasOonair;
	}


	public final void setCarpetasOonair(String carpetasOonair) {
		CarpetasOonair = carpetasOonair;
	}

	public String getFechaAceptacionNombramiento() {
		return FechaAceptacionNombramiento;
	}
	

	public void setFechaAceptacionNombramiento(String fechaAceptacionNombramiento) {
		FechaAceptacionNombramiento = fechaAceptacionNombramiento;
	}

	
	
}
