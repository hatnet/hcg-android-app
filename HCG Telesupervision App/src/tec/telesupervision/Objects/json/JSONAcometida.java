package tec.telesupervision.Objects.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONAcometida {
	
	@JsonProperty("cod") private String codObra;
	@JsonProperty("sit") private String situacion;
	@JsonProperty("dsit") private String situacionDescripcion;
	@JsonProperty("dir") private String Direccion;
	@JsonProperty("mun") private String Municipio;
	@JsonProperty("car") private String CarpetasOonair;
	@JsonProperty("comen") private String Comentarios;
	@JsonProperty("facep") private String FechaAceptacionNombramiento;
	@JsonProperty("fplan") private String FechaPlanificacion;
	@JsonProperty("fini") private String FechaInicio;
	@JsonProperty("fpes") private String FechaPeS;
	@JsonProperty("ffin") private String FechaFinal;
	@JsonProperty("tsGODF") private String TelesupervisionGODF;
	@JsonProperty("tstelGODF") private String TelesupervisionTelefonoGODF;
	@JsonProperty("tsfplan") private String TelesupervisionInicioPlanificacion;
	@JsonProperty("tsfRes") private String TelesupervisionFechaResultado;
	@JsonProperty("tsRes") private String TelesupervisionResultado;
	@JsonProperty("hsObra") private String HistorialObra;
	
	public JSONAcometida() {
		super();
	}

	public String getCodObra() {
		return codObra;
	}

	public void setCodObra(String codObra) {
		this.codObra = codObra;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getSituacionDescripcion() {
		return situacionDescripcion;
	}

	public void setSituacionDescripcion(String situacionDescripcion) {
		this.situacionDescripcion = situacionDescripcion;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	public String getMunicipio() {
		return Municipio;
	}

	public void setMunicipio(String municipio) {
		Municipio = municipio;
	}

	public String getCarpetasOonair() {
		return CarpetasOonair;
	}

	public void setCarpetasOonair(String carpetasOonair) {
		CarpetasOonair = carpetasOonair;
	}

	public String getComentarios() {
		return Comentarios;
	}

	public void setComentarios(String comentarios) {
		Comentarios = comentarios;
	}

	public String getFechaAceptacionNombramiento() {
		return FechaAceptacionNombramiento;
	}

	public void setFechaAceptacionNombramiento(String fechaAceptacionNombramiento) {
		FechaAceptacionNombramiento = fechaAceptacionNombramiento;
	}

	public String getFechaPlanificacion() {
		return FechaPlanificacion;
	}

	public void setFechaPlanificacion(String fechaPlanificacion) {
		FechaPlanificacion = fechaPlanificacion;
	}

	public String getFechaInicio() {
		return FechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}

	public String getFechaPeS() {
		return FechaPeS;
	}

	public void setFechaPeS(String fechaPeS) {
		FechaPeS = fechaPeS;
	}

	public String getFechaFinal() {
		return FechaFinal;
	}

	public void setFechaFinal(String fechaFinal) {
		FechaFinal = fechaFinal;
	}

	public String getTelesupervisionGODF() {
		return TelesupervisionGODF;
	}

	public void setTelesupervisionGODF(String telesupervisionGODF) {
		TelesupervisionGODF = telesupervisionGODF;
	}

	public String getTelesupervisionTelefonoGODF() {
		return TelesupervisionTelefonoGODF;
	}

	public void setTelesupervisionTelefonoGODF(String telesupervisionTelefonoGODF) {
		TelesupervisionTelefonoGODF = telesupervisionTelefonoGODF;
	}

	public String getTelesupervisionInicioPlanificacion() {
		return TelesupervisionInicioPlanificacion;
	}

	public void setTelesupervisionInicioPlanificacion(
			String telesupervisionInicioPlanificacion) {
		TelesupervisionInicioPlanificacion = telesupervisionInicioPlanificacion;
	}

	public String getTelesupervisionFechaResultado() {
		return TelesupervisionFechaResultado;
	}

	public void setTelesupervisionFechaResultado(
			String telesupervisionFechaResultado) {
		TelesupervisionFechaResultado = telesupervisionFechaResultado;
	}

	public String getTelesupervisionResultado() {
		return TelesupervisionResultado;
	}

	public void setTelesupervisionResultado(String telesupervisionResultado) {
		TelesupervisionResultado = telesupervisionResultado;
	}

	public String getHistorialObra() {
		return HistorialObra;
	}

	public void setHistorialObra(String historialObra) {
		HistorialObra = historialObra;
	}

}

