package tec.telesupervision.Objects.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SincronizacionElemento {

	@JsonProperty("elmP") private JSONPlanificacion elementoPlanificacion;
    @JsonProperty("elmAco") private JSONAcometida elementoAcometida;
    @JsonProperty("elmO50") private JSONObra50m elementoObra50m;
    @JsonProperty("elmRP") private JSONRecursoPreventivo elementoRecursoPreventivo;
    @JsonProperty("elmM") private JSONMensaje elementoMensaje;
     
    public SincronizacionElemento() {
		super();
	}

	public JSONPlanificacion getElementoPlanificacion() {
		return elementoPlanificacion;
	}

	public void setElementoPlanificacion(JSONPlanificacion elementoPlanificacion) {
		this.elementoPlanificacion = elementoPlanificacion;
	}

	public JSONAcometida getElementoAcometida() {
		return elementoAcometida;
	}

	public void setElementoAcometida(JSONAcometida elementoAcometida) {
		this.elementoAcometida = elementoAcometida;
	}

	public JSONObra50m getElementoObra50m() {
		return elementoObra50m;
	}

	public void setElementoObra50m(JSONObra50m elementoObra50m) {
		this.elementoObra50m = elementoObra50m;
	}

	public JSONMensaje getElementoMensaje() {
		return elementoMensaje;
	}

	public void setElementoMensaje(JSONMensaje elementoMensaje) {
		this.elementoMensaje = elementoMensaje;
	}

	public JSONRecursoPreventivo getElementoRecursoPreventivo() {
		return elementoRecursoPreventivo;
	}

	public void setElementoRecursoPreventivo(JSONRecursoPreventivo elementoRecursoPreventivo) {
		this.elementoRecursoPreventivo = elementoRecursoPreventivo;
	}

	
}
