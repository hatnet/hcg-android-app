package tec.telesupervision.Objects.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONRecursoPreventivo {


	@JsonProperty("cod") private String cod_JefeObra;
	@JsonProperty("nom") private String nom_JefeObra;
	
	public JSONRecursoPreventivo() {
		super();
	}

	public String getCod_JefeObra() {
		return cod_JefeObra;
	}

	public void setCod_JefeObra(String cod_JefeObra) {
		this.cod_JefeObra = cod_JefeObra;
	}

	public String getNom_JefeObra() {
		return nom_JefeObra;
	}

	public void setNom_JefeObra(String nom_JefeObra) {
		this.nom_JefeObra = nom_JefeObra;
	}


	
	
	
}
