package tec.telesupervision.Objects.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JSONObra50m {

	
	@JsonProperty("cod") private String codObra;
	@JsonProperty("codjoc") private String cod_JefeObra;
	@JsonProperty("codrprev") private String cod_RecPreventivo;
	@JsonProperty("sit") private String situacion;
	@JsonProperty("dsit") private String situacionDescripcion;
	@JsonProperty("dir") private String Direccion;
	@JsonProperty("mun") private String Municipio;
	@JsonProperty("car") private String CarpetasOonair;
	@JsonProperty("comen") private String Comentarios;
	@JsonProperty("traz") private String Trazado;
	@JsonProperty("mtraz") private String TrazadoMod;
	@JsonProperty("facep") private String FechaAceptacionNombramiento;
	@JsonProperty("fplan") private String FechaPlanificacion;
	@JsonProperty("ftras") private String FechaTrazadoSolicitud;
	@JsonProperty("ftrac") private String FechaTrazadoConfirmacion;
	@JsonProperty("fmtras") private String FechaTrazadoSolicitudMod;
	@JsonProperty("fmtrac") private String FechaTrazadoConfirmacionMod;
	@JsonProperty("fini") private String FechaInicio;
	@JsonProperty("fsolpes") private String FechaSolicitudPeS;
	@JsonProperty("facepcon") private String FechaAcepContenidos;
	@JsonProperty("ffinprb") private String FechaFinalPrueba;
	@JsonProperty("faceppeg") private String FechaAceptadaPeG;
	@JsonProperty("fpes") private String FechaPeS;
	@JsonProperty("ffin") private String FechaFinal;
	@JsonProperty("tsGODF") private String TelesupervisionGODF;
	@JsonProperty("tstelGODF") private String TelesupervisionTelefonoGODF;
	@JsonProperty("tsfplan") private String TelesupervisionInicioPlanificacion;
	@JsonProperty("tsfRes") private String TelesupervisionFechaResultado;
	@JsonProperty("tsRes") private String TelesupervisionResultado;
	@JsonProperty("hsObra") private String HistorialObra;	
	
	
	public JSONObra50m() {
		super();
	}


	public String getCodObra() {
		return codObra;
	}


	public void setCodObra(String codObra) {
		this.codObra = codObra;
	}


	public String getCod_JefeObra() {
		return cod_JefeObra;
	}


	public void setCod_JefeObra(String cod_JefeObra) {
		this.cod_JefeObra = cod_JefeObra;
	}


	public String getCod_RecPreventivo() {
		return cod_RecPreventivo;
	}


	public void setCod_RecPreventivo(String cod_RecPreventivo) {
		this.cod_RecPreventivo = cod_RecPreventivo;
	}


	public String getSituacion() {
		return situacion;
	}


	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}


	public String getSituacionDescripcion() {
		return situacionDescripcion;
	}


	public void setSituacionDescripcion(String situacionDescripcion) {
		this.situacionDescripcion = situacionDescripcion;
	}


	public String getDireccion() {
		return Direccion;
	}


	public void setDireccion(String direccion) {
		Direccion = direccion;
	}


	public String getMunicipio() {
		return Municipio;
	}


	public void setMunicipio(String municipio) {
		Municipio = municipio;
	}


	public String getCarpetasOonair() {
		return CarpetasOonair;
	}


	public void setCarpetasOonair(String carpetasOonair) {
		CarpetasOonair = carpetasOonair;
	}


	public String getComentarios() {
		return Comentarios;
	}


	public void setComentarios(String comentarios) {
		Comentarios = comentarios;
	}


	public String getTrazado() {
		return Trazado;
	}


	public void setTrazado(String trazado) {
		Trazado = trazado;
	}


	public String getTrazadoMod() {
		return TrazadoMod;
	}


	public void setTrazadoMod(String trazadoMod) {
		TrazadoMod = trazadoMod;
	}


	public String getFechaAceptacionNombramiento() {
		return FechaAceptacionNombramiento;
	}


	public void setFechaAceptacionNombramiento(String fechaAceptacionNombramiento) {
		FechaAceptacionNombramiento = fechaAceptacionNombramiento;
	}


	public String getFechaPlanificacion() {
		return FechaPlanificacion;
	}


	public void setFechaPlanificacion(String fechaPlanificacion) {
		FechaPlanificacion = fechaPlanificacion;
	}


	public String getFechaTrazadoSolicitud() {
		return FechaTrazadoSolicitud;
	}


	public void setFechaTrazadoSolicitud(String fechaTrazadoSolicitud) {
		FechaTrazadoSolicitud = fechaTrazadoSolicitud;
	}


	public String getFechaTrazadoConfirmacion() {
		return FechaTrazadoConfirmacion;
	}


	public void setFechaTrazadoConfirmacion(String fechaTrazadoConfirmacion) {
		FechaTrazadoConfirmacion = fechaTrazadoConfirmacion;
	}


	public String getFechaTrazadoSolicitudMod() {
		return FechaTrazadoSolicitudMod;
	}


	public void setFechaTrazadoSolicitudMod(String fechaTrazadoSolicitudMod) {
		FechaTrazadoSolicitudMod = fechaTrazadoSolicitudMod;
	}


	public String getFechaTrazadoConfirmacionMod() {
		return FechaTrazadoConfirmacionMod;
	}


	public void setFechaTrazadoConfirmacionMod(String fechaTrazadoConfirmacionMod) {
		FechaTrazadoConfirmacionMod = fechaTrazadoConfirmacionMod;
	}


	public String getFechaInicio() {
		return FechaInicio;
	}


	public void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}


	public String getFechaSolicitudPeS() {
		return FechaSolicitudPeS;
	}


	public void setFechaSolicitudPeS(String fechaSolicitudPeS) {
		FechaSolicitudPeS = fechaSolicitudPeS;
	}


	public String getFechaAcepContenidos() {
		return FechaAcepContenidos;
	}


	public void setFechaAcepContenidos(String fechaAcepContenidos) {
		FechaAcepContenidos = fechaAcepContenidos;
	}


	public String getFechaFinalPrueba() {
		return FechaFinalPrueba;
	}


	public void setFechaFinalPrueba(String fechaFinalPrueba) {
		FechaFinalPrueba = fechaFinalPrueba;
	}


	public String getFechaAceptadaPeG() {
		return FechaAceptadaPeG;
	}


	public void setFechaAceptadaPeG(String fechaAceptadaPeG) {
		FechaAceptadaPeG = fechaAceptadaPeG;
	}


	public String getFechaPeS() {
		return FechaPeS;
	}


	public void setFechaPeS(String fechaPeS) {
		FechaPeS = fechaPeS;
	}


	public String getFechaFinal() {
		return FechaFinal;
	}


	public void setFechaFinal(String fechaFinal) {
		FechaFinal = fechaFinal;
	}


	public String getTelesupervisionGODF() {
		return TelesupervisionGODF;
	}


	public void setTelesupervisionGODF(String telesupervisionGODF) {
		TelesupervisionGODF = telesupervisionGODF;
	}


	public String getTelesupervisionTelefonoGODF() {
		return TelesupervisionTelefonoGODF;
	}


	public void setTelesupervisionTelefonoGODF(String telesupervisionTelefonoGODF) {
		TelesupervisionTelefonoGODF = telesupervisionTelefonoGODF;
	}


	public String getTelesupervisionInicioPlanificacion() {
		return TelesupervisionInicioPlanificacion;
	}


	public void setTelesupervisionInicioPlanificacion(
			String telesupervisionInicioPlanificacion) {
		TelesupervisionInicioPlanificacion = telesupervisionInicioPlanificacion;
	}


	public String getTelesupervisionFechaResultado() {
		return TelesupervisionFechaResultado;
	}


	public void setTelesupervisionFechaResultado(
			String telesupervisionFechaResultado) {
		TelesupervisionFechaResultado = telesupervisionFechaResultado;
	}


	public String getTelesupervisionResultado() {
		return TelesupervisionResultado;
	}


	public void setTelesupervisionResultado(String telesupervisionResultado) {
		TelesupervisionResultado = telesupervisionResultado;
	}


	public String getHistorialObra() {
		return HistorialObra;
	}


	public void setHistorialObra(String historialObra) {
		HistorialObra = historialObra;
	}



}
