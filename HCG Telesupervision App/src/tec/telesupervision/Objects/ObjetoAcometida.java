package tec.telesupervision.Objects;


public class ObjetoAcometida {
	
	private String Cod_obra;
	private String Situacion;
	private String SituacionDescripcion;
	private String Direccion;
	private String Municipio;
	private Boolean CarpetasOonair;
	private String Comentarios;
	private String FechaAcepNombramiento;
	private String FechaPlanificacion;
	private String FechaInicio;
	private String FechaPeS;
	private String FechaFinal;
	private String TSFechaPlanificacion;
	private String TSGestorObra;
	private String TSTelefonoGO;
	private String TSFechaResultado;
	private String TSResultado;
	private String HistorialObra;
		
	public ObjetoAcometida() {
		super();
	}

	public ObjetoAcometida(String cod_obra, String situacion,
			String situacionDescripcion, String direccion, String municipio,
			Boolean carpetasOonair, String comentarios,
			String fechaAcepNombramiento, String fechaPlanificacion,
			String fechaInicio, String fechaPeS, String fechaFinal,
			String tSFechaPlanificacion, String tSGestorObra,
			String tSTelefonoGO, String tSFechaResultado, String tSResultado,
			String historialObra) {
		super();
		Cod_obra = cod_obra;
		Situacion = situacion;
		SituacionDescripcion = situacionDescripcion;
		Direccion = direccion;
		Municipio = municipio;
		CarpetasOonair = carpetasOonair;
		Comentarios = comentarios;
		FechaAcepNombramiento = fechaAcepNombramiento;
		FechaPlanificacion = fechaPlanificacion;
		FechaInicio = fechaInicio;
		FechaPeS = fechaPeS;
		FechaFinal = fechaFinal;
		TSFechaPlanificacion = tSFechaPlanificacion;
		TSGestorObra = tSGestorObra;
		TSTelefonoGO = tSTelefonoGO;
		TSFechaResultado = tSFechaResultado;
		TSResultado = tSResultado;
		HistorialObra = historialObra;
	}

	public String getCod_obra() {
		return Cod_obra;
	}

	public void setCod_obra(String cod_obra) {
		Cod_obra = cod_obra;
	}

	public String getSituacion() {
		return Situacion;
	}

	public void setSituacion(String situacion) {
		Situacion = situacion;
	}

	public String getSituacionDescripcion() {
		return SituacionDescripcion;
	}

	public void setSituacionDescripcion(String situacionDescripcion) {
		SituacionDescripcion = situacionDescripcion;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	public String getMunicipio() {
		return Municipio;
	}

	public void setMunicipio(String municipio) {
		Municipio = municipio;
	}

	public Boolean getCarpetasOonair() {
		return CarpetasOonair;
	}

	public void setCarpetasOonair(Boolean carpetasOonair) {
		CarpetasOonair = carpetasOonair;
	}

	public String getComentarios() {
		return Comentarios;
	}

	public void setComentarios(String comentarios) {
		Comentarios = comentarios;
	}

	public String getFechaAcepNombramiento() {
		return FechaAcepNombramiento;
	}

	public void setFechaAcepNombramiento(String fechaAcepNombramiento) {
		FechaAcepNombramiento = fechaAcepNombramiento;
	}

	public String getFechaPlanificacion() {
		return FechaPlanificacion;
	}

	public void setFechaPlanificacion(String fechaPlanificacion) {
		FechaPlanificacion = fechaPlanificacion;
	}

	public String getFechaInicio() {
		return FechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}

	public String getFechaPeS() {
		return FechaPeS;
	}

	public void setFechaPeS(String fechaPeS) {
		FechaPeS = fechaPeS;
	}

	public String getFechaFinal() {
		return FechaFinal;
	}

	public void setFechaFinal(String fechaFinal) {
		FechaFinal = fechaFinal;
	}

	public String getTSFechaPlanificacion() {
		return TSFechaPlanificacion;
	}

	public void setTSFechaPlanificacion(String tSFechaPlanificacion) {
		TSFechaPlanificacion = tSFechaPlanificacion;
	}

	public String getTSGestorObra() {
		return TSGestorObra;
	}

	public void setTSGestorObra(String tSGestorObra) {
		TSGestorObra = tSGestorObra;
	}

	public String getTSTelefonoGO() {
		return TSTelefonoGO;
	}

	public void setTSTelefonoGO(String tSTelefonoGO) {
		TSTelefonoGO = tSTelefonoGO;
	}

	public String getTSFechaResultado() {
		return TSFechaResultado;
	}

	public void setTSFechaResultado(String tSFechaResultado) {
		TSFechaResultado = tSFechaResultado;
	}

	public String getTSResultado() {
		return TSResultado;
	}

	public void setTSResultado(String tSResultado) {
		TSResultado = tSResultado;
	}

	public String getHistorialObra() {
		return HistorialObra;
	}

	public void setHistorialObra(String historialObra) {
		HistorialObra = historialObra;
	}

}
