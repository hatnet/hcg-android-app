package tec.telesupervision.Objects;

public class ObjetoPlanificacion {
	
	private String Cod_obra;
	private String Tipo_obra;
	private String Direccion;
	private String Municipio;
	
	private Boolean seleccionada;
	private Boolean carpetasOonair;
	
	
	
	public ObjetoPlanificacion() {
		super();
	}



	public ObjetoPlanificacion(String cod_obra, String tipo_obra, String direccion, String municipio,
			    			Boolean seleccionada, Boolean carpetasOonair) {
		super();
		this.Cod_obra = cod_obra;
		this.Tipo_obra = tipo_obra;
		this.Direccion = direccion;
		this.Municipio = municipio;
		this.seleccionada = seleccionada;
		this.carpetasOonair = carpetasOonair;
	}



	public final String getCod_obra() {
		return Cod_obra;
	}

	public final void setCod_obra(String cod_obra) {
		Cod_obra = cod_obra;
	}

	public final String getTipo_obra() {
		return Tipo_obra;
	}

	public final void setTipo_obra(String tipo_obra) {
		Tipo_obra = tipo_obra;
	}


	public final String getDireccion() {
		return Direccion;
	}

	public final void setDireccion(String direccion) {
		Direccion = direccion;
	}



	public final String getMunicipio() {
		return Municipio;
	}



	public final void setMunicipio(String municipio) {
		Municipio = municipio;
	}

	
	public final Boolean getSeleccionada() {
		return seleccionada;
	}



	public final void setSeleccionada(Boolean seleccionada) {
		this.seleccionada = seleccionada;
	}



	public final Boolean getCarpetasOonair() {
		return carpetasOonair;
	}



	public final void setCarpetasOonair(Boolean carpetasOonair) {
		this.carpetasOonair = carpetasOonair;
	}

}
