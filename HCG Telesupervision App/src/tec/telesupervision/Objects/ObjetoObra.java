package tec.telesupervision.Objects;

public class ObjetoObra {
	
	private String Cod_obra;
	private String Perfil;
	private String TipoObra;
	private String Situacion;
	private String SituacionDescripcion;
	private String Direccion;
	private String Municipio;
	private Boolean CarpetasOonair;
	private String Comentarios;
	private String Trazado;
	private String TrazadoMod;
	private String FechaAcepNombramiento;
	private String FechaPlanificacion;
	private String FechaInicio;
	private String FechaTrazadoSolicitud;
	private String FechaTrazadoConfirmacion;
	private String FechaTrazadoSolicitudMod;
	private String FechaTrazadoConfirmacionMod;
	private String FechaSolicitudPeS;
	private String FechaAcepContenidos;
	private String FechaFinalPrueba;
	private String FechaAceptadaPeG;
	private String FechaPeS;
	private String FechaFinal;
	private String TSFechaPlanificacion;
	private String TSGestorObra;
	private String TSTelefonoGO;
	private String TSFechaResultado;
	private String TSResultado;
	private String HistorialObra;
	
	
	public ObjetoObra() {
		super();
		
	}


	public ObjetoObra(String cod_obra, String perfil, String tipoObra,
			String situacion, String situacionDescripcion, String direccion,
			String municipio, Boolean carpetasOonair, String comentarios,
			String trazado, String trazadoMod, String fechaAcepNombramiento,
			String fechaPlanificacion, String fechaInicio,
			String fechaTrazadoSolicitud, String fechaTrazadoConfirmacion,
			String fechaTrazadoSolicitudMod,
			String fechaTrazadoConfirmacionMod, String fechaSolicitudPeS,
			String fechaAcepContenidos, String fechaFinalPrueba,
			String fechaAceptadaPeG, String fechaPeS, String fechaFinal,
			String tSFechaPlanificacion, String tSGestorObra,
			String tSTelefonoGO, String tSFechaResultado, String tSResultado,
			String historialObra) {
		super();
		Cod_obra = cod_obra;
		Perfil = perfil;
		TipoObra = tipoObra;
		Situacion = situacion;
		SituacionDescripcion = situacionDescripcion;
		Direccion = direccion;
		Municipio = municipio;
		CarpetasOonair = carpetasOonair;
		Comentarios = comentarios;
		Trazado = trazado;
		TrazadoMod = trazadoMod;
		FechaAcepNombramiento = fechaAcepNombramiento;
		FechaPlanificacion = fechaPlanificacion;
		FechaInicio = fechaInicio;
		FechaTrazadoSolicitud = fechaTrazadoSolicitud;
		FechaTrazadoConfirmacion = fechaTrazadoConfirmacion;
		FechaTrazadoSolicitudMod = fechaTrazadoSolicitudMod;
		FechaTrazadoConfirmacionMod = fechaTrazadoConfirmacionMod;
		FechaSolicitudPeS = fechaSolicitudPeS;
		FechaAcepContenidos = fechaAcepContenidos;
		FechaFinalPrueba = fechaFinalPrueba;
		FechaAceptadaPeG = fechaAceptadaPeG;
		FechaPeS = fechaPeS;
		FechaFinal = fechaFinal;
		TSFechaPlanificacion = tSFechaPlanificacion;
		TSGestorObra = tSGestorObra;
		TSTelefonoGO = tSTelefonoGO;
		TSFechaResultado = tSFechaResultado;
		TSResultado = tSResultado;
		HistorialObra = historialObra;
	}


	public String getCod_obra() {
		return Cod_obra;
	}


	public void setCod_obra(String cod_obra) {
		Cod_obra = cod_obra;
	}


	public String getPerfil() {
		return Perfil;
	}


	public void setPerfil(String perfil) {
		Perfil = perfil;
	}


	public String getTipoObra() {
		return TipoObra;
	}


	public void setTipoObra(String tipoObra) {
		TipoObra = tipoObra;
	}


	public String getSituacion() {
		return Situacion;
	}


	public void setSituacion(String situacion) {
		Situacion = situacion;
	}


	public String getSituacionDescripcion() {
		return SituacionDescripcion;
	}


	public void setSituacionDescripcion(String situacionDescripcion) {
		SituacionDescripcion = situacionDescripcion;
	}


	public String getDireccion() {
		return Direccion;
	}


	public void setDireccion(String direccion) {
		Direccion = direccion;
	}


	public String getMunicipio() {
		return Municipio;
	}


	public void setMunicipio(String municipio) {
		Municipio = municipio;
	}


	public Boolean getCarpetasOonair() {
		return CarpetasOonair;
	}


	public void setCarpetasOonair(Boolean carpetasOonair) {
		CarpetasOonair = carpetasOonair;
	}


	public String getComentarios() {
		return Comentarios;
	}


	public void setComentarios(String comentarios) {
		Comentarios = comentarios;
	}


	public String getTrazado() {
		return Trazado;
	}


	public void setTrazado(String trazado) {
		Trazado = trazado;
	}


	public String getTrazadoMod() {
		return TrazadoMod;
	}


	public void setTrazadoMod(String trazadoMod) {
		TrazadoMod = trazadoMod;
	}


	public String getFechaAcepNombramiento() {
		return FechaAcepNombramiento;
	}


	public void setFechaAcepNombramiento(String fechaAcepNombramiento) {
		FechaAcepNombramiento = fechaAcepNombramiento;
	}


	public String getFechaPlanificacion() {
		return FechaPlanificacion;
	}


	public void setFechaPlanificacion(String fechaPlanificacion) {
		FechaPlanificacion = fechaPlanificacion;
	}


	public String getFechaInicio() {
		return FechaInicio;
	}


	public void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}


	public String getFechaTrazadoSolicitud() {
		return FechaTrazadoSolicitud;
	}


	public void setFechaTrazadoSolicitud(String fechaTrazadoSolicitud) {
		FechaTrazadoSolicitud = fechaTrazadoSolicitud;
	}


	public String getFechaTrazadoConfirmacion() {
		return FechaTrazadoConfirmacion;
	}


	public void setFechaTrazadoConfirmacion(String fechaTrazadoConfirmacion) {
		FechaTrazadoConfirmacion = fechaTrazadoConfirmacion;
	}


	public String getFechaTrazadoSolicitudMod() {
		return FechaTrazadoSolicitudMod;
	}


	public void setFechaTrazadoSolicitudMod(String fechaTrazadoSolicitudMod) {
		FechaTrazadoSolicitudMod = fechaTrazadoSolicitudMod;
	}


	public String getFechaTrazadoConfirmacionMod() {
		return FechaTrazadoConfirmacionMod;
	}


	public void setFechaTrazadoConfirmacionMod(String fechaTrazadoConfirmacionMod) {
		FechaTrazadoConfirmacionMod = fechaTrazadoConfirmacionMod;
	}


	public String getFechaSolicitudPeS() {
		return FechaSolicitudPeS;
	}


	public void setFechaSolicitudPeS(String fechaSolicitudPeS) {
		FechaSolicitudPeS = fechaSolicitudPeS;
	}


	public String getFechaAcepContenidos() {
		return FechaAcepContenidos;
	}


	public void setFechaAcepContenidos(String fechaAcepContenidos) {
		FechaAcepContenidos = fechaAcepContenidos;
	}


	public String getFechaFinalPrueba() {
		return FechaFinalPrueba;
	}


	public void setFechaFinalPrueba(String fechaFinalPrueba) {
		FechaFinalPrueba = fechaFinalPrueba;
	}


	public String getFechaAceptadaPeG() {
		return FechaAceptadaPeG;
	}


	public void setFechaAceptadaPeG(String fechaAceptadaPeG) {
		FechaAceptadaPeG = fechaAceptadaPeG;
	}


	public String getFechaPeS() {
		return FechaPeS;
	}


	public void setFechaPeS(String fechaPeS) {
		FechaPeS = fechaPeS;
	}


	public String getFechaFinal() {
		return FechaFinal;
	}


	public void setFechaFinal(String fechaFinal) {
		FechaFinal = fechaFinal;
	}


	public String getTSFechaPlanificacion() {
		return TSFechaPlanificacion;
	}


	public void setTSFechaPlanificacion(String tSFechaPlanificacion) {
		TSFechaPlanificacion = tSFechaPlanificacion;
	}


	public String getTSGestorObra() {
		return TSGestorObra;
	}


	public void setTSGestorObra(String tSGestorObra) {
		TSGestorObra = tSGestorObra;
	}


	public String getTSTelefonoGO() {
		return TSTelefonoGO;
	}


	public void setTSTelefonoGO(String tSTelefonoGO) {
		TSTelefonoGO = tSTelefonoGO;
	}


	public String getTSFechaResultado() {
		return TSFechaResultado;
	}


	public void setTSFechaResultado(String tSFechaResultado) {
		TSFechaResultado = tSFechaResultado;
	}


	public String getTSResultado() {
		return TSResultado;
	}


	public void setTSResultado(String tSResultado) {
		TSResultado = tSResultado;
	}


	public String getHistorialObra() {
		return HistorialObra;
	}


	public void setHistorialObra(String historialObra) {
		HistorialObra = historialObra;
	}


}
