package tec.telesupervision.Objects;

public class ObjetoRecursoPreventivo {

	
	private String Cod_JefeObra;
	private String Nom_JefeObra;
	private boolean Seleccionado;
	
	public ObjetoRecursoPreventivo() {
		super();
	}

	public ObjetoRecursoPreventivo(String cod_JefeObra, String nom_JefeObra) {
		super();
		Cod_JefeObra = cod_JefeObra;
		Nom_JefeObra = nom_JefeObra;
	}

	public String getCod_JefeObra() {
		return Cod_JefeObra;
	}

	public void setCod_JefeObra(String cod_JefeObra) {
		Cod_JefeObra = cod_JefeObra;
	}

	public String getNom_JefeObra() {
		return Nom_JefeObra;
	}

	public void setNom_JefeObra(String nom_JefeObra) {
		Nom_JefeObra = nom_JefeObra;
	}

	public boolean isSeleccionado() {
		return Seleccionado;
	}

	public void setSeleccionado(boolean seleccionado) {
		Seleccionado = seleccionado;
	}
	
	

	
}
