package tec.telesupervision.utilidades;

import java.io.File;

import tec.telesupervision.utilidades.UtilidadesArchivos;
import android.os.Environment;
import android.util.Log;

public class GenerarLog {
	
	private static String BASE_DIRECTORY = "DiabeticControls";
	private static String NOMBRE_LOG = "Log_DiabeticControls.txt";
			
	//String application
	public static void generarFicheroLog() {		
			
		try {
			
			Log.i("tec.util.GeneralLog", "Petición generación log");
			
			String pathBase = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + BASE_DIRECTORY;
			String pathLog = pathBase + "/" + NOMBRE_LOG;
			
			//Generar la sentencia a ejecutar en la shell
			String comando = "logcat -f "+ pathLog; // + " *:V " + application + ":V ";	
			
			// Comprobar si existe el directorio donde almacenar los logs.
			File directorioBase = new File(pathBase);
			if (directorioBase.exists() == false) {
				UtilidadesArchivos ua = new UtilidadesArchivos();
				ua.crearDirectorio(pathBase);
				ua = null;
			}
			
			Runtime.getRuntime().exec(comando);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("tec.util.GeneralLog", e.getStackTrace().toString());
		}
		
	}

}
