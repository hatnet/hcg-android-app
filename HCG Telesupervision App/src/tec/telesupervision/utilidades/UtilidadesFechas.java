package tec.telesupervision.utilidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.util.Log;

public class UtilidadesFechas {
	
	public static final int FECHA_MENOR = -1;
	public static final int FECHA_IGUAL = 0;
	public static final int FECHA_MAYOR = 1;
	
	/**
	 * 
	 * @author	mjulia
	 * @date	03/04/2013
	 * @title	getNumSemanaActual
	 * @comment	Devuelve el n�mero de la semana dentro del a�o
	 *
	 * @return	N�mero de la semana dentro del a�o
	 *
	 */
	public int getNumSemanaActual() {
		
		int numSemana = -1;
		
		GregorianCalendar c1 = new GregorianCalendar();
		c1.setTime(new Date());
		
		numSemana = c1.get(Calendar.WEEK_OF_YEAR);		
		return numSemana;
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	03/04/2013
	 * @title	getNumMesActual
	 * @comment	Devuelve el n�mero del mes
	 *
	 * @return	N�mero del mes
	 *
	 */
	public int getNumMesActual() {
		
		int numMes = -1;
		
		GregorianCalendar c1 = new GregorianCalendar();
		c1.setTime(new Date());
		
		numMes =  c1.get(Calendar.MONTH);
		return numMes;
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	03/04/2013
	 * @title	getNumTrimestreActual
	 * @comment	Devuelve el trimestre actual
	 *
	 * @return	Trimestre actual
	 *
	 */
	public int getNumTrimestreActual() {
		
		int numTrimestre = -1;
		
		GregorianCalendar c1 = new GregorianCalendar();
		c1.setTime(new Date());
		
		int numMes =  c1.get(Calendar.MONTH);
		
		if (numMes >= 1 && numMes <= 3) {
			numTrimestre = 1;
		}
		
		if (numMes >= 4 && numMes <= 6) {
			numTrimestre = 2;
		}
		
		if (numMes >= 7 && numMes <= 9) {
			numTrimestre = 3;
		}
		
		if (numMes >= 10 && numMes <= 13) {
			numTrimestre = 4;
		}
		
		return numTrimestre;
	}
	
	public int getAnyoActual() {
		
		int anyo = -1;
		
		GregorianCalendar c1 = new GregorianCalendar();
		c1.setTime(new Date());
		
		anyo = c1.get(Calendar.YEAR);
		
		return anyo;
	}

	/**
	 * 
	 * @author	mjulia
	 * @date	17/10/2012
	 * @title	fechaAddMinutos
	 * @comment	A�ade un n�mero de minutos a una fecha
	 *
	 * @param fch		fecha
	 * @param minutos	n�mero de minutos
	 * @return	
	 *
	 */
	public Date fechaAddMinutos(Date fch, int minutos) {
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(fch.getTime());
        cal.add(Calendar.MINUTE, minutos);
        return new Date(cal.getTimeInMillis());
    }
	
	/**
	 * 
	 * @author	mjulia
	 * @date	17/10/2012
	 * @title	fechaAddHoras
	 * @comment	A�ade un n�mero de horas a una fecha
	 *
	 * @param fch	fecha
	 * @param horas	n�mero de horas
	 * @return	
	 *
	 */
	public Date fechaAddHoras(Date fch, int horas) {
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(fch.getTime());
        cal.add(Calendar.HOUR, horas);
        return new Date(cal.getTimeInMillis());
    }
	
	/**
	 * 
	 * @author	mjulia
	 * @date	17/10/2012
	 * @title	fechaAddDias
	 * @comment	A�ade un numero de dias a una fecha
	 *
	 * @param fch	fecha
	 * @param dias	n�mero de d�as
	 * @return	
	 *
	 */
	public Date fechaAddDias(Date fch, int dias) {
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(fch.getTime());
        cal.add(Calendar.DATE, dias);
        return new Date(cal.getTimeInMillis());
    }
	
	/**
	 * 
	 * @author	mjulia
	 * @date	17/10/2012
	 * @title	fechaAddDias
	 * @comment	A�ade un numero de dias a una fecha
	 *
	 * @param fch	fecha
	 * @param dias	n�mero de d�as
	 * @return	
	 *
	 */
	public Date fechaAddDiasLaborables(Date fch, int dias) {
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(fch.getTime());
        cal.add(Calendar.DATE, dias);
        
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
        	cal.add(Calendar.DATE, 2);
        }
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
        	cal.add(Calendar.DATE, 1);
        }
        
        return new Date(cal.getTimeInMillis());
    }

	/**
	 * 
	 * @author	mjulia
	 * @date	17/10/2012
	 * @title	fechaAddMeses
	 * @comment	A�ade un numero de messes a una fecha
	 *
	 * @param fch	fecha
	 * @param meses	n�mero de messes
	 * @return	
	 *
	 */
    public Date fechaAddMeses(Date fch, int meses) {
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(fch.getTime());
        cal.add(Calendar.MONTH, meses);
        return new Date(cal.getTimeInMillis());
    }

    /**
     * 
     * @author	mjulia
     * @date	17/10/2012
     * @title	fechaEnRango
     * @comment	Comprueba que una fecha este dentro de un rango de fechas
     *
     * @param fechaRango1		Fecha inicial del rango 
     * @param fechaRango2		Fecha final del rango
     * @param fechaComparacion	Fecha a comprobar si esta dentro del rango
     * @return					true si la fecha esta dentro del rango, false en caso contrario.
     *
     */
    public boolean fechaEnRango(Date fechaRango1, Date fechaRango2, Date fechaComparacion) {  
    	
    	boolean ok = false;
    	
    	Calendar cal1 = new GregorianCalendar();
    	cal1.setTime(fechaRango1);
    	
    	Calendar cal2 = new GregorianCalendar();
    	cal2.setTime(fechaRango2);
    	
    	Calendar cal3 = new GregorianCalendar();
    	cal3.setTime(fechaComparacion);
    	
    	if (cal3.after(cal1) && cal3.before(cal2)){
    		ok = true;    		
		}
    	
    	return ok;
    	
    }
    
    /**
     * 
     * @author	jjulia
     * @date	19/4/2013
     * @title	convertirStringToFecha
     * @comment	validarFecha 
     * @param fecha
     * @param formatoFechaBD
     * @return
     */
    public boolean validarFecha(String fecha,String formatoFechaBD) {  
    	  
    	if (fecha == null)  
    	return false;  
    	  
    	SimpleDateFormat dateFormat = new SimpleDateFormat(formatoFechaBD); 
    	  
    	if (fecha.trim().length() != dateFormat.toPattern().length())  
    	return false;  
    	  
    	dateFormat.setLenient(false);  
    	  
    	try {  
    	dateFormat.parse(fecha.trim());  
    	}  
    	catch (ParseException pe) {  
    	return false;  
    	}  
    	return true;  
    	
    }  
    
    /**
     * 
     *
     * @author	jfernandez
     * @date	19/10/2012
     * @title	convertirStringToFecha
     * @comment	Convierte un Strings a un Date
     *
     * @param fecha
     * @param formatoFechaBD
     * @return
     * @throws ParseException
     *
     */
    public Date convertirStringToFecha(String fecha,String formatoFechaBD) {
		
    	Date fechaOut =null;
			    	
				try {
					SimpleDateFormat formatoDelTexto = new SimpleDateFormat(formatoFechaBD);
					fechaOut = formatoDelTexto.parse(fecha);
				} catch (ParseException e) {
					Log.e(this.getClass().getName().toString(), e.toString());
					e.printStackTrace();
				}
    
		return fechaOut;   	    	   	    	
    }
    
    /**
     * 
     * @author	mjulia
     * @date	07/11/2012	
     * @title	convertirFechaToString
     * @comment	Convierte una fecha a un String con el formato recivido 
     *
     * @param fecha			Fecha a convertir
     * @param formatoFecha	Formato de fecha
     * @return				String con la fecha formateada.
     *
     */
    public String convertirFechaToString(Date fecha,String formatoFecha) {    	
    	
    	SimpleDateFormat sd = new SimpleDateFormat(formatoFecha);
    	String fechaRetorno =  sd.format(fecha);
    	return fechaRetorno;
    	
    }
    
    /**
     * 
     * @author	mjulia
     * @date	18/12/2012
     * @title	convertirFechaFormato
     * @comment	Convierte una fecha un formato determinado
     *
     * @param fecha				fecha a convertir
     * @param formatoFecha		formato de conversi�n
     * @return					Date con el formato indicado.
     * @throws ParseException	
     *
     */
    public Date convertirFechaFormato(Date fecha, String formatoFecha) throws ParseException {
    	
    	SimpleDateFormat sd = new SimpleDateFormat(formatoFecha);
    	String fechaRetorno =  sd.format(fecha);
    	Date fec = sd.parse(fechaRetorno);
    	return fec;
    	
    }

    /**
     * 
     * @author	mjulia
     * @date	07/11/2012
     * @title	diffDiasEntreFechas
     * @comment	Obtiene la diferencia de d�as entre dos fechas
     *
     * @param fechaFinal		Fecha final
     * @param fechaInicial		Fecha final
     */
    public long diffDiasEntreFechas(Date fechaInicial, Date fechaFinal) {
    	
    	long numDias = 0;
    	
    	GregorianCalendar c1 = new GregorianCalendar();
    	GregorianCalendar c2 = new GregorianCalendar();
    	
    	c1.setTime(fechaInicial);
    	c2.setTime(fechaFinal);    	
    	
    	numDias = c2.get(Calendar.DAY_OF_YEAR) - c1.get(Calendar.DAY_OF_YEAR);
    	
    	return numDias;
    }
    
    /**
     * 
     * @author	mjulia
     * @date	26/11/2012
     * @title	compararFechas
     * @comment	Compara dos fechas
     *
     * @param fecha				Fecha a comparar
     * @param fechaComparacion	Fecha con la que se compara.
     * @return					
     * 							Devuelve -1, 0 o 1 dependiendo del resultado de la comparaci�n.
     * 							UtilidadesFechas.FECHA_MENOR si la fecha es menor = -1
     * 							UtilidadesFechas.FECHA_IGUAL si la fecha es igual = 0
     * 							UtilidadesFechas.FECHA_MAYOR si la fecha es mayor = 1
     *
     */
    public int compararFechas(Date fecha, Date fechaComparacion) {
    	
    	GregorianCalendar c1 = new GregorianCalendar();
    	GregorianCalendar c2 = new GregorianCalendar();
    	
    	c1.setTime(fecha);
    	c2.setTime(fechaComparacion);
    	
    	return c1.compareTo(c2);
    	
    }
}
