package tec.telesupervision.utilidades;

public final class UtilidadesMatematicas {
	
	public UtilidadesMatematicas () {
	}
 
	public static boolean esPar(int x) {
		if ((x % 2) == 0) {
			return true;
		}
 
		return false;
	}
 
	public static boolean esImpar(int x) {
		return !esPar(x);
	}
	
	
	public void redimensionarArray(Object obj[],int i)    {
        Object[] tempVar = new Object[i];
        if (obj != null)
                 System.arraycopy(obj, 0, tempVar, 0, Math.min(obj.length, tempVar.length));
        obj=tempVar;
 }
	
}