package tec.telesupervision.utilidades;

import java.util.Locale;
import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;


/***
 * 
 * @author	jibanez
 * @date	20/03/2013
 * @title	
 * @comment	
 *	
 *
 */
public class UtilidadesTTS implements OnInitListener {
	
	private final String EMPTY = new String();
	
	private Context contexto;
	private OnInitListener listenerLocutor;
	private String textoLocutor;
	
	private TextToSpeech locutor;
	private Locale loc;
	
	private String codigoLenguaje;
	
	/***
	 * 
	 * @author	jibanez
	 * @date	20/03/2013
	 * @title	
	 * @comment	
	 *
	 * @param c	
	 *
	 */
	public UtilidadesTTS(Context c, String idioma) {
		
		inicializaClase(c,idioma,null,null);
	}
	
	/***
	 * 
	 * @author	jibanez
	 * @date	20/03/2013
	 * @title	
	 * @comment	
	 *
	 * @param c
	 * @param listener	
	 *
	 */
	public UtilidadesTTS(Context c, String idioma, OnInitListener listener) {
		
		inicializaClase(c,idioma,listener,null);
	}
	
	/***
	 * 
	 * @author	jibanez
	 * @date	20/03/2013
	 * @title	
	 * @comment	
	 *
	 * @param c
	 * @param listener
	 * @param texto	
	 *
	 */
	public UtilidadesTTS(Context c, String idioma, OnInitListener listener, String texto) {
		
		inicializaClase(c,idioma,listener,texto);
	}
	
	/***
	 * 
	 * @author	jibanez
	 * @date	20/03/2013
	 * @title	
	 * @comment	
	 *
	 * @param c
	 * @param listener
	 * @param texto	
	 *
	 */
	private void inicializaClase(Context c, String idioma, OnInitListener listener, String texto) {
		
		this.codigoLenguaje = idioma;
		
		String tmp[] = this.codigoLenguaje.split("_");
		
		this.loc = new Locale(tmp[0],tmp[1]);
		
		this.contexto = c;
		this.listenerLocutor = listener;
		this.textoLocutor = texto;

		if(listener==null) {
			this.listenerLocutor = this;
		}
		if(texto==null) {
			this.textoLocutor = this.EMPTY;
		}
		
		this.locutor = new TextToSpeech(this.contexto,this.listenerLocutor);

	}
	
	/***
	 * 
	 * @author	jibanez
	 * @date	26/03/2013
	 * @title	
	 * @comment	
	 *	
	 *
	 */
	public void dispose() {
		 
		if(this.locutor!=null) {
			this.locutor.stop();
			this.locutor.shutdown();
			this.locutor=null;
		}
	}
	
	
	/***
	 * 
	 */
	public void onInit(int status) {

		if(this.textoLocutor.compareTo(this.EMPTY)!=0) {
			this.procesarTextoTTS();
		}
	}
	
	/***
	 * 
	 * @author	jibanez
	 * @date	20/03/2013
	 * @title	
	 * @comment	
	 *
	 * @return	
	 *
	 */
	public boolean procesarTextoTTS() {
		
		boolean Ok = true;
		
		try {
			int ret = this.locutor.setLanguage(this.loc);
			Log.d("RET", Integer.toString(ret));
			if(ret==TextToSpeech.LANG_MISSING_DATA || ret==TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.d("procesarTextoTTS 1", "IDIOMA " + this.loc + " NO SOPORTADO.");
			}else {
				Log.d("procesarTextoTTS 1", "IDIOMA " + this.loc + " SOPORTADO.");
				locutor.speak(this.textoLocutor, TextToSpeech.QUEUE_ADD, null);
			}
									
		}catch(Exception e) {
			Ok = false;
			Log.e("procesarTextoTTS", e.getMessage() + ": " + this.textoLocutor);
		}
		
		return Ok;
	}
	
	/***
	 * 
	 * @author	jibanez
	 * @date	20/03/2013
	 * @title	
	 * @comment	
	 *
	 * @param texto
	 * @return	
	 *
	 */
	public boolean procesarTextoTTS(String texto) {
		
		boolean Ok = true;
		
		try {
			int ret = this.locutor.setLanguage(this.loc);
			Log.d("RET", Integer.toString(ret));
			
			if(ret==TextToSpeech.LANG_MISSING_DATA || ret==TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.d("procesarTextoTTS 2", "IDIOMA " + this.loc + " NO SOPORTADO.");
			}else {
				Log.d("procesarTextoTTS 2", "IDIOMA " + this.loc + " SOPORTADO.");
				locutor.speak(texto, TextToSpeech.QUEUE_ADD, null);
			}
			
		}catch(Exception e) {
			Ok = false;
			Log.e("procesarTextoTTS", e.getMessage() + ": " + texto);
		}
		
		return Ok;
	}

	
	

}
