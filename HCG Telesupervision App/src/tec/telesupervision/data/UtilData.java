package tec.telesupervision.data;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Locale;

import tec.telesupervision.Objects.ClaseListItem;
import tec.telesupervision.Objects.ObjetoAcometida;
import tec.telesupervision.Objects.ObjetoObra;
import tec.telesupervision.Objects.ObjetoObra50m;
import tec.telesupervision.Objects.ObjetoPlanificacion;
import tec.telesupervision.Objects.ObjetoRecursoPreventivo;
import tec.telesupervision.Objects.ObjetoTabla;
import tec.telesupervision.adaptador.AdaptadorVertical_Acometida;
import tec.telesupervision.adaptador.AdaptadorVertical_Obra50m;
import tec.telesupervision.adaptador.AdaptadorVertical_Planificacion;
import tec.telesupervision.adaptador.AdaptadorVertical_RecursoPreventivo;
import tec.telesupervision.data.instruccionesSQL.ordenacion;
import tec.telesupervision.ver2.R;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;


/**
 * 
 * @author	mjulia
 * @date	26/09/2012
 * @title	UtilData
 * @comment	Utilidades generales para la obteneci�n de datos de la base de datos.
 *
 */
public class UtilData {
	
	private Context c;
	
	private String claveValue = "*";
	private String claveTexto = "-- Seleccione --";

	//Declaraci�n de las URI necessarias para los m�todos que usan el proveedor de base de datos.
    //public static final Uri URI_CONFIGURACION_GENERAL = Uri.parse("content://tec.dc.providers/configuracionGeneral");
    //public static final Uri URI_MENSAJES = Uri.parse("content://tec.dc.providers/mensajes");
	//private static final Uri URI_MEDICIONES = Uri.parse("content://tec.dc.providers/mediciones");
	//private static final Uri URI_ALARMAS = Uri.parse("content://tec.dc.providers/alarmas");
	
	//Constantes para las fechas y el tiempo
	public static final String FORMAT_DATE_SHORT_BD = "yyyy-MM-dd";
	public static final String FORMAT_DATE_FULL_BD = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_DATE_FULL_CONVERT = "dd-MM-yyyy HH:mm:ss";
	public static final String FORMAT_DATE_FULL_VISUAL = "dd-MM-yyyy HH:mm";
	
	public static final String FORMAT_DATE_OUT = "dd-MM-yyyy";
	public static final String FORMAT_TIME_OUT = "HH:mm:ss";
	
	public static final String FORMAT_DATE_MONTH = "dd-MM";
	public static final String FORMAT_HOUR_MINUTE = "HH:mm";
	
	public static final String TABLA_IDIOMAS = "IDIOMAS";
	
	/**
	 * 
	 * @author	mjulia
	 * @date	26/09/2012
	 * @title	UtilData
	 * @comment	Constructor de la clase.
	 *
	 * @param c	Contexto de la aplicaci�n.
	 *
	 */
	public UtilData(Context c) {
		super();
		this.c = c;
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	26/09/2012
	 * @title	UtilData
	 * @comment	Constructor de la clase.
	 *
	 *
	 */
	public UtilData(){
		super();
	}

	/**
	 * 
	 * @author	jjulia
	 * @date	22/04/2013
	 * @title	obtenerTabla
	 * @comment	Obtiene una lista con los elementos de una determinada Tabla
	 *
	 * @param tabla			Id de la tabla de la que obtener la descripci�n.
	 * @return				Lista con los elementos de una determinada tabla.
	 *
	 */
	public ArrayList<ObjetoTabla> obtenerTabla(String tabla){
			
		ArrayList<ObjetoTabla> lista = new ArrayList<ObjetoTabla>();
		
		String sql = "SELECT * FROM tablas WHERE IDTabla = '"+ tabla.trim() +"'";
		
		DataBase db = new DataBase(this.c);
		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(sql, null);
		while (c.moveToNext()){
			ObjetoTabla tab= new ObjetoTabla();
			String sNum1 = c.getString(c.getColumnIndex("Num1"));
			String sNum2 = c.getString(c.getColumnIndex("Num2"));
			String sValorMin = c.getString(c.getColumnIndex("ValorMin"));
			String sValorMax = c.getString(c.getColumnIndex("ValorMax"));
			if (sNum1 != null){sNum1 = sNum1.replace(",", ".");} else {sNum1="0";}
			if (sNum2 != null){sNum2 = sNum2.replace(",", ".");} else {sNum2="0";}
			if (sValorMin != null){sValorMin = sValorMin.replace(",", ".");} else {sValorMin="0";}
			if (sValorMax != null){sValorMax = sValorMax.replace(",", ".");} else {sValorMax="0";}
			
			tab.setIdElemento(c.getString(c.getColumnIndex("IDElemento")));
			tab.setDescripcion(c.getString(c.getColumnIndex("Descripcion")));
			tab.setAlfa1(c.getString(c.getColumnIndex("Alfa1")));
			tab.setAlfa2(c.getString(c.getColumnIndex("Alfa2")));
			tab.setNum1(new BigDecimal(sNum1));
			tab.setNum2(new BigDecimal(sNum2));
			tab.setValorMin(new BigDecimal(sValorMin));
			tab.setValorMax(new BigDecimal(sValorMax));
			lista.add(tab);
		}
		
		c.close();
		db.close();
		
		return lista;
		
	}
	

	
	/**
	 * 
	 * @author	mjulia
	 * @date	08/05/2013
	 * @title	obtenerParametrosConfiguracionGeneral
	 * @comment	Obtiene un ContentValues con los valores de la tabla configuracionGeneral
	 * 			key del ContentValues = clave de configuracionGeneral
	 * 			value del ContentValues = valor de configuracionGeneral
	 *
	 * @return	ContentValues con los valores de la tabla configuracionGeneral
	 *
	 */
	public ContentValues obtenerParametrosConfiguracionGeneral() {
		
		ContentValues values = new ContentValues();		
		String query = "select * from configuracionGeneral";
		
		DataBase db = new DataBase(this.c);		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(query, null);
		while (c.moveToNext()){
			values.put(c.getString(0), c.getString(1));
		}
		c.close();
		c = null;
		
		if (values.size() < 1) {
			values = null;
		}
		
		db.close();
		db = null;
		
		return values;
		
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/05/2013
	 * @title	establecerIdiomaConfiguracion
	 * @comment	Establecer el idioma configurado
	 *
	 */
	public void establecerIdiomaConfiguracion(){		
		
	    String idiomaConfiguracion = obtenerParametroTablaConfiguracion("Idioma");
	   
	    Locale idioma= new Locale(idiomaConfiguracion);
	    Locale.setDefault(idioma);
	   	   
	    Configuration configuracion = new Configuration();
	    configuracion.locale = idioma;
	    this.c.getResources().updateConfiguration(configuracion, this.c.getResources().getDisplayMetrics());
	
	}
	
	
	/**
	 * 
	 * @author	mcanal
	 * @date	25/09/2012
	 * @title	obtenerClaveSustancia
	 * @comment	Obtiene la clave de un elemento de la tabla tablas
	 *
	 * @param idTabla		Clave de la tabla de tablas.
	 * @param descripcion 	Nombre de la sustancia de la que obtener su clave
	 * @return				Clave de la sustancia
	 *
	 */
	public String obtenerIDElementoPorDescripcion(String idTabla, String descripcion){
				
		String clave = "";
		
		String sql = "SELECT IDElemento FROM tablas WHERE IDTabla = '"+ idTabla.trim() +"'";
		sql += " AND Descripcion = '"+ descripcion.trim() +"'";
		
		DataBase db = new DataBase(c);
		//db.open();
		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(sql, null);
		if (c.getCount() == 1) {
			c.moveToFirst();
			clave = c.getString(0);
		}
		
		c.close();
		db.close();
		
		return clave;
		
	}	
	
	/**
	 * 
	 * @author	mjulia
	 * @date	26/09/2012
	 * @title	obtenerDescripcionPorIdElemento
	 * @comment	Obtiene la descripci�n de un elemento de una tabla del fichero tablas de la base de datos
	 *
	 * @param idTabla	Id de la tabla de la que obtener la descripci�n.
	 * @param clave		Id del elemento de la tabla.
	 * @return			Descripci�n del elemento de la tabla.
	 *
	 */
	public String obtenerDescripcionPorIdElemento(String idTabla, String clave) {
			
		String descripcion = "";
		
		String sql = "SELECT Descripcion FROM tablas WHERE IDTabla = '"+ idTabla.trim() +"'";
		sql += " AND IDElemento = '"+ clave.trim() +"'";
		
		DataBase db = new DataBase(c);
		//db.open();
		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(sql, null);
		if (c.getCount() == 1) {
			c.moveToFirst();
			descripcion = c.getString(0);
		}
		
		c.close();
		db.close();
		
		return descripcion;
		
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	15/10/2012
	 * @title	obtenerValorMaximoPorIDElemento
	 * @comment	Obtiene el valor m�ximo de un elemento de tablas seg�n su id
	 *
	 * @param tabla			Id de la tabla de la que obtener la descripci�n.
	 * @param clave			Id del elemento de la tabla.
	 * @return				Valor m�ximo del elemento
	 *
	 */
	public double obtenerValorMaximoPorIDElemento(String tabla, String clave){
			
		double valorMax = 0;
		
		String sql = "SELECT ValorMax FROM tablas WHERE IDTabla = '"+ tabla.trim() +"'";
		sql += " AND IDElemento = '"+ clave.trim() +"'";
		
		DataBase db = new DataBase(c);
		//db.open();
		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(sql, null);
		if (c.getCount() == 1) {
			c.moveToFirst();
			valorMax = c.getDouble(0);
		}
		
		c.close();
		db.close();
		
		return valorMax;
		
	}
	
	/***
	 * 	
	 * @author	jibanez
	 * @date	03/01/2013
	 * @title	obtenerElementoTabla
	 * @comment	Devuelve un elemento de la tabla TABLAS
	 *
	 * @param tabla		-> Nombre de la tabla del fichero de tablas.
	 * @param columna	-> Columna de la cual queremos obtener el valor.
	 * @param clave		-> Argumento del elemento de la tabla.
	 * @return	
	 *			Devuelve un objeto String con el valor deseado.
	 */
	public String obtenerElementoTabla(String tabla, String columna, String clave){
			
		String valor = "";
		
		String sql = "SELECT " + columna.trim() + " FROM tablas WHERE IDTabla = '"+ tabla.trim() +"'";
		sql += " AND IDElemento = '"+ clave.trim() +"'";
		
		DataBase db = new DataBase(c);
		//db.open();
		
		boolean openBD = db.open();
		while (openBD == false) {
			 openBD = db.open();
		}

		Cursor c = db.getQuery(sql, null);
		if (c.getCount() >= 1) {
			c.moveToFirst();
			valor = c.getString(0);
		}
		
		c.close();
		db.close();
		
		return valor;
		
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	28/01/2013
	 * @title	obtenerParametroTablaConfiguracion
	 * @comment	Obtiene el valor de un par�metro definido en la tabla de configuraci�n.
	 *
	 * @param claveParametro	Clave del parametro solicitado
	 * @return					Valor del par�metro solicitado	
	 *
	 */
	public String obtenerParametroTablaConfiguracion(String claveParametro){
			
		String valor = "";		
		String query = "select valor from configuracionGeneral where clave = '" + claveParametro + "'";
		
		DataBase db = new DataBase(c);
		Cursor c = null;
		try {
			//db.open();
			boolean openBD = db.open();
			while (openBD == false) {
				 openBD = db.open();
			}
			
			c = db.getQuery(query, null);
			if (c.getCount() == 1) {
				c.moveToFirst();
				valor = c.getString(0);
			}			
		} catch(Exception e) {
			e.printStackTrace();
			Log.e(this.getClass().getName(), e.getStackTrace().toString());
		} finally {
			if (c != null) {c.close();}
			db.close();
		}
		
		return valor;
	}
	
	

		
	/**
	 * 
	 * @author	jjulia
	 * @date	05/10/2012
	 * @title	obtenerCampoFechaBaseDatosEnFormato
	 * @comment	Convertir una fecha recuperada de la base de datos a un formato determinado
	 *
	 * @param valorFechaBaseDatos  	Fecha recuperada de la base de datos
	 * @param formatoFechaBD		Formato de la fecha en la base de datos
	 * @param formatoSalida			Formato de salida 
	 * @return
	 * @throws ParseException
	 *
	 */
	public String obtenerCampoFechaBaseDatosEnFormato(String valorFechaBaseDatos, String formatoFechaBD, String formatoSalida) throws ParseException{
				
		String fechaOut =null;
		
		SimpleDateFormat df_yyyymmdd = new SimpleDateFormat(formatoFechaBD); 
		SimpleDateFormat df_ddmmyyyy = new SimpleDateFormat(formatoSalida); 
		
		Date cnvfecha = df_yyyymmdd.parse(valorFechaBaseDatos);		
		fechaOut = df_ddmmyyyy.format(cnvfecha);
		
		return fechaOut;
		
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	05/10/2012
	 * @title	obtenerCampoCalendar
	 * @comment	Convertir un string fecha y un string hora en un campo Calendario
	 *
	 * @param fecha			  	Fecha en formato FORMAT_DATE_OUT
	 * @param hora       		Hora en formato FORMAT_TIME_OUT
	 * @return
	 * @throws ParseException
	 * 
	 *  Ejemplos de como utilizar el Calendario
     *  int dia = cal.get(Calendar.DAY_OF_MONTH);
	 *  int mes = cal.get(Calendar.MONTH)+1;
     *  int any = cal.get(Calendar.YEAR);
	 *  int hor = cal.get(Calendar.HOUR_OF_DAY);
	 *  int min = cal.get(Calendar.MINUTE);
	 *  int sec = cal.get(Calendar.SECOND);
	 *
	 */
	public Calendar obtenerCampoCalendar(String fecha, String hora) throws ParseException{
							
		Calendar cal = GregorianCalendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE_FULL_CONVERT);
	   
	    Date d=sdf.parse(fecha+' '+hora);
	    cal.setTime(d);
	   		
		return cal;
	}

	
	
	/***
	 * 
	 * @author	jibanez
	 * @date	28/11/2012
	 * @title	llenarSpinner
	 * @comment	Funci�n para rellenar un objeto Spinner (lista desplegable).
	 *
	 * @param cmb
	 * 				Objeto Spinner que se va a rellenar.
	 * @param nombreTabla
	 * 				Nombre de la tabla de la tabla "tablas", cuyos elementos se cargaran en el Spinner.
	 * @param columnaValue
	 * 				Nombre de la columna de la tabla "tablas" que se utilizar� como value de cada elemento del Spinner.
	 * @param columnaTexto
	 * 				Nombre de la columna de la tabla "tablas" que se utilizar� como descripci�n de cada elemento del Spinner.
	 * @param seleccione	
	 *				Indica si se quiere mostrar un primer elemento en el Spinner, que indique al usuario que aun no se ha seleccionado ning�n elemento.
	 * @param ordenarPor	
	 *				Nombre de la columna por la que se realizar� la ordenadoci�n de los elementos mostrados
	 */
	public void llenarSpinner(Spinner cmb, String nombreTabla, String columnaValue, String columnaTexto, boolean seleccione, String ordenarPor){
				
		LinkedList<ClaseListItem> elementos = new LinkedList<ClaseListItem>();
		
		String valor;
		String descripcion;
		
		if(seleccione==true){
			this.claveValue = this.c.getResources().getString(R.string.combo_valor);
			this.claveTexto = this.c.getResources().getString(R.string.combo_descripcion);
			elementos.add(new ClaseListItem(this.claveValue, this.claveTexto));
		}
        
		String sql;
		sql = "SELECT " + columnaValue + ", " + columnaTexto;
		sql += " FROM tablas";
		sql += " WHERE IDTabla = '" + nombreTabla + "'";
		sql += " ORDER BY " + ordenarPor;

		DataBase dbs = new DataBase(this.c);
		//dbs.open();

		boolean openBD = dbs.open();
		while (openBD == false) {
			 openBD = dbs.open();
		}
		
		Cursor cur = dbs.getQuery(sql, null);
		while (cur.moveToNext()){
			
			valor = cur.getString(cur.getColumnIndex(columnaValue));
			descripcion = cur.getString(cur.getColumnIndex(columnaTexto));
			
			elementos.add(new ClaseListItem(valor, descripcion));
		} 
		
		cur.close();
		dbs.close();
		
		ArrayAdapter<ClaseListItem> spinner_adapter = new ArrayAdapter<ClaseListItem>(this.c, android.R.layout.simple_spinner_item, elementos);
		spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		cmb.setAdapter(spinner_adapter);
		
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	04/07/2013
	 * @title	llenarSpinner
	 * @comment	Cargar un combo a partir de una tabla de tablas
	 *
	 * @param cmb				Spinner donde cargar la tabla
	 * @param nombreTabla		Nombre de la tabla a cargar
	 * @param filtroWhere		Condici�n where de la consulta sql
	 * @param columnaValue		Nombre de la columna a mostrar como id del elemento
	 * @param columnaTexto		Nombre de la columna a mostrar como descripci�n del elemento
	 * @param seleccione		boolean para determinar si se a�ade seleccione como primer elemento del Spinner
	 * @param ordenarPor		Ordenaci�n de los datos
	 * @param setIdSeleccionado	Id del elemento a dejar seleccionado.
	 *
	 */
	public void llenarSpinner(Spinner cmb, String nombreTabla, String filtroWhere, String columnaValue, String columnaTexto, boolean seleccione, String ordenarPor, String setIdSeleccionado){
		
		LinkedList<ClaseListItem> elementos = new LinkedList<ClaseListItem>();
		
		String valor;
		String descripcion;
		int position =-1;
		
		if(seleccione==true){
			this.claveValue = this.c.getResources().getString(R.string.combo_valor);
			this.claveTexto = this.c.getResources().getString(R.string.combo_descripcion);
			elementos.add(new ClaseListItem(this.claveValue, this.claveTexto));
		}
        
		String sql;
		sql = "SELECT " + columnaValue + ", " + columnaTexto;
		sql += " FROM tablas";
		sql += " WHERE IDTabla = '" + nombreTabla + "'";
		sql += " AND (" + filtroWhere + ") ";
		sql += " ORDER BY " + ordenarPor;

		DataBase dbs = new DataBase(this.c);
		//dbs.open();
		
		boolean openBD = dbs.open();
		while (openBD == false) {
			 openBD = dbs.open();
		}

		int cont =-1;
		Cursor cur = dbs.getQuery(sql, null);
		while (cur.moveToNext()){
			
			cont +=1;
			valor = cur.getString(cur.getColumnIndex(columnaValue));
			descripcion = cur.getString(cur.getColumnIndex(columnaTexto));
			
			elementos.add(new ClaseListItem(valor, descripcion));
			if(valor.trim().equals(setIdSeleccionado.trim())==true){
				position=cont;
			}
			
		} 
		
		
		cur.close();
		dbs.close();
		
		ArrayAdapter<ClaseListItem> spinner_adapter = new ArrayAdapter<ClaseListItem>(this.c, android.R.layout.simple_spinner_item, elementos);
		spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		cmb.setAdapter(spinner_adapter);
		if (position > -1){
			if(seleccione==true){
				position += 1;
			}
			cmb.setSelection(position);
		}
		
	}
	
	/**
	 * 
	 * @author	mjulia
	 * @date	08/05/2013
	 * @title	seleccionarElementoSpinner
	 * @comment	Selecciona un elemento del Spinner
	 *
	 * @param cmb		Spinner
	 * @param value		Id del elemento a seleccionar.
	 *
	 */
	public void seleccionarElementoSpinner(Spinner cmb, String value){
		
		ClaseListItem item;
		
		if(cmb.getCount()>0){
			int i = 0;
			for(i=0;i<cmb.getCount();i++){

				item = (ClaseListItem) cmb.getItemAtPosition(i);
				if(item.getId().compareTo(value)==0){
					break;
				}
				
			}
			cmb.setSelection(i);
		}
		
		item = null;
	}
	
	
	

	
	/***
	 * 
	 * @author	jibanez
	 * @date	29/07/2013
	 * @title	
	 * @comment	
	 *
	 * @param idioma
	 * @return	
	 *
	 */
	public String obtenerCodigoLenguaje(String idioma) {
		
		String codigo = "es_ES";
		
		String sql;
		sql = "SELECT * ";
		sql += "FROM tablas ";
		sql += "WHERE IDTabla = '" + TABLA_IDIOMAS + "' ";
		sql += "AND Alfa1 = '" + idioma + "' ";
		
		DataBase dbs = new DataBase(this.c);
		
		boolean openBD = dbs.open();
		while (openBD == false) {
			 openBD = dbs.open();
		}

		Cursor cur = dbs.getQuery(sql, null);
		while (cur.moveToNext()){
			codigo = cur.getString(cur.getColumnIndex("Alfa2"));
		} 

		cur.close();
		dbs.close();

		return codigo;
	}
	
	

	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	30/09/2013
	 * @title	ObtenerListaPlanificacionesPendientes
	 * @comment	Obtener lista de las plaificaciones pendientes segun consulta sql
	 *
	 * @param sql	Consulta sql para obtener las planificaciiones pendientes
	 * @return		Lista de objetos planificacion
	 *
	 */
	public ArrayList<ObjetoPlanificacion> ObtenerListaPlanificacionesPendientes(String sql) {

		// Genera la matriz de salida
		ArrayList<ObjetoPlanificacion> m_obras = new ArrayList<ObjetoPlanificacion>();
	
	    // Obtine los datos de la base de datos local	
		DataBase db = new DataBase(this.c);
		Cursor c = null;
		try {
			
			boolean openBD = db.open();
			while (openBD == false) {
				 openBD = db.open();
			}
			c = db.getQuery(sql, null);
		
			while (c.moveToNext()) {

				// Genera elemento de la matriz
				ObjetoPlanificacion oAcom = new ObjetoPlanificacion();
				
				oAcom.setCod_obra(c.getString(c.getColumnIndex("Cod_Obra")));
				oAcom.setTipo_obra(c.getString(c.getColumnIndex("Tipo_Obra")));
				oAcom.setDireccion(c.getString(c.getColumnIndex("Direccion")));
				oAcom.setMunicipio(c.getString(c.getColumnIndex("Municipio")));
											
				Boolean outCarpetasOonair=false;
				if (c.getString(c.getColumnIndex("CarpetasOonair")) != null){
					if (c.getString(c.getColumnIndex("CarpetasOonair")).trim().toLowerCase().equals("true")==true){
						outCarpetasOonair=true;
					}
				}
				oAcom.setCarpetasOonair(outCarpetasOonair);
				
				Boolean seleccionada=false;
				oAcom.setSeleccionada(seleccionada);
				
				// A�ade el elemento a la matriz
				m_obras.add(oAcom);

			}

		} catch (SQLException e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} finally {
			// Cierra la conexi�n a la base de datos
			c.close();
			db.close();
		}

		// Devuelve la matriz de datos
		return m_obras;

	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	30/09/2013
	 * @title	ObtenerListaObrasEnCursoAcometidas
	 * @comment	Obtener lista de las obras en curso tipo acometida segun consulta sql
	 *
	 * @param sql	Consulta sql para obtener las planificaciiones pendientes
	 * @return		Lista de objetos acometida
	 *
	 */
	public ArrayList<ObjetoAcometida> ObtenerListaObrasEnCursoAcometidas(String sql) {

		// Genera la matriz de salida
		ArrayList<ObjetoAcometida> m_obras = new ArrayList<ObjetoAcometida>();
	
	    // Obtine los datos de la base de datos local	
		DataBase db = new DataBase(this.c);
		Cursor c = null;
		try {
			
			boolean openBD = db.open();
			while (openBD == false) {
				 openBD = db.open();
			}
			c = db.getQuery(sql, null);
		
			while (c.moveToNext()) {

				// Genera elemento de la matriz
				ObjetoAcometida oAcom = new ObjetoAcometida();
				
				oAcom.setCod_obra(c.getString(c.getColumnIndex("Cod_Obra")));
				oAcom.setSituacion(c.getString(c.getColumnIndex("Situacion")));
				oAcom.setSituacionDescripcion(c.getString(c.getColumnIndex("SituacionDes")));
				oAcom.setDireccion(c.getString(c.getColumnIndex("Direccion")));
				oAcom.setMunicipio(c.getString(c.getColumnIndex("Municipio")));
				
				Boolean outCarpetasOonair=false;
				if (c.getString(c.getColumnIndex("CarpetasOonair")) != null){
					if (c.getString(c.getColumnIndex("CarpetasOonair")).trim().toLowerCase().equals("true")==true){
						outCarpetasOonair=true;
					}
				}
				oAcom.setCarpetasOonair(outCarpetasOonair);
				
				if (c.getString(c.getColumnIndex("Comentarios")) != null){
					oAcom.setComentarios(c.getString(c.getColumnIndex("Comentarios")));
				}else{
					oAcom.setComentarios("");
				}
				
				String outFechaAcepNomb="";
				String outFechaPlanificacion="";
				String outFechaInicio="";
				String outFechaPeS="";
				String outFechaFinal="";
				String outTSFechaPlanificacion="";
				String outTSFechaResultado="";
					
				if (c.getString(c.getColumnIndex("FechaAcepNomb")) != null){
					if (c.getString(c.getColumnIndex("FechaAcepNomb")).trim().equals("")==false){
						outFechaAcepNomb = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaAcepNomb")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaPlan")) != null){
					if (c.getString(c.getColumnIndex("FechaPlan")).trim().equals("")==false){
						outFechaPlanificacion = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaPlan")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_OUT);
					}
				}
				if (c.getString(c.getColumnIndex("FechaIni")) != null){
					if (c.getString(c.getColumnIndex("FechaIni")).trim().equals("")==false){
						outFechaInicio = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaIni")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaPeS")) != null){
					if (c.getString(c.getColumnIndex("FechaPeS")).trim().equals("")==false){
						outFechaPeS = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaPeS")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaFin")) != null){
					if (c.getString(c.getColumnIndex("FechaFin")).trim().equals("")==false){
						outFechaFinal = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaFin")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				
				if (c.getString(c.getColumnIndex("TSInicioPlan")) != null){
					if (c.getString(c.getColumnIndex("TSInicioPlan")).trim().equals("")==false){
						outTSFechaPlanificacion = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("TSInicioPlan")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				
				if (c.getString(c.getColumnIndex("TSResultadoFec")) != null){
					if (c.getString(c.getColumnIndex("TSResultadoFec")).trim().equals("")==false){
						outTSFechaResultado = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("TSResultadoFec")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
							
				oAcom.setFechaPlanificacion(outFechaPlanificacion);
				oAcom.setFechaInicio(outFechaInicio);
				oAcom.setFechaPeS(outFechaPeS);
				oAcom.setFechaFinal(outFechaFinal);
				oAcom.setFechaAcepNombramiento(outFechaAcepNomb);
				oAcom.setTSFechaPlanificacion(outTSFechaPlanificacion);
				oAcom.setTSFechaResultado(outTSFechaResultado);
				
				oAcom.setTSGestorObra(c.getString(c.getColumnIndex("TSGestorObra")));
				oAcom.setTSTelefonoGO(c.getString(c.getColumnIndex("TSTelefonoGO")));
				oAcom.setTSResultado(c.getString(c.getColumnIndex("TSResultado")));
					
				oAcom.setHistorialObra(c.getString(c.getColumnIndex("HistorialObra")));
								
				// A�ade el elemento a la matriz
				m_obras.add(oAcom);

			}

		} catch (SQLException e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} finally {
			// Cierra la conexi�n a la base de datos
			c.close();
			db.close();
		}

		// Devuelve la matriz de datos
		return m_obras;

	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	30/09/2013
	 * @title	ObtenerListaObrasEnCursoObras50m
	 * @comment	Obtener lista de las obras en curso tipo obras <= 50m. segun consulta sql
	 *
	 * @param sql	Consulta sql para obtener las planificaciiones pendientes
	 * @return		Lista de objetos acometida
	 *
	 */
	public ArrayList<ObjetoObra50m> ObtenerListaObrasEnCursoObras50m(String sql) {

		// Genera la matriz de salida
		ArrayList<ObjetoObra50m> m_obras = new ArrayList<ObjetoObra50m>();
	
	    // Obtine los datos de la base de datos local	
		DataBase db = new DataBase(this.c);
		Cursor c = null;
		try {
			
			boolean openBD = db.open();
			while (openBD == false) {
				 openBD = db.open();
			}
			c = db.getQuery(sql, null);
		
			while (c.moveToNext()) {

				// Genera elemento de la matriz
				ObjetoObra50m oAcom = new ObjetoObra50m();
				
				oAcom.setCod_obra(c.getString(c.getColumnIndex("Cod_Obra")));
				oAcom.setPerfil(c.getString(c.getColumnIndex("Perfil")));
				oAcom.setSituacion(c.getString(c.getColumnIndex("Situacion")));
				oAcom.setSituacionDescripcion(c.getString(c.getColumnIndex("SituacionDes")));
				oAcom.setDireccion(c.getString(c.getColumnIndex("Direccion")));
				oAcom.setMunicipio(c.getString(c.getColumnIndex("Municipio")));
				
				Boolean outCarpetasOonair=false;
				if (c.getString(c.getColumnIndex("CarpetasOonair")) != null){
					if (c.getString(c.getColumnIndex("CarpetasOonair")).trim().toLowerCase().equals("true")==true){
						outCarpetasOonair=true;
					}
				}
				oAcom.setCarpetasOonair(outCarpetasOonair);
				
				if (c.getString(c.getColumnIndex("Comentarios")) != null){
					oAcom.setComentarios(c.getString(c.getColumnIndex("Comentarios")));
				}else{
					oAcom.setComentarios("");
				}
				if (c.getString(c.getColumnIndex("Trazado")) != null){
					oAcom.setTrazado(c.getString(c.getColumnIndex("Trazado")));
				}else{
					oAcom.setTrazado("");
				}
				if (c.getString(c.getColumnIndex("TrazadoMod")) != null){
					oAcom.setTrazadoMod(c.getString(c.getColumnIndex("TrazadoMod")));
				}else{
					oAcom.setTrazadoMod("");
				}
				
				String outFechaAcepNomb="";
				String outFechaPlanificacion="";
				String outFechaTrazadoSolicitud="";
				String outFechaTrazadoConfirmacion="";
				String outFechaTrazadoSolicitudMod="";
				String outFechaTrazadoConfirmacionMod="";
				String outFechaInicio="";
				String outFechaSolicitudPeS="";
				String outFechaAcepContenidos="";
				String outFechaFinalPrueba="";
				String outFechaAceptadaPeG="";
				String outFechaPeS="";
				String outFechaFinal="";
				String outTSFechaPlanificacion="";
				String outTSFechaResultado="";
					
				if (c.getString(c.getColumnIndex("FechaAcepNomb")) != null){
					if (c.getString(c.getColumnIndex("FechaAcepNomb")).trim().equals("")==false){
						outFechaAcepNomb = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaAcepNomb")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaPlan")) != null){
					if (c.getString(c.getColumnIndex("FechaPlan")).trim().equals("")==false){
						outFechaPlanificacion = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaPlan")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_OUT);
					}
				}
				if (c.getString(c.getColumnIndex("FechaTraSol")) != null){
					if (c.getString(c.getColumnIndex("FechaTraSol")).trim().equals("")==false){
						outFechaTrazadoSolicitud = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaTraSol")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaTraCon")) != null){
					if (c.getString(c.getColumnIndex("FechaTraCon")).trim().equals("")==false){
						outFechaTrazadoConfirmacion = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaTraCon")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaTraSolMod")) != null){
					if (c.getString(c.getColumnIndex("FechaTraSolMod")).trim().equals("")==false){
						outFechaTrazadoSolicitudMod = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaTraSolMod")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaTraConMod")) != null){
					if (c.getString(c.getColumnIndex("FechaTraConMod")).trim().equals("")==false){
						outFechaTrazadoConfirmacionMod = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaTraConMod")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaIni")) != null){
					if (c.getString(c.getColumnIndex("FechaIni")).trim().equals("")==false){
						outFechaInicio = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaIni")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaSolicitudPeS")) != null){
					if (c.getString(c.getColumnIndex("FechaSolicitudPeS")).trim().equals("")==false){
						outFechaSolicitudPeS = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaSolicitudPeS")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaAcepConten")) != null){
					if (c.getString(c.getColumnIndex("FechaAcepConten")).trim().equals("")==false){
						outFechaAcepContenidos = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaAcepConten")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaFinalPrueba")) != null){
					if (c.getString(c.getColumnIndex("FechaFinalPrueba")).trim().equals("")==false){
						outFechaFinalPrueba = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaFinalPrueba")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaAceptadaPeG")) != null){
					if (c.getString(c.getColumnIndex("FechaAceptadaPeG")).trim().equals("")==false){
						outFechaAceptadaPeG = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaAceptadaPeG")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				
				if (c.getString(c.getColumnIndex("FechaPeS")) != null){
					if (c.getString(c.getColumnIndex("FechaPeS")).trim().equals("")==false){
						outFechaPeS = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaPeS")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaFin")) != null){
					if (c.getString(c.getColumnIndex("FechaFin")).trim().equals("")==false){
						outFechaFinal = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaFin")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				
				if (c.getString(c.getColumnIndex("TSInicioPlan")) != null){
					if (c.getString(c.getColumnIndex("TSInicioPlan")).trim().equals("")==false){
						outTSFechaPlanificacion = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("TSInicioPlan")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				
				if (c.getString(c.getColumnIndex("TSResultadoFec")) != null){
					if (c.getString(c.getColumnIndex("TSResultadoFec")).trim().equals("")==false){
						outTSFechaResultado = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("TSResultadoFec")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
							
				oAcom.setFechaPlanificacion(outFechaPlanificacion);
				oAcom.setFechaInicio(outFechaInicio);
				oAcom.setFechaTrazadoSolicitud(outFechaTrazadoSolicitud);
				oAcom.setFechaTrazadoConfirmacion(outFechaTrazadoConfirmacion);
				oAcom.setFechaTrazadoSolicitudMod(outFechaTrazadoSolicitudMod);
				oAcom.setFechaTrazadoConfirmacionMod(outFechaTrazadoConfirmacionMod);
				oAcom.setFechaSolicitudPeS(outFechaSolicitudPeS);
				oAcom.setFechaAcepContenidos(outFechaAcepContenidos);
				oAcom.setFechaFinalPrueba(outFechaFinalPrueba);
				oAcom.setFechaAceptadaPeG(outFechaAceptadaPeG);
				oAcom.setFechaPeS(outFechaPeS);
				oAcom.setFechaFinal(outFechaFinal);
				oAcom.setFechaAcepNombramiento(outFechaAcepNomb);
				oAcom.setTSFechaPlanificacion(outTSFechaPlanificacion);
				oAcom.setTSFechaResultado(outTSFechaResultado);
				
				oAcom.setTSGestorObra(c.getString(c.getColumnIndex("TSGestorObra")));
				oAcom.setTSTelefonoGO(c.getString(c.getColumnIndex("TSTelefonoGO")));
				oAcom.setTSResultado(c.getString(c.getColumnIndex("TSResultado")));
				
				oAcom.setHistorialObra(c.getString(c.getColumnIndex("HistorialObra")));
							
				// A�ade el elemento a la matriz
				m_obras.add(oAcom);

			}

		} catch (SQLException e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} finally {
			// Cierra la conexi�n a la base de datos
			c.close();
			db.close();
		}

		// Devuelve la matriz de datos
		return m_obras;

	}
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	30/09/2014
	 * @title	ObtenerDetalleGenericoObra
	 * @comment	Obtener el objeto generico Obra con todos los datos de detalle de la obra
	 *
	 * @param codigoObra	El c�digo de la obra a cargar
	 * @return		        Objeto gen�rico de la obra
	 *
	 */
	public ObjetoObra ObtenerDetalleGenericoObra(String codigoObra) {
		
		
		String sql = instruccionesSQL.obtenerDetalleGenericoObra(codigoObra);

		//Generar el objeto de retorno
		ObjetoObra oObra = new ObjetoObra();
				
		
	    // Obtine los datos de la base de datos local	
		DataBase db = new DataBase(this.c);
		Cursor c = null;
		try {
			
			boolean openBD = db.open();
			while (openBD == false) {
				 openBD = db.open();
			}
			c = db.getQuery(sql, null);
		
			if (c.moveToFirst()) {

				oObra.setCod_obra(c.getString(c.getColumnIndex("Cod_Obra")));
				oObra.setPerfil(c.getString(c.getColumnIndex("Perfil")));
				oObra.setTipoObra(c.getString(c.getColumnIndex("Tipo_Obra")));
				oObra.setSituacion(c.getString(c.getColumnIndex("Situacion")));
				oObra.setSituacionDescripcion(c.getString(c.getColumnIndex("SituacionDes")));
				oObra.setDireccion(c.getString(c.getColumnIndex("Direccion")));
				oObra.setMunicipio(c.getString(c.getColumnIndex("Municipio")));
				
				Boolean outCarpetasOonair=false;
				if (c.getString(c.getColumnIndex("CarpetasOonair")) != null){
					if (c.getString(c.getColumnIndex("CarpetasOonair")).trim().toLowerCase().equals("true")==true){
						outCarpetasOonair=true;
					}
				}
				oObra.setCarpetasOonair(outCarpetasOonair);
				
				if (c.getString(c.getColumnIndex("Comentarios")) != null){
					oObra.setComentarios(c.getString(c.getColumnIndex("Comentarios")));
				}else{
					oObra.setComentarios("");
				}
				if (c.getString(c.getColumnIndex("Trazado")) != null){
					oObra.setTrazado(c.getString(c.getColumnIndex("Trazado")));
				}else{
					oObra.setTrazado("");
				}
				if (c.getString(c.getColumnIndex("TrazadoMod")) != null){
					oObra.setTrazadoMod(c.getString(c.getColumnIndex("TrazadoMod")));
				}else{
					oObra.setTrazadoMod("");
				}
				
				String outFechaAcepNomb="";
				String outFechaPlanificacion="";
				String outFechaTrazadoSolicitud="";
				String outFechaTrazadoConfirmacion="";
				String outFechaTrazadoSolicitudMod="";
				String outFechaTrazadoConfirmacionMod="";
				String outFechaInicio="";
				String outFechaSolicitudPeS="";
				String outFechaAcepContenidos="";
				String outFechaFinalPrueba="";
				String outFechaAceptadaPeG="";
				String outFechaPeS="";
				String outFechaFinal="";
				String outTSFechaPlanificacion="";
				String outTSFechaResultado="";
					
				if (c.getString(c.getColumnIndex("FechaAcepNomb")) != null){
					if (c.getString(c.getColumnIndex("FechaAcepNomb")).trim().equals("")==false){
						outFechaAcepNomb = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaAcepNomb")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaPlan")) != null){
					if (c.getString(c.getColumnIndex("FechaPlan")).trim().equals("")==false){
						outFechaPlanificacion = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaPlan")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_OUT);
					}
				}
				if (c.getString(c.getColumnIndex("FechaTraSol")) != null){
					if (c.getString(c.getColumnIndex("FechaTraSol")).trim().equals("")==false){
						outFechaTrazadoSolicitud = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaTraSol")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaTraCon")) != null){
					if (c.getString(c.getColumnIndex("FechaTraCon")).trim().equals("")==false){
						outFechaTrazadoConfirmacion = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaTraCon")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaTraSolMod")) != null){
					if (c.getString(c.getColumnIndex("FechaTraSolMod")).trim().equals("")==false){
						outFechaTrazadoSolicitudMod = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaTraSolMod")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaTraConMod")) != null){
					if (c.getString(c.getColumnIndex("FechaTraConMod")).trim().equals("")==false){
						outFechaTrazadoConfirmacionMod = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaTraConMod")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaIni")) != null){
					if (c.getString(c.getColumnIndex("FechaIni")).trim().equals("")==false){
						outFechaInicio = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaIni")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaSolicitudPeS")) != null){
					if (c.getString(c.getColumnIndex("FechaSolicitudPeS")).trim().equals("")==false){
						outFechaSolicitudPeS = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaSolicitudPeS")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaAcepConten")) != null){
					if (c.getString(c.getColumnIndex("FechaAcepConten")).trim().equals("")==false){
						outFechaAcepContenidos = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaAcepConten")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaFinalPrueba")) != null){
					if (c.getString(c.getColumnIndex("FechaFinalPrueba")).trim().equals("")==false){
						outFechaFinalPrueba = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaFinalPrueba")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaAceptadaPeG")) != null){
					if (c.getString(c.getColumnIndex("FechaAceptadaPeG")).trim().equals("")==false){
						outFechaAceptadaPeG = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaAceptadaPeG")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaPeS")) != null){
					if (c.getString(c.getColumnIndex("FechaPeS")).trim().equals("")==false){
						outFechaPeS = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaPeS")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("FechaFin")) != null){
					if (c.getString(c.getColumnIndex("FechaFin")).trim().equals("")==false){
						outFechaFinal = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("FechaFin")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				
				if (c.getString(c.getColumnIndex("TSInicioPlan")) != null){
					if (c.getString(c.getColumnIndex("TSInicioPlan")).trim().equals("")==false){
						outTSFechaPlanificacion = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("TSInicioPlan")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				if (c.getString(c.getColumnIndex("TSResultadoFec")) != null){
					if (c.getString(c.getColumnIndex("TSResultadoFec")).trim().equals("")==false){
						outTSFechaResultado = obtenerCampoFechaBaseDatosEnFormato(
								c.getString(c.getColumnIndex("TSResultadoFec")),
								UtilData.FORMAT_DATE_FULL_BD,
								UtilData.FORMAT_DATE_FULL_VISUAL);
					}
				}
				
				oObra.setFechaPlanificacion(outFechaPlanificacion);
				oObra.setFechaInicio(outFechaInicio);
				oObra.setFechaTrazadoSolicitud(outFechaTrazadoSolicitud);
				oObra.setFechaTrazadoConfirmacion(outFechaTrazadoConfirmacion);
				oObra.setFechaTrazadoSolicitudMod(outFechaTrazadoSolicitudMod);
				oObra.setFechaTrazadoConfirmacionMod(outFechaTrazadoConfirmacionMod);
				oObra.setFechaSolicitudPeS(outFechaSolicitudPeS);
				oObra.setFechaAcepContenidos(outFechaAcepContenidos);
				oObra.setFechaFinalPrueba(outFechaFinalPrueba);
				oObra.setFechaAceptadaPeG(outFechaAceptadaPeG);
				oObra.setFechaPeS(outFechaPeS);
				oObra.setFechaFinal(outFechaFinal);
				oObra.setFechaAcepNombramiento(outFechaAcepNomb);
				oObra.setTSFechaPlanificacion(outTSFechaPlanificacion);
				oObra.setTSFechaResultado(outTSFechaResultado);
				
				oObra.setTSGestorObra(c.getString(c.getColumnIndex("TSGestorObra")));
				oObra.setTSTelefonoGO(c.getString(c.getColumnIndex("TSTelefonoGO")));
				oObra.setTSResultado(c.getString(c.getColumnIndex("TSResultado")));
			
				oObra.setHistorialObra(c.getString(c.getColumnIndex("HistorialObra")));
							
			}

		} catch (SQLException e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} finally {
			// Cierra la conexi�n a la base de datos
			c.close();
			db.close();
		}

		// Devuelve el objeto obra con los datos
		return oObra;

	}
	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	cargarListaPlanificacionAcometidas
	 * @comment	Cargar datos en la lista de planificaci�n de acometidas
	 *
	 * @param act		Activity donde reside la Lista
	 * @param orden		Ordenaci�n de la lista
	 *
	 */
	public void cargarListaPlanificacionAcometidas(Activity act, ordenacion orden) {
			
		String sql = instruccionesSQL.seleccionarPlanificacionAcometidas(orden);
		
		ArrayList<ObjetoPlanificacion> m_obras;
		m_obras = ObtenerListaPlanificacionesPendientes(sql);
		
		ListView lstPlanificacion = (ListView) act.findViewById(R.id.lstPlanifAcometidas);
		AdaptadorVertical_Planificacion m_adapter = new AdaptadorVertical_Planificacion(this.c, R.layout.listitem_vertical_planificacion, m_obras);
		lstPlanificacion.setAdapter(m_adapter);
				
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2013
	 * @title	cargarListaPlanificacionObras50m
	 * @comment	Cargar datos en la lista de planificaci�n de obras <= 50m.
	 *
	 * @param act		Activity donde reside la Lista
	 * @param orden		Ordenaci�n de la lista
	 *
	 */
	public void cargarListaPlanificacionObras50m(Activity act, ordenacion orden) {
			
		String sql = instruccionesSQL.seleccionarPlanificacionObras50m(orden);
		
		ArrayList<ObjetoPlanificacion> m_obras;
		m_obras = ObtenerListaPlanificacionesPendientes(sql);
		
		ListView lstPlanificacion = (ListView) act.findViewById(R.id.lstPlanifObras50m);
		AdaptadorVertical_Planificacion m_adapter = new AdaptadorVertical_Planificacion(this.c, R.layout.listitem_vertical_planificacion, m_obras);
		lstPlanificacion.setAdapter(m_adapter);
				
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2014
	 * @title	cargarListaObrasEnCursoAcometidas
	 * @comment	Cargar datos en la lista de obras en curso tipo acometidas
	 *
	 * @param act		Activity donde reside la Lista
	 * @param orden		Ordenaci�n de la lista
	 * @param codobra 	C�digo de la obra a posicionarse
	 *
	 */
	public void cargarListaObrasEnCursoAcometidas(Activity act, ordenacion orden, String codObra) {
			
		String sql = instruccionesSQL.seleccionarMisAcometidas(orden);
		
		ArrayList<ObjetoAcometida> m_obras;
		m_obras = ObtenerListaObrasEnCursoAcometidas(sql);
		
		ListView lstAcometidas = (ListView) act.findViewById(R.id.lstAcometidas);
		AdaptadorVertical_Acometida m_adapter = new AdaptadorVertical_Acometida(this.c, R.layout.listitem_vertical_acometida, m_obras);
		lstAcometidas.setAdapter(m_adapter);
	
		//Posicinar la lista de acoemtida en una determinada obra
		if (codObra.equals("")==false){
			int savedPosition=0;
			for (int i = 0; i < m_adapter.getCount(); ++i) {
				ObjetoAcometida obj = m_adapter.getItem(i);
				if (obj.getCod_obra().equals(codObra)==true){
					savedPosition=i;
					break;
				}
			}
			lstAcometidas.setSelection(savedPosition);
		}
		
		
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	16/10/2014
	 * @title	cargarListaObrasEnCursoObras50m
	 * @comment	Cargar datos en la lista de obras en curso tipo obras <= 50m.
	 *
	 * @param act		Activity donde reside la Lista
	 * @param orden		Ordenaci�n de la lista
	 * @param codobra 	C�digo de la obra a posicionarse
	 *
	 */
	public void cargarListaObrasEnCursoObras50m(Activity act, ordenacion orden, String codObra) {
			
		String sql = instruccionesSQL.seleccionarMisObras50m(orden);
		
		ArrayList<ObjetoObra50m> m_obras;
		m_obras = ObtenerListaObrasEnCursoObras50m(sql);
		
		ListView lstObras50m = (ListView) act.findViewById(R.id.lstObras50m);
		AdaptadorVertical_Obra50m m_adapter = new AdaptadorVertical_Obra50m(this.c, R.layout.listitem_vertical_obra50m, m_obras);
		lstObras50m.setAdapter(m_adapter);
	
		//Posicinar la lista de acoemtida en una determinada obra
		if (codObra.equals("")==false){
			int savedPosition=0;
			for (int i = 0; i < m_adapter.getCount(); ++i) {
				ObjetoObra50m obj = m_adapter.getItem(i);
				if (obj.getCod_obra().equals(codObra)==true){
					savedPosition=i;
					break;
				}
			}
			lstObras50m.setSelection(savedPosition);
		}
		
		
	}

	
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	11/11/2014
	 * @title	cargarListaRecursosPreventivos
	 * @comment	Obtener del SW y cargar la lista de recursos preventivos de una obra
	 *
	 * @param act		Activity donde reside la Lista
	 * @param codobra 	C�digo de la obra
	 * @param jefeobra	Jefe de obra de la obra (usuario delApp.Mobile9
	 *
	 */
	public void cargarListaRecursosPreventivos(View act, String codObra, String jefeObra) {
			
		Resources res = this.c.getResources();
				
		//Conectar con el servicio web y obtener la lista de recursos preventivos
		final ArrayList<ObjetoRecursoPreventivo> m_recursos;
		m_recursos = cargarListaRecursosPreventivos_ObtenerListaRecursos(codObra, jefeObra);
		if (m_recursos != null){

			//Establecer el titulo de la lista
			TextView lblRecPreventivo =  (TextView) act.findViewById(R.id.lblRecPreventivo);
			String titulo=res.getString(R.string.title_dialog_recurso_preventivo);
			titulo += " - " + codObra;
			lblRecPreventivo.setText(titulo);
			
			
			//Cargar la lista de recursos preventivos obtenida
			ListView lstRecursos = (ListView) act.findViewById(R.id.lstRecPreventivos);
			AdaptadorVertical_RecursoPreventivo m_adapter = new AdaptadorVertical_RecursoPreventivo(this.c, R.layout.listitem_vertical_recursopreventivo, m_recursos);
			lstRecursos.setAdapter(m_adapter);
					
		}
	}
	
	public ArrayList<ObjetoRecursoPreventivo> cargarListaRecursosPreventivos_ObtenerListaRecursos(String codObra, String jefeObra) {

		ArrayList<ObjetoRecursoPreventivo> retorn = new ArrayList<ObjetoRecursoPreventivo>();
		
		String sql = instruccionesSQL.seleccionarRecursoPreventivo();
			
	
	    // Obtine los datos de la base de datos local	
		DataBase db = new DataBase(this.c);
		Cursor c = null;
		try {
			
			boolean openBD = db.open();
			while (openBD == false) {
				 openBD = db.open();
			}
			c = db.getQuery(sql, null);
		
			while (c.moveToNext()) {

				// Genera elemento de la matriz
				ObjetoRecursoPreventivo rec = new ObjetoRecursoPreventivo();
				
				rec.setCod_JefeObra(c.getString(c.getColumnIndex("Codigo")));
				rec.setNom_JefeObra(c.getString(c.getColumnIndex("Nombre")));
				
				if (rec.getCod_JefeObra().equals(jefeObra)==true){
					rec.setSeleccionado(true);
				}else{
					rec.setSeleccionado(false);
				}
				
				// A�ade el elemento a la matriz
				retorn.add(rec);

			}

		} catch (SQLException e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			Log.e(this.getClass().getName().toString(), e.toString());
			e.printStackTrace();
		} finally {
			// Cierra la conexi�n a la base de datos
			c.close();
			db.close();
		}

		// Devuelve la matriz de datos
		return retorn;
		
		
		
		
		
		
//		
//		
//		
//		
//		String respuesta="";
//		try{
//			
//			UtilData ud = new UtilData(this.c);
//			boolean autorizado = Boolean.valueOf(ud.obtenerParametroTablaConfiguracion("Autorizacion_Internet"));;
//			String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
//			String password = ud.obtenerParametroTablaConfiguracion("userPasswordUsuario");
//			if (userName.trim().equals("")==true || password.trim().equals("")==true){
//				autorizado=false;
//			}
//				
//			ConexionesSincronizacion us = new ConexionesSincronizacion(this.c);
//			if (us.networkAvailable() != true ) {
//				autorizado=false;
//			}
//			
//			if(autorizado == true) {
//					
//				Log.i(this.getClass().getName(), "EJECUTAR RECURSOS PREVENTIVOS. Conectar con servicio web.");
//				respuesta=us.recepcionarRecursosPreventivos(codObra, userName, password);
//				
//				if (respuesta.equals("")==false){
//					
//					ObjectMapper mapper = new ObjectMapper();
//					DescargaRecursosPreventivos descargaJSON = mapper.readValue(respuesta, DescargaRecursosPreventivos.class );  
//										
//					Iterator<JSONRecursoPreventivo> iter = descargaJSON.iterator();
//					while (iter.hasNext()) {
//						JSONRecursoPreventivo elemento = iter.next();
//						ObjetoRecursoPreventivo obj = new ObjetoRecursoPreventivo();
//						obj.setCod_JefeObra(elemento.getCod_JefeObra());
//						obj.setNom_JefeObra(elemento.getNom_JefeObra());
//						
//						if (elemento.getCod_JefeObra().equals(jefeObra)==true){
//							obj.setSeleccionado(true);
//						}else{
//							obj.setSeleccionado(false);
//						}
//							
//						retorn.add(obj);
//					}
//				}
//				
//													
//			}else{
//				
//				Log.e(this.getClass().getName(),  new Date().toString()+" - "+this.c.getResources().getString(R.string.error_SIncronizacion_NoAutorizado));
//
//			}
//						
//		} catch (Exception e) {	
//			
//			e.printStackTrace();
//			Log.e(this.getClass().getName(), e.getStackTrace().toString() +" "+ new Date().toString());
//
//		}
//		
//		return retorn;
		
	}
		
	
	
	
	
}
