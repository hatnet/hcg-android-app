package tec.telesupervision.data;

public class instruccionesSQL {
	
		  	  
	public static enum ordenacion{
		OBRA,
		MUNICIPIO,
		ESTADO	
	}
	
	public static String seleccionarMisAcometidas(ordenacion orden){
		
		String sql = "";
		sql += " select t0.* ";
		sql += " from acometidas t0 ";
		
		if (orden.equals(ordenacion.OBRA)==true){
			sql += " order by t0.Cod_Obra ";
		}
		if (orden.equals(ordenacion.MUNICIPIO)==true){
			sql += " order by t0.Municipio, t0.Situacion, t0.FechaAcepNomb, t0.FechaPeS, t0.FechaIni, t0.FechaPlan ";
		}
		if (orden.equals(ordenacion.ESTADO)==true){
			sql += " order by t0.Situacion, t0.FechaAcepNomb, t0.FechaPeS, t0.FechaIni, t0.FechaPlan ";
		}
		
		return sql;
		
	}
	
	
	public static String seleccionarMisObras50m(ordenacion orden){
			
			String sql = "";
			sql += " select t0.* ";
			sql += " from obras50m t0 ";
			
					
			if (orden.equals(ordenacion.OBRA)==true){
				sql += " order by t0.Cod_Obra ";
			}
			if (orden.equals(ordenacion.MUNICIPIO)==true){
				sql += " order by t0.Municipio, t0.Situacion, t0.FechaAcepNomb, t0.FechaPeS, t0.FechaIni, t0.FechaPlan ";
			}
			if (orden.equals(ordenacion.ESTADO)==true){
				sql += " order by t0.Situacion, t0.FechaAcepNomb, t0.FechaPeS, t0.FechaIni, t0.FechaPlan ";
			}
			
			return sql;
			
	}
		
	public static String obtenerDetalleGenericoObra(String codigoObra){
		
		String sql = "";
		
		sql += " select Cod_Obra, Perfil, '2' as Tipo_Obra, Situacion, SituacionDes, Direccion, Municipio, CarpetasOonair, Comentarios, Trazado, TrazadoMod, FechaAcepNomb, FechaPlan, FechaTraSol, FechaTraCon, FechaTraSolMod, FechaTraConMod, FechaIni, FechaSolicitudPeS, FechaAcepConten, FechaFinalPrueba, FechaAceptadaPeG, FechaPeS, FechaFin, TSGestorObra, TSTelefonoGO, TSInicioPlan, TSResultadoFec, TSResultado, HistorialObra ";
		sql += " from obras50m ";
		sql += " where cod_obra='" + codigoObra + "' ";

		sql += " union ";

		sql += " select Cod_Obra, 'JOC' as Perfil, '1' as Tipo_Obra, Situacion, SituacionDes, Direccion, Municipio, CarpetasOonair, Comentarios, NULL as Trazado, NULL as TrazadoMod, FechaAcepNomb, FechaPlan, NULL as FechaTraSol, NULL as FechaTraCon, NULL as FechaTraSolMod, NULL as FechaTraConMod, FechaIni, NULL as FechaSolicitudPeS, NULL as FechaAcepConten, NULL as FechaFinalPrueba, NULL as FechaAceptadaPeG, FechaPeS, FechaFin, TSGestorObra, TSTelefonoGO, TSInicioPlan, TSResultadoFec, TSResultado, HistorialObra ";
		sql += " from acometidas ";
		sql += " where cod_obra='" + codigoObra + "' ";

		return sql;
				
	}
	
	public static String seleccionarPlanificacionAcometidas(ordenacion orden){
		
		String sql = "";
		sql += " select * ";
		sql += " from planificacion ";
		sql += " where tipo_Obra='1' ";
				
		if (orden.equals(ordenacion.OBRA)==true){
			sql += " order by Cod_Obra ";
		}
		if (orden.equals(ordenacion.MUNICIPIO)==true){
			sql += " order by Municipio ";
		}
				
		return sql;
		
	}
	
	public static String seleccionarPlanificacionObras50m(ordenacion orden){
		
		String sql = "";
		sql += " select * ";
		sql += " from planificacion ";
		sql += " where tipo_Obra='2' ";
				
		if (orden.equals(ordenacion.OBRA)==true){
			sql += " order by Cod_Obra ";
		}
		if (orden.equals(ordenacion.MUNICIPIO)==true){
			sql += " order by Municipio ";
		}
				
		return sql;
		
	}
	
	
	public static String seleccionarRecursoPreventivo(){
		
		String sql = "";
		sql += " select * ";
		sql += " from recursosPreventivos ";
		sql += " order by Nombre ";
						
		return sql;
		
	}
	
}
