package tec.telesupervision.data;


import java.io.IOException;
import java.io.InputStream;

import tec.telesupervision.utilidades.UtilidadesArchivos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * 
 * @author	jjulia
 * @date	06/11/2013
 * @title	DBHelper
 * @comment	Clase para gestionar la base de datos local.
 *			Esta clase permite gestionar los eventos onCreate y onUpgrade
 *			El resto de la operativa con base de datos de momento se mantiene en la clase BaseDatos
 *
 */
public class DBHelper {
	 
	   private static final int DATABASE_VERSION = 5;
	   private static final String DATABASE_NAME = "telesupervisionv2.db";
	   private static final String DATABASE_CREATE = "createTelesupervision.sql";
	 	   	 
	   private Context context;
	   private SQLiteDatabase db;
	   private MyDBOpenHelper openHelper;
	 
	   /**
	    * 
	    * @author	jjulia
	    * @date		06/11/2013
	    * @title	DBHelper
	    * @comment	Constructor de la clase
	    *
	    * @param context	Contexto de la aplicaci�n
	    *
	    */
	   public DBHelper(Context context) {
	      this.context = context;
	      this.openHelper = new MyDBOpenHelper(this.context);
	   }
	 
	   /**
	    * 
	    * @author	jjulia
	    * @date		06/11/2013
	    * @title	open	
	    * @comment	Funci�n para obtener una instancia de la base de datos abierta
	    *
	    * @return	Instancia de la base de datos como DBHelper
	    *
	    */
	   public DBHelper open(){
	      this.db = openHelper.getWritableDatabase();      
	      return this;
	   }
	 
	   /**
	    * 
	    * @author	jjulia
	    * @date		06/11/2013
	    * @title	close
	    * @comment	Procedimiento para cerrar la instancia de la base de datos
	    *	
	    *
	    */
	   public void close() {
	      this.db.close();
	   }
	   
	   
	   /**
	    * 
	    * @author	Jordi
	    * @date		06/11/2013
	    * @title	regenerar
	    * @comment	Procedimiento para regenera la base de datos en base 
	    * 			al script SQL inicial
	    *
	    * @throws IOException
	    *
	    */
	   public void regenerar() throws IOException {
		   createDataBase(this.db);  
	   }
	 
	   
	 /**
	  * 
	  * @author		jjulia
	  * @date		06/11/2013
	  * @title		MyDBOpenHelper
	  * @comment	Clase para gestionar el versionado de la base de datos
	  *		
	  */
	   private  class MyDBOpenHelper extends SQLiteOpenHelper {
	 
	      MyDBOpenHelper(Context context) {
	         super(context, DATABASE_NAME, null, DATABASE_VERSION);
	      }
	 
	      @Override
	      public void onCreate(SQLiteDatabase db) {
	    	  
	    	  try {
	    		  Log.w(this.getClass().getName(), "Creaci�n estructura y datos iniciales desde de la Base de Datos. ");
	    		  createDataBase(db);
			  } catch (IOException e) {
				  Log.e(this.getClass().getName(), e.getStackTrace().toString());
			  }
	    	  
	      }
	 
	      @Override
	       public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    	  
	          Log.w(this.getClass().getName(), "Actualizaci�n estructura Base de Datos a versi�n: " + newVersion + " desde la versi�n: " + oldVersion + ". ");
			  onCreate(db);
			  
	      }
	         
	     
	         
	   }
	   
	   /**
	    * 
	    * @author	jjulia
	    * @date		06/11/2013
	    * @title	createDataBase
	    * @comment	Procedimiento para crear la base de datos local inicial
	    * 			en base al script SQL que existe en \assets
	    *
	    * @param db		Instancia de la base de datos donde ejecutar el script
	    * @throws IOException
	    *
	    */
	   private void createDataBase(SQLiteDatabase db) throws IOException {
	
		    InputStream is = context.getAssets().open(DATABASE_CREATE);
		    String sentencia= UtilidadesArchivos.convertStreamToString(is);
		    String[] exec = sentencia.split(";");
		    
		    for (int x=0; x<exec.length; x++){
		    	if (exec[x] != null){
		    		if(exec[x].trim().equals("")==false){
		    			db.execSQL(exec[x]);
		    		}
		    	}
	   		} 

	   }
	   
	 	   
	}