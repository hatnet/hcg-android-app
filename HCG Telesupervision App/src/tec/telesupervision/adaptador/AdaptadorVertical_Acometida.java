package tec.telesupervision.adaptador;

import java.util.ArrayList;

import tec.telesupervision.Objects.ObjetoAcometida;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.data.instruccionesSQL.ordenacion;
import tec.telesupervision.mod.aplicacion.DetalleObraActivity;
import tec.telesupervision.services.ProcesosEjecucionAcciones;
import tec.telesupervision.ver2.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * 
 * @author 	jjulia
 * @date 	05/09/2014
 * @title 	Adaptador para cargar una matriz de datos "ObjetoAcometida" en un Layout concreto
 * @comment
 * 
 */
public class AdaptadorVertical_Acometida extends ArrayAdapter<ObjetoAcometida> {

	private Context context;
	private Activity act;
	private ArrayList<ObjetoAcometida> objects;
	
	private final int REQUEST_CODE_ACTIVITY_VIS = 1;
	
	
	/***
	 * 
	 * @author jjulia
	 * @date 05/10/2012
	 * @title Constructor del adaptador
	 * @comment
	 * 
	 * @param context		      Contexto
	 * @param textViewResourceId  Layout particular donde deben cargarse los datos
	 * @param objects             Matriz con los datos a cargar
	 * 
	 */
	public AdaptadorVertical_Acometida(Context context, int textViewResourceId, ArrayList<ObjetoAcometida> objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.context = context;
		this.act= (Activity) context;
		
		
	}
	
	
	/**
	 * Gestionar la carga de los datos en los elementos del listitem
	 */
	public View getView(int position, View convertView, ViewGroup parent) {

		Resources res = context.getResources();
		
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.listitem_vertical_acometida, null);
		}
		
		
		// Obtener los valores del listitem
		ObjetoAcometida i = objects.get(position);
		if (i != null) {
							
			// Obtener los controles 
			TextView lblObra = (TextView) v.findViewById(R.id.lblObra);
			TextView lblSituacion = (TextView) v.findViewById(R.id.lblSituacion);
			TextView lblAviso = (TextView) v.findViewById(R.id.lblAviso);
			ImageButton btnAccion = (ImageButton) v.findViewById(R.id.btnVariable);
			LinearLayout linContenido = (LinearLayout) v.findViewById(R.id.linContenido);
						
			//Dar estilo inicial a los layouts
			linContenido.setBackgroundColor(res.getColor(R.color.colorWhite));
					
		
			// Cargar los datos en los controles
			String obra="";
			String situacion="";
			String aviso="";
			
			if (i.getDireccion().trim().contains("-") == true){
				obra= i.getCod_obra()+" - "+i.getDireccion();
			}else{
				obra= i.getCod_obra()+" - "+i.getMunicipio()+" - "+i.getDireccion();
			}
			
			
			if (i.getSituacion().equals("01")==true){
				situacion=i.getSituacionDescripcion()+ " " +  i.getFechaPlanificacion();
//				if (i.getComentarios().equals("")==false){
//					situacion += " - "+Html.fromHtml(i.getComentarios());
//				}
				if (i.getFechaAcepNombramiento().equals("")) {
					aviso = context.getResources().getString(R.string.mensaje_pendiente_aceptacion);
				}
				
			}else if (i.getSituacion().equals("01.1")==true){
					situacion=i.getSituacionDescripcion()+ " " +  i.getFechaPlanificacion();
//					if (i.getComentarios().equals("")==false){
//						situacion += " - "+Html.fromHtml(i.getComentarios());
//					}
					if (i.getFechaAcepNombramiento().equals("")) {
						aviso = context.getResources().getString(R.string.mensaje_pendiente_aceptacion);
					}	
			} else if (i.getSituacion().equals("02")==true){
				situacion=i.getSituacionDescripcion()+ " " +  i.getFechaInicio();
				
			} else if (i.getSituacion().equals("04")==true){
				situacion=i.getSituacionDescripcion()+ " " +  i.getFechaPeS();
				
			} else if (i.getSituacion().equals("05")==true){
				situacion=i.getSituacionDescripcion()+ " " +  i.getFechaFinal();
				aviso = context.getResources().getString(R.string.mensaje_pendiente_aceptacionpes_aco);
				
			} else if (i.getSituacion().equals("06")==true){
				situacion=i.getSituacionDescripcion()+ " " +  i.getFechaFinal();
				
			} else {
				situacion="";
			}
			
			lblAviso.setTextColor(res.getColor(R.color.colorRed2));
			if (i.getTSResultado().equals("4")==true){
				aviso = context.getResources().getString(R.string.mensaje_situacion_actual_rechazada);
				linContenido.setBackgroundColor(res.getColor(R.color.colorRedLight));
				lblAviso.setTextColor(res.getColor(R.color.colorWhite));
			}
			
			if (i.getTSResultado().equals("0")==true){
				aviso = context.getResources().getString(R.string.mensaje_aceptada_pes);
				linContenido.setBackgroundColor(res.getColor(R.color.colorGreenLight));
				lblAviso.setTextColor(res.getColor(R.color.colorWhite));
			}
			
			lblObra.setText(obra);
			lblObra.setTag(i);
			lblSituacion.setText(situacion);
			lblAviso.setText(aviso);
			
			if (aviso.equals("")==true){
				lblAviso.setVisibility(View.GONE);
			}else{
				lblAviso.setVisibility(View.VISIBLE);
			}
			
			//Establece el controlador de evento para abrir el detalle de la obra
			lblObra.setOnLongClickListener(new OnLongClickListener() {
					public boolean onLongClick(View v) {
						ObjetoAcometida i = (ObjetoAcometida) v.getTag();
						
						Intent intent = new Intent(context, DetalleObraActivity.class);
						String key = "codigoObra";
						String value =  i.getCod_obra().toString();
						intent.putExtra(key, value);
						act.startActivityForResult(intent, REQUEST_CODE_ACTIVITY_VIS);
												
						return false;
					}
			});
			
			//Establecer acci�n del bot�n variable en funci�n de la situaci�n de la obra
			btnAccion.setTag(i);
		    btnAccion.setVisibility(View.GONE);
		    
			if (i.getTSResultado().equals("4")==false){
				if (i.getSituacion().equals("01.1")==true){
					
					linContenido.setBackgroundColor(res.getColor(R.color.colorWhite));
	
					if (i.getFechaAcepNombramiento().equals("")==false) {
				    	btnAccion.setVisibility(View.VISIBLE);
					    btnAccion.setBackgroundColor(res.getColor(R.color.colorWhite));
					    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_inicio", null, context.getPackageName()));
					    btnAccion.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								ObjetoAcometida i = (ObjetoAcometida) v.getTag();
								procesarAccionSeleccionada("Iniciar", i);
							}
						});
					}
					
				} else if (i.getSituacion().equals("02")==true){
					
					linContenido.setBackgroundColor(res.getColor(R.color.colorBlue1));
				    
				    btnAccion.setVisibility(View.VISIBLE);
				    btnAccion.setBackgroundColor(res.getColor(R.color.colorBlue1));
				    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_pes", null, context.getPackageName()));
				    btnAccion.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							ObjetoAcometida i = (ObjetoAcometida) v.getTag();
							procesarAccionSeleccionada("PeS", i);
						}
					});
					
					
				} else if (i.getSituacion().equals("04")==true){
					
					linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
				    		    
				    btnAccion.setVisibility(View.VISIBLE);
				    btnAccion.setBackgroundColor(res.getColor(R.color.colorBlue2));
				    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_final", null, context.getPackageName()));
				    btnAccion.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							ObjetoAcometida i = (ObjetoAcometida) v.getTag();
							procesarAccionSeleccionada("Finalizar", i);
						}
					});
					
				} else if (i.getSituacion().equals("05")==true){
					
					linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
					btnAccion.setVisibility(View.GONE);
					
				} else {
					
				}
			}
			
		}

		// Devuelve la vista con los datos cargados en el Layout
		return v;

	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	03/10/2013
	 * @title	procesarAccionSeleccionada
	 * @comment	Procesar la acci�n seleccionada de la l�nea de acometidas
	 *
	 * @param accion	Clave de la acci�n seleccionada
	 * @param obj	    Objeto acometida seleccionada 
	 *
	 */
	private void procesarAccionSeleccionada(final String accion, final ObjetoAcometida obj){
		
	    String titulo = context.getResources().getString(R.string.title_dialog_confirmacion_accion);
		String mensaje ="";
		
		if (accion.equals("Iniciar")==true){
			mensaje += context.getResources().getString(R.string.mensaje_acometida_iniciar);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		if (accion.equals("PeS")==true){
			mensaje += context.getResources().getString(R.string.mensaje_acometida_pes);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		if (accion.equals("Finalizar")==true){
			mensaje += context.getResources().getString(R.string.mensaje_acometida_finalizar);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		
		String si = context.getResources().getString(R.string.yes_dialog);
		String no = context.getResources().getString(R.string.no_dialog);
						
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setTitle(titulo);
		builder.setMessage(mensaje);
		builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		builder.setPositiveButton(si, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
							
				//Ejecutar la acci�n seleccionada
				if (accion.equals("Iniciar")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionInicio(obj.getCod_obra());
				}
				if (accion.equals("PeS")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionPeS(obj.getCod_obra());
				}
				if (accion.equals("Finalizar")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionFinal(obj.getCod_obra());
				}
				
				//Recargar la lista de acometidas 
				UtilData ud = new UtilData(context);
				ud.cargarListaObrasEnCursoAcometidas(act, ordenacion.ESTADO, obj.getCod_obra());
				ud.cargarListaObrasEnCursoObras50m(act, ordenacion.ESTADO, obj.getCod_obra());
						
			}
		});

		final AlertDialog alert = builder.create();
		alert.show();
		
	}
	

	
	

}