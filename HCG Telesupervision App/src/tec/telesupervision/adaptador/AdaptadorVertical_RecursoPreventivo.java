package tec.telesupervision.adaptador;

import java.util.ArrayList;

import tec.telesupervision.Objects.ObjetoRecursoPreventivo;
import tec.telesupervision.ver2.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

/**
 * 
 * @author 	jjulia
 * @date 	05/11/2014
 * @title 	AdaptadorVertical_RecursoPreventivo
 * @comment	Adaptador para cargar una matriz de datos "RecursoPreventivo" en un Layout concreto
 * 
 */
public class AdaptadorVertical_RecursoPreventivo extends ArrayAdapter<ObjetoRecursoPreventivo> {

	private ArrayList<ObjetoRecursoPreventivo> objects;
	private LayoutInflater layoutInflater;


	/***
	 * 
	 * @author 	jjulia
	 * @date 	05/11/2014
	 * @title 	Constructor del adaptador
	 * @comment
	 * 
	 * @param context		         Contexto
	 * @param textViewResourceId     Layout particular donde deben cargarse los datos
	 * @param objects                Matriz con los datos a cargar
	 * 
	 */
	public AdaptadorVertical_RecursoPreventivo(Context context,	int textViewResourceId, ArrayList<ObjetoRecursoPreventivo> objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.layoutInflater = LayoutInflater.from(context);

	}
	
	
	 @Override
	 public View getView(final int position, View convertView, ViewGroup parent)
	 {
	        // holder pattern
	        Holder holder = null;
	        if (convertView == null)
	        {
	            holder = new Holder();
	 
	            convertView = layoutInflater.inflate(R.layout.listitem_vertical_recursopreventivo, null);
	            holder.setCheckView((CheckBox) convertView.findViewById(R.id.checkBox));
	            holder.setTextViewNombre((TextView) convertView.findViewById(R.id.textViewNombre));
	            holder.setTextViewCodigo((TextView) convertView.findViewById(R.id.textViewCodigo));
	            convertView.setTag(holder);
	        }
	        else
	        {
	            holder = (Holder) convertView.getTag();
	        }
	 
	        
	        final ObjetoRecursoPreventivo i = objects.get(position);
	        holder.getTextViewNombre().setText(i.getNom_JefeObra());
	        holder.getTextViewCodigo().setText(i.getCod_JefeObra());
	        
	        
	        //Procedimiento para asegurar que se modifica el ObjetoRecursoPreventivo originalmente asociado a este checkbox
            //para evitar que al reciclar la vista se reinicie el ObjetoRecursoPreventivo que antes se mostraba en esta
            //fila. Es imprescindible tagear el ObjetoRecursoPreventivo antes de establecer el valor del checkbox
	        holder.getCheckView().setTag(i.getCod_JefeObra());
	        holder.getCheckView().setChecked(i.isSeleccionado());
	        
	        changeBackground(getContext(), convertView, i.isSeleccionado());
	        final View fila= convertView;
	        
	        holder.getCheckView().setOnCheckedChangeListener(new OnCheckedChangeListener()
	        {
	        	public void onCheckedChanged(CompoundButton view, boolean isChecked)
	            {
	                
	                if (i.getCod_JefeObra().equals(view.getTag().toString()))
	                {
	                	i.setSeleccionado(isChecked);
	                	changeBackground(AdaptadorVertical_RecursoPreventivo.this.getContext(), fila,isChecked);
	               
	                	//desmarca todas los dem�s
	                	ObjetoRecursoPreventivo row = null;
	                	for(int i=0 ; i<getCount() ; i++)
	                	{
	                	if (i != position && isChecked)
	                	{
	                	row = (ObjetoRecursoPreventivo) getItem(i);
	                	row.setSeleccionado(false);
	                	}
	                	}
	                	notifyDataSetChanged();
	                
	                
	                }
	            }
	        });
	        
	        
	        
	        
	        
	        return convertView;
	 }
	 
	 private void changeBackground(Context context, View row, boolean checked)
	    {
	        if (checked)
	        {
	            row.setBackgroundDrawable((context.getResources().getDrawable(R.drawable.listview_selector_checked)));
	        }
	        else
	        {
	            row.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.listview_selector));
	        }
	    }
}



class Holder
{
	CheckBox checkView;
    TextView textViewNombre;
    TextView textViewCodigo;
    
    
    public CheckBox getCheckView() {
		return checkView;
	}
	public void setCheckView(CheckBox checkView) {
		this.checkView = checkView;
	}
	public TextView getTextViewNombre() {
		return textViewNombre;
	}
	public void setTextViewNombre(TextView textViewNombre) {
		this.textViewNombre = textViewNombre;
	}
	public TextView getTextViewCodigo() {
		return textViewCodigo;
	}
	public void setTextViewCodigo(TextView textViewCodigo) {
		this.textViewCodigo = textViewCodigo;
	}
	
}