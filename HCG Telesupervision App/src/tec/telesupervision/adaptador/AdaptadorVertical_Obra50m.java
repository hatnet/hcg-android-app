package tec.telesupervision.adaptador;

import java.util.ArrayList;

import tec.telesupervision.Objects.ObjetoObra50m;
import tec.telesupervision.Objects.ObjetoRecursoPreventivo;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.data.instruccionesSQL.ordenacion;
import tec.telesupervision.mod.aplicacion.DetalleObraActivity;
import tec.telesupervision.services.ProcesosEjecucionAcciones;
import tec.telesupervision.ver2.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 
 * @author 	jjulia
 * @date 	05/09/2014
 * @title 	Adaptador para cargar una matriz de datos "ObjetoObra50m" en un Layout concreto
 * @comment
 * 
 */
public class AdaptadorVertical_Obra50m extends ArrayAdapter<ObjetoObra50m> {

	private Context context;
	private Activity act;
	private ArrayList<ObjetoObra50m> objects;
	
	private final int REQUEST_CODE_ACTIVITY_VIS = 1;
	
	
	/***
	 * 
	 * @author jjulia
	 * @date 05/10/2012
	 * @title Constructor del adaptador
	 * @comment
	 * 
	 * @param context		      Contexto
	 * @param textViewResourceId  Layout particular donde deben cargarse los datos
	 * @param objects             Matriz con los datos a cargar
	 * 
	 */
	public AdaptadorVertical_Obra50m(Context context, int textViewResourceId, ArrayList<ObjetoObra50m> objects) {
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.context = context;
		this.act= (Activity) context;
		
		
	}
	
	
	/**
	 * Gestionar la carga de los datos en los elementos del listitem
	 */
	public View getView(int position, View convertView, ViewGroup parent) {

		Resources res = context.getResources();
		
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.listitem_vertical_obra50m, null);
		}
		
		
				// Obtener los valores del listitem
				ObjetoObra50m i = objects.get(position);
				if (i != null) {
									
					// Obtener los controles 
					TextView lblObra = (TextView) v.findViewById(R.id.lblObra);
					TextView lblSituacion = (TextView) v.findViewById(R.id.lblSituacion);
					TextView lblAviso = (TextView) v.findViewById(R.id.lblAviso);
					ImageButton btnAccion = (ImageButton) v.findViewById(R.id.btnVariable);
					LinearLayout linContenido = (LinearLayout) v.findViewById(R.id.linContenido);
								
					//Dar estilo inicial a los layouts
					linContenido.setBackgroundColor(res.getColor(R.color.colorWhite));
							
				
					// Cargar los datos en los controles
					String obra="";
					String situacion="";
					String aviso="";
					
					if (i.getDireccion().trim().contains("-") == true){
						obra= i.getCod_obra()+" - "+i.getDireccion();
					}else{
						obra= i.getCod_obra()+" - "+i.getMunicipio()+" - "+i.getDireccion();
					}
				
					if (i.getSituacion().equals("01")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaPlanificacion();

						if (i.getFechaAcepNombramiento().equals("")) {
							aviso = context.getResources().getString(R.string.mensaje_pendiente_aceptacion);
						}
				
					} else if (i.getSituacion().equals("01.1")==true){
							situacion=i.getSituacionDescripcion()+ " " +  i.getFechaPlanificacion();

							if (i.getFechaAcepNombramiento().equals("")) {
								aviso = context.getResources().getString(R.string.mensaje_pendiente_aceptacion);
							}
					} else if (i.getSituacion().equals("02")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaInicio();
						
					} else if (i.getSituacion().equals("02.1")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaTrazadoSolicitud();
						aviso = context.getResources().getString(R.string.mensaje_pendiente_confirmaciontrazado);
						
					} else if (i.getSituacion().equals("02.2")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaTrazadoConfirmacion();
					
					} else if (i.getSituacion().equals("02.3")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaTrazadoSolicitudMod();
						aviso = context.getResources().getString(R.string.mensaje_pendiente_confirmacionmodificaciontrazado);
						
					} else if (i.getSituacion().equals("02.4")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaTrazadoConfirmacionMod();
						
					} else if (i.getSituacion().equals("03.1")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaSolicitudPeS();
						aviso = context.getResources().getString(R.string.mensaje_pendiente_aceptacioncontenidos);
						
					} else if (i.getSituacion().equals("03.2")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaAcepContenidos();
						
					} else if (i.getSituacion().equals("03.3")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaFinalPrueba();
						aviso = context.getResources().getString(R.string.mensaje_pendiente_aceptacionpes);
						
					} else if (i.getSituacion().equals("03.4")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaAceptadaPeG();
						if(i.getPerfil().equals("JOC")==true){
							aviso = context.getResources().getString(R.string.mensaje_pendiente_aceptacionpes);
						}
						
					} else if (i.getSituacion().equals("04")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaPeS();
						
					} else if (i.getSituacion().equals("04.1")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaFinal();
						aviso = context.getResources().getString(R.string.mensaje_pendiente_aceptacionfinal);
						
					} else if (i.getSituacion().equals("04.2")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getFechaFinal();
						aviso = context.getResources().getString(R.string.mensaje_pendiente_cierredocumental);
					
					} else if (i.getSituacion().equals("06")==true){
						situacion=i.getSituacionDescripcion()+ " " +  i.getTSFechaResultado();
						//aviso = context.getResources().getString(R.string.mensaje_aceptada);
						
					} else {
						situacion="";
					}
					
					lblAviso.setTextColor(res.getColor(R.color.colorRed2));
					if (i.getTSResultado().equals("4")==true){
						aviso = context.getResources().getString(R.string.mensaje_situacion_actual_rechazada);
						linContenido.setBackgroundColor(res.getColor(R.color.colorRedLight));
						lblAviso.setTextColor(res.getColor(R.color.colorWhite));
					}
					
					if (i.getTSResultado().equals("0")==true){
						linContenido.setBackgroundColor(res.getColor(R.color.colorGreenLight));
						lblAviso.setTextColor(res.getColor(R.color.colorWhite));
					}
				
					
					lblObra.setText(obra);
					lblObra.setTag(i);
					lblSituacion.setText(situacion);
					lblAviso.setText(aviso);

					if (aviso.equals("")==true){
						lblAviso.setVisibility(View.GONE);
					}else{
						lblAviso.setVisibility(View.VISIBLE);
					}
					
				
					//Establece el controlador de evento para abrir el detalle de la obra
					lblObra.setOnLongClickListener(new OnLongClickListener() {
							public boolean onLongClick(View v) {
								ObjetoObra50m i = (ObjetoObra50m) v.getTag();
																
								Intent intent = new Intent(context, DetalleObraActivity.class);
								String key = "codigoObra";
								String value =  i.getCod_obra().toString();
								intent.putExtra(key, value);
								act.startActivityForResult(intent, REQUEST_CODE_ACTIVITY_VIS);
								
								return true;
							}
					});
					
					
					//Establecer acci�n del bot�n variable en funci�n de la situaci�n de la obra
					btnAccion.setTag(i);
				    btnAccion.setVisibility(View.GONE);
				    
				    if (i.getTSResultado().equals("4")==false && i.getPerfil().equals("JOC")==true){
				    
							if (i.getSituacion().equals("01.1")==true){
								
								linContenido.setBackgroundColor(res.getColor(R.color.colorWhite));
						    			    			    	
								if (i.getFechaAcepNombramiento().equals("")==false) {
							    	btnAccion.setVisibility(View.VISIBLE);
								    btnAccion.setBackgroundColor(res.getColor(R.color.colorWhite));
								    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_inicio", null, context.getPackageName()));
								    btnAccion.setOnClickListener(new OnClickListener() {
										public void onClick(View v) {
											ObjetoObra50m i = (ObjetoObra50m) v.getTag();
											procesarAccionSeleccionada("Iniciar", i);
										}
									});
								}
							    
							
							} else if (i.getSituacion().equals("02")==true){
									
									linContenido.setBackgroundColor(res.getColor(R.color.colorBlue1));
									
									btnAccion.setVisibility(View.VISIBLE);
									btnAccion.setBackgroundColor(res.getColor(R.color.colorBlue1));
									btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_solicitud_trazado", null, context.getPackageName()));
									btnAccion.setOnClickListener(new OnClickListener() {
											public void onClick(View v) {
												ObjetoObra50m i = (ObjetoObra50m) v.getTag();
												procesarAccionSeleccionada("Trazado", i);
											}
									});    
									
								
						    } else if (i.getSituacion().equals("02.1")==true){
								
									linContenido.setBackgroundColor(res.getColor(R.color.colorYelow3));
						
								    
						    } else if (i.getSituacion().equals("02.2")==true){
								
									linContenido.setBackgroundColor(res.getColor(R.color.colorYelow3));
									
									if (i.getFechaTrazadoConfirmacion().equals("")==false) {
										btnAccion.setVisibility(View.VISIBLE);
										btnAccion.setBackgroundColor(res.getColor(R.color.colorYelow3));
									    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_acepconten", null, context.getPackageName()));
									    btnAccion.setOnClickListener(new OnClickListener() {
											public void onClick(View v) {
												ObjetoObra50m i = (ObjetoObra50m) v.getTag();
												procesarAccionSeleccionada("InicioPrueba", i);
											}
										});    
									}
								
						    } else if (i.getSituacion().equals("02.3")==true){
								
								linContenido.setBackgroundColor(res.getColor(R.color.colorOrange2));
								
						    
					    	} else if (i.getSituacion().equals("02.4")==true){
							
								linContenido.setBackgroundColor(res.getColor(R.color.colorOrange2));
								
								if (i.getFechaTrazadoConfirmacion().equals("")==false) {
									btnAccion.setVisibility(View.VISIBLE);
									btnAccion.setBackgroundColor(res.getColor(R.color.colorOrange2));
								    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_acepconten", null, context.getPackageName()));
								    btnAccion.setOnClickListener(new OnClickListener() {
										public void onClick(View v) {
											ObjetoObra50m i = (ObjetoObra50m) v.getTag();
											procesarAccionSeleccionada("InicioPrueba", i);
										}
									});     
								}
								
					    	} else if (i.getSituacion().equals("03.1")==true){
								
				    			linContenido.setBackgroundColor(res.getColor(R.color.colorBlue11));
				    			btnAccion.setVisibility(View.GONE);
								
								
								
					    	} else if (i.getSituacion().equals("03.2")==true){
								
								linContenido.setBackgroundColor(res.getColor(R.color.colorBlue11));
								btnAccion.setVisibility(View.VISIBLE);
								btnAccion.setBackgroundColor(res.getColor(R.color.colorBlue11));
								btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_conexion", null, context.getPackageName()));
								btnAccion.setOnClickListener(new OnClickListener() {
										public void onClick(View v) {
											ObjetoObra50m i = (ObjetoObra50m) v.getTag();
											procesarAccionConexionPurgado(i);
										}
								});  							
								
					    	
								
					    	} else if (i.getSituacion().equals("03.3")==true || i.getSituacion().equals("03.4")==true){
								
					    			linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
					    			btnAccion.setVisibility(View.GONE);
									
									
								
							} else if (i.getSituacion().equals("04")==true){
								
								linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
								btnAccion.setVisibility(View.VISIBLE);
								btnAccion.setBackgroundColor(res.getColor(R.color.colorBlue2));
								if (i.getTSResultado().equals("0")==true){
									linContenido.setBackgroundColor(res.getColor(R.color.colorGreenLight));
									btnAccion.setBackgroundColor(res.getColor(R.color.colorGreenLight));
								}	    
							    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_finalobra", null, context.getPackageName()));
							    btnAccion.setOnClickListener(new OnClickListener() {
									public void onClick(View v) {
										ObjetoObra50m i = (ObjetoObra50m) v.getTag();
										procesarAccionSeleccionada("Finalizar", i);
									}
								});
							
								
						    } else if (i.getSituacion().equals("04.1")==true){
								
									linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
						    
						    } else if (i.getSituacion().equals("04.2")==true){
								
								linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
					
															
							} else {
								
							}
							
							
						}
				    
					    if (i.getTSResultado().equals("4")==false && i.getPerfil().equals("")==true){
					    	
					    	if (i.getSituacion().equals("03.3")==true){
								
				    			linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
				    			btnAccion.setVisibility(View.GONE);
							
					    	}
					    	
					    	if (i.getSituacion().equals("03.4")==true){
								
								linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
								btnAccion.setVisibility(View.VISIBLE);
								btnAccion.setBackgroundColor(res.getColor(R.color.colorBlue2));
								btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_pes", null, context.getPackageName()));
								btnAccion.setOnClickListener(new OnClickListener() {
										public void onClick(View v) {
											ObjetoObra50m i = (ObjetoObra50m) v.getTag();
											procesarAccionSeleccionada("PeS", i);
										}
								});  							
								
					    	} 
					    	
					    	if (i.getSituacion().equals("04")==true){
								
								linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
								btnAccion.setVisibility(View.VISIBLE);
								btnAccion.setBackgroundColor(res.getColor(R.color.colorBlue2));
								if (i.getTSResultado().equals("0")==true){
									linContenido.setBackgroundColor(res.getColor(R.color.colorGreenLight));
									btnAccion.setBackgroundColor(res.getColor(R.color.colorGreenLight));
								}	    
							    btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_finalobra", null, context.getPackageName()));
							    btnAccion.setOnClickListener(new OnClickListener() {
									public void onClick(View v) {
										ObjetoObra50m i = (ObjetoObra50m) v.getTag();
										procesarAccionSeleccionada("Finalizar", i);
									}
								});
							
						    }
					    	
					    	 if (i.getSituacion().equals("04.1")==true){
									
					    			linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
					    			btnAccion.setVisibility(View.GONE);
									
							}
					    	 
					    	 if (i.getSituacion().equals("04.2")==true){
									
					    			linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
					    			btnAccion.setVisibility(View.GONE);
									
							}
					    	
					    }
					    
					    
					    if (i.getTSResultado().equals("4")==false && i.getPerfil().equals("RPREV")==true){
					    	
					    	if (i.getSituacion().equals("03.3")==true){
								
				    			linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
				    			btnAccion.setVisibility(View.GONE);
							
					    	}
					    	
					    	if (i.getSituacion().equals("03.4")==true){
								
								linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
								btnAccion.setVisibility(View.VISIBLE);
								btnAccion.setBackgroundColor(res.getColor(R.color.colorBlue2));
								btnAccion.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_pes_rp", null, context.getPackageName()));
								btnAccion.setOnClickListener(new OnClickListener() {
										public void onClick(View v) {
											ObjetoObra50m i = (ObjetoObra50m) v.getTag();
											procesarAccionSeleccionada("PeS", i);
										}
								});  							
								
					    	} 
					    	
					    	 if (i.getSituacion().equals("04")==true || i.getSituacion().equals("04.1")==true || i.getSituacion().equals("04.2")==true){
									
					    			linContenido.setBackgroundColor(res.getColor(R.color.colorBlue2));
					    			btnAccion.setVisibility(View.GONE);
									
							}
					    	
					    }
				    
				    
				}

				// Devuelve la vista con los datos cargados en el Layout
				return v;

			}
			
			
			/**
			 * 
			 * @author	jjulia
			 * @date	03/10/2013
			 * @title	procesarAccionSeleccionada
			 * @comment	Procesar la acci�n seleccionada de la l�nea de acometidas
			 *
			 * @param accion	Clave de la acci�n seleccionada
			 * @param obj	    Objeto acometida seleccionada 
			 *
			 */
			private void procesarAccionSeleccionada(final String accion, final ObjetoObra50m obj){
				
			    String titulo = context.getResources().getString(R.string.title_dialog_confirmacion_accion);
				String mensaje ="";
								
				if (accion.equals("Iniciar")==true){
					mensaje += context.getResources().getString(R.string.mensaje_obra50m_iniciar);
					mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
				}

				if (accion.equals("Trazado")==true){
					mensaje += context.getResources().getString(R.string.mensaje_obra50m_trazado);
					mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
				}
				
				if (accion.equals("InicioPrueba")==true){
					mensaje += context.getResources().getString(R.string.mensaje_obra50m_inicio_prueba);
					mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
				}
//				if (accion.equals("SolicitudPeG")==true){
//					mensaje += context.getResources().getString(R.string.mensaje_obra50m_final_prueba);
//					mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
//				}
												
				if (accion.equals("PeS")==true){
					mensaje += context.getResources().getString(R.string.mensaje_obra50m_pes);
					mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
				}
				
				if (accion.equals("Finalizar")==true){
					mensaje += context.getResources().getString(R.string.mensaje_obra50m_finalizar);
					mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
				}
				
				
				String si = context.getResources().getString(R.string.yes_dialog);
				String no = context.getResources().getString(R.string.no_dialog);
								
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(titulo);
				builder.setMessage(mensaje);
				builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

				builder.setPositiveButton(si, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
									
						//Ejecutar la acci�n seleccionada
						if (accion.equals("Iniciar")==true){
							ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
							prc.ejecucionAccionInicio(obj.getCod_obra());
						}
						if (accion.equals("Trazado")==true){
							ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
							prc.ejecucionAccionTrazado(obj.getCod_obra());
						}
						if (accion.equals("InicioPrueba")==true){
							ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
							prc.ejecucionAccionSolicitudPeS(obj.getCod_obra());
						}
//						if (accion.equals("SolicitudPeG")==true){
//							ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
//							prc.ejecucionAccionFinalPrueba(obj.getCod_obra());
//						}
						if (accion.equals("PeS")==true){
							ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
							prc.ejecucionAccionPeS(obj.getCod_obra());
						}
						if (accion.equals("Finalizar")==true){
							ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
							prc.ejecucionAccionFinal(obj.getCod_obra());
						}
						
						//Recargar la lista de acometidas 
						UtilData ud = new UtilData(context);
						ud.cargarListaObrasEnCursoAcometidas(act, ordenacion.ESTADO, obj.getCod_obra());
						ud.cargarListaObrasEnCursoObras50m(act, ordenacion.ESTADO, obj.getCod_obra());
								
					}
				});

				final AlertDialog alert = builder.create();
				alert.show();
				
			}
			
			
			/**
			 * 
			 * @author	jjulia
			 * @date	11/11/2014
			 * @title	procesarAccionConexionPurgado
			 * @comment	Procedimiento para la acci�n de Conexi�n y Purgado y la selecci�n del Recurso preventivo
			 *
			 * @param obj	Objeto obra seleccionada
			 *
			 */
			private void procesarAccionConexionPurgado(ObjetoObra50m obj){
				
				//Generamos un dialogo para seleccionar el recurso preventivo y confirmar la acci�n
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
				final View layout = inflater.inflate(R.layout.dialog_recursopreventivo,(ViewGroup) act.findViewById(R.layout.activity_detalleobra));
				builder.setView(layout);
				
				final AlertDialog alertDialog = builder.create();
				
				//Carga los recursos preventivos en la lista
				UtilData ud = new UtilData(context);
				String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
				ud.cargarListaRecursosPreventivos(layout,  obj.getCod_obra(),  userName);
				
			
				//Establece los controladores de eventos para las acciones de los botenes del dialogo
				Button btnAceptar =  (Button) layout.findViewById(R.id.btnAceptarRecurso);
				Button btnCancelar =  (Button) layout.findViewById(R.id.btnCancelarRecurso);
				
				btnAceptar.setTag(obj);
						
				btnCancelar.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						alertDialog.dismiss();
					}
				
				});
				
				btnAceptar.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						Button btn = (Button) v ;  
						String Cod_RecursoPreventivo="";
						
						//Localiza el Recurso preventivo seleccionado
						ListView lstRecursos = (ListView) layout.findViewById(R.id.lstRecPreventivos);
						AdaptadorVertical_RecursoPreventivo m_adapter= (AdaptadorVertical_RecursoPreventivo) lstRecursos.getAdapter();

						for(int i=0 ; i<m_adapter.getCount() ; i++){
							ObjetoRecursoPreventivo obj = m_adapter.getItem(i);
							if (obj.isSeleccionado()==true){
								Cod_RecursoPreventivo=obj.getCod_JefeObra();
								break;
							}
						}
						
											
						//Ejecuta la acci�n 
						ObjetoObra50m aObra = (ObjetoObra50m) btn.getTag();	
															
						ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
						prc.ejecucionAccionFinalPrueba(aObra.getCod_obra(), Cod_RecursoPreventivo);

						//Recargar las listas de acometidas y obras
						UtilData ud = new UtilData(context);
						ud.cargarListaObrasEnCursoAcometidas(act, ordenacion.ESTADO, aObra.getCod_obra());
						ud.cargarListaObrasEnCursoObras50m(act, ordenacion.ESTADO, aObra.getCod_obra());
													
						alertDialog.dismiss();
					}
				
				});
				
				// Mostramos el alertdialog
				alertDialog.show();
								
			}
			

	

}