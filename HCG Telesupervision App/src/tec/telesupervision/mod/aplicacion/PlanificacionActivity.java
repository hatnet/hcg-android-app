package tec.telesupervision.mod.aplicacion;

import tec.telesupervision.data.UtilData;
import tec.telesupervision.data.instruccionesSQL.ordenacion;
import tec.telesupervision.ver2.R;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.OnTabChangeListener;

public class PlanificacionActivity extends Activity { 
	
	private Context context;
	private Activity act;

	/**
	 * Procedimiento inicial de la Actividad
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_planificacion);
		
		this.context= (Context) this;
		this.act= (Activity) this;
			
		establecerTituloCabecera();
		establecerTituloPlanificacion();
		inicializarBotonesCabecera();
		inicializarTabControl();
		
	}
	

	
	
	/**
	 * Evitar que se pierdan los valores cuando se canvia la orientaci�n de la
	 * pantalla en el manifiesto se debe a�adir a la actividad ...
	 * 
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	/**
	 * Evento al recuperar el primer plano la activity
	 */
	@Override
	protected void onResume() {
		super.onResume();

		TabHost tabs=(TabHost)findViewById(android.R.id.tabhost);
		UtilData ud = new UtilData(context);
		
		if (tabs.getCurrentTabTag().equals("Acometidas")==true){
			ud.cargarListaPlanificacionAcometidas(act, ordenacion.MUNICIPIO);
		}
		if (tabs.getCurrentTabTag().equals("Obras50m")==true){
			ud.cargarListaPlanificacionObras50m(act, ordenacion.MUNICIPIO);
		}

	}

	
	/***
	 * 
	 * @author		jjulia
	 * @date		08/08/2013
	 * @title		inicializarBotonesCabecera
	 * @comment		Iniciliazar los botones gen�ricos de cabecera para el m�dulo en particular.
	 *	
	 *
	 */
	private void inicializarBotonesCabecera(){
	
		ImageButton btnCancelarPlanificacion = (ImageButton) findViewById(R.id.btnBotonCabecera1);
		btnCancelarPlanificacion.setImageResource(getResources().getIdentifier("drawable/" + "cabecera_cancelar", null, getPackageName()));
		btnCancelarPlanificacion.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
						
				finish();
			}
		});
		
		ImageButton btnSinUso2 = (ImageButton) findViewById(R.id.btnBotonCabecera2);
		btnSinUso2.setVisibility(View.GONE);
	
	}
	
	
	/**
	 *
	 * @author	jjulia
	 * @date	24/09/2013
	 * @title	inicializarTabControl
	 * @comment	Procedimiento para la inicializaci�n del control de Pesta�as
	 *	
	 */
	private void inicializarTabControl(){
		
		Resources res = getResources();
		 
		TabHost tabs=(TabHost)findViewById(android.R.id.tabhost);
		tabs.setup();
		 
		TabHost.TabSpec spec=tabs.newTabSpec("Acometidas");
		spec.setContent(R.id.tbPlanifAcometidas);
		spec.setIndicator(res.getString(R.string.tab_title_PlanifAcometidas), res.getDrawable(R.drawable.tab_planificacion_acometida));
		tabs.addTab(spec);
		 
		spec=tabs.newTabSpec("Obras50m");
		spec.setContent(R.id.tbPlanifObras50m);
		spec.setIndicator(res.getString(R.string.tab_title_PlanifObras50m), res.getDrawable(R.drawable.tab_planificacion_obra50m));
		tabs.addTab(spec);
		
		tabs.setCurrentTab(0);
		
		//Evento de cambio de pesta�a
		tabs.setOnTabChangedListener(new OnTabChangeListener() {
		    public void onTabChanged(String tabId) {
		    	UtilData ud = new UtilData(context);
		    	
		        if (tabId.equals("Acometidas")==true){
		        	ud.cargarListaPlanificacionAcometidas(act, ordenacion.MUNICIPIO);
		        }
		        if (tabId.equals("Obras50m")==true){
		        	ud.cargarListaPlanificacionObras50m(act, ordenacion.MUNICIPIO);
		        }
		    }
		});
		
	}

	
	/***
	 * 
	 * @author		jjulia
	 * @date		08/08/2013
	 * @title		onCreateOptionsMenu / onOptionsItemSelected
	 * @comment		Gestionar el men� de opciones de la actividad
	 *	
	 *
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_activity_planificacion, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		UtilData ud = new UtilData(this);
		TabHost tabs=(TabHost)findViewById(android.R.id.tabhost);
		
		switch (item.getItemId()) {
		case R.id.ordenacionMunicipio:
					
			if (tabs.getCurrentTabTag().equals("Acometidas")==true){
				ud.cargarListaPlanificacionAcometidas(act, ordenacion.MUNICIPIO);
			}
			if (tabs.getCurrentTabTag().equals("Obras50m")==true){
				ud.cargarListaPlanificacionObras50m(act, ordenacion.MUNICIPIO);
			}
			break;

		case R.id.ordenacionObra:
			
			if (tabs.getCurrentTabTag().equals("Acometidas")==true){
				ud.cargarListaPlanificacionAcometidas(act, ordenacion.OBRA);
			}
			if (tabs.getCurrentTabTag().equals("Obras50m")==true){
				ud.cargarListaPlanificacionObras50m(act, ordenacion.OBRA);
			}
			break;
	
		}

		return true;
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	02/10/2013
	 * @title	establecerTituloCabecera
	 * @comment	Establecer el t�tulo de la cabecera
	 *	
	 *
	 */
	private void establecerTituloCabecera(){
		
		UtilData ud = new UtilData(this);
		Resources res = this.getResources();
    	String titulo=res.getString(R.string.app_description)+ " - "+ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
		this.setTitle(titulo);
		
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	02/10/2013
	 * @title	establecerTituloPlanificacion
	 * @comment	Establecer el t�tulo de planificaci�n
	 *	
	 *
	 */
	private void establecerTituloPlanificacion(){
		
		Resources res = this.getResources();
						
		// Establecer el t�tulo de la presentaci�n de Planificaci�n
		TextView lblPlanificacion = (TextView) findViewById(R.id.lblTitulo);
			
    	String tituloPlanificacion=res.getString(R.string.title_planificacion);
		lblPlanificacion.setText(tituloPlanificacion);
		
	}
	
	

	
}
