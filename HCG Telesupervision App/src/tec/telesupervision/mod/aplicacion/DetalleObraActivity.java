package tec.telesupervision.mod.aplicacion;

import java.util.Date;

import tec.controles.calendar.CalendarControl;
import tec.telesupervision.Objects.ObjetoObra;
import tec.telesupervision.Objects.ObjetoPlanificacion;
import tec.telesupervision.Objects.ObjetoRecursoPreventivo;
import tec.telesupervision.adaptador.AdaptadorVertical_RecursoPreventivo;
import tec.telesupervision.data.UtilData;
import tec.telesupervision.data.instruccionesSQL.ordenacion;
import tec.telesupervision.services.ProcesosEjecucionAcciones;
import tec.telesupervision.utilidades.UtilidadesFechas;
import tec.telesupervision.ver2.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.BufferType;

public class DetalleObraActivity extends Activity { 
	
	private Context context;
	private Activity act;
	private String codigoObra;
	private ObjetoObra oObra;

	/**
	 * Procedimiento inicial de la Actividad
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detalleobra);
		
		this.context= (Context) this;
		this.act= (Activity) this;
		
	  	Bundle extras = this.getIntent().getExtras();    	
    	this.codigoObra = extras.getString("codigoObra");
    	this.oObra=null;
    	
		establecerTituloCabecera();
		inicializarBotonesCabecera();
	
		establecerPresentacionObra();    	
		
	}
	
	
	/***
	 * 
	 * Procedimiento para establecer  y cargar los datos de la obra
	 */
	private void establecerPresentacionObra(){
		
		inicializarDatosObra();
		establecerTituloDetalleObra();
		inicializarCargarDatosObraEnControles();
		inicializarBotonesAccion();
		
	}
	
	
	
	/**
	 * Evitar que se pierdan los valores cuando se canvia la orientaci�n de la
	 * pantalla en el manifiesto se debe a�adir a la actividad ...
	 * 
	 */
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	
	/**
	 * Evento al recuperar el primer plano la activity
	 */
	@Override
	protected void onResume() {
		super.onResume();
		
	}
	

	/**
	 * Evento al pulsar el bot�n Atr�s del dispositivo m�vil
	 */
	@Override
	public void onBackPressed() {

		finalizarDetalle();

	}
		
	
	
	/***
	 * Procedimeinto para cerra la ventana de detalle y devolver el C�digo de Obra a la actividad principal
	 */
	private void finalizarDetalle(){
	
		Intent resupd = new Intent();
		resupd.putExtra("codigoObra", this.codigoObra );
		this.setResult(RESULT_OK, resupd);
		this.finish();
	}

	
	/***
	 * 
	 * @author		jjulia
	 * @date		08/08/2013
	 * @title		inicializarBotonesCabecera
	 * @comment		Iniciliazar los botones gen�ricos de cabecera para el m�dulo en particular.
	 *	
	 *
	 */
	private void inicializarBotonesCabecera(){
	
		ImageButton btnCancelarPlanificacion = (ImageButton) findViewById(R.id.btnBotonCabecera1);
		btnCancelarPlanificacion.setImageResource(getResources().getIdentifier("drawable/" + "cabecera_cancelar", null, getPackageName()));
		btnCancelarPlanificacion.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
						
				 finalizarDetalle();
			}
		});
		
		ImageButton btnSinUso2 = (ImageButton) findViewById(R.id.btnBotonCabecera2);
		btnSinUso2.setVisibility(View.GONE);
	
	}

		
	/**
	 * 
	 * @author	jjulia
	 * @date	02/10/2013
	 * @title	establecerTituloCabecera
	 * @comment	Establecer el t�tulo de la cabecera
	 *	
	 *
	 */
	private void establecerTituloCabecera(){
		
		UtilData ud = new UtilData(this);
		Resources res = this.getResources();
    	String titulo=res.getString(R.string.app_description)+ " - "+ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
		this.setTitle(titulo);
		
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	02/10/2014
	 * @title	establecerTituloDetalleObra
	 * @comment	Establecer el t�tulo de la presentaci�n
	 *	
	 *
	 */
	private void establecerTituloDetalleObra(){
		
		Resources res = this.getResources();
		TextView lblTitulo = (TextView) findViewById(R.id.lblTitulo);
			
    	String titulo="";
    	if (this.oObra.getTipoObra().equals("1")==true){
    		titulo=res.getString(R.string.title_1_detalleobra) + " - " + this.codigoObra ;
    	}
    	if (this.oObra.getTipoObra().equals("2")==true){
    		titulo=res.getString(R.string.title_2_detalleobra) + " - " + this.codigoObra ;
    	}
    	lblTitulo.setText(titulo);
		
	}
	
	
	/***
	 * 
	 * @author		jjulia
	 * @date		01/10/2014
	 * @title		inicializarDatosObra
	 * @comment		Iniciliazar la colecci�n de los datos gen�ricos de la obra
	 *	
	 *
	 */
	private void inicializarDatosObra(){
		
		UtilData ud = new UtilData(this);
		this.oObra = ud.ObtenerDetalleGenericoObra(this.codigoObra);
		
	}
	
	/***
	 * 
	 * @author		jjulia
	 * @date		01/10/2014
	 * @title		inicializarDatosObra
	 * @comment		Cargar los datos gen�ricos de la obra en controles
	 *	
	 *
	 */
	private void inicializarCargarDatosObraEnControles(){
				
		TextView lblDireccion = (TextView) this.findViewById(R.id.lblDireccion);
		TextView lblGestorObra = (TextView) this.findViewById(R.id.lblGestorObra);
		TextView lblSituacion = (TextView) this.findViewById(R.id.lblSituacion);
		TextView lblHTML = (TextView) this.findViewById(R.id.lblHTML);
		
	
		String direccion ="";
		if (this.oObra.getDireccion().trim().contains("-") == true){
			direccion= this.oObra.getDireccion();
		}else{
			direccion= this.oObra.getMunicipio()+" - "+this.oObra.getDireccion();
		}
		lblDireccion.setText(direccion);
				
		lblGestorObra.setText(this.oObra.getTSGestorObra());
		
		lblSituacion.setText(oObra.getSituacionDescripcion());
		
		
		if (oObra.getHistorialObra() != null){
			lblHTML.setText(Html.fromHtml(oObra.getHistorialObra()), BufferType.SPANNABLE);
			lblHTML.setVisibility(View.VISIBLE);
		}
		
		
	}
	

	/***
	 * 
	 * @author		jjulia
	 * @date		08/08/2013
	 * @title		inicializarBotonesAccion
	 * @comment		Iniciliazar los botones gen�ricos deacci�n para la situaci�n particular de la obra.
	 *	
	 *
	 */
	private void inicializarBotonesAccion(){
	
		Resources res = context.getResources();
		
		ImageButton btnVar1 = (ImageButton) findViewById(R.id.btnVariable1);
		ImageButton btnVar2 = (ImageButton) findViewById(R.id.btnVariable2);
		ImageButton btnVar3 = (ImageButton) findViewById(R.id.btnVariable3);
		btnVar1.setVisibility(View.GONE);
		btnVar2.setVisibility(View.GONE);
		btnVar3.setVisibility(View.GONE);
		
		//Acci�n comentarios de obra
		btnVar1.setVisibility(View.VISIBLE);
		btnVar1.setTag(this.oObra);
		btnVar1.setImageResource(res.getIdentifier("drawable/" + "accion_comentarios", null, getPackageName()));
		btnVar1.setBackgroundColor(res.getColor(R.color.colorWhite));
		btnVar1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				ObjetoObra i = (ObjetoObra) v.getTag();
				procesarAccionComentariosObra(i);
						
			}
		});
		
		//Botones de acciones complementarias s�lo sino est� Rechazada
		if (this.oObra.getTSResultado().equals("4")==false){
		
		
				// Situaci�n 01: Prevista
				if (this.oObra.getSituacion().equals("01.1")==true){
					if (this.oObra.getFechaAcepNombramiento().equals("")==false) {
						
						btnVar2.setVisibility(View.VISIBLE);
						btnVar2.setTag(this.oObra);
						btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_inicio", null, context.getPackageName()));
						btnVar2.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								ObjetoObra i = (ObjetoObra) v.getTag();
								procesarAccionSeleccionada("Iniciar", i);
							}
						});   
					}
				}
				
				// Situaci�n 01/01.1: Prevista ejecuci�n / Disposici�n ejecuci�n
				if (this.oObra.getSituacion().equals("01")==true || this.oObra.getSituacion().equals("01.1")==true){
				
					    btnVar2.setVisibility(View.VISIBLE);
						btnVar2.setTag(this.oObra);
						btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_imprevistosproyecto", null, context.getPackageName()));
						btnVar2.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								ObjetoObra i = (ObjetoObra) v.getTag();
								procesarAccionImprevistosProyecto(i);
							}
						});  
						
						btnVar3.setVisibility(View.VISIBLE);
						btnVar3.setTag(this.oObra);
						btnVar3.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar3.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_cambioplanificacion", null, context.getPackageName()));
						btnVar3.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
									ObjetoObra i = (ObjetoObra) v.getTag();
									procesarAccionCambioPlanificacion(i);
								}
						});   
							
				}
				
			
						
				// Situaci�n 02: Iniciada
				if (this.oObra.getSituacion().equals("02")==true){
				
					if (this.oObra.getTipoObra().equals("1")==true){
						btnVar2.setVisibility(View.VISIBLE);
						btnVar2.setTag(this.oObra);
						btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_pes", null, context.getPackageName()));
						btnVar2.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								ObjetoObra i = (ObjetoObra) v.getTag();
								procesarAccionSeleccionada("1_PeS", i);
							}
						});   
					}
					if (this.oObra.getTipoObra().equals("2")==true){
						btnVar2.setVisibility(View.VISIBLE);
						btnVar2.setTag(this.oObra);
						btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_solicitud_trazado", null, context.getPackageName()));
						btnVar2.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								ObjetoObra i = (ObjetoObra) v.getTag();
								procesarAccionSeleccionada("Trazado", i);
							}
						});   
					}
					
				}
				
				
				// Situaci�n 02.2: Trazado confirmado
				if (this.oObra.getSituacion().equals("02.2")==true){
						
					btnVar2.setVisibility(View.VISIBLE);
					btnVar2.setTag(this.oObra);
					btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
					btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_mod_trazado", null, context.getPackageName()));
					btnVar2.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							ObjetoObra i = (ObjetoObra) v.getTag();
							procesarAccionSeleccionada("ModificarTrazado", i);
						}
					});   
					
					btnVar3.setVisibility(View.VISIBLE);
					btnVar3.setTag(this.oObra);
					btnVar3.setBackgroundColor(res.getColor(R.color.colorWhite));
					btnVar3.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_acepconten", null, context.getPackageName()));
					btnVar3.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							ObjetoObra i = (ObjetoObra) v.getTag();
							procesarAccionSeleccionada("InicioPrueba", i);
								}
						});   
				}
				
				// Situaci�n 02.4: Modificaci�n Trazado confirmado
				if (this.oObra.getSituacion().equals("02.4")==true){
								
					btnVar2.setVisibility(View.VISIBLE);
					btnVar2.setTag(this.oObra);
					btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
					btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_acepconten", null, context.getPackageName()));
					btnVar2.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							ObjetoObra i = (ObjetoObra) v.getTag();
							procesarAccionSeleccionada("InicioPrueba", i);
								}
						});   
				}
						
				// Situaci�n 3.2: Solicitud de PeG
				if (this.oObra.getSituacion().equals("03.2")==true){
				
					if (this.oObra.getTipoObra().equals("2")==true){
						btnVar2.setVisibility(View.VISIBLE);
						btnVar2.setTag(this.oObra);
						btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_conexion", null, context.getPackageName()));
						btnVar2.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								ObjetoObra i = (ObjetoObra) v.getTag();
								procesarAccionConexionPurgado(i);
							}
						});   
					}
					
				}
				
				
				
				// Situaci�n 3.3: Solicitud conexi�n y purgado
				if (this.oObra.getSituacion().equals("03.3")==true && (oObra.getPerfil().equals("JOC")==true || oObra.getPerfil().equals("")==true)){
				
					if (this.oObra.getTipoObra().equals("2")==true){
						btnVar2.setVisibility(View.VISIBLE);
						btnVar2.setTag(this.oObra);
						btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_camrecprev", null, context.getPackageName()));
						btnVar2.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								ObjetoObra i = (ObjetoObra) v.getTag();
								procesarAccionCambioRecPreventivo(i);
							}
						});   
					}
					
				}
				
				// Situaci�n 3.4: Aceptaci�n conexi�n y purgado
				if (this.oObra.getSituacion().equals("03.4")==true  && (oObra.getPerfil().equals("JOC")==true || oObra.getPerfil().equals("")==true)){
				
					if (this.oObra.getTipoObra().equals("2")==true){
						btnVar2.setVisibility(View.VISIBLE);
						btnVar2.setTag(this.oObra);
						btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_camrecprev", null, context.getPackageName()));
						btnVar2.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								ObjetoObra i = (ObjetoObra) v.getTag();
								procesarAccionCambioRecPreventivo(i);
							}
						});   
					
					
						btnVar3.setVisibility(View.VISIBLE);
						btnVar3.setTag(this.oObra);
						btnVar3.setImageResource(res.getIdentifier("drawable/" + "accion_ctr", null, getPackageName()));
						btnVar3.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar3.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								
								UtilData ud = new UtilData(context);
								String telf = ud.obtenerParametroTablaConfiguracion("telefonoCTR");
								if (telf.trim().equals("")==false ){
									
									String telefono = "tel:"+telf.trim();
								
									Intent callIntent  = new Intent(Intent.ACTION_CALL);
							        callIntent.setData(Uri.parse(telefono));
							        startActivity(callIntent);
							        
								}
										
							}
						});
					
					}
					
				}
				
				
				// Situaci�n 04: Puesta en servicio
				if (this.oObra.getSituacion().equals("04")==true){
						
					if (this.oObra.getTipoObra().equals("1")==true){
						btnVar2.setVisibility(View.VISIBLE);
						btnVar2.setTag(this.oObra);
						btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_final", null, context.getPackageName()));
						btnVar2.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								ObjetoObra i = (ObjetoObra) v.getTag();
								procesarAccionSeleccionada("Final", i);
							}
						});   
					}
					
					if (this.oObra.getTipoObra().equals("2")==true && oObra.getPerfil().equals("JOC")==true){
						btnVar2.setVisibility(View.VISIBLE);
						btnVar2.setTag(this.oObra);
						btnVar2.setBackgroundColor(res.getColor(R.color.colorWhite));
						btnVar2.setImageResource(context.getResources().getIdentifier("drawable/" + "accion_finalobra", null, context.getPackageName()));
						btnVar2.setOnClickListener(new OnClickListener() {
							public void onClick(View v) {
								ObjetoObra i = (ObjetoObra) v.getTag();
								procesarAccionSeleccionada("Final", i);
							}
						});   
					}
					
					
					
				}
		
				
				// Situaci�n 06: Cerrada y finalizada
				if (this.oObra.getSituacion().equals("06")==true){
						
					btnVar1.setVisibility(View.INVISIBLE);
					btnVar2.setVisibility(View.INVISIBLE);
					btnVar3.setVisibility(View.INVISIBLE);
						
				}
		}
				
		
		//Bot�n espec�fico para llamar al Gestor de Obra
		ImageButton btnLlamar = (ImageButton) findViewById(R.id.btnAccionTelefono);
		btnLlamar.setTag(this.oObra);
		btnLlamar.setImageResource(res.getIdentifier("drawable/" + "accion_telefono", null, getPackageName()));
		btnLlamar.setBackgroundColor(res.getColor(R.color.colorWhite));
		btnLlamar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				ObjetoObra i = (ObjetoObra) v.getTag();
				if (i.getTSTelefonoGO().trim().equals("")==false ){
					
					String telefono = "tel:"+i.getTSTelefonoGO().trim();
				
					Intent callIntent  = new Intent(Intent.ACTION_CALL);
			        callIntent.setData(Uri.parse(telefono));
			        startActivity(callIntent);
			        
				}
						
			}
		});
	
	}
	
		
	/**
	 * 
	 * @author	jjulia
	 * @date	01/10/2014
	 * @title	procesarAccionComentariosObra
	 * @comment	Procedimiento para la introducci�n de los comentarios y su env�o a la web central
	 *
	 * @param obj	Objeto obra seleccionada
	 *
	 */
	private void procesarAccionComentariosObra(ObjetoObra obj){
		
		//Generamos un dialogo para introducir los comentarios a la obra y confirmar la acci�n
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_comentariosobra,(ViewGroup) act.findViewById(R.layout.activity_detalleobra));
		builder.setView(layout);
		
		final AlertDialog alertDialog = builder.create();
		final EditText tbComentarios =  (EditText) layout.findViewById(R.id.tbComentarios);
		final TextView lblComentarios =  (TextView) layout.findViewById(R.id.lblComentarios);
		
		//Establece el t�tulo del dialogo
		String obra="";
		if (obj.getDireccion().trim().contains("-") == true){
			obra= obj.getCod_obra()+" - "+obj.getDireccion();
		}else{
			obra= obj.getCod_obra()+" - "+obj.getMunicipio()+" - "+obj.getDireccion();
		}
		lblComentarios.setText(obra);
	
		//Establece los controladores de eventos para las acciones de los botenes del dialogo
		Button btnAceptar =  (Button) layout.findViewById(R.id.btnAceptarComentarios);
		Button btnCancelar =  (Button) layout.findViewById(R.id.btnCancelarComentarios);
		
		btnAceptar.setTag(obj);
				
		btnCancelar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		
		});
		
		btnAceptar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Button btn = (Button) v ;  
				ObjetoObra aObra = (ObjetoObra) btn.getTag();	
				String Comentarios = tbComentarios.getText().toString();
								
				if (Comentarios.equals("") == false) { 
															
					//Ejecutar la acci�n de comentarios de obra
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionComentarios(aObra.getCod_obra(), Comentarios);
					
					//Recargar datos de la obra
					establecerPresentacionObra();
										
				}
				
				alertDialog.dismiss();
				finish();	
			}
		
		});
		
		// Mostramos el alertdialog
		alertDialog.show();
						
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	01/10/2014
	 * @title	procesarAccionCambioPlanificacion
	 * @comment	Procedimiento para la introducci�n del cambio de la fecha prevista de ejecucion y su env�o a la web central
	 *
	 * @param obj	Objeto obra seleccionada
	 *
	 */
	private void procesarAccionCambioPlanificacion(ObjetoObra obj){
		
		// Generamos un dialogo para introducir la fecha de planificaci�n y
		// confirmar la acci�n
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_planificacion,(ViewGroup) act.findViewById(R.layout.activity_planificacion));
		builder.setView(layout);

		final AlertDialog alertDialog = builder.create();
		final CalendarControl calPlanificacion = (CalendarControl) layout.findViewById(R.id.calPlanificacion);
		final TextView lblPlanificacion = (TextView) layout.findViewById(R.id.lblPlanificacion);

		// Establece el t�tulo del dialogo
		String obra = "";
		if (obj.getDireccion().trim().contains("-") == true) {
			obra = obj.getCod_obra() + " - " + obj.getDireccion();
		} else {
			obra = obj.getCod_obra() + " - " + obj.getMunicipio() + " - "	+ obj.getDireccion();
		}
		lblPlanificacion.setText(obra);

		// Establece los controladores de eventos para las acciones de los
		// botenes del dialogo
		Button btnAceptar = (Button) layout.findViewById(R.id.btnAceptarPlanificacion);
		Button btnCancelar = (Button) layout.findViewById(R.id.btnCancelarPlanificacion);

		btnAceptar.setTag(obj);

		btnCancelar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				alertDialog.dismiss();
			}

		});

		btnAceptar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Button btn = (Button) v;
				ObjetoObra oplan = (ObjetoObra) btn.getTag();

				Date fecha = calPlanificacion.getSelectedDate();
				if (fecha != null) {

					UtilidadesFechas utf = new UtilidadesFechas();
					String fPlanificacion = utf.convertirFechaToString(fecha, UtilData.FORMAT_DATE_SHORT_BD);

					// Ejecutar la acci�n de planificaci�n
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionCambioPlanificacion(oplan.getCod_obra(), fPlanificacion);

				}

				alertDialog.dismiss();
			}

		});

		// Mostramos el alertdialog
		alertDialog.show();
		
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	01/10/2014
	 * @title	procesarAccionImprevistosProyecto
	 * @comment	Procedimiento para la introducci�n de los comentarios en imprevistos proyecto y su env�o a la web central
	 *
	 * @param obj	Objeto obra seleccionada
	 *
	 */
	private void procesarAccionImprevistosProyecto(ObjetoObra obj){
		
		//Generamos un dialogo para introducir los comentarios a la obra y confirmar la acci�n
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.dialog_comentariosobra,(ViewGroup) act.findViewById(R.layout.activity_detalleobra));
		builder.setView(layout);
		
		final AlertDialog alertDialog = builder.create();
		final EditText tbComentarios =  (EditText) layout.findViewById(R.id.tbComentarios);
		final TextView lblComentarios =  (TextView) layout.findViewById(R.id.lblComentarios);
		final TextView lblTitulo =  (TextView) layout.findViewById(R.id.lblEtiComentarios);
		
		//Establece el t�tulo del dialogo
		String obra="";
		if (obj.getDireccion().trim().contains("-") == true){
			obra= obj.getCod_obra()+" - "+obj.getDireccion();
		}else{
			obra= obj.getCod_obra()+" - "+obj.getMunicipio()+" - "+obj.getDireccion();
		}
		lblComentarios.setText(obra);
		lblTitulo.setText(context.getResources().getString(R.string.mensaje_imprevistos_proyecto));
	
		//Establece los controladores de eventos para las acciones de los botenes del dialogo
		Button btnAceptar =  (Button) layout.findViewById(R.id.btnAceptarComentarios);
		Button btnCancelar =  (Button) layout.findViewById(R.id.btnCancelarComentarios);
		
		btnAceptar.setTag(obj);
				
		btnCancelar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		
		});
		
		btnAceptar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Button btn = (Button) v ;  
				ObjetoObra aObra = (ObjetoObra) btn.getTag();	
				String Comentarios = tbComentarios.getText().toString();
								
				if (Comentarios.equals("") == false) { 
															
					//Ejecutar la acci�n de comentarios de obra
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionImprevistosProyectos(aObra.getCod_obra(), Comentarios);
					
					//Recargar datos de la obra
					establecerPresentacionObra();
										
				}
				
				alertDialog.dismiss();
				finish();	
			}
		
		});
		
		// Mostramos el alertdialog
		alertDialog.show();
						
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	03/10/2013
	 * @title	procesarAccionSeleccionada
	 * @comment	Procesar la acci�n seleccionada de la l�nea de acometidas
	 *
	 * @param accion	Clave de la acci�n seleccionada
	 * @param obj	    Objeto obra seleccionada 
	 *
	 */
	private void procesarAccionSeleccionada(final String accion, final ObjetoObra obj){
		
	    String titulo = context.getResources().getString(R.string.title_dialog_confirmacion_accion);
		String mensaje ="";
						
		if (accion.equals("Iniciar")==true){
			mensaje += context.getResources().getString(R.string.mensaje_obra50m_iniciar);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		if (accion.equals("1_PeS")==true){
			mensaje += context.getResources().getString(R.string.mensaje_acometida_pes);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		if (accion.equals("Trazado")==true){
			mensaje += context.getResources().getString(R.string.mensaje_obra50m_trazado);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		if (accion.equals("ModificarTrazado")==true){
			mensaje += context.getResources().getString(R.string.mensaje_obra50m_modificartrazado);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		if (accion.equals("InicioPrueba")==true){
			mensaje += context.getResources().getString(R.string.mensaje_obra50m_inicio_prueba);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
//		if (accion.equals("SolicitudPeG")==true){
//			mensaje += context.getResources().getString(R.string.mensaje_obra50m_final_prueba);
//			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
//		}
		if (accion.equals("Final")==true){
			mensaje += context.getResources().getString(R.string.mensaje_acometida_finalizar);
			mensaje +=" "+ obj.getCod_obra()+" - "+obj.getDireccion();
		}
		
		
		String si = context.getResources().getString(R.string.yes_dialog);
		String no = context.getResources().getString(R.string.no_dialog);
						
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setTitle(titulo);
		builder.setMessage(mensaje);
		builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		builder.setPositiveButton(si, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
							
				//Ejecutar la acci�n seleccionada
				if (accion.equals("Iniciar")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionInicio(obj.getCod_obra());
				}
				if (accion.equals("1_PeS")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionPeS(obj.getCod_obra());
				}
				if (accion.equals("Trazado")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionTrazado(obj.getCod_obra());
				}
				if (accion.equals("ModificarTrazado")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionModificacionTrazado(obj.getCod_obra());
				}
				if (accion.equals("InicioPrueba")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionSolicitudPeS(obj.getCod_obra());
				}
				if (accion.equals("1_Final")==true){
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionFinal(obj.getCod_obra());
				}
//				if (accion.equals("SolicitudPeG")==true){
//					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
//					prc.ejecucionAccionFinalPrueba(obj.getCod_obra());
//				}
				
				//Recargar datos de la obra
				establecerPresentacionObra();
				finish();	
			}
		});

		final AlertDialog alert = builder.create();
		alert.show();
		
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	11/11/2014
	 * @title	procesarAccionConexionPurgado
	 * @comment	Procedimiento para la acci�n de Conexi�n y Purgado y la selecci�n del Recurso preventivo
	 *
	 * @param obj	Objeto obra seleccionada
	 *
	 */
	private void procesarAccionConexionPurgado(ObjetoObra obj){
		
		//Generamos un dialogo para seleccionar el recurso preventivo y confirmar la acci�n
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.dialog_recursopreventivo,(ViewGroup) act.findViewById(R.layout.activity_detalleobra));
		builder.setView(layout);
		
		final AlertDialog alertDialog = builder.create();
		
		//Carga los recursos preventivos en la lista
		UtilData ud = new UtilData(context);
		String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
		ud.cargarListaRecursosPreventivos(layout,  obj.getCod_obra(),  userName);
			
				
		//Establece los controladores de eventos para las acciones de los botenes del dialogo
		Button btnAceptar =  (Button) layout.findViewById(R.id.btnAceptarRecurso);
		Button btnCancelar =  (Button) layout.findViewById(R.id.btnCancelarRecurso);
		
		btnAceptar.setTag(obj);
				
		btnCancelar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		
		});
		
		btnAceptar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Button btn = (Button) v ;  
				String Cod_RecursoPreventivo="";
				
				//Localiza el Recurso preventivo seleccionado
				ListView lstRecursos = (ListView) layout.findViewById(R.id.lstRecPreventivos);
				AdaptadorVertical_RecursoPreventivo m_adapter= (AdaptadorVertical_RecursoPreventivo) lstRecursos.getAdapter();

				for(int i=0 ; i< m_adapter.getCount() ; i++){
					ObjetoRecursoPreventivo obj = m_adapter.getItem(i);
					if (obj.isSeleccionado()==true){
						Cod_RecursoPreventivo=obj.getCod_JefeObra();
						break;
					}
				}
				
									
				//Ejecuta la acci�n 
				ObjetoObra aObra = (ObjetoObra) btn.getTag();	
													
				ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
				prc.ejecucionAccionFinalPrueba(aObra.getCod_obra(), Cod_RecursoPreventivo);

				//Recargar datos de la obra
				establecerPresentacionObra();
											
				alertDialog.dismiss();
				finish();	
			}
		
		});
		
		// Mostramos el alertdialog
		alertDialog.show();
						
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	11/11/2014
	 * @title	procesarAccionCambioRecPreventivo
	 * @comment	Procedimiento para la acci�n de Cambio del Recurso preventivo
	 *
	 * @param obj	Objeto obra seleccionada
	 *
	 */
	private void procesarAccionCambioRecPreventivo(ObjetoObra obj){
		
		//Generamos un dialogo para seleccionar el recurso preventivo y confirmar la acci�n
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.dialog_recursopreventivo_cambio,(ViewGroup) act.findViewById(R.layout.activity_detalleobra));
		builder.setView(layout);
		
		final AlertDialog alertDialog = builder.create();
		
		//Carga los recursos preventivos en la lista
		UtilData ud = new UtilData(context);
		String userName = ud.obtenerParametroTablaConfiguracion("userNombreUsuario");
		ud.cargarListaRecursosPreventivos(layout,  obj.getCod_obra(),  userName);
		
	
		//Establece los controladores de eventos para las acciones de los botenes del dialogo
		Button btnAceptar =  (Button) layout.findViewById(R.id.btnAceptarRecurso);
		Button btnCancelar =  (Button) layout.findViewById(R.id.btnCancelarRecurso);
		
		btnAceptar.setTag(obj);
				
		btnCancelar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		
		});
		
		btnAceptar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Button btn = (Button) v ;  
				String Cod_RecursoPreventivo="";
				String Mot_RecursoPreventivo="";
				
				EditText tbMotivo = (EditText) layout.findViewById(R.id.tbMotivo);
				Mot_RecursoPreventivo=tbMotivo.getText().toString();
				
				if (Mot_RecursoPreventivo.equals("")==false){
				
					//Localiza el Recurso preventivo seleccionado
					ListView lstRecursos = (ListView) layout.findViewById(R.id.lstRecPreventivos);
					AdaptadorVertical_RecursoPreventivo m_adapter= (AdaptadorVertical_RecursoPreventivo) lstRecursos.getAdapter();
	
					for(int i=0 ; i< m_adapter.getCount() ; i++){
						ObjetoRecursoPreventivo obj = m_adapter.getItem(i);
						if (obj.isSeleccionado()==true){
							Cod_RecursoPreventivo=obj.getCod_JefeObra();
							break;
						}
					}
					
										
					//Ejecuta la acci�n 
					ObjetoObra aObra = (ObjetoObra) btn.getTag();	
														
					ProcesosEjecucionAcciones prc = new ProcesosEjecucionAcciones(context);
					prc.ejecucionAccionCambioRecPreventivo(aObra.getCod_obra(), Cod_RecursoPreventivo, Mot_RecursoPreventivo);
	
					//Recargar datos de la obra
					establecerPresentacionObra();
											
					alertDialog.dismiss();
					finish();	
					
				}else{
					
					Toast.makeText(context, context.getResources().getString(R.string.mensaje_obra_motivoobligatorio), Toast.LENGTH_LONG).show();
									
				}
			}
		
		});
		
		// Mostramos el alertdialog
		alertDialog.show();
						
	}
	
	
	
	
}
