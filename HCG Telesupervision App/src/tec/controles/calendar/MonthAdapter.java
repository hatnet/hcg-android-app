package tec.controles.calendar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import tec.telesupervision.ver2.R;
import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public abstract class MonthAdapter extends BaseAdapter {

	private GregorianCalendar mCalendar;
	private Calendar mCalendarToday;
	private Context mContext;
	private DisplayMetrics mDisplayMetrics;
	private List<String> mItems;
	private int mMonth;
	private int mYear;
	private int mDaysShown;
	private int mDaysLastMonth;
	private int mDaysNextMonth;
	private int mTitleHeight, mDayHeight;
	private final String[] mDays = { "LU", "MA", "MI", "JU", "VI", "SA", "DO" };
	private final int[] mDaysInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	
	private Date fechaSeleccionada = null;
	
	private int colorDiasMes;
	private int colorDiasNoMes;
	private int colorDiasSemana;
	private int colorFondoDiaSeleccionado;
	
	private int colorTextoDia;
	private int colorTextoDiaActual;
	private int colorTextoDiaSeleccionado;
	
		
	public Date getFechaSeleccionada() {
		return fechaSeleccionada;
	}

	public void setFechaSeleccionada(Date fechaSeleccionada) {
		this.fechaSeleccionada = fechaSeleccionada;
	}

	public MonthAdapter(Context c, int month, int year, DisplayMetrics metrics, Date fechaSeleccionada) {
		this.mContext = c;
		this.mMonth = month;
		this.mYear = year;
		this.mCalendar = new GregorianCalendar(mYear, mMonth, 1);
		this.mCalendarToday = Calendar.getInstance();
		this.mDisplayMetrics = metrics;
		this.fechaSeleccionada = fechaSeleccionada;
		
		this.colorDiasMes = Color.rgb(205, 222, 255);
		this.colorDiasNoMes = Color.rgb(240, 240, 240);	
		this.colorDiasSemana = Color.rgb(255, 255, 255);
		this.colorFondoDiaSeleccionado = Color.rgb(135, 164, 188);
		
		this.colorTextoDia = Color.rgb(0, 0, 0);
		this.colorTextoDiaActual = Color.rgb(255, 0, 0);
		this.colorTextoDiaSeleccionado = Color.rgb(255, 255, 255);
				
		populateMonth();
	}
	
	/**
	 * @param date - null if day title (0 - dd / 1 - mm / 2 - yy)
	 * @param position - position in item list
	 * @param item - view for date
	 */
	protected abstract void onDate(int[] date, int position, View item);
	
	private void populateMonth() {
		mItems = new ArrayList<String>();		
		for (String day : mDays) {
			
			String strDay = "";
			if(day.equals("LU")) {
				strDay = this.mContext.getResources().getString(R.string.calendar_lun);
			} else if(day.equals("MA")) {
				strDay = this.mContext.getResources().getString(R.string.calendar_mar);
			} else if(day.equals("MI")) {
				strDay = this.mContext.getResources().getString(R.string.calendar_mie);
			} else if(day.equals("JU")) {
				strDay = this.mContext.getResources().getString(R.string.calendar_jue);
			} else if(day.equals("VI")) {
				strDay = this.mContext.getResources().getString(R.string.calendar_vie);
			} else if(day.equals("SA")) {
				strDay = this.mContext.getResources().getString(R.string.calendar_sab);
			} else if(day.equals("DO")) {
				strDay = this.mContext.getResources().getString(R.string.calendar_dom);
			}			
			mItems.add(strDay);
			mDaysShown++;
		}
		
		int firstDay = getDay(mCalendar.get(Calendar.DAY_OF_WEEK));
		int prevDay;
		if (mMonth == 0)
			prevDay = daysInMonth(11) - firstDay + 1;
		else
			prevDay = daysInMonth(mMonth - 1) - firstDay + 1;
		for (int i = 0; i < firstDay; i++) {
			mItems.add(String.valueOf(prevDay + i));
			mDaysLastMonth++;
			mDaysShown++;
		} 
		
		int daysInMonth = daysInMonth(mMonth);	
		for (int i = 1; i <= daysInMonth; i++) {
			mItems.add(String.valueOf(i));
			mDaysShown++;
		}
		
		mDaysNextMonth = 1;
		while (mDaysShown % 7 != 0) {
			mItems.add(String.valueOf(mDaysNextMonth));
			mDaysShown++;
			mDaysNextMonth++;
		}
		
		mTitleHeight = 30;
		int rows = (mDaysShown / 7);
		mDayHeight = (mDisplayMetrics.heightPixels - mTitleHeight 
				- (rows * 8) - getBarHeight()) / (rows - 1) / 2;
	}
	
	private int daysInMonth(int month) {
		int daysInMonth = mDaysInMonth[month];
		if (month == 1 && mCalendar.isLeapYear(mYear))
			daysInMonth++;
		return daysInMonth;
	}
	
	private int getBarHeight() {
		switch (mDisplayMetrics.densityDpi) {
		case DisplayMetrics.DENSITY_HIGH:
			return 48;
		case DisplayMetrics.DENSITY_MEDIUM:
			return 32;
		case DisplayMetrics.DENSITY_LOW:
			return 24;
		default:
			return 48;
		}
	}
	
	private int getDay(int day) {
		switch (day) {
		case Calendar.MONDAY:
			return 0;
		case Calendar.TUESDAY:
			return 1;
		case Calendar.WEDNESDAY:
			return 2;
		case Calendar.THURSDAY:
			return 3;
		case Calendar.FRIDAY:
			return 4;
		case Calendar.SATURDAY:
			return 5;
		case Calendar.SUNDAY:
			return 6;
		default:
			return 0;
		}
	}
	
	private boolean isToday(int day, int month, int year) {
		if (mCalendarToday.get(Calendar.MONTH) == month
				&& mCalendarToday.get(Calendar.YEAR) == year
				&& mCalendarToday.get(Calendar.DAY_OF_MONTH) == day) {
			return true;
		}
		return false;
	}
	
	public int[] getDate(int position) {
		int date[] = new int[3];
		if (position <= 6) {
			return null; // day names
		} else if (position <= mDaysLastMonth + 6) {
			// previous month
			date[0] = Integer.parseInt(mItems.get(position));
			if (mMonth == 0) {
				date[1] = 11;	
				date[2] = mYear - 1;
			} else {
				date[1] = mMonth - 1;
				date[2] = mYear;
			}
		} else if (position <= mDaysShown - mDaysNextMonth  ) {
			// current month
			date[0] = position - (mDaysLastMonth + 6);
			date[1] = mMonth;
			date[2] = mYear;
		} else {
			// next month
			date[0] = Integer.parseInt(mItems.get(position));
			if (mMonth == 11) {
				date[1] = 0;
				date[2] = mYear + 1;
			} else {
				date[1] = mMonth + 1;
				date[2] = mYear;
			}
		}
		return date;
	}
		
	public View getView(int position, View convertView, ViewGroup parent) {
		final TextView view = new TextView(mContext);
		view.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
		view.setText(mItems.get(position));
		view.setTextColor(Color.BLACK);

		int[] date = getDate(position);
		pintarCelda(date, view);

		onDate(date, position, view);
		return view;
	}
	
	public void pintarCelda(int[] date, TextView view) {
		
		if (date != null) {
			view.setHeight(mDayHeight);
						
			if (date[1] != mMonth) {
				// previous or next month
				if (fechaSeleccionada != null) {
					
					Calendar c = new GregorianCalendar();
					c.setTime(fechaSeleccionada);					
					int[] fechSel = new int[3];
					fechSel[0]= c.get(Calendar.DAY_OF_MONTH);;
					fechSel[1]= c.get(Calendar.MONTH);
					fechSel[2]= c.get(Calendar.YEAR);
					c = null;
					
					//Comprobar si la fecha de la celda es la misma que la fecha seleccionada, si es la misma pintar el color de fondo 
					if ((date[0] == fechSel[0]) && (date[1] == fechSel[1]) && (date[2] == fechSel[2])) {
						view.setBackgroundColor(this.colorFondoDiaSeleccionado);
						view.setTextColor(this.colorTextoDiaSeleccionado);
					} else {
						view.setBackgroundColor(this.colorDiasNoMes);
						view.setTextColor(this.colorTextoDia);
					}
					
				} else {
					view.setBackgroundColor(this.colorDiasNoMes);
					view.setTextColor(this.colorTextoDia);
				}
			} else {
				// current month
				if (fechaSeleccionada != null) {
					
					Calendar c = new GregorianCalendar();
					c.setTime(fechaSeleccionada);					
					int[] fechSel = new int[3];
					fechSel[0]= c.get(Calendar.DAY_OF_MONTH);;
					fechSel[1]= c.get(Calendar.MONTH);
					fechSel[2]= c.get(Calendar.YEAR);
					c = null;
					
					//Comprobar si la fecha de la celda es la misma que la fecha seleccionada, si es la misma pintar el color de fondo 
					if ((date[0] == fechSel[0]) && (date[1] == fechSel[1]) && (date[2] == fechSel[2])) {
						view.setBackgroundColor(this.colorFondoDiaSeleccionado);
						view.setTextColor(this.colorTextoDiaSeleccionado);
					} else {
						view.setBackgroundColor(this.colorDiasMes);
						view.setTextColor(this.colorTextoDia);
					}
					
				} else {
					view.setBackgroundColor(this.colorDiasMes);
					view.setTextColor(this.colorTextoDia);
				}
				if (isToday(date[0], date[1], date[2] )) {
					view.setTextColor(this.colorTextoDiaActual);
				}
			}
			
		} else {
			view.setBackgroundColor(this.colorDiasSemana);
			view.setHeight(mTitleHeight);
		}
		view.setTextSize(10);
		//view.setTextSize(mContext.getResources().getDimension(R.dimen.fontSize_small_plus));
	}
	
	public int getCount() {
		return mItems.size();
	}

	public Object getItem(int position) {
		return mItems.get(position);
	}

	public long getItemId(int position) {
		return position;
	}
	
}
