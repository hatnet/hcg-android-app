package tec.controles.calendar;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import tec.telesupervision.ver2.R;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 
 * @author	jjulia
 * @date	15/10/2013
 * @title	CalendarControl
 * @comment	Control calendario
 *	
 */
public class CalendarControl extends LinearLayout{
		
	private Context context;
	private Activity act;
	private Date fechaSeleccionada = null;
	private int day;
	private int month;
	private int year;
	
	private Calendar mCalendar;
	private GridView mGridView;
	private MonthAdapter mAdapter;
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	CalendarControl
	 * @comment	Constructores est�ndard de una clase control
	 *
	 * @param context	
	 * @param attrs	
	 *
	 */
	public CalendarControl(Context context) {
	    super(context);
	    this.context = context;
	    this.act = (Activity) context;
	    this.fechaSeleccionada=new Date();
	    inicializar();
	}
	 
	public CalendarControl(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    this.context = context;
	    this.act = (Activity) context;
	    this.fechaSeleccionada=new Date();
	    inicializar();
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	setDate
	 * @comment	Propiedad p�blica para establecer una fecha en el control calendario
	 *
	 * @param date	Fecha que se quiere establecer
	 *
	 */
	public void setDate(Date date)
	{
	    this.fechaSeleccionada=date;
	    
	    Calendar c = new GregorianCalendar();
		c.setTime(this.fechaSeleccionada);
		day = c.get(Calendar.DAY_OF_MONTH);
		month = c.get(Calendar.MONTH);
		year = c.get(Calendar.YEAR);
		 
	    establecerFechaEnCalendario();
	
	}
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	getSelectedDate
	 * @comment	Propiedad p�blica para obtener la fecha seleccionada en el calendario
	 *
	 * @return	
	 *
	 */
	public Date getSelectedDate() {		
		return this.fechaSeleccionada;
	}
	

	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	inicializar
	 * @comment	Procedimiento para inicializar el control
	 *
	 */
    private void inicializar()
	{
	     //Utilizamos el layout 'calendar_control' como interfaz del control
	     String infService = Context.LAYOUT_INFLATER_SERVICE;  
	     LayoutInflater li = (LayoutInflater)getContext().getSystemService(infService);
	     li.inflate(R.layout.calendar_control, this, true);
	  
	     //Definir listeners para los botones de cambio de mes
		 Button btnMesAnt = (Button) findViewById(R.id.btnMesAnterior);
		 Button btnMesSig = (Button) findViewById(R.id.btnMesSiguiente);
		 btnMesAnt.setOnClickListener(btnMesAntListener);
		 btnMesSig.setOnClickListener(btnMesSigListener);
		 
		 //Inicializar la fecha del calendario
		 Calendar c = new GregorianCalendar();
		 c.setTime(this.fechaSeleccionada);
			
		 day = c.get(Calendar.DAY_OF_MONTH);
		 month = c.get(Calendar.MONTH);
		 year = c.get(Calendar.YEAR);
		 
		 //Establecer la fecha en el calendario
		 establecerFechaEnCalendario();
				
	}
    
    /**
     * 
     * @author	jjulia
     * @date	15/10/2013
     * @title	establecerFechaEnCalendario
     * @comment	Procedimiento para establecer el calendario a una determinada fecha
     *
     */
	private void establecerFechaEnCalendario(){
    	 	    	 			
		 // Presentar la descripci�n del mes 
		 TextView lblMesCalendar = (TextView) findViewById(R.id.lblMes);
		 String descripcionMes = obtenerDescripcionMesCalendar(month, year);
		 
		 lblMesCalendar.setText(descripcionMes);
		 lblMesCalendar.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		 
		 // Cargar los dia del mes en el Grid
		 this.mGridView = (GridView) findViewById(R.id.gridview);
		 vincularGrid(mGridView, month, year);
			
    }
	
    
	/**
	 *  
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	obtenerDescripcionMesCalendar
	 * @comment	Procedimiento para obtener la descripci�n del mes y el a�o
	 *
	 * @param 	month	N�mero del mes (en base a 0)
	 * @param 	year	N�mero del a�o
	 * @return	Texto descriptivo del mes y el a�o
	 *
	 */
	private String obtenerDescripcionMesCalendar(int month, int year) {
			
		String mes = "";
			
		switch (month) {
		case 0:
			mes = context.getResources().getString(R.string.calendar_enero);
			break;
		case 1:
			mes = context.getResources().getString(R.string.calendar_febrero);
			break;
		case 2:
			mes = context.getResources().getString(R.string.calendar_marzo);
			break;
		case 3:
			mes = context.getResources().getString(R.string.calendar_abril);
			break;
		case 4:
			mes = context.getResources().getString(R.string.calendar_mayo);
			break;
		case 5:
			mes = context.getResources().getString(R.string.calendar_junio);
			break;
		case 6:
			mes = context.getResources().getString(R.string.calendar_julio);
			break;
		case 7:
			mes = context.getResources().getString(R.string.calendar_agosto);
			break;
		case 8:
			mes = context.getResources().getString(R.string.calendar_septiembre);
			break;
		case 9:
			mes = context.getResources().getString(R.string.calendar_octubre);
	    	break;
		case 10:
			mes = context.getResources().getString(R.string.calendar_nomviembre);
			break;
		case 11:
			mes = context.getResources().getString(R.string.calendar_diciembre);
			break;
		}
		return mes + " " + String.valueOf(year);
			
	}
	
	
	/**
	 * 
	 * @author	jjulia
	 * @date	15/10/2013
	 * @title	Procedimiento para generar la matriz de dias del mes y gestionar los eventos
	 * @comment	
	 *
	 * @param mGridView	Objeto matriz de d�as
	 * @param m			N�mero del m�s
	 * @param y			N�mero del a�o
	 *
	 */
	private void vincularGrid(GridView mGridView, int m, int y) {

		final DisplayMetrics metrics = new DisplayMetrics();
		this.act.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		
		this.mAdapter = new MonthAdapter(this.context, m, y, metrics, this.fechaSeleccionada) {
		@Override
		protected void onDate(int[] date, int position, View item) {
			}
		};

		mGridView.setAdapter(mAdapter);
		mGridView.setOnItemClickListener(itemGridListener);

	}
	
	private OnItemClickListener itemGridListener = new OnItemClickListener() {

		public void onItemClick(AdapterView<?> parent, View view, int position,	long id) {

			// Recuperar un array de int con la fecha para obtener la fecha seleccionada.
			// date[0] --> dia
			// date[1] --> mes
			// date[2] --> a�o
			int[] date = mAdapter.getDate(position);
			
			//Si date es null es una celda de t�tulo.
			if (date != null) {
				day = date[0];
				month = date[1];
				year = date[2];
				
				Calendar c = new GregorianCalendar();
				c.set(year, month, day);
				fechaSeleccionada = c.getTime();
				
				//Repintar las celdas del grid
				for (int i = 0; i < mGridView.getChildCount(); i++){
					
					//Obtener la posici�n de la celda
					int [] d = mAdapter.getDate(i);
					TextView txt = (TextView) mGridView.getChildAt(i);
					mAdapter.setFechaSeleccionada(fechaSeleccionada);
					mAdapter.pintarCelda(d, txt);
					
				}
			}
		}

	};
	
	private OnClickListener btnMesAntListener = new OnClickListener() {

		public void onClick(View v) {
			
			//Comprobar si el calendario es null si es as� creamos una nueva instancia con la fecha actual del calendario
			if (mCalendar == null) {
				mCalendar= new GregorianCalendar();
				mCalendar.set(year, month, day);
			}

			// Restar un mes a la instancia del calendario
			mCalendar.add(Calendar.MONTH, -1);

			day = mCalendar.get(Calendar.DAY_OF_MONTH);
			month = mCalendar.get(Calendar.MONTH);
			year = mCalendar.get(Calendar.YEAR);

			establecerFechaEnCalendario();

		}
	};

	private OnClickListener btnMesSigListener = new OnClickListener() {

		public void onClick(View v) {

			//Comprobar si el calendario es null si es as� creamos una nueva instancia con la fecha actual del calendario
			if (mCalendar == null) {
				mCalendar= new GregorianCalendar();
				mCalendar.set(year, month, day);
			}

			// Sumar un mes a la instancia del calendario
			mCalendar.add(Calendar.MONTH, +1);

			day = mCalendar.get(Calendar.DAY_OF_MONTH);
			month = mCalendar.get(Calendar.MONTH);
			year = mCalendar.get(Calendar.YEAR);

			establecerFechaEnCalendario();

		}
	};

		 
}
